#!/bin/bash

# GVL
cp Gate2LEA-GVL.m Gate2LEA.m
/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-GVL.in >FinalGVL-1
mv fitmoy.dat fitmoy-GVL-1.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-GVL-1.mat

/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-GVL.in >FinalGVL-2
mv fitmoy.dat fitmoy-GVL-2.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-GVL-2.mat

/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-GVL.in >FinalGVL-3
mv fitmoy.dat fitmoy-GVL-3.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-GVL-3.mat

/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-GVL.in >FinalGVL-4
mv fitmoy.dat fitmoy-GVL-4.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-GVL-4.mat


# CO
cp Gate2LEA-CO.m Gate2LEA.m
/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-CO.in >FinalCO-1
mv fitmoy.dat fitmoy-CO-1.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-CO-1.mat

/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-CO.in >FinalCO-2
mv fitmoy.dat fitmoy-CO-2.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-CO-2.mat

/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-CO.in >FinalCO-3
mv fitmoy.dat fitmoy-CO-3.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-CO-3.mat

/opt/est/LEA3D/LEA3D_v2_January2011/eDESIGN/lea job-CO.in >FinalCO-4
mv fitmoy.dat fitmoy-CO-4.dat
mv molStruct_of_all_molecules.mat molStruct_of_all_molecules-CO-4.mat
