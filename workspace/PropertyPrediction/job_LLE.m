function [ molStruct ] = job_LLE( molStruct,compounds,specs )

for k=1:length(molStruct)
    molStruct(k).x_LLE = NaN;
end

%% Job procedure

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating Liquid-Liquid-Equilibrium of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

T = specs.T;
active_mixture = specs.LLEcomp;

parfor k=1:length(molStruct) %parfor
 
    %% LLE calculations
     molStruct(k).x_LLE = sub_LLE(active_mixture, k,compounds(k,:), T, specs);

         % Update parfor progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
         
end

if strcmp(specs.Mode, 'screening')
   fprintf 'All LLE calculated!\n';
end

end

