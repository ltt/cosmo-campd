function [ molStruct ] = job_kinetics_liquid( molStruct, compounds,specs )
% Procedure for calculation of reaction kinetics in liquid phase

%% General

% Preallocate fields
n = length(specs.system); % number of compounds per system

for k=1:length(molStruct);
    molStruct(k).MW_solvent = NaN;
    molStruct(k).deltaG_solv_reactants = NaN;
    molStruct(k).deltaG_solv_TS = NaN;
   
end

% Set reaction temperature
T = specs.T_reaction; % Kelvin


% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating reaction kinetics in liquid phase with ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end


%% Job procedure

for k=1:length(molStruct) %parfor
 
 
%% deltaG_solv calculation 
[deltaG_solv] = sub_deltaG_solv(k,compounds(k,:), T, specs);


molStruct(k).deltaG_solv_reactants = deltaG_solv(1:specs.n_reactants,1);
molStruct(k).deltaG_solv_TS = deltaG_solv((specs.n_reactants+1):(specs.n_reactants+specs.n_TS),1);



%Update parfor progressbar
if strcmp(specs.Mode, 'screening')
    parfor_progress;
end


end
end