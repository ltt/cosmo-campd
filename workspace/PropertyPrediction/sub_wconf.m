function [ConfNum, wconf] = sub_wconf(compounds,k,specs)
% Calculates the distribution of confomers and the Sigma Profiles 
%
% Input:    compounds
%           k: unique number k used for identiying files  
%           specs 
%           
%
% Output:   [number of conformer, weigth of conformer]
%

 %% Generate jobs "jobPrint"
 
    jobPrint = ''; % Initialize jobPrint   
    jobPrint = strcat(jobPrint, ['x_pure = 1 Tk=298.15 wconf ctab # Automatic Mixture Calculation', '\n']); 
    
 %% Call COSMOtherm
    
    [tempFile] = cosmoSingleCall( k, compounds, specs,jobPrint);
   
 %% ReadOutputFile 
 
    [ConfNum, wconf] = extractWconf(tempFile);
    
 %% Delete temporary files after calculation     
    
    if exist([tempFile, '.inp'])>0
    delete([tempFile, '.inp'])
    end
    if exist([tempFile, '.tab'])>0
    delete([tempFile, '.tab'])
    end
    if exist([tempFile, '.out'])>0
    delete([tempFile, '.out'])
    end

end

function [ConfNum, wconf] = extractWconf(tempFile)
%% Get Results

%% Confomer weigth

    % Check if .out file is empty
    s = dir([tempFile,'.tab']);
    if s.bytes == 0
        result = NaN; % Empty file
        disp('ERROR: tab file empty');
    else
        fileID = fopen([tempFile,'.tab'],'r');
        line = fgetl(fileID);
        i = 0;
        while ~numel(strfind(line,'Nr Molecule/Conformer')) && i < 3000 % Move file pointer to line of interest
             line = fgetl(fileID);
             i = i + 1; % Prevent eternal loop
        end
        formatSpec = '%f %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f';
        lastConf = 0;
        ConfNum = 0;
        while ~lastConf
            line = fgetl(fileID); % For wconf: information below "Nr Molecules/Conformer"
            if line == -1
                lastConf = 1;
                break
            else
                content = textscan(line,formatSpec); % Read line
                ConfNum = ConfNum + 1;
                wconf(ConfNum) = content{1,13}; % Extract conformer weigth           
            end
        end 
    end

    fclose(fileID);
end


