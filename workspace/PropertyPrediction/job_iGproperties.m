function molStruct = job_iGproperties(molStruct, compounds, specs)
% Calculate gaseous heat capacities using QM = Gaussian and Tamkin on B3LYP level
% and then based on gas heat capacities and heat of vaporization the liquid heat
% capcacities
% Returns gaseous heat capacities as NASA polynomial

%% General
nComp = length(specs.system);           % number of Compounds
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system
T = linspace(specs.T_start, specs.T_end, specs.T_interval)'; 
R=8.314;% kJ/kmol/K

% Get all solvents
solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating heat capacities of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(solvents));
end

%add path to QMiG folder
addpath([pwd,'/PropertyPrediction/ExtTools/QMiG/']); 

%% Get properties for non-solvents
cp_ig_co = cell(length(compounds)-1,1);
cp_ig = cell(length(compounds)-1,1);
int_heat = cell(length(compounds)-1,1);
s_ig = cell(length(compounds)-1,1);
v_ig = cell(length(compounds)-1,1);
g_ig = cell(length(compounds)-1,1);


% Get GaussianFiles
SMILES = specs.SMILES;
non_solvents = struct('name', compounds(1,1:end-1));
[ok] = getGaussianFiles(non_solvents,SMILES,specs);

% Get properties
parfor k = 1:nComp-nSolvs
    [int_heat{k,1},cp_ig{k,1},s_ig{k,1},v_ig{k,1},g_ig{k,1}] = sub_iGproperties(k,compounds{1,k}, T,specs); 
end
   

for k = 1:nComp-nSolvs
    % Fit cp ig to NASA polynomial for further use
    cp_ig{k,1} = cp_ig{k,1}/R; % cp is dimensionless for fit

    % Starting guess
    para0 = [0, 0, 0, 0, 0];

    % Fit
    options = optimoptions('lsqcurvefit','Display','off','MaxFunEvals',1e5, 'Algorithm','levenberg-marquardt');
    cp_ig_co{k,1} = lsqcurvefit(@cp_NASA_poly, para0, T, cp_ig{k,1}, [],[], options);
    cp_ig{k,1} = cp_ig{k,1}*R; % cp in J/molK for save in molStruct
end

%% Get properties for solvents
int_heat_sol = cell(length(solvents),1);
cp_ig_co_sol = cell(length(solvents),1);
cp_ig_sol = cell(length(solvents),1);
s_ig_sol = cell(length(solvents),1);
v_ig_sol = cell(length(solvents),1);
g_ig_sol = cell(length(solvents),1);

% Find the SMILES of all solvents
SMILES = cell(length(solvents),1);
for k = 1:length(solvents)
    for j=1:nSolvs
        idxsolv = find(strcmp(solvents(k), compounds(:,nComp-nSolvs+j)));
        if ~isempty(idxsolv)
            idxsolv = idxsolv(1);
            break
        end
    end
    if iscell(molStruct(idxsolv).SMILES(j))
        SMILES{k} = molStruct(idxsolv).SMILES{j} ;
    else
        SMILES{k} = molStruct(idxsolv).SMILES ;
    end
end


% Get GaussianFiles
[ok] = getGaussianFiles(molStruct,SMILES,specs);

% Get iGproperties
parfor k = 1:length(solvents)
    [int_heat_sol{k,1},cp_ig_sol{k,1},s_ig_sol{k,1},v_ig_sol{k,1},g_ig_sol{k,1}] = sub_iGproperties(k,solvents{k},T,specs);
end 
   
for k = 1:length(solvents)
    % Fit cp ig to NASA polynomial for further use
    cp_ig_sol{k,1} = cp_ig_sol{k,1}/R;

    % Starting guess
    para0 = [0, 0, 0, 0, 0];

    % Fit
    options = optimoptions('lsqcurvefit','Display','off','MaxFunEvals',1e5, 'Algorithm','levenberg-marquardt');
    cp_ig_co_sol{k,1} = lsqcurvefit(@cp_NASA_poly, para0, T, cp_ig_sol{k,1}, [],[], options);
    cp_ig_sol{k,1} = cp_ig_sol{k,1}*R;
    
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
    
end

%% Merge and fill molStruct
for k=1:length(molStruct)
    if nComp-nSolvs > 0
    	molStruct(k).cp_gas_co = cp_ig_co;
        molStruct(k).cp_gas = cp_ig;
    else
        molStruct(k).cp_gas_co = [];
        molStruct(k).cp_gas = [];
    end
    for is=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+is)));
        molStruct(k).cp_gas{end+1} = cp_ig_sol{idxsolv};
        molStruct(k).cp_gas_co{end+1} =  cp_ig_co_sol{idxsolv};
    end
end

end

function fitted = cp_NASA_poly(c,T)
% Calculate cp from NASA polynomial
fitted = (c(1) + c(2)*T + c(3)* T .^ 2 + c(4)* T .^ 3+ c(5)* T .^ 4) ; %NASA polynomial
end
