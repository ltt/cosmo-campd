function [result,ok] = sub_cpig_GC(SMILES, k)
%% Calculates the ideal heat capacity using the Benson's GC method
%
% Input:  SMILES Code of compound
%           specs 
%
% Output:   NASA polynomial of cp ideal gas
%
% NOTE: ONLY USE THIS SCRIPT WITH MATLABR2018B or newer
% NOTE: ONLY WORKS FOR MOLECULES CONTAINING C, H, O

%% General settings
tempFile = ['PropertyPrediction/ExtTools/RMGThermo/temp/', SMILES, '_', num2str(k)];

%% Call python script calling the RMG Tool
[status, cmdout] = system(['python PropertyPrediction/ExtTools/RMGThermo/wrapRMGThermo.py ''', SMILES, ''' ', num2str(k), '']);
   
%% ReadOutputFile 
[result,ok] = extract_cpig_GC(tempFile);
    
%% Delete temporary files after calculation     

% output file
if exist([tempFile, '.out'], 'file')>0
    delete([tempFile, '.out'])
end

% Folder
if exist(tempFile, 'dir')>0
    rmdir(tempFile, 's')
end

end

function [result,ok] = extract_cpig_GC(tempFile)
% RETURNS: [NASA coefficients]
% Check if .out file is empty
s = dir([tempFile, '.out']);
if s.bytes == 0
    result = NaN([1,2]); % Empty file
    ok = 0;
    disp('ERROR: Output file empty');
else
    ok = 1;
          
    fileID = fopen([tempFile, '.out'],'r');
    line = fgetl(fileID); 
    formatSpec = '%*s %f %f %f %f %f %*f %*f';   
    while ~feof(fileID)
        if ~isempty(strfind(line,'NASA polynomial for low temperature range (T<1000 K)'))
            line = fgetl(fileID);                           % information below "NASA polynomial..."
            result = textscan(line,formatSpec); % Read line 
        end        
        line = fgetl(fileID); 
    end
   
    result = cell2mat(result);    
    fclose(fileID);        
end
end