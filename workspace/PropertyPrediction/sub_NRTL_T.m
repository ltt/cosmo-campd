function results = sub_NRTL_T (k,compounds, T, specs) 
%% Implemented by Ramona G�tz, supervised by Lorenz Fleitmann @LTT, RWTH Aachen University.
%% Changes from old Version 
%    -   function calc_NRTL: new method for fit of NRTL parameters to given
%        gamma data (see function description)

%% Calculates the temperature dependend binary NRTL Parameters for given compounds 
% Input:    compounds (vector including strings with names of compounds)
%           k: unique number k used for identiying files 
%           T: temperature n-vector  
%           specs (includes struct entry specs.composition --> m-vector)
%
% Output    Struct with 3 fields 
%           1) resultsGamma: Gamma1 and Gamma2 calculated by COMSO-RS used for the NRTL
%           fit (nxm matrices; n:temperatures; m: composition)
%           2) NRTLParam_T (3x4 matrix): temperature dependend NRTL Parameters in order 
%               1) alpha coefficients (2 column entries)
%               2) tau12 coefficients (4 column entries)
%               3) tau21 coefficients (4 column entries)
%
% The Function contains 7 Sub functions  
%
%   1) getGammaArray: Calculates a (T,x) sized array with ln(gamma) values in COSMO-Rs 
%   2) calcNRTL: Fits NRTL parameters to the function lnGammafromParam for given
%      gamma array 
%   3) lnGammafromParam: Objective function for fit: calculates ln gamma
%       array (size [(T,x) (T,x)]) for gamma1 and gamma2 from NRTL
%       parameters and a cell array X containing the matching pairs of (T,x)
%   4) calc_Err: Calculates the error of the NRTL fit 
%   5) NRTL_function: calculates lngamma for composition x and given NRTL
%       parameters
%   6,7) calcAlphaFromPar/calcTauFromPar: calculate NRTL parameters from NRTL(T) parameters

%% Calculate Gamma Array 

[resultslnGamma1,resultslnGamma2] = getGammaArray(k,compounds, T, specs);

%% Calculate NRTL Parameters
NRTLParam_T = calcNRTL(resultslnGamma1,resultslnGamma2,T,specs);

%% Calculate Err (Mean relative deviation + maximum relative deviation for each binary combination)

Err = calc_Err(NRTLParam_T,resultslnGamma1,resultslnGamma2,T,specs);

%% Set Results 

results.resultslnGamma{1,1} = resultslnGamma1;
results.resultslnGamma{1,2} = resultslnGamma2;
results.NRTLParam_T = NRTLParam_T;
results.Err = Err;
      
end

function [resultslnGamma1,resultslnGamma2] = getGammaArray(k,compounds, T, specs) 

    %% Get Composition 
    composition = specs.composition; 

  
    %% Generate list of concentrations and temperatures
    T_vector = zeros(length(T)*length(composition),1);
    composition_vector = zeros(length(T)*length(composition),2);

    for i=1:length(composition);
        for j=1:length(T) 
            composition_vector(j+(i-1)*length(T),:) = [composition(i), 1-composition(i)];
            T_vector(j+(i-1)*length(T)) = T(j);
        end   
    end
    resultvector = sub_gamma(composition_vector,k,compounds, T_vector, specs);
    
    results1 = resultvector(:,1);
    results2 = resultvector(:,2);
    
    if length(results1) ~= length(T_vector)
        error('ERROR: Number of results differs from number of jobs!');  
    end
    
    % Order for further NRTL-fitting:
    % - rows: Temperature dependence
    % - columns: Composition dependence
    resultslnGamma1 = zeros(length(T), length(composition));
    resultslnGamma2 = zeros(length(T), length(composition));
    
    for i = 1:length(composition)
        resultslnGamma1(:,i) = results1((i-1)*length(T)+1:i*length(T));
        resultslnGamma2(:,i) = results2((i-1)*length(T)+1:i*length(T));
    end
    
end

function NRTLParam_T = calcNRTL(resultslnGamma1,resultslnGamma2,T,specs) 
%% Fit NRTL 
% The regression is done in one step (directly determining the 10 (2+4+4) coefficients)
% using the MATLAB nonlinear least-squares solver and the Levenberg-Marquardt algorithm.
% 
% Alpha is calculated afterwards and checked for its bounds (0.05 <= alpha <= 1)
%% Set initial data & prelocate Fields
    
composition = specs.composition;
  
%% Fit NRTL Para to Gamma(x)
 
options = optimoptions('lsqcurvefit','Display','off','TolX', 1e-9, 'TolFun',1e-9,'MaxFunEvals',1e6, 'MaxIter', 1000, 'Algorithm','trust-region-reflective');
para0 = zeros(1, 10);
%Bounds for alpha coefficients
lb(1:10) = -inf; lb(1) = -5.5; lb(2) = -0.02;
ub(1:10) = inf; ub(1) = 6.5; ub(2) = 0.02;

% xdata for fit: nxm cell array with [T,composition] pairs
xdata = cell(length(T), length(composition));
for n = 1:length(T)
    for m = 1:length(composition)
        xdata{n,m} = [T(n) , composition(m)];
    end
end

para = lsqcurvefit(@lnGammafromParam, para0, [xdata, xdata], [resultslnGamma1, resultslnGamma2], lb, ub, options);

%% Change order of NRTL parameters for OUTPUT array NRTLParam_T

NRTLParam_T = zeros(3,4);
%alpha
NRTLParam_T(1,1:2) = para(1:2);
%tau12
NRTLParam_T(2,:) = para(3:6);
%tau21
NRTLParam_T(3,:) = para(7:10);

end

function lngamma = lnGammafromParam(para,X)
    % INPUT:    - para: NRTL parameters (2 parameters for alpha, 4 for each tau)
    %           - X: cell with two times the pairs of n temperatures and m composition data 
    %--------------------------------------------------------------------------------------------
    % Name      | Description               | Size                         | Unit | 
    % para      | NRTL parameters           | [1x10]                       | [-]  |
    % X         | 2 x [T,composition] pairs | [n x 2*m] cell w. [1x2] each | [K, molefraction]  |  
    %--------------------------------------------------------------------------------------------
    %
    % OUTPUT:   ln(gamma1) and ln(gamma2) for the temperature composition
    %           pairs calculated using the NRTL parameters "para"
    %--------------------------------------------------------
    % Name    | Description              | Size      | Unit  | 
    % lngamma | [ln(gamma1), ln(gamma2)] | [n x 2*m] | [-]   |
    %--------------------------------------------------------


dim = size(X);
lngamma = zeros(dim);
xdata = X(:, 1:(dim(2)/2)); %Get the original xdata (half of X) - nxm pairs of [T, composition]

dim = size(xdata);
    for i = 1:dim(1) %rows: temperatures
        for j = 1:dim(2) %columns: compositions
            T = xdata{i,j}(1);
            x1 = xdata{i,j}(2);
            x2 = 1-x1;
            
            alpha = para(1) + para(2)*T;
            tau12 = para(3) + para(4)/T + para(5) * log(T) + para(6)*T;
            tau21 = para(7) + para(8)/T + para(9) * log(T) + para(10)*T;
            
            G12= exp(-alpha*tau12);
            G21= exp(-alpha*tau21);

            lngamma1 =   x2^2*(tau21*(G21/(x1+x2*G21))^2+tau12*G12/(x2+x1*G12)^2);
 
            lngamma2 =   x1^2*(tau12*(G12/(x2+x1*G12))^2+tau21*G21/(x1+x2*G21)^2);
 
            lngamma(i,j) = lngamma1;
            lngamma(i,(j+dim(2))) = lngamma2;
        end
    end 
end

function Err = calc_Err(NRTLParam_T,resultslnGamma1,resultslnGamma2,T,specs)
      
lngammaCalc = zeros(length(T), length(specs.composition) * 2);

%% Calc NRTL Parameter from NRLT(T) Parameter
NRTLPar = zeros(length(T),3);
NRTLPar(:,1)=calcAlphaFromPar(T,NRTLParam_T(1,1:2));
NRTLPar(:,2)=calcTauFromPar(T,NRTLParam_T(2,:));
NRTLPar(:,3)=calcTauFromPar(T,NRTLParam_T(3,:));

%% Calc Gamma from NRLT Parameter         
    for i=1:length(T)
        temp2 =NRTL_function(specs.composition,NRTLPar(i,:)); 
        lngammaCalc(i,:)= [temp2(1,:),temp2(2,:)];
    end

%% Calc mean relative deviation MRD
MRD = mean(abs(exp(lngammaCalc - [resultslnGamma1,resultslnGamma2])-1), 'all'); %Mean relative deviation

MaxErr = max(abs(exp(lngammaCalc - [resultslnGamma1,resultslnGamma2])-1), [], 'all'); %Maximum relative deviation

Err = [MRD, MaxErr];
            
end

function lngamma = NRTL_function(x,para)

x1=x;
x2=(1-x);
G12= exp(-para(1)*para(2));
G21= exp(-para(1)*para(3));
tau12=para(2);
tau21=para(3);

lngamma = zeros(2,length(x));

lngamma1 = x2.^2.*(tau21.*(G21./(x1+x2.*G21)).^2+tau12*G12./(x2+x1.*G12).^2);
lngamma2 = x1.^2.*(tau12.*(G12./(x2+x1.*G12)).^2+tau21*G21./(x1+x2.*G21).^2);

lngamma(1,:)=lngamma1;
lngamma(2,:)=lngamma2;

end

function alpha = calcAlphaFromPar(T,NRTLPar)

alpha = NRTLPar(1) + NRTLPar(2) .*T;

end
function tau= calcTauFromPar(T,NRTLpar)


tau = NRTLpar(1) + NRTLpar(2)./T + NRTLpar(3).*log(T)  + NRTLpar(4) .* T;

end
