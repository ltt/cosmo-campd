function molStruct = job_NRTL(molStruct,compounds,specs)
% Property Prediction procedure for NRTL-parameter

%% General

nComp = length(specs.system); % number of Compounds
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system

solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

resultsNRTL = cell(0);
resultsNRTLSol = cell(0);
resultsNRTLSolSol = cell(length(solvents), length(solvents));

warning('off', 'all');


%% Job procedure
T = linspace(specs.T_start, specs.T_end, specs.T_interval)';

%% Calculate NRTL Parameters for all non Solvents

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating NRTL parameters of ', num2str(length(molStruct)), ' solvent systems\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(nchoosek(nComp-nSolvs, 2)+length(solvents)*(nComp-nSolvs)+nchoosek(length(solvents), nSolvs-1));
end

parfor i = 1:nComp-nSolvs
    for j = 1:nComp-nSolvs
        if i<j
            compVec = [compounds(1,i),compounds(1,j)];
            resultsNRTL{i,j} = sub_NRTL_T(i,compVec, T, specs);
                
            % Update parfor-progressbar
            if strcmp(specs.Mode, 'screening')
                parfor_progress;
            end            
        end
    end
end

% Set the NRTL Parameters calculated in the previous step
for k = 1: length(molStruct)
    for i = 1:nComp-nSolvs
        for j = 1:nComp-nSolvs
            if i<j && nComp >2
                molStruct(k).TauParam{i,j}      = resultsNRTL{i,j}.NRTLParam_T(2,:);
                molStruct(k).TauParam{j,i}      = resultsNRTL{i,j}.NRTLParam_T(3,:); 
                molStruct(k).AlphaParam{i,j}    = resultsNRTL{i,j}.NRTLParam_T(1,1:2);
                molStruct(k).AlphaParam{j,i}    = resultsNRTL{i,j}.NRTLParam_T(1,1:2);               
                %molStruct(k).resultsGamma{i,j}  = resultsNRTL{i,j}.resultsGamma;   
                molStruct(k).err_NRTL{i,j}      = resultsNRTL{i,j}.Err;
            end
        end   
    end
end

%% Calculate NRTL Parameters for all solvents 
% Get NRTL Parameter for all solvent - compound pairs
parfor k = 1: length(solvents)  
    for i = 1: nComp-nSolvs        
        compVec = [compounds(k,i),solvents(k)];
        resultsNRTLSol{k,i} = sub_NRTL_T(k,compVec, T, specs);
        
        % Update parfor-progressbar
        if strcmp(specs.Mode, 'screening')
            parfor_progress;
        end    
    end
end

% Set the NRTL Parameters calculated in the previous step
for k = 1: length(molStruct)
    for i = 1:nSolvs
        pos = nComp-nSolvs+i;
        currentsolvent = compounds(k,pos);
        idxsolv = find(strcmp(solvents, currentsolvent));       
        for j = 1:nComp-nSolvs
            molStruct(k).TauParam{j,pos}      = resultsNRTLSol{idxsolv,j}.NRTLParam_T(2,:);
            molStruct(k).TauParam{pos,j}      = resultsNRTLSol{idxsolv,j}.NRTLParam_T(3,:); 
            molStruct(k).AlphaParam{j,pos}    = resultsNRTLSol{idxsolv,j}.NRTLParam_T(1,1:2);
            molStruct(k).AlphaParam{pos,j}    = resultsNRTLSol{idxsolv,j}.NRTLParam_T(1,1:2);               
            %molStruct(k).resultsGamma{j,pos}  = resultsNRTLSol{idxsolv,j}.resultsGamma;   
            molStruct(k).err_NRTL{j,pos}      = resultsNRTLSol{idxsolv,j}.Err;
        end
    end
end

%% Calculate NRTL Parameters for all solvents in case of mixtures     
% Get NRTL Parameter for all solvent - solvent pairs
if nSolvs >1
bound = length(solvents);
parfor k = 1: bound
    for i = 1: bound
        if k<i
            compVec = [solvents(k), solvents(i)];
            resultsNRTLSolSol{k,i} = sub_NRTL_T(k,compVec, T, specs);
            
            % Update parfor-progressbar
            if strcmp(specs.Mode, 'screening')
                parfor_progress;
            end    
            
        end
    end 
end
end
          
for k = 1: length(molStruct)     
    for i = 1:nSolvs
        for j = 1:nSolvs
            if i<j
                % Solvent 1
                pos1 = nComp-nSolvs+i;
                currentsolvent1 = compounds(k,pos1);
                idxsolv1 = find(strcmp(solvents, currentsolvent1));
                
                % Solvent 2
                pos2 = nComp-nSolvs+j;
                currentsolvent2 = compounds(k,pos2);
                idxsolv2 = find(strcmp(solvents, currentsolvent2)); 
                
                if idxsolv1>idxsolv2
                    tempp1 = pos2;
                    tempi1 = idxsolv2;
                    pos2 = pos1;
                    pos1 = tempp1;
                    idxsolv2 = idxsolv1;
                    idxsolv1 = tempi1;
                    clear tempp2 tempi2
                end

                molStruct(k).TauParam{pos1, pos2}      = resultsNRTLSolSol{idxsolv1,idxsolv2}.NRTLParam_T(2,:);
                molStruct(k).TauParam{pos2,pos1}      = resultsNRTLSolSol{idxsolv1,idxsolv2}.NRTLParam_T(3,:);
                molStruct(k).AlphaParam{pos1, pos2}    = resultsNRTLSolSol{idxsolv1,idxsolv2}.NRTLParam_T(1,1:2);
                molStruct(k).AlphaParam{pos2,pos1}    = resultsNRTLSolSol{idxsolv1,idxsolv2}.NRTLParam_T(1,1:2);
                %molStruct(k).resultsGamma{pos1, pos2}  = resultsNRTLSolSol.resultsGamma;
                molStruct(k).err_NRTL{pos1, pos2}      = resultsNRTLSolSol{idxsolv1,idxsolv2}.Err;
            end
        end
    end
end


%% Fill gaps of identical compounds
for k = 1: length(molStruct)   
    for i = 1:nComp
        molStruct(k).TauParam{i,i}      = zeros(1,4);
        molStruct(k).AlphaParam{i,i}    = zeros(1,2);
    end
end


if strcmp(specs.Mode, 'screening')
   fprintf 'NRTL property prediction completed\n';
end

end
