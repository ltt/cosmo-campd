function molStruct = job_Henry(molStruct,compounds,specs)

nComp = length(compounds)-length(molStruct); % number of Compounds
%% Calculate Henry Coefficients for Absorption column
% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating Henry Coefficients of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

T = linspace(specs.T_start, specs.T_end, specs.T_interval)';  

% Preallocate
HenryCoeff_Data = num2cell(NaN(1,length(molStruct)));
[molStruct.HenryCoeff_Data] = HenryCoeff_Data{:};

for k = 1: length(molStruct)
    molStruct(k).HenryCoeff_Data = sub_henry(k ,compounds(k,:), T, specs);   
        
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
end


%% For mabsorption fit to polynomial
% Preallocate
HenryCoeff = num2cell(NaN(length(molStruct),length(specs.system)));
[molStruct.HenryCoeff] = HenryCoeff;

parfor k = 1: length(molStruct)
    molStruct(k).HenryCoeff = fit_Henry(molStruct(k).HenryCoeff_Data, compounds(k,:), T);       
end


if strcmp(specs.Mode, 'screening')
   fprintf 'Property prediction for Absorption completed\n';
end

end

%% Function Appendix

function HenryCoeff = fit_Henry(HenryCoeff_Data,compounds, T)

% HenryCoeff as used in Aspen Plus and in bar
% ln(H) = a + b/T + c ln(T) + d T + e /T^2

HenryCoeff = cell(length(compounds),1);

for i=1:length(compounds)
    HenryCOSMO = cell2mat(HenryCoeff_Data(:,i));
    para0 = [0, 0, 0, 0, 0];
    options = optimoptions('lsqcurvefit','Display','off','MaxFunEvals',1e5, 'Algorithm','levenberg-marquardt');
    HenryCoeff{i,1} = lsqcurvefit(@Henry_function, para0, T, HenryCOSMO, [],[], options);
end

end

function fitted = Henry_function(a,T)

% As used in Aspen Plus
% ln(H) = a + b/T + c ln(T) + d T + e /T^2

fitted = exp(a(1) + a(2)./T + a(3)*log(T) + a(4)*T + a(5)./((T).^2));

end