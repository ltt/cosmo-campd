function molStruct = job_Molweight(molStruct,compounds,specs)

%% General
nComp = length(specs.system);           % number of Compounds
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system

% Get all solvents
solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating molecular weights of ', num2str(length(solvents)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

MW_solvents = zeros(length(solvents),1);

parfor k=1:length(solvents)
    
    MW_solvents(k) = sub_molweight(solvents(k), k, specs);
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end


%% Merge and fill molStruct
for k=1:length(molStruct)
    for is=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+is)));
        molStruct(k).MW_solvent(is) =  MW_solvents(idxsolv);
    end
end

end
