## @file        wrapRMGThermo.py
## @author      Malte Doentgen
## @date        2015/07/15
## @brief       wraps RMG thermo-data estimator
## @details
#	Instructions for running the RMG-Java ThermoDataEstimator from
#	python based on SMILES strings. Shell commands are intented and
#	and start with a '$' sign.
#
#	1. Install RMG (from the RMG-Java readme)
#		$ git clone git://github.com/GreenGroup/RMG-Java.git
#		$ cd RMG-Java
#		$ make
#		$ make test
#		$ echo "export RMG=$PWD" >> ~/.bashrc
#
#	2. Run wrapRMGThermo.py for a SMILES <smi>
#		$ python wrapRMGThermo.py <smi>
#	   This will generate an input-file and subsequently runs RMG's
#	   ThermoDataEstimator. NASA coefficients are extracted and used
#	   to compute G(T) for temperatures ranging from 300 to 3000 K.
#
#	3. compute thermo-data for a list of SMILES
#		$ python wrapRMGThermo.py < <smi-list>
#	   The free enthalpie G(T) is computed for all SMILES supplied
#	   by the <smi-list> file. Results are stored in 'thermo.tab'.
#	   Each line of <smi-list> which does not start with '!' or '#'
#	   is read and each SMILES separates by blanc space is used for
#	   thermo-data computation. The signs '!' and '#' can be used
#	   as comment line initiators.
#
#	Evaluation of ThermoDataEstimator accuracy:
#	T = 298K     H2       H     CH4     CH3     H2O     OH
#	Chase1998 -9.31   43.93  -31.16   20.99  -71.25  -3.77
#	Estimate  -9.42   44.16  -31.53   20.96  -71.76  -3.74
#	Error      1.21%   0.52%   1.19%   0.15%   0.72%  0.68%
#
#	Chase1998: M.W. Chase Jr., J.Phys.Chem.Ref.Data, Monograph 9, 1998, 1-1951
#

import sys
import os
import time
import math
import subprocess
import openbabel

conv = openbabel.OBConversion()
conv.SetInAndOutFormats('smi', 'mdl')

# . initilize thermo arrays
R = 0.002	# kcal/mol
t0 = 300.0; t2 = 3000.0; dt = 100.0
T = [t0 + i*dt for i in xrange(int((t2-t0)/dt)+1)]
H = [0.0 for t in T]; S = [0.0 for t in T]; G = [0.0 for t in T]
species = {}

# . read input
if len(sys.argv) > 1: args = sys.argv[1:]
else:
	print '  Enter SMILES / enter "DONE"'
	args = []
	done = False
	while not done:
		try: tmp = raw_input('')
		except EOFError: done = True
		if tmp.lower() == 'done': done = True
		elif len(tmp) > 0 and tmp[0] != '!' and tmp[0] != '#': args += tmp.split()

print '  high/low switch at %.02f K' %(1000)
print '%20s %10s %10s %10s %10s %10s %10s %10s' %('molecule', 'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7')
for runner in range(1):
	# . RMG part: if not exiting
        smi = args[0]
        no = args[1]
        tmpOutput = 'TDEresultsCHEMKIN.dat'


	if not os.path.isfile(smi+'/'+tmpOutput):
		# . convert SMILES to OBMol
		mol = openbabel.OBMol()
		conv.ReadString(mol, smi)
		for atom in openbabel.OBMolAtomIter(mol):
			if atom.ImplicitHydrogenCount() > 0:
				mol.AddHydrogens(atom)
	
		# . write RMG input in subfolder
		temporaryFolder = os.path.join('PropertyPrediction/ExtTools/RMGThermo/temp/', "%s_%s" %(smi, no))
		if not os.path.exists(temporaryFolder):
			os.mkdir(temporaryFolder)
		os.chdir(temporaryFolder)

		tmpInput = 'input.txt'
		writer = open(tmpInput, 'w')
		writer.write('Database: RMG_database\n\n')
		writer.write('BensonOnly\n')
		writer.write('both\n')
		writer.write('0\n')
		writer.write('check\n\n')
		# make following statements dynamic ?
                writer.write('MaxCarbonNumberPerSpecies:     30\n') # LFl: was 30
		writer.write('MaxOxygenNumberPerSpecies:     10\n')
		writer.write('MaxRadicalNumberPerSpecies:    10\n')
		writer.write('MaxSulfurNumberPerSpecies:     10\n')
		writer.write('MaxSiliconNumberPerSpecies:    10\n')
                writer.write('MaxHeavyAtomNumberPerSpecies: 100\n') # LFl: was 100
		writer.write('MaxCycleNumberPerSpecies:      10\n')
		writer.write('END\n\n')
		# ---
		writer.write('PrimaryThermoLibrary:\n')
		writer.write('Name: RMG-minimal\n')
		writer.write('Location: primaryThermoLibrary\n')
		writer.write('END\n\n')
		writer.write('%s\n' %smi)
		
		atype = {1: 'H', 6: 'C', 7: 'N', 8: 'O', 14: 'Si', 16: 'S'}
		btype = {1: 'S', 2: 'D', 3: 'T'}
		for atom in openbabel.OBMolAtomIter(mol):
			idx = atom.GetId()+1
			element = atype[atom.GetAtomicNum()]
			rad = atom.GetImplicitValence() - atom.GetValence()
			if mol.NumAtoms() == 1 and atom.IsHydrogen(): rad = 1
			bond = [[b.GetNbrAtom(atom).GetId()+1, btype[b.GetBO()]] for b in openbabel.OBAtomBondIter(atom)]
			writer.write('%d  %s %d %s\n' %(idx, element, rad, ' '.join('{%d,%s}' %(b[0], b[1]) for b in bond)))
		
		writer.write('\n')
		writer.close()
		
		# . estimate thermo data
		runTime = 10	# seconds
		try: RMGpath = os.environ['RMG']
		except KeyError: RMGpath = False
		if RMGpath:
			thermo = subprocess.Popen(['java','-classpath',RMGpath+'/bin/RMG.jar','ThermoDataEstimator',tmpInput])
			time.sleep(runTime)
			thermo.terminate()
			thermo.wait()
		else: print '!!! ERROR: cannot find/execute RMG.jar'; sys.exit()	# exit point
		os.chdir('..')
	# ... RMG part done

	# . harvest thermo-data
	if smi not in species:
		reader = open(smi+'_'+no+'/'+tmpOutput, 'r')
		tmplines = reader.readlines()
		T0, T2, T1 = [float(t) for t in tmplines[-5].split()[-4:-1]]
		NASA = [[float(tmplines[-4][0:15]), float(tmplines[-4][15:30]), float(tmplines[-4][30:45]), float(tmplines[-4][45:60]), float(tmplines[-4][60:75]), float(tmplines[-3][0:15]), float(tmplines[-3][15:30])],
			[float(tmplines[-3][30:45]), float(tmplines[-3][45:60]), float(tmplines[-3][60:75]), float(tmplines[-2][0:15]), float(tmplines[-2][15:30]), float(tmplines[-2][30:45]), float(tmplines[-2][45:60])]]
		reader.close()
	
		# . compute H, S and G
		for i in xrange(len(T)):
			t = T[i]
			if t < T1:	# low-temperature  NASA coefficients
				H[i] = NASA[0][0] +NASA[0][1]*t /2.0 +NASA[0][2]*t**2 /3.0 +NASA[0][3]*t**3 /4.0 +NASA[0][4]*t**4 /5.0 +NASA[0][5]/t
				S[i] = NASA[0][0]*math.log(t)+NASA[0][1]*t +NASA[0][2]*t**2 /2.0 +NASA[0][3]*t**3 /3.0 +NASA[0][4]*t**4 /4.0 +NASA[0][6]
				G[i] = R*t*(H[i] - S[i])
			else:		# high-temperature NASA coefficients
				H[i] = NASA[1][0] +NASA[1][1]*t /2.0 +NASA[1][2]*t**2 /3.0 +NASA[1][3]*t**3 /4.0 +NASA[1][4]*t**4 /5.0 +NASA[1][5]/t
				S[i] = NASA[1][0]*math.log(t)+NASA[1][1]*t +NASA[1][2]*t**2 /2.0 +NASA[1][3]*t**3 /3.0 +NASA[1][4]*t**4 /4.0 +NASA[1][6]
				G[i] = R*t*(H[i] - S[i])
		species[smi] = list(G)
		print '%20s %10.3e %10.3e %10.3e %10.3e %10.3e %10.3e %10.3e' %(smi, NASA[0][0], NASA[0][1], NASA[0][2], NASA[0][3], NASA[0][4], NASA[0][5], NASA[0][6])
		print '%20s %10.3e %10.3e %10.3e %10.3e %10.3e %10.3e %10.3e' %('', NASA[1][0], NASA[1][1], NASA[1][2], NASA[1][3], NASA[1][4], NASA[1][5], NASA[1][6])

                #. LF Output
                outputLF = "%s_%s.out" %(smi,no)
                writer = open(outputLF, 'w')
                writer.write('Benson''s GC method for ideal gas heat capacities\n')
                writer.write('NASA polynomial for low temperature range (T<1000 K)\n')
                writer.write('%-20s %10.3e %10.3e %10.3e %10.3e %10.3e %10.3e %10.3e' %(smi, NASA[0][0], NASA[0][1], NASA[0][2], NASA[0][3], NASA[0][4], NASA[0][5], NASA[0][6]))
                writer.close()


