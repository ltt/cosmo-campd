import csv 
from tamkin import *

def TAMkinexe ():
#import os 
#
#print __file__
#print os.path.abspath(__file__)
#print os.path.dirname(__file__)
#print os.path.join(os.path.dirname(__file__),)

	 from molmod import deg, kjmol
	 import numpy

# NpT is used when nothing else is specified (1bar)
#######################################

#NpT:
	 Molecule1      		= load_molecule_g03fchk("methane_c0.fchk")
#######################################

#NMA:
	 nmaMolecule1		= NMA(Molecule1, ConstrainExt())
#######################################

#pf:
	 pfMolecule1		= PartFun(nmaMolecule1, [ExtTrans(), ExtRot(symmetry_number=1)])
#######################################

#thermo analysis:
	 T = [273.15,300] 
	 taMolecule1		= ThermoAnalysis(pfMolecule1,T)
#######################################

#save Reactant-Results in .csv files:
	 taMolecule1.write_to_file("methane_c0.csv")
#######################################

	 # outputfile evaluation 
	 file = open("methane_c0.csv", "r")

	 csv_reader = csv.reader(file, delimiter=",")
	 csv_reader = [x for x in csv_reader if x != []]
	 line_num = 0
	 for row in csv_reader:
	 	 if row[0] == "Internal heat":
	 	 	 inheat_index = line_num +2 
	 	 if row[0] == "Heat capacity":
	 	 	 heatcap_index = line_num +2
	 	 if row[0] == "Free energy":
	 	 	 freeng_index = line_num +2
	 	 if row[0] == "Chemical potential":
	 	 	 chempot_index = line_num +2
	 	 if row[0] == "Entropy":
	 	 	 entropy_index = line_num +2
	 	 line_num +=1
	 file.close()
	 temperature = [] 
	 inheat = []
	 heatcap = []
	 freeng = [] 
	 chempot = []
	 entropy = []
	 for i in range(1, len(csv_reader[1])):
	 	 temperature.append(csv_reader[1][i]) 
	 	 inheat.append(csv_reader[inheat_index][i])
	 	 heatcap.append(csv_reader[heatcap_index][i])
	 	 freeng.append(csv_reader[freeng_index][i])
	 	 chempot.append(csv_reader[chempot_index][i])
	 	 entropy.append(csv_reader[entropy_index][i])
	 
	 return [temperature,inheat,heatcap,freeng,chempot,entropy]
	 
