function [ok]=tamkin_inputfile_system(name_fchk,name_csv,T)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
% This function writes a TAMkin file and starts a TAMkin ThermoAnalysis % 
% calculation.                                                          %
% A python script which executes the TAMkin calculation is written in   %
% the first part. Afterwards, the TAMkin .csv output file is converted  % 
% to a matlab array. The evaluation of the TAMkin .csv file is written  %
% in the python TAMkin execution file,too.                              %
%                                                                       %
% written by:                                                           %
% Stefan Fritz (Supervisor: Christoph Gertig) 
% modified by Christoph Gertig
%                                                                       %
%  |¯¯¯¯¯¯¯¯¯¯| |¯¯¯¯¯¯¯¯¯¯| |¯¯¯¯¯¯¯¯¯¯| ___.___|¯¯¯|___\¯/_,,         %
%  |__________| |__________| |__________| |______||__________|          %
%   OO¯¯¯¯OO --- OO¯¯¯¯OO --- OO¯¯¯¯OO - O¯¯¯¯O - OO¯¯¯¯OO°%  \         %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% write program in python for TAMkin execution 

% open new file
fileID=fopen('TAMkin.py','w');

% start file writing -> standard file for TAMkin calculations
fprintf(fileID,'import csv \n');
fprintf(fileID,'from tamkin import *\n\n');
%fprintf(fileID,'def TAMkinexe ():\n');
fprintf(fileID,'#import os \n');
fprintf(fileID,'#\n');
fprintf(fileID,'#print __file__\n');
fprintf(fileID,'#print os.path.abspath(__file__)\n');
fprintf(fileID,'#print os.path.dirname(__file__)\n');
fprintf(fileID,'#print os.path.join(os.path.dirname(__file__),)\n\n');
fprintf(fileID,'from molmod import deg, kjmol\n');
fprintf(fileID,'import numpy\n\n');
fprintf(fileID,'# NpT is used when nothing else is specified (1bar)\n');
fprintf(fileID,'#######################################\n\n');
fprintf(fileID,'#NpT:\n');
temp_fchk = ['Molecule1      		= load_molecule_g03fchk("',name_fchk,'")\n'];
%temp_fchk = 'Molecule1      		= load_molecule_g03fchk("test.fchk")\n';
fprintf(fileID,temp_fchk);
fprintf(fileID,'#######################################\n\n');
fprintf(fileID,'#NMA:\n');
fprintf(fileID,'nmaMolecule1		= NMA(Molecule1, ConstrainExt(gradient_threshold=0.01))\n');
fprintf(fileID,'#######################################\n\n');
fprintf(fileID,'#pf:\n');
fprintf(fileID,'pfMolecule1		= PartFun(nmaMolecule1, [ExtTrans(), ExtRot()])\n');
fprintf(fileID,'#######################################\n\n');
fprintf(fileID,'#thermo analysis:\n');
%text1 = ['taMolecule1		= ThermoAnalysis(pfMolecule1,',num2str(T),')\n'];
a = ['T = ['];
for i =1:length(T)-1
    b = [num2str(T(i)),','];
    a = [a,b];
end
a = [a,num2str(T(end)),'] \n'];
fprintf(fileID,a);
text1 = ['taMolecule1		= ThermoAnalysis(pfMolecule1,T)\n'];
fprintf(fileID,text1);
fprintf(fileID,'#######################################\n\n');
fprintf(fileID,'#save Reactant-Results in .csv files:\n');

text2 = ['taMolecule1.write_to_file("',name_csv,'")\n'];
%text2 = ['taMolecule1.write_to_file("test.csv")\n'];
fprintf(fileID,text2);
fprintf(fileID,'#######################################\n\n');

%% This part writes an additional program in the python file to evaluates the TAMkin .csv file

% open & load .csv file
fprintf(fileID,'# outputfile evaluation \n');

read_csv = ['file = open("',name_csv,'", "r")\n\n'];
%read_csv = 'file = open(test.csv, "r")\n\n';
fprintf(fileID,read_csv);
fprintf(fileID,'csv_reader = csv.reader(file, delimiter=",")\n');

% delete empty elements in csv_reader
fprintf(fileID,'csv_reader = [x for x in csv_reader if x != []]\n');

% definition of counter variable
fprintf(fileID,'line_num = 0\n');

fclose(fileID);

ok=1;


