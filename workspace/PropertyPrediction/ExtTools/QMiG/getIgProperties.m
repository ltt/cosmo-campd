function [int_heat,cp_ig,s_ig,v_ig,g_ig] = getIgProperties(T_vec,energy_file,SMILES,GaussianFileName,k,specs)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                       %
% This function runs a gaussian and a TAMkin calculation to calculate   %
% internal heat, heat capacity, entropy, chemical potential and free    %
% energy of a molecule, given by the energy file. This material         %
% values are for calculated with ideal gas adoption. It is also possible% 
% to calculate this values for several, predifined temperatures         %
% (given by T_vec).                                                     % 
%                                                                       %
% written by:                                                           %
% Stefan Fritz (Supervisor: Christoph Gertig)                                       %
%                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




