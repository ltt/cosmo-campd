function [weightedresult, result,ok] = sub_Ecosmo(compound, wconf, specs)
%% Get cosmo-Energy from cosmo-file for a compound
%
%   Input:  compound 
%           specs
%
% Output:   E_cosmo
%

%% Check for file in database and extract Ecosmo for each conformer

[~, ~, ~, dbPath, ~, ~, ~, ~, ~, ~, ~] = main_header(specs);

folder = compound{1}(1);
if strcmp(folder, '(' ) ||  strcmp(folder, '[' )
    folder = '0';
end
confnumber = length(wconf);

result = zeros(1,confnumber);

for w=0:confnumber-1
    cosmoFile = fullfile(dbPath, folder, [compound{1},'_c', num2str(w), '.cosmo']);
    if ~exist(cosmoFile, 'file')
        disp(['ERROR: cosmo-File not in Database. Solvent: ', compound{1},'_c', num2str(w)]);
    else
        ok = 1;          
        fileID = fopen(cosmoFile,'r');
        line = fgetl(fileID); 
        formatSpec = '%*s %*s %*s %*s %*s %*s %*s %f';   
        while ~feof(fileID)
            if ~isempty(strfind(line,'Total energy + OC corr.'))
                tempresult = textscan(line,formatSpec); % Read line 
                break
            end        
            line = fgetl(fileID); % For gamma: information below "Nr Compound"
        end

        result(w+1) = cell2mat(tempresult);    
        fclose(fileID);        
    end
end

%% Weight by wconf

weightedresult =0;
for w=1:confnumber
    weightedresult = weightedresult + wconf(w)*result(w);
end

end