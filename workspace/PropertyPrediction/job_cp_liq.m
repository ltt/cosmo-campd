function molStruct = job_cp_liq(molStruct,compounds,specs)
% Calculate gaseous heat capacities based on gas heat capacities and 
% heat of vaporization the liquid heat capacities
% Returns liquid heat capacities as NASA polynomial

%% General
nComp = length(specs.system);           % number of Compounds
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system
T = linspace(specs.T_start, specs.T_end, specs.T_interval)'; 

% Get all solvents
solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating liquid heat capacities of ', num2str(length(molStruct)), ' solvents\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(solvents));
end

%% Fit cps for non-solvent components
properties = struct('cp_gas', '', 'dhvap', '', 'Tcrit', '');
for k=1:nComp-nSolvs
    properties(k).cp_gas = molStruct(1).cp_gas{k} ;
    properties(k).dhvap = molStruct(1).dhvap{k};
    properties(k).Tcrit = molStruct(1).Tcrit;
end

for k = 1:nComp-nSolvs
    [cp_liq_co{k,1},cp_liq{k,1}]  = fit_cp_liq(properties(k), T);
end

%% Fit cps for solvents
cp_liq_sol = cell(length(solvents),1);

% Find pure component parameters of solvents and set properties
for k = 1:length(solvents)
    for j=1:nSolvs
        idxsolv = find(strcmp(solvents(k), compounds(:,nComp-nSolvs+j)));
        if ~isempty(idxsolv)
            idxsolv = idxsolv(1);
            break
        end
    end
    properties(k).cp_gas = molStruct(idxsolv).cp_gas{nComp-nSolvs+j} ;
    properties(k).dhvap = molStruct(idxsolv).dhvap{nComp-nSolvs+j};
    properties(k).Tcrit = molStruct(idxsolv).Tcrit;
end

% Do the actual fit
for k = 1:length(solvents)
    [cp_liq_co_sol{k,1},cp_liq_sol{k,1}] = fit_cp_liq(properties(k), T);
end

%% Merge and fill molStruct
for k=1:length(molStruct)
    if nComp-nSolvs > 0
    	molStruct(k).cp_liq_co = cp_liq_co;
        molStruct(k).cp_liq = cp_liq;
    else
        molStruct(k).cp_liq_co = [];
        molStruct(k).cp_liq = [];
    end
    for is=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+is)));
        molStruct(k).cp_liq_co{end+1} =  cp_liq_co_sol{idxsolv};
        molStruct(k).cp_liq{end+1} =  cp_liq_sol{idxsolv};
    end
end

end


function [cp_liq_para, cp_liquid] = fit_cp_liq(properties, T)
%Fit function to get parameter for liquid heat capacities in NASA polynomial

% Calculate liquid heat capacities for each temperature
R = 8.314; % kJ/kmol/K
cp_gas = properties.cp_gas; %/kJ/kmolK
dhvap = properties.dhvap(:,2)*4184; %/kJ/kmol
dt = T(2)-T(1);

dhvap_1 = dhvap(1:end-1);
dhvap_2 = dhvap(2:end);

% cp_liquid
cp_liquid = cp_gas(1:end-1) + (dhvap_1-dhvap_2)/dt ;
cp_liquid = cp_liquid/R;

% Starting guess
para0 = [0, 0, 0, 0, 0];

% Fit
T_vec = T(1:end-1)+dt/2; % as for every interval in the middle
options = optimoptions('lsqcurvefit','Display','off','MaxFunEvals',1e5, 'Algorithm','levenberg-marquardt');
cp_liq_para = lsqcurvefit(@cp_NASA_poly, para0, T_vec, cp_liquid, [],[], options);
cp_liquid = cp_liquid*R;

end

function fitted = cp_NASA_poly(c,T)
% Calculate cp from NASA polynomial
fitted = (c(1) + c(2)*T + c(3)* T .^ 2 + c(4)* T .^ 3+ c(5)* T .^ 4) ; %NASA polynomial
end