function molStruct = job_T_boil_con(molStruct,compounds,specs)
% Property prediction to check T_boil constraint
% Calculates T_boil and checks if in allowed temperature range

%% General
nSolvs = length(find(~cellfun('isempty',strfind(specs.system, 'Solvent ')))); %  Number of solvents per system
nComp = length(specs.system);       % number of Compounds

% Preallocate fields
for k=1:length(molStruct)
    molStruct(k).T_boil = NaN;
    molStruct(k).T_boil_ok = NaN;
end


%% Job procedure
solvents = compounds(:,end);
for i=1:nSolvs-1
    solvents = [solvents; compounds(:,end-i)];
end
solvents = unique(solvents,'stable');

T_boil_solvents = zeros(length(solvents),1);
T_boil_ok_solvents = zeros(length(solvents),1);

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculating T_boil for ', num2str(length(solvents)), ' solvents\nProgress:\n'];
    fprintf(display_text);
    parfor_progress(length(solvents));
end

for k = 1:length(solvents)
    T_boil_solvents(k)= sub_T_boil(k, solvents(k), specs);
    T_boil_ok_solvents(k) = func_check_T_boil( T_boil_solvents(k), specs);
    
    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
    
end

%% Merge and fill molStruct
for k=1:length(molStruct)
    for i=1:nSolvs
        idxsolv = find(strcmp(solvents, compounds(k,nComp-nSolvs+i)));
        molStruct(k).T_boil(i) =  T_boil_solvents(idxsolv);
        molStruct(k).T_boil_ok(i) = T_boil_ok_solvents(idxsolv);
    end
    molStruct(k).T_boil_ok = min(molStruct(k).T_boil_ok);
end

end

function [T_boil_ok] = func_check_T_boil(T_boil, specs)
% Checking boiling point temperature of solvent 
% The lower and upper bound of acceptable boiling temperature are given in
% the "specs"-struct, fields T_boil_max and T_boil_min

%% Create bounds for checking T_boil

if strcmp(specs.T_boil_max, 'no_T_max')
    T_max = 1e99;
else
    T_max = specs.T_boil_max;
    if ischar(T_max)
        T_max = str2double(T_max);
    end
   
end

if strcmp(specs.T_boil_min, 'no_T_min')
    T_min = 0;
else
    T_min = specs.T_boil_min;
    if ischar(T_min)
        T_min = str2double(T_min);
    end
   
end

%% Check T_boil of given compound

if(T_boil > T_max)
        T_boil_ok = 0;
elseif(T_boil < T_min)
        T_boil_ok = 0;
else    T_boil_ok = 1;
end

end
