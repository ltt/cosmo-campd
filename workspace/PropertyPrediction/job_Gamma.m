function molStruct = job_Gamma(molStruct, composition, compounds,specs)
% Return activity coefficients from COSMOtherm for given concentration

T = specs.T;

% Initialize parfor_progressbar
if strcmp(specs.Mode, 'screening')
    display_text = ['\nCalculate activity coefficients for given composition\nProgress:\n'];
    fprintf (display_text);
    parfor_progress(length(molStruct));
end

parfor k=1:length(molStruct)
    if ~isnan(composition)
            molStruct(k).lngamma = sub_gamma(composition(k,:), k,compounds(k,:) , T, specs);
    end

    % Update parfor-progressbar
    if strcmp(specs.Mode, 'screening')
        parfor_progress;
    end
        
end
        
end
