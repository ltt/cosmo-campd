#!/usr/bin/perl

###################################################################################################
#----------------------------------------LEA Run Options -----------------------------------------#
#                                                                                                 #
# Set options for LEA3D (COSMO-CAMD approach) 	                                                  #
#                                                                                                 #
# Written by Lorenz Fleitmann, supervised by Jan Scheffcyzk    			                  #
# Lehrstuhl für Technische Thermodynamik (LTT), RWTH Aachen University,      Juni, 2016           #
#                                                                                                 #
###################################################################################################

# Directories for COSMO-Programs
$generalFolder  = "/opt/est/";
$cosmoconfdir   = "COSMOconfX16/COSMOconf/cosmoconf.pl";
$rdkitdir       = "COSMOquick16/extapps/RDKit/bin/confcreate_linux";
$cosmoCFDBdir   = "CFDB_3.7";
$cosmofragdir   = "COSMOquick16/COSMOfrag/cf_linux";
$licensefragdir = "COSMOquick16/licensefiles";
$databasedir    = "/daten/Central_COSMO_Database_v16/";
$COSMOFileGen   = "COSMOFileGen/";

# Directories for Gate2LEA:
$Gatedir	= "Gate/";
$gatecosmo	= "$Gatedir/cosmoFiles";
$gatemolstruct  = "$Gatedir/molStruct_temp";
$gateout2lea	= "$Gatedir/output2LEA";

# Choose COSMO-RS parametrization:
$cosmomethod = "TZVP-MF";
#$cosmomethod = "TZVPD-FINE"; 
#$cosmomethod = "COSMOfrag";
#$cosmomethod = 'TZVP';
#$cosmomethod = 'SVP-AM1';

# Specify limits for nbatom or set "no_nbatom_cutoff":
$nb_max = 12;
#$nb_max = "no_nbatom_cutoff";

# For COSMOfrag precalculation set following variable to 1
$cosmofrag_precalc = 0;
#$cosmofrag_precalc = 1;
$lb_cosmofrag = 0.001;

# Specify limits for any atom:
#$atom_to_limit = "O";
#$atom_limit = 2;

# Specify undesired types of molecules:
$peroxide  = "OO";
$oxalate   = "C(=O)C(=O)";
$anhydride = "C(=O)OC(=O)";
$oxygenfluoride1 = "OF";
$oxygenfluoride2 = "FO";
$aldehydes1 = 'O=C';
$aldehydes2 = 'C=O';
$aldehydes3 = '(C=O)';
$sulfhalmid1 = 'S(Br)';
$sulfhalmid2 = 'S(Cl)';
$sulfhalmid3 = 'S(F)';
$sulfhalend1 = 'SBr';
$sulfhalend2 = 'SCl';
$sulfhalend3 = 'SF';
$halaldend1 = 'C(Br)=O';
$halaldend2 = 'C(Cl)=O';
$halaldend3 = 'C(F)=O';




# if a targeted initialisation is needed:
 $init_firstgen = 0; #cold start -> random initialisation    
# $init_firstgen = 1; #warm start -> result from screening
# $init_firstgen = 2; #re-start -> result from last calculation
# $init_firstgen = 3; #continue start -> start at the break point


# For debugging choose $if_debug = 1. The function <STDIN> will be applied.
 $if_debug = 0;
 #$if_debug = 1;


# Maximum number of CPU-cores (nproc returns number of cores in this machine) 
$numberofCPU = `nproc`;      
chomp($numberofCPU);
#$numberofCPU = 8;
#$numberofCPU = $numberofCPU -1;

# Specify email for update on LEA3D progress
$recipient = 'lorenz.fleitmann@ltt.rwth-aachen.de';
#$recipient = 'christoph.gertig@ltt.rwth-aachen.de';
#$recipient = 'yifan.wang236@gmail.com';
#$recipient = 'christoph.gertig@ltt.rwth-aachen.de, lorenz.fleitmann@rwth-aachen.de';

#-----------------------------------------------------------------------------------------

@undesired_mols = ($peroxide, $oxalate, $anhydride, $oxygenfluoride1, $oxygenfluoride2, $aldehydes3, $sulfhalmid1, $sulfhalmid2, $sulfhalmid3);
#@undesired_mols = ();
@undesired_endings = ($aldehydes2, $sulfhalend1, $sulfhalend2, $sulfhalend3, $halaldend1, $halaldend2, $halaldend3);
@undesired_beginnings = ($aldehydes1);



if ($cosmomethod eq "TZVP-MF"){
        $COSMOdatabasename = "TZVP-COSMO";
        $ctd = "BP_TZVP_C30_1701.ctd";
        $resultsdir = "Results_of_job_BP-TZVP-MF-COSMO+GAS";}
elsif ($cosmomethod eq "TZVPD-FINE"){
        $COSMOdatabasename = "TZVPD-FINE";
        $ctd = "BP_TZVPD_FINE_C30_1701.ctd";
        $resultsdir = "Results_of_BP-TZVPD-FINE-COSMO+GAS";}
elsif (($cosmomethod eq "COSMOfrag") or ($cosmomethod eq 'SVP-AM1')){
        $COSMOdatabasename = "SVP-AM1";
        $ctd = "BP_SVP_AM1_C30_1701.ctd";
	$resultsdir = "Results_of_job_BP-SVP-AM1-COSMO+GAS";}
elsif ($cosmomethod eq "TZVP"){
        $COSMOdatabasename = "TZVP-COSMO";
        $ctd = "BP_TZVP_C30_1701.ctd";
        $resultsdir = "Results_of_job_BP-TZVP-COSMO+GAS";}

$databasedir    = $databasedir.$cosmomethod."/";


die "\n\nLimit imposed on atomtype $atom_to_limit, but no integer limit was specified. Please specify limit in \$atom_limit. Code stopped" if (defined($atom_to_limit) and (!defined($atom_limit)));

if($if_debug){

    if ($init_firstgen  == 1){
        print "\n\nInitialization of this run with result of screening selected. If correct, please press enter\n";
        <STDIN>;
    }
    elsif ($init_firstgen  == 2){
        print "\n\nInitialization of this run with last generation of previous run selected. If correct, please press enter\n";
        <STDIN>;
        
    }
    elsif($init_firstgen  == 3){
        print "\n\nInitialization of this run with last generation of (aborted) previous run selected. If correct, please press enter\n";
        <STDIN>;

    }
}

if (!(($cosmomethod eq 'TZVP-MF') or ($cosmomethod eq 'COSMOfrag'))){
	print "\n\nCOSMO-RS parametrization was chosen to $cosmomethod, but so far there is only a good database on TZVP-MF level. Do you really want to continue?";
	<STDIN>;
}
1;
