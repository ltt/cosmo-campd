function molStruct = conductLCA(molStruct, path)
% CONDUCT LCA Calculates environmental impacts using estimations from a neural
%             network and process data stored in molStruct
%   Author: Johanna Kleinekorte (johanna.kleinekorte@ltt.rwth-aachen.de)
%   Date:   28.06.2018
%
%   Input:      molStruct - contains process data provided from short cut
%               and molecular properties frm COSMO-RS
%
%               path - path for saving the resulting molStruct
%
%   Output:     molStruct - extended by environmental impacts for each
%               molecule
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Settings
choseNaN = 0;                                                               % distinguish between replacing emty entries with NaN or zero

output_cats = cumsum(ones(17,1))';                                          % for numeration count next row entries in categories

%% Conduct LCA
categories = {'ALO','CC_ohneBio','CC','FD','FET','FE','HT','IR','MET',...
    'ME','MRD','OD','PMF','POF','TA','TET','WD'};
load WBfile;                                                                % Weights and Biases have to be stored in this file. Hyperparameters belong only to a certain W&B set. For new weights and biases, hyperparameters need to be found again

for i = 1:length(output_cats)
    category = categories{output_cats(i)};
    
    molStruct = Start_Model(molStruct,category,choseNaN,WB,output_cats(i));             % using the neural networks for estimations
    
    molStruct = calcFullLCA(molStruct, category,output_cats(i),choseNaN);   % calculate the full LCA based on process data and ANN estimation

end
save([path,'molStruct.mat'],'molStruct');
end

function [molStruct] = Start_Model(molStruct,category,choseNaN,allWB,output_cat)
% START_MODEL Get estimated Environmental Impacts from Neural Network Model
%
%   Input:      molStruct - contains process data provided from short cut
%               and molecular properties frm COSMO-RS
%
%               category - string containing the abbreviation of regarded
%               impact category (e.g., 'CC' for climate change)
%
%               choseNaN - boolean, wether NaN values should be ignored or
%               replaced by 0 (1 - ignore, 0 - replace)
%
%               allWB - cell containing initial weights for the neural
%               network. The hyperparameters are specified for one set of
%               initial weights!
%
%               output_cat - index of the chosen impact category
%
%   Output:     molStruct - extended by predicted environmental impacts for
%               each molecule
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Apply the neural network

InOut = convertToInOut(molStruct, category,choseNaN);
[Y_est] = Func_NeuralNetwork(InOut,allWB,output_cat);                       % estimate Environmental Impact by neural network model

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Save Output Data into molStruct

for i = 1:length(molStruct)
    molStruct(i).(category) = Y_est(i);
end
end
function [InOut] = convertToInOut(molStruct, category,choseNaN)
% CONVERT TO INOUT transforms molStruct into a descriptor matrix X and a
% target matrix T for the neural network
%
%   Input:      molStruct - contains process data provided from short cut
%               and molecular properties frm COSMO-RS
%
%               category - string containing the abbreviation of regarded
%               impact category (e.g., 'CC' for climate change)
%
%               choseNaN - boolean, wether NaN values should be ignored or
%               replaced by 0 (1 - ignore, 0 - replace)
%
%   Output:     InOut - string containing matrices:
%               - X_red (selected descriptors for regarded impact category)
%               - X (all possible descriptors)
%               - T (target = training impacts of regarded impact category)
%               - T_all (targets for all impact categories)
%               each column represents one molecule, each row is one
%               target/ descriptor
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
files = subdir(['*',category,'*.mat']);

for k = 1:length(files)
    oneFile = files(k).name;
    if ~isempty(strfind(oneFile,'best results NN')) % hier noch ein geeignetes Suchwort eingeben
        path_generateNN = oneFile;
    elseif (strfind(oneFile,'InOut')>0) & ~isempty(strfind(oneFile,'penter'))
        if ~choseNaN & ~isempty(strfind(oneFile,'InOut mit 0 f�r NaN'))
            load(oneFile);
        elseif choseNaN & ~isempty(strfind(oneFile,'InOut mit NaN'))
            load(oneFile);
        end
    end
end

%% adding the names of the estimates
InOut.names = {molStruct.name};
%% adding the hyper parameter
NN = load(path_generateNN);
% InOut.hyperParams = NN.Results.x;
InOut.hyperParams = NN.bestSet.X;

%% exceeding the descriptors X by chemicals to be estimated
fields = fieldnamesr(molStruct(1));                                         % provides names of all fields and subfields
num_chemicals = length(molStruct);
X_new = zeros(length(InOut.legend),num_chemicals);

for i=1:length(InOut.legend)
    
    %     fitField = strfind(fields,InOut.legend{i});
    if length(InOut.legend{i})>5
        fitField = strfind(fields,InOut.legend{i}(1:end-2));                % delivers all positions, where the legend entry is found in a field name
    else
        fitField = strfind(fields,InOut.legend{i}(1:end));                  % delivers all positions, where the legend entry is found in a field name
    end
    index = find(~cellfun(@isempty,fitField));                              % returns the index of the required fields
    
    entry = {molStruct.(fields{index(end)})};
    empties = cellfun('isempty',entry);
    if choseNaN
        entry(empties)={NaN};
    else
        entry(empties)={0};
    end
    entry = cell2mat(entry);
    
    k = length(entry)/num_chemicals;
    X_reshaped = reshape(entry,k, num_chemicals);
    
    if k > 1
        % Achtung: Code funktioniert nur bis maximal 9 Vektoreintr�gen
        x = str2double(InOut.legend{i}(end));
        X_reshaped = X_reshaped(x,:);
    end
    
    X_new(i,:) = X_reshaped;
    
end

InOut.X_red = [InOut.X_red, X_new];                                         % adding the new descriptors to InOut
end
function [Y_est] = Func_NeuralNetwork(InOut,allWB,output_cat)
%FUNC_NEURALNETWORK Estimates environmental impacts for a chemical
% using an existing neural network
%
%   Input:      InOut - string containing matrices:
%               - X_red (selected descriptors for regarded impact category)
%               - X (all possible descriptors)
%               - T (target = training impacts of regarded impact category)
%               - T_all (targets for all impact categories)
%               each column represents one molecule, each row is one
%               target/ descriptor
%
%               allWB - initial weight for neural network
%
%               output_cat - index of the chosen impact category
%
%   Output:     Y_est - vector with estimated environmental impacts for the
%               regarded impact category.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load Rows of Descriptors
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
X_raw = InOut.X_red;
T = InOut.T;

hp = InOut.hyperParams;                                                     % load file where hyperparameters (e. g. output of GA) are saved

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Load Hyperparameters

hp1 = hp(1);                                                                % Number of Hidden Layers (incl. Output Layer)
hp2 = hp(2);                                                                % Number of Neurons in 1st Hidden Layer
hp3 = hp(3);                                                                % Number of Neurons in 2nd Hidden Layer
hp4 = hp(4);                                                                % Regularization Parameter lambda
hp5 = hp(5);                                                                % Number of Epochs
hp6 = hp(6);                                                                % Learning Rule (1: trainlm, 2: trainscg, 3: trainbfg)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Setup architecture of neural network

number_of_inputs = 1;                                                       % number of input layers (always 1)
number_of_neurons_in_input_layer = size(X_raw,1);                           % number of input neurons (change requires different settings in the connections below)
if hp1 == 2
    number_of_layers = 2;                                                   % number of hidden layers (includes output layer)
    number_of_neurons_in_hidden_layer{1} = hp2;                             % number of neurons in 1st hidden layer
    number_of_neurons_in_hidden_layer{2} = 0;                               % number of neurons in 2nd hidden layer
end
if hp1 == 3
    number_of_layers = 3;                                                   % number of hidden layers (includes output layer)
    number_of_neurons_in_hidden_layer{1} = hp2;                             % number of neurons in 1st hidden layer
    number_of_neurons_in_hidden_layer{2} = hp3;                             % number of neurons in 2nd hidden layer
end
number_of_neurons_in_output_layer = 1;                                      % number of neurons in output layer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Specify data representation, training parameters, etc.

norm_input = 1;                                                             % 0: no normalization, 1: normalization on interval [-1,1] (acc. to LeCun1991)
regularizationParam = hp4;                                                  % Set Regulization parameter lambda
numEpochsMax = hp5;                                                         % Set maximum number of epochs
% only trainbr, trainscg and traingdx allows personalized training
% functions
if hp6 == 1
    trainFunction = 'trainlm';                                              % Set training Function (e.g. trainlm, trainbfg, traingd, trainscg, trainbr)
elseif hp6 == 2
    trainFunction = 'trainscg';
elseif hp6 == 3
    trainFunction = 'trainbfg';
end
performFunction = 'mse';                                                    % Set the performance function (e.g. sse, mae, sae, mse, crossentropy)
L_initFcn = 'initnw';                                                       % Initialization Function for Hidden and Output Layers (e.g. initnw, rands) [!!!not necessary because fixed set of initial weights overrides weights anways!!!];
HL_transferFcn = 'tansig';                                                  % Transfer Function for Hidden Layers;
OL_transferFcn = 'purelin';                                                 % Transfer Function for Output Layer;
N_initFcn = 'initlay';                                                      % Set Initialization Function of the whole network according to the Init Fcns of the respective layer

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Get and process data

if norm_input == 0
    X = X_raw;                                                              % X contains all training examples plus the estimation example
elseif norm_input == 1
    X = normalizeMatrix(X_raw);                                             % scales each value to the interval [-1,+1]
end

% Dividing the descriptor set into descriptors for training and the
% decriptors of the chemicals to be estimated
X_train = X(:,1:size(T,2));
X_est = X(:,size(T,2)+1:end);

[T_norm, Ts] = mapminmax(T);                                                % normalizes Matrix to -1 to +1 interval. Settings saved to Ts (necessary to reverse normalization after neural network)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Establish network and create connections

net=network;                                                                % creates NN

net.numInputs = number_of_inputs;                                           % number of input layers (always 1)
net.numLayers = number_of_layers;                                           % number of hidden layers (includes output layer)

for i=1:number_of_layers
    
    net.biasConnect(i) = 1;                                                 % connect one bias to each neuron in each layer (except input layer)
    
end

net.inputConnect(1) = 1;                                                    % Layer 1 (Input Layer) is connected to the input
net.outputConnect(number_of_layers) = 1;                                    % The last layer is connected to the output

for i=2:number_of_layers
    
    net.layerConnect(i,i-1) = 1;                                            % connect each layer to the previous one
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input layer definition

net.inputs{1}.size = number_of_neurons_in_input_layer;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Hidden layer definition and initialization

for i=1:number_of_layers-1                                                  % Loop through all Hidden Layers
    
    net.layers{i}.name = [num2str(i)  '. Hidden Layer'];                    % Name for Hidden Layers
    net.layers{i}.transferFcn = HL_transferFcn;                             % Transfer Function for Hidden Layers
    net.layers{i}.initFcn = L_initFcn;                                      % Initialization Function for Hidden Layers
    net.layers{i}.size = number_of_neurons_in_hidden_layer{i};              % Number of neurons in each Hidden Layer
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Output layer definition and initialization

net.layers{number_of_layers}.name='Output Layer';                           % Name for Output Layers
net.layers{number_of_layers}.transferFcn = OL_transferFcn;                  % Transfer Function for Output Layer
net.layers{number_of_layers}.initFcn = L_initFcn;                           % Initialization Function for Output Layer
net.layers{number_of_layers}.size = number_of_neurons_in_output_layer;      % Number of neurons in Output Layer ('1' in our case)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Training Parameters
net.performFcn = performFunction;                                           % set performance function of the network (e.g. mse)
net.trainFcn = trainFunction;                                               % set training function of the network (e.g. trainlm)

if strcmp(trainFunction,'trainlm')
    net.trainParam.mu_max = 5e13;                                           % maximum mu is set (applies just for Levenberg-Marquardt alg.)
end

net.performParam.regularization = regularizationParam;                      % regularization parameter for L2-regularization (lambda * C + (1-lambda) * L2) - different comp. to literature
net.trainParam.showWindow = false;                                           % show neural network toolbox window (yes: true, no: false)
net.trainParam.epochs = numEpochsMax;                                       % maximum number of epochs for training

net.initFcn = N_initFcn;                                                    % set Initialization Function

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Start training

% Initialize network
net = init(net);                                                            % initialize neural network

net = setwb(net, allWB{output_cat});                                        % set weights and biases according to stored allWBfile (input parameter is a vector of all W&Bs

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Training

[my_net tr] = train(net,X_train,T_norm,nn7);                                % trains the network and saves the trained network to 'my_net', configuration details to 'tr'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Estimation

Y_est_norm = my_net(X_est,nn7);                                             % feeds the input set that shall be used for estimation to the above built neural network 'my_net'. Output is on normalized scale
Y_est = mapminmax('reverse', Y_est_norm, Ts);                               % 'reverse normalization' with scaling factors that were used for normalization (stored in Ts)

end
function [molStruct] = calcFullLCA (molStruct, category,output_cat,choseNaN)
% CALC FULL LCA conducts a full LCA for given molecules and a given impact
% category
%
%   Input:      molStruct - contains process data provided from short cut
%               and molecular properties frm COSMO-RS
%
%               category - string containing the abbreviation of regarded
%               impact category (e.g., 'CC' for climate change)
%
%               output_cat - number of regarded impact category
%
%               choseNaN - boolean, wether NaN values should be ignored or
%               replaced by 0 (1 - ignore, 0 - replace)
%
%   Output:     molStruct - extended by the environmental impacts
%               caused over the full life cycle for each molecule

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% load impacts for heat supply
load('ReCiPe_heat.mat');
%% get data from molStruct
MW = [molStruct.MW];
impactKg = [molStruct.(category)];
heat = impact_heat.Midpoint(output_cat);
n_Feed = 1; % [kmol/s]
x_Feed_water = 0.95;

for i = 1:length(molStruct)
    if ~isempty(molStruct(i).Qreb)
        Q_min(i) = molStruct(i).Qreb*1e-6;
        x_solv(i) = molStruct(i).x_LLE_aq_solvent;
        x_water(i) = molStruct(i).x_LLE_aq_water;
    elseif choseNaN
        Q_min(i) = NaN;
        x_solv(i) = NaN;
        x_water(i) = NaN;
    else
        Q_min(i) = 0;
        x_solv(i) = 0;
        x_water(i) = 0;
    end
end

%% calculating LCA
% impact of solvent
n_solv = x_Feed_water*n_Feed*x_solv./x_water; % (kmol/s)
m_solv = n_solv.*MW; %[kg/s]
impact_solvent = impactKg.*m_solv; % [impact/s]

% impact of process
impact_process = Q_min*heat*n_Feed; % [MJ/kmol]*[impact/MJ]*[kmol/s] = [impact/s]

% overall impact = sum of partial impacts
impact_total = impact_solvent + impact_process;

for i=1:length(molStruct)
    molStruct(i).LCA.(category) = [impact_solvent(i),impact_process(i),impact_total(i)];
end
end
function varargout = subdir(varargin)
%SUBDIR Performs a recursive file search
%
% subdir
% subdir(name)
% files = subdir(...)
%
% This function performs a recursive file search.  The input and output
% format is identical to the dir function.
%
% Input variables:
%
%   name:   pathname or filename for search, can be absolute or relative
%           and wildcards (*) are allowed.  If ommitted, the files in the
%           current working directory and its child folders are returned
%
% Output variables:
%
%   files:  m x 1 structure with the following fields:
%           name:   full filename
%           date:   modification date timestamp
%           bytes:  number of bytes allocated to the file
%           isdir:  1 if name is a directory; 0 if no
%
% Example:
%
%   >> a = subdir(fullfile(matlabroot, 'toolbox', 'matlab', '*.mat'))
%
%   a =
%
%   67x1 struct array with fields:
%       name
%       date
%       bytes
%       isdir
%
%   >> a(2)
%
%   ans =
%
%        name: '/Applications/MATLAB73/toolbox/matlab/audiovideo/chirp.mat'
%        date: '14-Mar-2004 07:31:48'
%       bytes: 25276
%       isdir: 0
%
% See also:
%
%   dir

% Copyright 2006 Kelly Kearney


%---------------------------
% Get folder and filter
%---------------------------

narginchk(0,1);
nargoutchk(0,1);

if nargin == 0
    folder = pwd;
    filter = '*';
else
    [folder, name, ext] = fileparts(varargin{1});
    if isempty(folder)
        folder = pwd;
    end
    if isempty(ext)
        if isdir(fullfile(folder, name))
            folder = fullfile(folder, name);
            filter = '*';
        else
            filter = [name ext];
        end
    else
        filter = [name ext];
    end
    if ~isdir(folder)
        error('Folder (%s) not found', folder);
    end
end

%---------------------------
% Search all folders
%---------------------------

pathstr = genpath_local(folder);
pathfolders = regexp(pathstr, pathsep, 'split');  % Same as strsplit without the error checking
pathfolders = pathfolders(~cellfun('isempty', pathfolders));  % Remove any empty cells

Files = [];
pathandfilt = fullfile(pathfolders, filter);
for ifolder = 1:length(pathandfilt)
    NewFiles = dir(pathandfilt{ifolder});
    if ~isempty(NewFiles)
        fullnames = cellfun(@(a) fullfile(pathfolders{ifolder}, a), {NewFiles.name}, 'UniformOutput', false);
        [NewFiles.name] = deal(fullnames{:});
        Files = [Files; NewFiles];
    end
end

%---------------------------
% Prune . and ..
%---------------------------

if ~isempty(Files)
    [~, ~, tail] = cellfun(@fileparts, {Files(:).name}, 'UniformOutput', false);
    dottest = cellfun(@(x) isempty(regexp(x, '\.+(\w+$)', 'once')), tail);
    Files(dottest & [Files(:).isdir]) = [];
end

%---------------------------
% Output
%---------------------------

if nargout == 0
    if ~isempty(Files)
        fprintf('\n');
        fprintf('%s\n', Files.name);
        fprintf('\n');
    end
elseif nargout == 1
    varargout{1} = Files;
end
end
function [p] = genpath_local(d)
% Modified genpath that doesn't ignore:
%     - Folders named 'private'
%     - MATLAB class folders (folder name starts with '@')
%     - MATLAB package folders (folder name starts with '+')

files = dir(d);
if isempty(files)
    return
end
p = '';  % Initialize output

% Add d to the path even if it is empty.
p = [p d pathsep];

% Set logical vector for subdirectory entries in d
isdir = logical(cat(1,files.isdir));
dirs = files(isdir);  % Select only directory entries from the current listing

for i=1:length(dirs)
    dirname = dirs(i).name;
    if    ~strcmp( dirname,'.') && ~strcmp( dirname,'..')
        p = [p genpath(fullfile(d,dirname))];  % Recursive calling of this function.
    end
end
end
function NAMES = fieldnamesr(S,varargin)
%FIELDNAMESR Get structure field names in recursive manner.
%
%   NAMES = FIELDNAMESR(S) returns a cell array of strings containing the
%   structure field names associated with s, the structure field names
%   of any structures which are fields of s, any structures which are
%   fields of fields of s, and so on.
%
%   NAMES = FIELDNAMESR(S,DEPTH) is an optional field which allows the depth
%   of the search to be defined. Default is -1, which does not limit the
%   search by depth. A depth of 1 will return only the field names of s
%   (behaving like FIELDNAMES). A depth of 2 will return those field
%   names, as well as the field names of any structure which is a field of
%   s.
%
%   NAMES = FIELDNAMESR(...,'full') returns the names of fields which are
%   structures, as well as the contents of those structures, as separate
%   cells. This is unlike the default where a structure-tree is returned.
%
%   NAMES = FIELDNAMESR(...,'prefix') returns the names of the fields
%   prefixed by the name of the input structure.
%
%   NAMES = FIELDNAMESR(...,'struct') returns only the names of fields
%   which are structures.
%
%   See also FIELDNAMES, ISFIELD, GETFIELD, SETFIELD, ORDERFIELDS, RMFIELD.

%   Developed in MATLAB 7.13.0.594 (2011b).
%   By AATJ (adam.tudorjones@pharm.ox.ac.uk). 2011-10-11. Released under
%   the BSD license.

%Set optional arguments to default settings, if they are not set by the
%caller.
if size(varargin,1) == 0
    optargs = {-1} ;
    optargs(1:length(varargin)) = varargin ;
    depth = optargs{:} ;
    
    full = [0] ; [prefix] = [0] ; [struct] = [0] ;
else
    if any(strcmp(varargin,'full') == 1)
        full = 1 ;
    else
        full = 0 ;
    end
    
    if any(strcmp(varargin,'prefix') == 1)
        prefix = 1 ;
    else
        prefix = 0 ;
    end
    
    if any(strcmp(varargin,'struct') == 1)
        struct = 1 ;
    else
        struct = 0 ;
    end
    
    if any(cellfun(@isnumeric,varargin) == 1)
        depth = varargin{find(cellfun(@isnumeric,varargin))} ;
    else
        depth = -1 ;
    end
end


%Return fieldnames of input structure, prefix these with "S.".
NAMES = cellfun(@(x) strcat('S.',x),fieldnames(S),'UniformOutput',false) ;

%Set state counters to initial values (k terminates recursive loop, g makes
%recursive loop behave in a different way.
k = 1 ; g = 0 ;

fndstruct = {} ;

%k is ~0 while all fields have not been searched to exhaustion or to
%specified depth.
while k ~= 0
    fndtemp = {} ;
    
    k = length(NAMES) ;
    
    %g set to 1 prevents fieldnames from being added to output NAMES.
    %Useful when determining whether fields at the lowest specified depth
    %are structures, without adding their child fieldnames (i.e. at
    %specified depth + 1) to NAMES output.
    if depth == 1
        g = 1 ;
    end
    
    for i = 1:length(NAMES)
        %If the current fieldname is a structure, find its child
        %fieldnames, add to NAMES if not at specified depth (g = 0). Add to
        %fndstruct (list of structures).
        if isstruct(eval(NAMES{i})) == 1
            if g ~= 1
                fndtemp2 = fieldnames(eval(NAMES{i})) ;
                fndtemp2 = cellfun(@(x) strcat(sprintf('%s.'            ...
                    ,NAMES{i}),x),fndtemp2,'UniformOutput',false) ;
                fndtemp = cat(1,fndtemp,fndtemp2) ;
            elseif g == 1
                fndtemp = cat(1,fndtemp,NAMES{i}) ;
                k = k - 1 ;
            end
            fndstruct = cat(1,fndstruct,NAMES{i}) ;
        else
            fndtemp = cat(1,fndtemp,NAMES{i}) ;
            k = k - 1 ;
        end
    end
    
    NAMES = fndtemp ;
    
    %If we have reached depth, stop recording children fieldnames to NAMES
    %output, but determine whether fields at final depth are structures or
    %not (g). After this, terminate loop by setting k to 0.
    if depth ~= -1                                                      ...
            && any(cellfun(@(x) size(find(x == '.'),2),NAMES) == depth)
        g = 1 ;
    elseif depth ~= -1                                                  ...
            && any(cellfun(@(x) size(find(x == '.'),2),NAMES) > depth)
        k = 0 ;
    end
end

%Return names of fields which are structures, as well as fields which are
%not structures, if 'full' optional argument is set.
if full == 1
    for i = 1:length(fndstruct)
        
        %If depth is specified, add structure names to appropriate depth to
        %output NAMES.
        if depth == -1 || size(find(fndstruct{i} == '.'),2) <= depth-1
            structi = find(cellfun(@(x) isempty(x) == 0                 ...
                ,strfind(NAMES,fndstruct{i})),1) ;
            
            %If the current structure name is related to the first field of
            %NAMES, add structure name to NAMES in a particular way, else
            %add it in another way.
            if structi > 1
                NAMES = cat(1,NAMES(1:(structi-1)),fndstruct{i}         ...
                    ,NAMES(structi:end)) ;
            elseif isempty(structi)
                error ('AATJ:fieldnamesr,NotFindStructure'              ...
                    ,'Could not find structure name in NAMES') ;
            else
                NAMES = cat(1,fndstruct{i},NAMES) ;
            end
        end
    end
end

%Return only fields which are structures, if optional argument is defined.
%If 'full' is not set, do not include both parent and child structures.
if struct == 1
    if full == 0
        fndstruct2 = {} ;
        for i = 1:length(fndstruct)
            if isempty(cell2mat(strfind(fndstruct,strcat(fndstruct{i},'.')))) == 1
                fndstruct2(end+1,1) = fndstruct(i) ;
            end
        end
        fndstruct = fndstruct2 ;
    end
    NAMES = fndstruct ;
end

%Prefix input structure name on all fields of output cell array NAME if
%user set this option.
if prefix == 1
    structname = inputname(1) ;
    NAMES = cellfun(@(x) strcat(sprintf('%s.',structname),x(3:end))     ...
        ,NAMES,'UniformOutput',false) ;
else
    NAMES = cellfun(@(x) sprintf('%s',x(3:end)),NAMES,'UniformOutput'   ...
        ,false) ;
end
end
function output = normalizeMatrix(X)
%normalizes a matrix rowwise(!) so that all values are between -1 and 1.
X_length = size(X,2);

mx = (max(X, [], 2 ));
mx_Matrix = repelem( mx, 1, X_length);
mi = (min(X, [], 2 ));
mi_Matrix = repelem( mi, 1, X_length);

div = mx_Matrix - mi_Matrix;
output = 2*((X - mi_Matrix) ./ div) - 1 ;            %normalize between -1 and 1
%output = (X - mi_Matrix) ./ div;                   %normalize between  0 and 1

%until here, there is a problem if one row in div is 0
%set all NaN to 0
idx = isnan(output);
output (idx) = 0;

end