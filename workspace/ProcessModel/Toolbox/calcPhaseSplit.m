function [xI, xII] =  calcPhaseSplit(AlphaParam, TauParam, T, z, x0)
% Calculate a phase split between using the NRTL model

% Set tolerances
beta = 0.5;
n_comp = length(z);
nitermax = 10000;
TOL_mu = 1e-9;
TOL_beta = 1e-9;
TOL_gbeta = 1e-9;

% Get NRTL Parameter
[alpha, tau] = getAlphaTau(AlphaParam, TauParam, T);

%% Initialization
%%
x=x0;

% Mole balance for Phase 2
for i=1:(n_comp-1)
    y(i) = (z(i)-(1-beta)*x(i))/beta;
end

y(n_comp) = 1-sum( y(1:(n_comp-1)) );


gamma_x = (getGamma(x, n_comp, alpha, tau));
gamma_y = (getGamma(y, n_comp, alpha, tau));
K = gamma_x./gamma_y;
delta_mu = abs(gamma_y.*y - gamma_x.*x);



%% RR-Algorithm
%%
nit=0;
while((nit < nitermax)  && ( max(delta_mu) > TOL_mu )  )
    nit = nit + 1;


    [beta,x,y] = RRSOLVER(n_comp, z, K, nitermax, TOL_beta, TOL_gbeta);
    if beta < TOL_beta
        x=z;
        y=z;
%             disp('single phase1')
        break
    elseif beta >= (1-TOL_beta)
        x=z;
        y=z;
%             disp('single phase2')
        break
    end

    gamma_x = (getGamma(x, n_comp,alpha, tau));
    gamma_y = (getGamma(y, n_comp,alpha, tau));
    K = gamma_x./gamma_y;
    delta_mu = abs(gamma_y.*y - gamma_x.*x);
    
end

%% Results

xI = x(1,:);
xII = y(1,:);

end
