function Ternary_Diagram(molStruct, T, specs, varargin)

addpath([pwd,'/TernaryFromCompounds/DisplayTernary/Ternary_Diagram/']);
addpath([pwd,'/TernaryFromCompounds/DisplayTernary/MiscFunc/']);
    
   
for j = 1:length(T)
    main_ternPlot(molStruct,T(j),specs,varargin)
end

end

