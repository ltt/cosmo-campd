function [alpha, tau] = getNRTLPara(molStruc,T)
tau = zeros(3);
alpha = zeros(3);
alphaPara = molStruc.AlphaParam;
tauPara = molStruc.TauParam;

    for i =1:3
        for j =1:3
            if(i~=j)
                alpha(i,j) =alphaPara{i,j}(1)+alphaPara{i,j}(2)*T;
                tau(i,j) = tauPara{i,j}(1) + tauPara{i,j}(2)/T + tauPara{i,j}(3) *log(T) +tauPara{i,j}(4) * T;
            end
        end
    end
%     
    
    %Change Position
    
    temp1 = zeros(3);
    temp2 = zeros(3);
    
    temp1(1,2)= alpha(2,1);
    temp1(1,3)= alpha(2,3);
    temp1(2,1)= alpha(1,2);
    temp1(2,3)= alpha(1,3);
    temp1(3,1)= alpha(3,2);
    temp1(3,2)= alpha(3,1);
    
    temp2(1,2)= tau(2,1);
    temp2(1,3)= tau(2,3);
    temp2(2,1)= tau(1,2);
    temp2(2,3)= tau(1,3);
    temp2(3,1)= tau(3,2);
    temp2(3,2)= tau(3,1);
    
    alpha = temp1;
    tau = temp2;
%     
    
    
end