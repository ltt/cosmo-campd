function main_ternPlot(molStruct,T,specs,vara)   

%% Systems, parameters and experimental data

comp1 = specs.compounds{1, 2};
comp2 = specs.compounds{1, 1};
comp3 = molStruct.name; 

[alpha, tau] = getNRTLPara(molStruct,T);


%% Parameters
%%
beta = 0.5;
R=8.314; %kJ/kmol/K

n_comp = 3;
n_par = size(6,2);
nitermax = 10000;
TOL_mu = 1e-9;
TOL_beta = 1e-9;
TOL_gbeta = 1e-9;
beta0 = beta;

% Set Schrittweite
epsilon = 0.002; % Schrittweite
if exist('vara')
    for i = 1:(length(vara)/2)   
        if strcmp(vara(2*i-1),'epsilon')
            epsilon = vara{2*i};
        end
    end
end

ALPHA_max = 1;

zF = find_zF(molStruct,T);
x0 = [1, 0 , 0];      % Startwert f�r die Zusammensetzung einer Phase %#JSc: For LLE in Water systems -> Use Water phase = 0.99 pure as intial guess
    
%% LLE Calculation
%%
    % #JSc: Calculate phase split for feed
    [ x_calc(1,:), y_calc(1,:), beta_calc(1,:) ] = LLE_NRTL_calc( zF(1,:), x0, n_comp, tau, alpha, nitermax, TOL_mu, TOL_beta, TOL_gbeta, R, T, beta0 );  
    zF(1,:) = beta*y_calc(1,:)+(1-beta)*x_calc(1,:); % In NRTL: RR-Algorithm determines phase split based on NRTL-gamma values

    % #JSc: Update feed concentration with epsilon
    for i = 1:2000
        abort = 0;
        k = (x_calc(i,1:2)-y_calc(i,1:2))./(norm(x_calc(i,1:2)-y_calc(i,1:2))); % update step for concentration based on epsilon. ASK: How is update determined?
        if isnan(k)
            break % If no phase split occurs anymore: break!
        end
        zF_corr = [ -k(2) k(1)] * epsilon;
        zF_neu = zF(i,1:2) + zF_corr;
        zF(i+1,:) = [zF_neu 1- zF_neu(1)-zF_neu(2)];

        if(sum(zF(i+1,:) < 0) == 1)
            zF(i+1,:) = (zF(i+1,:) >= 0) .* zF(i+1,:);
            zF(i+1,:) = zF(i+1,:) / sum(zF(i+1,:));
            abort = 1;
        end

        [ x_calc(i+1,:), y_calc(i+1,:), beta_calc(i+1,:) ] = LLE_NRTL_calc( zF(i+1,:), x_calc(i,:), n_comp, tau, alpha, nitermax, TOL_mu, TOL_beta, TOL_gbeta, R, T, beta0 );

        zF(i+1,:) = beta*y_calc(i+1,:)+(1-beta)*x_calc(i+1,:);

        if abort 
            break;
        end
    end
    % Depending on how many phase splits were possible: Use last
    % connode to plot last phase-split
    lastconode = round(ALPHA_max*size(zF,1));
    zF=zF(1:lastconode,:);
    x_calc=x_calc(1:lastconode,:);
    y_calc=y_calc(1:lastconode,:);



ALPHA=linspace(0,ALPHA_max,size(zF,1));



%% Plot LLE
%%

figure 
% calculate plotting vectors
gesamt_calc = [x_calc;y_calc];
for meas = 1:size(x_calc,1)
    gesamt_neu_calc(:,:,meas) = [x_calc(meas,:); y_calc(meas,:)];                 
end


% plot calculated values
g1=ternplot(x_calc(:,1), x_calc(:,2), x_calc(:,3), 'g','LineWidth',2); % LLE are = connection of single LLE points
hold all
ternplot(y_calc(:,1), y_calc(:,2), y_calc(:,3), 'r','LineWidth',2)

% plot overall composition
   % g2=ternplot(zF(:,1), zF(:,2), zF(:,3), 'k');

tieLine = 1;  
tieLineNum = 10;
if exist('vara')
    for i = 1:(length(vara)/2)   
        if strcmp(vara(2*i-1),'tieLine')
            if isnumeric(vara{2*i})
                tieLineNum =   vara{2*i};
            end
        end
    end
end
if tieLine
% plot tie-lines of designed experiments
    for meas = [1:round(size(x_calc,1)/tieLineNum):size(x_calc,1)-1,size(x_calc,1)]
        g3=ternplot(gesamt_neu_calc(:,1,meas),gesamt_neu_calc(:,2,meas),gesamt_neu_calc(:,3,meas),'b');
        ternplot(x_calc(meas,1), x_calc(meas,2), x_calc(meas,3), 'bo')
        ternplot(y_calc(meas,1), y_calc(meas,2), y_calc(meas,3), 'bo')
    end
end
% legend

title_text = ['T = ' ,num2str(T),' K'];

lable_text{1,1} = comp1;
lable_text{2,1} = comp2;
lable_text{3,1} = comp3;

no_legend =0;
no_lable =0;

    if exist('vara')
        for i = 1:(length(vara)/2)   
            if strcmp(vara(2*i-1),'title')
                if strcmp(vara(2*i),'off')
                    title_text = []; 
                else
                    title_text = vara(2*i);
                end
            elseif strcmp(vara(2*i-1),'lable')
                if strcmp(vara(2*i),'off')        
                    no_lable=1;
                else
                    if ~isempty(vara{2*i}{1})
                        lable_text{1,1} = vara{2*i}(1);
                    end
                    if ~isempty(vara{2*i}{2})
                        lable_text{2,1} = vara{2*i}(2);
                    end
                    if ~isempty(vara{2*i}{3})
                        lable_text{3,1} = vara{2*i}(3);
                    end
                end
            elseif strcmp(vara(2*i-1),'legend')
                if strcmp(vara(2*i),'off')
                    no_legend = 1;
                end
            end 
        end
    end

    if ~ no_lable
        ternlabel(lable_text);
    end

    if ~ no_legend
        if tieLine
            legend([g1, g3],'Calculated Binodale','Calculated Conode','Location','NorthEast');
        else
            legend([g1],'Calculated Binodale','Location','NorthEast');
        end
    end

        title(title_text)

end
  
