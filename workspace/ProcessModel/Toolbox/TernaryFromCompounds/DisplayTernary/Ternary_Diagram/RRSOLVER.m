function [beta, x, y] = RRSOLVER(nc, z, K, ni, TOL_beta, TOL_gbeta)
% Attention:    beta=n''/n_tot
%               K(i) = x(i)''/x(i)' = gamma(i)'/gamma(i)''
%               betafunction: sum(i, x(i)''-x(i)') = sum(i, z(i)*(K(i)-1) /
%               (1-beta+beta*K(i)))            
%               

    % nc: Number of components
    % z : feed mole fractions
    % K : k-factors
    
    % berechnung aufgrund von Rachford-Rice

    beta = 0.5; %Startwert für beta, beta kann nur zw. 0 (nichts verdampft) und 1 (alles verdampft) liegen
    beta_max = 1;
    beta_min = 0;
%     beta_max = 1/(1-min(K));
%     beta_min = -1/(max(K)-1);

    delta_beta = 1;
    delta_g = 1;

    zaehler = 0;
    
    for zaehler = 0:ni
        zaehler = zaehler +1;
        g = 0;
        g_alt = g; %daraus wird gdelta berechnet und mit Toleranz für Iterationsabbruch verglichen
        for i = 1:nc
            g = g + z(i).*(K(i)-1)./(1-beta+beta.*K(i)); 
        end     
%         for i=1:nc
%             g = g + z(i)./(beta+K(i)*(1-beta));
%         end
%         g = g-1;
%------------        
        
        

        if g < 0
            beta_max = beta;
        else beta_min = beta;
        end

%         if beta_max > 1
%            beta_max = 1; 
%         end

        g_strich = 0;

        for i = 1:nc
            g_strich = g_strich - z(i).*(K(i)-1)^2./(beta.*K(i)-beta+1)^2;
        end
%         for i=1:nc
%             g_strich = g_strich - z(i)*(1-K(i))./((beta+K(i)*(1-beta)).^2);
%         end
%------------  

        beta_neu = beta - g/g_strich;

        
% normal RR %%%%%%%%%%%%%%%%%%%        
        if beta_neu >= beta_min && beta_neu <= beta_max && ni > 1
           delta_beta = abs(beta-beta_neu);
           beta = beta_neu;           
        else beta_neu = (beta_min + beta_max)/2;
             beta = beta_neu;
        end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% SLOPPY RR for ni = 1 %%%%%%%%%%%                                     
%         if beta_neu >= beta_min && beta <= beta_max && ni == 1  
%             l = (1-beta)*z./(1-beta+beta*K);          
%             v = (beta*K.*z)./(1-beta+beta*K);         
%             beta_neu = sum(v);                        
%             delta_beta = abs(beta-beta_neu);          
%             beta = beta_neu;                          
%         else beta_neu = (beta_min + beta_max)/2;      
%              delta_beta = abs(beta-beta_neu);         
%              beta = beta_neu;                         
%         end                                           
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%      

        delta_g = abs(g_alt - g);

        
        if delta_beta <= TOL_beta && delta_g <= TOL_gbeta
            break
        end
    end
    
    if ni > 1
        for i=1:nc
            l(i) = (1-beta)*z(i)./(1-beta+beta*K(i));  
            v(i) = (beta*K(i).*z(i))./(1-beta+beta*K(i));
            x(i) = l(i)/(1-beta);
            y(i) = v(i)/beta;   
        end
        
%         for i=1:nc-1
%             l(i) = (1-beta)*z(i)./(1-beta+beta*K(i));  
%             v(i) = (beta*K(i).*z(i))./(1-beta+beta*K(i));
%             x(i) = l(i)/(1-beta);
%             y(i) = v(i)/beta;   
%         end
%         x(nc)=1-sum( x(1:(nc-1)) );
%         y(nc)=1-sum( y(1:(nc-1)) );
    end
end







