function [gamma]=NRTL(x, n_comp, alpha, tau,~,~)
%NRTL activity coefficient calculation

%%

% G
    G=eye(n_comp,n_comp);
    for i=1:n_comp
        for j=1:n_comp
            if i~=j
                G(i,j) = exp(-alpha(i,j)*tau(i,j));
            end
        end
    end

%%

%B(i)
    for i=1:n_comp
        B(i)=0;
        for j=1:n_comp
            B(i) = B(i) + tau(j,i)*G(j,i)*x(j);
        end
    end

%A(i)
    for i=1:n_comp
        A(i)=0;
        for l=1:n_comp
            A(i) = A(i) + G(l,i)*x(l);
        end
    end


%% gamma(i)
%%
    for i=1:n_comp
        summe_lngamma(i)=0;
        for j=1:n_comp
            summe_lngamma(i) = summe_lngamma(i) + x(j)*G(i,j)/ A(j)*(tau(i,j)-B(j)/A(j));
        end
    end

    for i=1:n_comp
        lngamma(i) = B(i)/A(i)+summe_lngamma(i);
        gamma(i) = exp(lngamma(i));
    end

end

