function [ psat ] = vapour_pressure( AntoineParam, T )
%% Psat calculation from Antoine Parameters

n = length(AntoineParam);

psat = zeros(n,1);

for i = 1 : n
    psat(i) = 1e-3 * exp(AntoineParam{i}(1) - AntoineParam{i}(2)/(T + AntoineParam{i}(3)));
end

end