function [ T_dew ] = dew_point( properties, x_in, p )
% Calculate dew of mixture / component
%   Inputs: AntoineParam, Alpha- and TauParam
%           composition x_in and pressure in bar
%   Output: T_boil in Kelvin

%% Solve bubble point function

T_dew = fzero(@(T) dew_point_function(T, x_in, p, properties),500,optimset('Display', 'off')); % boiling point

end

function F = dew_point_function(T, x, p, properties)
%% Properties
AlphaParam = properties.AlphaParam;
TauParam = properties.TauParam;
AntoineParam = properties.AntoineParam;

N = length(AntoineParam);

%% Get activity coefficients
gamma = getGamma(x, N, AlphaParam, TauParam, T);

%% Get difference between pressure from temperature guess and specification

% Error is equal to specified pressure in mbar
F = -1/ p /1000;

for i=1:N 
   F = F + (x(i) / gamma(i) / exp(AntoineParam{i,1}(1) - AntoineParam{i,1}(2) /( T+AntoineParam{i,1}(3))));  
end

end