function [alpha, tau] = getAlphaTau(alphaParam, tauParam, T)
    %% Implemented by A. Demuth, bachelor thesis supervised by Jan Scheffczyk @LTT, RWTH Aachen University.	
    %% Calculates NRTL parameters (tau, alpha) for a given temperature T and given parametrizations (see below)
    % INPUT:    TauParam and AlphaParam are cell arrays with dimensions
    %           nxn. Each entry contains a [1x4] (TauParam) or [1x2]
    %           (AlphaParam) double, so that the parametrizations are given
    %           by: % tau_ij(T) = a_ij + b_ij / T + c_ij * ln(T) + dij * T
    %           and % alpha_ij(T) = alpha_ij,0 + alpha_ij,1 * T.
    %-------------------------------------------------------------------
    % Name      | Description           | Size                  | Unit | 
    % T         | temperature in Kelvin | [1x1]                 | [K]  |
    % tauParam  | alpha param. matrix   | [nxn] w. [1x4] each   | [-]  |
    % alphaParam| tau param. matrix     | [nxn] w. [1x4] each   | [-]  |    
    %-------------------------------------------------------------------
    %
    % OUTPUT:   Tau and alpha are matrices with dimensions nxn. 
    %------------------------------------------------
    % Name  | Description           | Size  | Unit  | 
    % alpha | alpha matrix          | [nxn] | [-]   |
    % tau   | tau matrix            | [nxn] | [-]   |
    %------------------------------------------------

    n = length(tauParam) ;

    % NRTL:
    alpha = ones(n);
    tau = zeros(n);
    for i = 1:n
        for j = 1:n
            tau(i,j) = tauParam{i,j}(1)	+ tauParam{i,j}(2)/T + tauParam{i,j}(3)*log(T) + tauParam{i,j}(4)*T;
            alpha(i,j) = alphaParam{i,j}(1) + alphaParam{i,j}(2)*T;
        end
    end
end