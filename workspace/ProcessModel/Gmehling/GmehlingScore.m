function [ molStruct ] = GmehlingScore( molStruct, specs)
%% Evaluate and Score the molStruct molecules according to Gmehling Scoring Function

if strcmp(specs.Mode, 'screening')
    display (['Score being calculated: Gmehling-Score']);
end

for k=1:length(molStruct)

%% Gmehling:
    x_LLE = molStruct(k).x_LLE;
    if ~isnan(x_LLE)
        lngammaI  = molStruct(k).lngammaI;
        lngammaII = molStruct(k).lngammaII;
        
            if x_LLE(1) > x_LLE(3)
            K1 = exp(lngammaI(1)-lngammaII(1));
            K2 = exp(lngammaI(2)-lngammaII(2));
            molStruct(k).Score_wsl = (1-x_LLE(2))^3;
            else
            K1 = exp(lngammaII(1)-lngammaI(1));
            K2 = exp(lngammaII(2)-lngammaI(2));
            molStruct(k).Score_wsl = (1-x_LLE(4))^3;
            end
        K12 = K1/K2;
        molStruct(k).Score_ws  = K12;
        molStruct(k).Score_wc  = K1^1.5;
        molStruct(k).GmehlingScore = log10(molStruct(k).Score_ws*molStruct(k).Score_wc*molStruct(k).Score_wsl);
    else
        molStruct(k).Score_wsl = NaN;
        molStruct(k).Score_ws  = NaN;
        molStruct(k).Score_wc  = NaN;
        molStruct(k).GmehlingScore = 0;
    end

end

end

