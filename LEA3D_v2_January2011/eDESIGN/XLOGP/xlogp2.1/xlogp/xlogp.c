# include "xlogp.h"

int main(int argc, char *argv[])
{
	FILE *fin,*fout;
	char filename[160],buf[160],head[80];
	Molecule mol;
	int mark,mode,num;

	if(argc==2) 
		{	
		 mode=0;
		 strcpy(filename,argv[1]);
		}
	else if(argc==3) 
		{
		 if(argv[1][0]!='-') Flag_Error();
		 else if(argv[1][1]=='s') mode=0;
		 else if(argv[1][1]=='m') mode=1;
		 else Flag_Error();
		 strcpy(filename,argv[2]);
		}
	else
		{
		 puts("\nYou have probably misused this program.");
		 puts("The correct synopsis is: xlogp [-s/-m] input_file");
		 puts("Please try again.\n");
		 exit(1);
		}

	if((fout=fopen("xlogp.mol2","w"))==NULL) 
		Openning_File_Error("xlogp.mol2");
        fclose(fout);

        if((fout=fopen("xlogp.log","w"))==NULL)
                Openning_File_Error("xlogp.log");
        fclose(fout);

	if(mode==0)	// calculation for a single molecule
		{
	 	 mol.Get_Coefficients();
	  	 mol.Read_From_Mol2(filename);
	 	 mol.First_Check();
	 	 mol.Atom_Typing();
	 	 mol.Factor_Detecting();
	 	 mol.Calculate_LogP();
	 	 mol.Write_Out_Mol2("xlogp.mol2");
	 	 mol.Write_Out_Log("xlogp.log");
	 	 printf("LogP = %6.2f   %s\n", mol.logp, filename);
		}
	else		// calculation for multiple molecules
		{
	 	 if((fin=fopen(filename,"r"))==NULL) 
			Openning_File_Error(filename);
		 else {mol.Get_Coefficients(); num=0;}

	 	 while(1)
			{
			 mol.Clean_Members(); mark=FALSE;

         		 do
                	{
                 	 if(fgets(buf,160,fin)==NULL) {mark=TRUE; break;}
                 	 else sscanf(buf,"%s",head);
                	}
         		 while(strcmp(head,"@<TRIPOS>MOLECULE"));

	 	 	 if(mark==TRUE) break;
	 	 	 else if(mol.Extract_From_Mol2(fin)==FALSE) 
				 Mol2_Format_Error(filename);
	 	 	 else
				{
		 	 	 mol.First_Check();
		  	  	 mol.Atom_Typing();
		 	 	 mol.Factor_Detecting();
		 	 	 mol.Calculate_LogP();
				 mol.Write_Out_Mol2("xlogp.mol2");
				 mol.Write_Out_Log("xlogp.log");
		 	 	 num++;
		 	 	 printf("No.%-5d  LogP = %6.2f   %s\n", 
					num, mol.logp, mol.name);
			 	}
		   	}

	 	 fclose(fin);
		}

	return 0;
}

void Memory_Allocation_Error()
{
        printf("\n");
        printf(":-( Memory allocation error!\n");
        printf("Program has to stop.\n");
        printf("Maybe there is not enough memory to run the program\n\n");
        exit(1);
}

void Openning_File_Error(char *filename)
{
        printf("\n");
        printf(":-( Cannot open the file %s\n", filename);
        printf("Please check it again.\n\n");
        exit(1);
}

void Mol2_Format_Error(char *filename)
{
        printf("\n");
	printf("There is something missing in file %s.\n", filename);
        printf("This file may not be in proper Mol2 format.\n");
        printf("Please check it again.\n\n");
        exit(1);
}

char *Get_Time()
{
        struct tm *ptr;
        time_t lt;

        lt=time(NULL);
        ptr=localtime(&lt);

        return asctime(ptr);
}

void Flag_Error()
{
	puts("\nYou have probably used a wrong flag.");
	puts("Please choose '-s' or '-m'.\n");
	exit(1);
}

