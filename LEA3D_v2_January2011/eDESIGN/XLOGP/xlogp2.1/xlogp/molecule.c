# include "xlogp.h"

Atom::Atom()
{
	int i;

	id=0;
	valid=0;
	num_neib=0;
	for(i=0;i<6;i++) neib[i]=0;
	strcpy(name,"Un");
	strcpy(type,"Un");
	strcpy(xtype,"Un");
	coor[0]=coor[1]=coor[2]=0.000;
	weight=0;
	logp=0.000;
	strcpy(hb,"");
	ring=0;
}

Atom::~Atom()
{
	// deconstructor
}

Bond::Bond()
{
	int i;

	id=0;
	valid=0;
	atom_1=atom_2=0;
	strcpy(type,"");
	ring=0;
	num_neib=0;
	for(i=0;i<10;i++) neib[i]=0;
}

Bond::~Bond()
{
	// deconstructor
}

Group::Group()
{
        valid=0;

        num_neib=0;
        num_nonh=0;
        num_h=0;
        num_hetero=0;
	num_pi=0;
	num_nar=0;
	num_car=0;
	db_type=0;
}

Group::~Group()
{
        // deconstructor
}

Molecule::Molecule()
{
        valid=0;
        strcpy(name,"");
        weight=0;
        strcpy(formula,"");
        logp=0.000;

        num_atom=0; num_bond=0;
	atom=NULL; bond=NULL;

        int i;

        for(i=0;i<10;i++) {factor[i].num=0; strcpy(factor[i].symbol,"");}

        for(i=0;i<90;i++) ca[i]=0.000;
        for(i=0;i<10;i++) cb[i]=0.000;
}

Molecule::~Molecule()
{
	delete [] atom; 
	delete [] bond;
}

void Molecule::Clean_Members()
{
	valid=0;
        strcpy(name,"");
        weight=0;
        strcpy(formula,"");
        logp=0.000;

        num_atom=0; num_bond=0;

	delete [] atom; atom=NULL;
        delete [] bond; bond=NULL;

	int i; for(i=0;i<10;i++) factor[i].num=0; 

	return;
}

void Molecule::Show_Molecule() const
{
	int i;

	printf("Molecule %s\n", name);
	printf("\tFormula = %s\n",formula);
	printf("\tWeight = %d\n",weight);
	printf("\tLogP = %5.2f\n",logp);

	printf("Number of atoms = %d\n",num_atom);

        for(i=0;i<num_atom;i++)
                {
                 printf("Atom: ");
                 printf("%3d ",   atom[i].id);
		 printf("%2d ",   atom[i].valid);
                 printf("%6s ",   atom[i].type);
		 printf("%20s ",  atom[i].xtype);
                 printf("%2s ",   atom[i].hb);
		 printf("<%1d> ", atom[i].ring);
                 printf("%6.3f ", atom[i].logp);
                 printf("\n");
                }

        printf("Number of bonds = %d\n", num_bond);

        for(i=0;i<num_bond;i++)
                {
                 printf("Bond: "); 
		 printf("%3d ", bond[i].id);
		 printf("%1d ", bond[i].valid);
		 printf("%3d ", bond[i].atom_1);
		 printf("%3d ", bond[i].atom_2);
		 printf("%2s ", bond[i].type); 
		 printf("\n");
		}

	return;
}

void Molecule::Read_From_Mol2(char *filename)
{
	FILE *fp;
	int i,tmp;
	char buf[160],head[40];

	if((fp=fopen(filename,"r"))==NULL) Openning_File_Error(filename);

        do 
		{
                 if(fgets(buf,160,fp)!=NULL) sscanf(buf,"%s",head);
                 else Mol2_Format_Error(filename);
                }
        while(strcmp(head,"@<TRIPOS>MOLECULE"));

       	fgets(buf,160,fp); strcpy(name,buf);
	tmp=strlen(name)-1; name[tmp]='\0';	// remove the terminal return

	fgets(buf,160,fp);
        sscanf(buf,"%d%d",&num_atom,&num_bond);

	delete [] atom;  delete [] bond; 

	atom=new Atom[num_atom];
	if(atom==NULL) Memory_Allocation_Error();

	bond=new Bond[num_bond];
	if(bond==NULL) Memory_Allocation_Error();

        do 
                {
                 if(fgets(buf,160,fp)!=NULL) sscanf(buf,"%s",head);
                 else Mol2_Format_Error(filename);
                }
        while(strcmp(head,"@<TRIPOS>ATOM"));

	for(i=0;i<num_atom;i++)
		{
		 if(fgets(buf,160,fp)==NULL) Mol2_Format_Error(filename);
		 sscanf(buf,"%d%s%f%f%f%s%*s%*s%*f", 
			&atom[i].id, 
			 atom[i].name,
			&atom[i].coor[0],
			&atom[i].coor[1],
			&atom[i].coor[2],
			 atom[i].type);
		 atom[i].valid=1;
		}

	do
                {
                 if(fgets(buf,160,fp)!=NULL) sscanf(buf,"%s",head);
                 else Mol2_Format_Error(filename);
                }
        while(strcmp(head,"@<TRIPOS>BOND"));

	for(i=0;i<num_bond;i++)
		{
		 if(fgets(buf,160,fp)==NULL) Mol2_Format_Error(filename); 
		 sscanf(buf,"%d%d%d%s",
			&bond[i].id, 
			&bond[i].atom_1,
			&bond[i].atom_2, 
			 bond[i].type);
		 bond[i].valid=1;
		}

	fclose(fp);

	valid=1;	// read the molecule okay

	return;
}

int Molecule::Extract_From_Mol2(FILE *fp)
{
	int i,tmp;
	char buf[160],head[40];

       	fgets(buf,160,fp); strcpy(name,buf);
	tmp=strlen(name)-1; name[tmp]='\0';	// remove the terminal return

	fgets(buf,160,fp);
        sscanf(buf,"%d%d",&num_atom,&num_bond);

	delete [] atom;  delete [] bond; 

	atom=new Atom[num_atom];
	if(atom==NULL) Memory_Allocation_Error();

	bond=new Bond[num_bond];
	if(bond==NULL) Memory_Allocation_Error();

        do 
                {
                 if(fgets(buf,160,fp)!=NULL) sscanf(buf,"%s",head);
		 else return FALSE;
                }
        while(strcmp(head,"@<TRIPOS>ATOM"));

	for(i=0;i<num_atom;i++)
		{
		 if(fgets(buf,160,fp)==NULL) return FALSE; 
		 sscanf(buf,"%d%s%f%f%f%s%*s%*s%*f", 
			&atom[i].id, 
			 atom[i].name,
			&atom[i].coor[0],
			&atom[i].coor[1],
			&atom[i].coor[2],
			 atom[i].type);
		 atom[i].valid=1;
		}

	do
                {
                 if(fgets(buf,160,fp)!=NULL) sscanf(buf,"%s",head);
                 else return FALSE;
                }
        while(strcmp(head,"@<TRIPOS>BOND"));

	for(i=0;i<num_bond;i++)
		{
		 if(fgets(buf,160,fp)==NULL) return FALSE; 
		 sscanf(buf,"%d%d%d%s",
			&bond[i].id, 
			&bond[i].atom_1,
			&bond[i].atom_2, 
			 bond[i].type);
		 bond[i].valid=1;
		}

	return TRUE;
}

void Molecule::Write_Out_Mol2(char *filename) const
{
	FILE *fp;
	int i;

	if((fp=fopen(filename,"a"))==NULL) Openning_File_Error(filename);

	fprintf(fp,"# Name:          %s\n",name); 
	fprintf(fp,"# Creation time: %s\n\n",Get_Time());

	fprintf(fp,"@<TRIPOS>MOLECULE\n");
	fprintf(fp,"%s\n",name);
	fprintf(fp,"%5d %5d %5d %5d %5d\n",num_atom,num_bond,1,1,1);
	fprintf(fp,"SMALL\n");
	fprintf(fp,"USER_CHARGES\n");
	fprintf(fp,"\n\n");

	fprintf(fp,"@<TRIPOS>ATOM\n");

	for(i=0;i<num_atom;i++)
		{
		 fprintf(fp,"%7d %-8s %9.4f %9.4f %9.4f %-9s  ",
			 atom[i].id, 
			 atom[i].name, 
			 atom[i].coor[0],
			 atom[i].coor[1], 
			 atom[i].coor[2], 
			 atom[i].type);
		 fprintf(fp,"%1d <%1d>      %7.3f\n", 1, 1, atom[i].logp); 
		}
	
	fprintf(fp,"@<TRIPOS>BOND\n");

	for(i=0;i<num_bond;i++)
		{
		 fprintf(fp,"%6d %4d %4d %-3s\n", 
			 bond[i].id, 
			 bond[i].atom_1, 
			 bond[i].atom_2, 
			 bond[i].type);
		}

	fclose(fp);

	return;
}

void Molecule::Write_Out_Log(char *filename) const
{
        FILE *fp;
        int i,mark;

        if((fp=fopen(filename,"a"))==NULL) Openning_File_Error(filename);

	fprintf(fp,"#\n");
	fprintf(fp,"# XLOGP v2.0 logP calculation: %s", Get_Time());
	fprintf(fp,"#\n");
	fprintf(fp,"# Name of molecule : %s\n", name);
	fprintf(fp,"# Molecular formula: %s\n", formula); 
	fprintf(fp,"# Molecular weight : %d\n", weight);
	fprintf(fp,"#\n");

	fprintf(fp,"----------------------------------------------\n");
	fprintf(fp,"No.  type  symbol              \tcontribution\n");
	fprintf(fp,"----------------------------------------------\n");

	for(i=0;i<num_atom;i++)
		{
		 if(atom[i].type[0]=='H') continue;
		 
		 fprintf(fp,"%-3d   %-2d   %-20s\t%6.3f\n",
			 atom[i].id, atom[i].valid, 
			 atom[i].xtype, atom[i].logp);
		}
	fprintf(fp,"----------------------------------------------\n");

	mark=FALSE;

	for(i=0;i<10;i++)
		{
		 if(factor[i].num==0) continue;
		 
		 fprintf(fp,"%-30s\t%6.3f\n", 
			 factor[i].symbol, factor[i].num*cb[i]);
		 mark++;
		}

	if(mark!=FALSE)
		fprintf(fp,"----------------------------------------------\n");

	fprintf(fp,"Calculated LogP =%6.2f\n",logp);

	fclose(fp);

	return;
}

void Molecule::Get_Weight() 
{
        int i,tmp;

	tmp=0;

        for(i=0;i<num_atom;i++)
                {
                 if(atom[i].valid==0) continue;
                 else tmp+=atom[i].weight; 
                }

	this->weight=tmp;

        return; 
}

void Molecule::Get_Formula()
{
	int i;
        int num[11]={0,0,0,0,0,0,0,0,0,0,0}; // C,H,N,O,P,S,F,Cl,Br,I,Un
	char element[11][3];
	char tmp[10],tmp_formula[40];

	strcpy(element[0],"C");
	strcpy(element[1],"H");
	strcpy(element[2],"N");
	strcpy(element[3],"O");
	strcpy(element[4],"P");
	strcpy(element[5],"S");
	strcpy(element[6],"F");
	strcpy(element[7],"Cl");
	strcpy(element[8],"Br");
	strcpy(element[9],"I");
	strcpy(element[10],"Un");

	for(i=0;i<num_atom;i++)
		{
		 if(atom[i].valid==0) num[10]++;
		 else if(!strcmp(atom[i].type,"C.3")) num[0]++;
		 else if(!strcmp(atom[i].type,"C.2")) num[0]++;
		 else if(!strcmp(atom[i].type,"C.1")) num[0]++;
		 else if(!strcmp(atom[i].type,"C.cat")) num[0]++;
                 else if(!strcmp(atom[i].type,"C.ar")) num[0]++;
                 else if(!strcmp(atom[i].type,"H")) num[1]++;
                 else if(!strcmp(atom[i].type,"H.spc")) num[1]++;
		 else if(!strcmp(atom[i].type,"N.4")) num[2]++;
                 else if(!strcmp(atom[i].type,"N.3")) num[2]++;
                 else if(!strcmp(atom[i].type,"N.2")) num[2]++;
		 else if(!strcmp(atom[i].type,"N.1")) num[2]++;
                 else if(!strcmp(atom[i].type,"N.ar")) num[2]++;
                 else if(!strcmp(atom[i].type,"N.pl3")) num[2]++;
		 else if(!strcmp(atom[i].type,"N.am")) num[2]++;
                 else if(!strcmp(atom[i].type,"O.3")) num[3]++;
		 else if(!strcmp(atom[i].type,"O.2")) num[3]++;
                 else if(!strcmp(atom[i].type,"O.co2")) num[3]++;
                 else if(!strcmp(atom[i].type,"P.3")) num[4]++;
		 else if(!strcmp(atom[i].type,"S.3")) num[5]++;
                 else if(!strcmp(atom[i].type,"S.2")) num[5]++;
                 else if(!strcmp(atom[i].type,"S.o")) num[5]++;
		 else if(!strcmp(atom[i].type,"S.o2")) num[5]++;
                 else if(!strcmp(atom[i].type,"F")) num[6]++;
                 else if(!strcmp(atom[i].type,"Cl")) num[7]++;
                 else if(!strcmp(atom[i].type,"Br")) num[8]++;
		 else if(!strcmp(atom[i].type,"I")) num[9]++;
                 else num[10]++;	// Unknown
		}

	strcpy(tmp_formula,"");

	for(i=0;i<11;i++)
		{
		 if(num[i]==0) continue;
		 else
			{
			 strcat(tmp_formula,element[i]);
			 if(num[i]==1) continue;
			 else 
				{
				 sprintf(tmp,"%d", num[i]);
				 strcat(tmp_formula,tmp);
				}
			}
		}

	strcpy(this->formula,tmp_formula);

        return;
}

void Molecule::First_Check()
{
        int i;
	Group group;

	// first, assign atomic weight to atoms

        for(i=0;i<num_atom;i++)
                {
                 if(!strcmp(atom[i].type,"C.3")) atom[i].weight=12;
                 else if(!strcmp(atom[i].type,"C.2")) atom[i].weight=12;
                 else if(!strcmp(atom[i].type,"C.1")) atom[i].weight=12;
                 else if(!strcmp(atom[i].type,"C.cat")) atom[i].weight=12;
                 else if(!strcmp(atom[i].type,"C.ar")) atom[i].weight=12;
                 else if(!strcmp(atom[i].type,"H")) atom[i].weight=1;
                 else if(!strcmp(atom[i].type,"H.spc")) atom[i].weight=1;
                 else if(!strcmp(atom[i].type,"N.4")) atom[i].weight=14;
                 else if(!strcmp(atom[i].type,"N.3")) atom[i].weight=14;
                 else if(!strcmp(atom[i].type,"N.2")) atom[i].weight=14;
                 else if(!strcmp(atom[i].type,"N.1")) atom[i].weight=14;
                 else if(!strcmp(atom[i].type,"N.ar")) atom[i].weight=14;
                 else if(!strcmp(atom[i].type,"N.pl3")) atom[i].weight=14;
                 else if(!strcmp(atom[i].type,"N.am")) atom[i].weight=14;
                 else if(!strcmp(atom[i].type,"O.3")) atom[i].weight=16;
                 else if(!strcmp(atom[i].type,"O.2")) atom[i].weight=16;
                 else if(!strcmp(atom[i].type,"O.co2")) atom[i].weight=16;
                 else if(!strcmp(atom[i].type,"P.3")) atom[i].weight=31;
                 else if(!strcmp(atom[i].type,"S.3")) atom[i].weight=32;
                 else if(!strcmp(atom[i].type,"S.2")) atom[i].weight=32;
                 else if(!strcmp(atom[i].type,"S.o")) atom[i].weight=32;
                 else if(!strcmp(atom[i].type,"S.o2")) atom[i].weight=32;
                 else if(!strcmp(atom[i].type,"F")) atom[i].weight=19;
                 else if(!strcmp(atom[i].type,"Cl")) atom[i].weight=35;
                 else if(!strcmp(atom[i].type,"Br")) atom[i].weight=80;
                 else if(!strcmp(atom[i].type,"I")) atom[i].weight=127;
                 else atom[i].weight=0;
                }

	// second, detect connections 

        for(i=0;i<num_atom;i++) Find_Neighbors_Of_An_Atom(atom[i].id);

        for(i=0;i<num_bond;i++) Find_Neighbors_Of_A_Bond(bond[i].id);

	// third, get some molecular properties

	Get_Weight();

	Get_Formula();

	Detect_Rings();

	// then fix the possible errors in atom type assignment

        for(i=0;i<num_atom;i++)
        	{
         	 group=Find_A_Group(atom[i].id);
         	 if(Check_Atom_Type(group)==FALSE)
                	{
                 	 strcpy(atom[i].type, group.center.type);
			}
		 else continue;
                }

        return;
}

void Molecule::Find_Neighbors_Of_An_Atom(int id)
{
	int i,j,tmp1,tmp2,temp,num;

	// first, find all the neighboring atoms

	num=0;

        for(i=0;i<num_bond;i++)
                {
		 if(bond[i].valid==0) continue;
                 else if(bond[i].atom_1==id) 
			{
			 atom[id-1].neib[num]=bond[i].atom_2;
			 num++;
			}
                 else if(bond[i].atom_2==id)
                        {
			 atom[id-1].neib[num]=bond[i].atom_1;
			 num++;
                        }
                 else continue;
                }

	atom[id-1].num_neib=num;

	// second, rank the root's neighbours in terms of atomic weights

	if(num>1)
		{
		 for(i=0;i<num-1;i++)
		 for(j=i+1;j<num;j++)
		{
		 tmp1=atom[atom[id-1].neib[i]-1].weight;
		 tmp2=atom[atom[id-1].neib[j]-1].weight;

		 if(tmp1>=tmp2) continue;
		 else
			{
			 temp=atom[id-1].neib[i];
			 atom[id-1].neib[i]=atom[id-1].neib[j];
			 atom[id-1].neib[j]=temp;
			}
		}
		}

	return;
}

void Molecule::Find_Neighbors_Of_A_Bond(int id)
{
	int i,num;

	num=0;

	for(i=0;i<num_bond;i++)
		{
		 if(bond[i].valid==0) continue;
	 	 else if(bond[i].id==id) continue;
	 	 else if(Two_Bonds_Connection_Check(bond[i],bond[id-1])) 
			{
		 	  bond[id-1].neib[num]=bond[i].id;
		 	  num++;
			}
		 else continue;
		}

	bond[id-1].num_neib=num;

	return;
}

int Molecule::Two_Bonds_Connection_Check(Bond bond1, Bond bond2) const
// this function returns the ID of the joint atom
{
        int id;

        id=bond1.atom_1;

        if(id==bond2.atom_1) return id;
        else if(id==bond2.atom_2) return id;

        id=bond1.atom_2;

        if(id==bond2.atom_1) return id;
        else if(id==bond2.atom_2) return id;

        return FALSE;       // two bonds are not connected
}

int Molecule::Get_NonH_Neighbors_Num(int atom_id) const
{
        int i,tmp;
        int num_neib,num_nonh=0;

	atom_id--;

        num_neib=atom[atom_id].num_neib;

        for(i=0;i<num_neib;i++)
                {
                 tmp=atom[atom_id].neib[i];
                 if(atom[tmp-1].type[0]=='H') continue;
                 else num_nonh++;
                }

        return num_nonh;
}

void Molecule::Detect_Rings()
{
	int i,j,mark,count;

	// first of all, check whether there is any ring in the molecule

	if((num_bond-num_atom+1)==0)
		{
		 for(i=0;i<num_atom;i++) atom[i].ring=0;
		 for(i=0;i<num_bond;i++) bond[i].ring=0;
		 return;
		}

	// detect terminal atoms

	for(i=0;i<num_atom;i++) 	
		{
		 if(Get_NonH_Neighbors_Num(atom[i].id)==1) atom[i].ring=0;
		 else atom[i].ring=-1;	// ambiguous at present
		}

	// collapse the structure to eliminate branches

	do		
		{
		 mark=0;
		 for(i=0;i<num_atom;i++)
			{
			 if(atom[i].ring!=-1) continue;

			 count=0;	// count possible ring neighbors	
		 	 for(j=0;j<atom[i].num_neib;j++)
			{
			 if(atom[atom[i].neib[j]-1].ring==0) continue;
			 else count++;
			}

			 if(count<=1) {atom[i].ring=0; mark++;}
			}
		}
	while(mark);

	// detect branching bonds

	mark=0;

	for(i=0;i<num_bond;i++) 
		{
		 if(atom[bond[i].atom_1-1].ring==0) bond[i].ring=0;
		 else if(atom[bond[i].atom_2-1].ring==0) bond[i].ring=0;
		 else {bond[i].ring=-1; mark++;}	// ambiguous
		}

	if(mark==0) return; 	// finished, no ring detected

	// check all the ambiguous bonds
	
	for(i=0;i<num_bond;i++)	
		{
		 if(bond[i].ring!=-1) continue;
		 else bond[i].ring=Judge_If_An_Bond_In_Ring(bond[i].id);
		}

	// find all the atoms in rings

	for(i=0;i<num_bond;i++) 
		{
		 if(bond[i].ring!=1) continue;
		 else
			{
			 atom[bond[i].atom_1-1].ring=1;
			 atom[bond[i].atom_2-1].ring=1;
			}
		}

	// define all the other atoms

	for(i=0;i<num_atom;i++)	
		{
		 if(atom[i].ring!=-1) continue;
		 else atom[i].ring=0;
		}
	
	return;
}

void Molecule::Reset_Choices(int id,int choice[]) const
// id is ID of the bond
{
	int i,tmp;

	for(i=0;i<10;i++) choice[i]=0;

	if(bond[id-1].ring!=0) 
		{
		 for(i=0;i<bond[id-1].num_neib;i++)
			{
			 tmp=bond[id-1].neib[i];
			 if(bond[tmp-1].ring==0) choice[i]=0;
			 else choice[i]=tmp; 
			}
		}

	return;
}

void Molecule::Check_Choices(int head, int choice[]) const
// head: ID of the head atom of this bond
{
	int i;

	for(i=0;i<10;i++)
		{
		 if(choice[i]==0) continue;
		 else if(choice[i]>num_bond) choice[i]=0;
		 else if(bond[choice[i]-1].ring==0) choice[i]=0;
		 else if(bond[choice[i]-1].atom_1==head) choice[i]=0;
		 else if(bond[choice[i]-1].atom_2==head) choice[i]=0;
		 else continue;
		}

	return;
}

void Molecule::Clean_Choices(int wrong, int choice[]) const
// wrong: the ID of the unwanted bond
{
        int i;

        for(i=0;i<10;i++)
                {
                 if(choice[i]==0) continue;
                 else if(choice[i]>num_bond) choice[i]=0;
                 else if(choice[i]==wrong) choice[i]=0;
                 else continue;
                }

        return;
}

int Molecule::Judge_If_An_Bond_In_Ring(int bond_id) const
// this is a bond-based algorithm to detect rings in a molecule.
{
	int i,j,tmp;
	int p,next;
	int atom_chain[200], bond_chain[200];
	int choice[200][10];

	for(i=0;i<200;i++)
		{
		 atom_chain[i]=bond_chain[i]=0;
		 for(j=0;j<10;j++) choice[i][j]=0;
		}

	for(i=0;i<num_bond;i++) Reset_Choices(bond[i].id,choice[i]);

	// push the bond into the searching path

	p=0;	// pointer of the searching path

	bond_chain[p]=bond_id; 
	atom_chain[p]=bond[bond_id-1].atom_1;	// head of this bond

	for(;;)
	{
		// find the next bond to enlong the searching path
		do
		{
		 tmp=bond_chain[p]-1;

		 Check_Choices(atom_chain[p], choice[tmp]);
		
		 if(p>0) Clean_Choices(bond_chain[p-1], choice[tmp]);

		 next=FALSE;

		 for(i=0;i<10;i++)	// select the next bond
			{
			 if(choice[tmp][i]==0) continue;
			 else {next=choice[tmp][i]; break;}
			}

		 if(next==FALSE)	// no choice is available
			{
			 if(p==0) return FALSE;	// this bond is not in ring.
			 else		// pop out the bond of the path
				{
				 Reset_Choices(bond_chain[p],choice[tmp]);
				 bond_chain[p]=0;
				 atom_chain[p]=0;
				 p--;
				 continue;
				}
			}

                 for(i=0;i<=p;i++)
                        {
                         if(next!=bond_chain[i]) continue;
                         else           // this is a wrong choice
                                {
                                 Clean_Choices(next,choice[tmp]);
                                 next=FALSE; break;
                                }
                        }

		 if(next!=FALSE) 
			{
			 if(Two_Bonds_Connection_Check(bond[bond_chain[0]-1],
			    bond[next-1])==atom_chain[0])    // find a ring
				{
				 // puts("Ring detected:");
				 for(i=0;i<=p;i++) 
					{
					 bond[bond_chain[i]-1].ring=1;
					/*
					 printf("Atom %d - %d\n",
					 bond[bond_chain[i]-1].atom_1,
					 bond[bond_chain[i]-1].atom_2);
					*/
					}
				 bond[next-1].ring=1;
				/*
				 printf("Atom %d - %d\n\n",
					bond[next-1].atom_1,
					bond[next-1].atom_2);
			        */
				 return TRUE;
				}
			}
			 
		} while(next==FALSE);

		Clean_Choices(next,choice[tmp]);
		tmp=Two_Bonds_Connection_Check(bond[tmp],bond[next-1]);
		p++;
		bond_chain[p]=next;
		atom_chain[p]=tmp;
	}
}

int Molecule::Connection_1_2_Check(int id1, int id2) const
{
        int i;

	if(id1==id2) return FALSE;

        for(i=0;i<atom[id2-1].num_neib;i++)
                {
                 if(id1==atom[id2-1].neib[i]) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_1_3_Check(int id1, int id2) const
{
        int i,j;

	if(id1==id2) return FALSE;
	if(Connection_1_2_Check(id1,id2)==TRUE) return FALSE;

	id1--; id2--;

        for(i=0;i<atom[id1].num_neib;i++)
        for(j=0;j<atom[id2].num_neib;j++)
                {
                 if(atom[id1].neib[i]==atom[id2].neib[j]) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_1_4_Check(int id1, int id2) const
{
        int i,j;

	if(id1==id2) return FALSE;
        if(Connection_1_2_Check(id1,id2)==TRUE) return FALSE;
	if(Connection_1_3_Check(id1,id2)==TRUE) return FALSE;

	id1--; id2--;

        for(i=0;i<atom[id1].num_neib;i++)
        for(j=0;j<atom[id2].num_neib;j++)
                {
                 if(Connection_1_2_Check(atom[id1].neib[i],
                    atom[id2].neib[j])==TRUE) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_1_5_Check(int id1, int id2) const
{
        int i,j;

	if(id1==id2) return FALSE;
        if(Connection_1_2_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_3_Check(id1,id2)==TRUE) return FALSE;
	if(Connection_1_4_Check(id1,id2)==TRUE) return FALSE;

	id1--; id2--;

        for(i=0;i<atom[id1].num_neib;i++)
        for(j=0;j<atom[id2].num_neib;j++)
                {
                 if(Connection_1_3_Check(atom[id1].neib[i],
                    atom[id2].neib[j])==TRUE) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_1_6_Check(int id1, int id2) const
{
	int i,j;

	if(id1==id2) return FALSE;
	if(Connection_1_2_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_3_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_4_Check(id1,id2)==TRUE) return FALSE;
	if(Connection_1_5_Check(id1,id2)==TRUE) return FALSE;

	id1--; id2--;

	for(i=0;i<atom[id1].num_neib;i++)
        for(j=0;j<atom[id2].num_neib;j++)
                {
                 if(Connection_1_4_Check(atom[id1].neib[i],
                    atom[id2].neib[j])==TRUE) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_1_7_Check(int id1, int id2) const
{
        int i,j;

        if(id1==id2) return FALSE;
        if(Connection_1_2_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_3_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_4_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_5_Check(id1,id2)==TRUE) return FALSE;
	if(Connection_1_6_Check(id1,id2)==TRUE) return FALSE;

        id1--; id2--;

        for(i=0;i<atom[id1].num_neib;i++)
        for(j=0;j<atom[id2].num_neib;j++)
                {
                 if(Connection_1_5_Check(atom[id1].neib[i],
                    atom[id2].neib[j])==TRUE) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_1_8_Check(int id1, int id2) const
{
        int i,j;

        if(id1==id2) return FALSE;
        if(Connection_1_2_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_3_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_4_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_5_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_6_Check(id1,id2)==TRUE) return FALSE;
	if(Connection_1_7_Check(id1,id2)==TRUE) return FALSE;

        id1--; id2--;

        for(i=0;i<atom[id1].num_neib;i++)
        for(j=0;j<atom[id2].num_neib;j++)
                {
                 if(Connection_1_6_Check(atom[id1].neib[i],
                    atom[id2].neib[j])==TRUE) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_1_9_Check(int id1, int id2) const
{
        int i,j;

        if(id1==id2) return FALSE;
        if(Connection_1_2_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_3_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_4_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_5_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_6_Check(id1,id2)==TRUE) return FALSE;
        if(Connection_1_7_Check(id1,id2)==TRUE) return FALSE;
	if(Connection_1_8_Check(id1,id2)==TRUE) return FALSE;

        id1--; id2--;

        for(i=0;i<atom[id1].num_neib;i++)
        for(j=0;j<atom[id2].num_neib;j++)
                {
                 if(Connection_1_7_Check(atom[id1].neib[i],
                    atom[id2].neib[j])==TRUE) return TRUE;
                 else continue;
                }

        return FALSE;
}

int Molecule::Connection_Check(int id1, int id2) const
{
	if(id1==id2) return 1;
        else if(Connection_1_2_Check(id1,id2)==TRUE) return 2;
        else if(Connection_1_3_Check(id1,id2)==TRUE) return 3;
        else if(Connection_1_4_Check(id1,id2)==TRUE) return 4;
        else if(Connection_1_5_Check(id1,id2)==TRUE) return 5;
	else if(Connection_1_6_Check(id1,id2)==TRUE) return 6;
	else if(Connection_1_7_Check(id1,id2)==TRUE) return 7;
        else if(Connection_1_8_Check(id1,id2)==TRUE) return 8;
        else if(Connection_1_9_Check(id1,id2)==TRUE) return 9;
	else return 10;
}

int Molecule::Adjacent_Aromatic_Check(int id) const
{
	int i,num,tmp,mark;

	id--;

	mark=FALSE; num=atom[id].num_neib;

	for(i=0;i<num;i++)
		{
		 tmp=atom[id].neib[i];
		 if(!strcmp(atom[tmp-1].type,"C.ar")) {mark=tmp; break;}
		 else continue;
		}

	return mark;
}

int Molecule::Adjacent_Ring_Check(int id) const
{
        int i,num,tmp,mark;

        id--;

        mark=FALSE; num=atom[id].num_neib;

        for(i=0;i<num;i++)
                {
                 tmp=atom[id].neib[i];
                 if(atom[tmp-1].ring!=0) {mark=tmp; break;}
                 else continue;
                }

        return mark;
}

int Molecule::Hydrophobic_Neighbor_Check(int id) const
{
	int i,mark;

	id--;

	mark=TRUE;

	for(i=0;i<num_atom;i++)
		{
		 if(i==id) continue;
		 else if(!strcmp(atom[i].type,"F")||
			 !strcmp(atom[i].type,"Cl")||
			 !strcmp(atom[i].type,"Br")||
			 !strcmp(atom[i].type,"I")||
			 atom[i].type[0]=='N'||
			 atom[i].type[0]=='O'||
			 atom[i].type[0]=='S'||
			 atom[i].type[0]=='P')
		 	{
			 if(Connection_Check(atom[id].id,atom[i].id)<=4)
				{mark=FALSE; break;}
			 else continue;
			}
		 else continue;
		}

	return mark;
}

Group Molecule::Find_A_Group(int id) const
// id is the ID of the central atom
{
	int i,j,num;
	Group group;	

	// define the center

	group.center=atom[id-1];

	// find the center's neighbours

        num=group.center.num_neib;  

	for(i=0;i<num;i++)
		{
		 group.neib[i]=atom[group.center.neib[i]-1];

		 for(j=0;j<num_bond;j++)
			{
			 if((bond[j].atom_1==group.center.neib[i])&&
			    (bond[j].atom_2==group.center.id))
				{
				 group.bond[i]=bond[j];
				 break;
				}
			 else if((bond[j].atom_2==group.center.neib[i])&& 
				 (bond[j].atom_1==group.center.id))
				{
				 group.bond[i]=bond[j];
				 break;
				}
			 else continue;
			}
		}

	// count the necessary parameters 

	group.num_neib=num;

	group.num_h=0; group.num_nonh=0; 

	for(i=0;i<num;i++)
		{
		 if(group.neib[i].type[0]=='H') group.num_h++;
		 else group.num_nonh++;
		}

	group.num_hetero=0;

	for(i=0;i<group.num_nonh;i++)
		{
		 if(!strcmp(group.bond[i].type,"2")) continue;
		 else if(!strcmp(group.bond[i].type,"3")) continue;
		 else if(!strcmp(group.bond[i].type,"ar")) continue;
               	 else if(group.neib[i].type[0]=='N') group.num_hetero++; 
               	 else if(group.neib[i].type[0]=='O') group.num_hetero++; 
		 else continue;
		}

	group.num_pi=0;

	for(i=0;i<group.num_nonh;i++)
                {
		 if(!strcmp(group.bond[i].type,"2")) continue;
		 else if(!strcmp(group.bond[i].type,"3")) continue;
                 else if(!strcmp(group.neib[i].type,"C.ar")) group.num_pi++;
                 else if(!strcmp(group.neib[i].type,"C.2")) group.num_pi++;
                 else if(!strcmp(group.neib[i].type,"C.1"))group.num_pi++;
                 else if(!strcmp(group.neib[i].type,"C.cat")) group.num_pi++;
		 else if(!strcmp(group.neib[i].type,"N.2")) group.num_pi++;
		 else if(!strcmp(group.neib[i].type,"N.1")) group.num_pi++;
		 else if(!strcmp(group.neib[i].type,"N.ar")) group.num_pi++;
		 else continue;
                }

	group.num_nar=group.num_car=0;

	for(i=0;i<group.num_nonh;i++)
                {
		 if(strcmp(group.bond[i].type,"ar")) continue;
                 else if(!strcmp(group.neib[i].type,"N.ar")) group.num_nar++;
		 else if(!strcmp(group.neib[i].type,"C.ar")) group.num_car++;
		 else if(group.neib[i].type[0]=='C') group.num_car++;
		 else group.num_nar++;
		}

	group.db_type=0;

        for(i=0;i<group.num_nonh;i++)
                {
		 if(!strcmp(group.center.type,"O.co2")||
		    !strcmp(group.center.type,"O.2")||
		    !strcmp(group.center.type,"S.2"))
			{
			 if(group.neib[i].type[0]=='C') group.db_type=1;
			 else if(group.neib[i].type[0]=='N') group.db_type=2;
			 else if(group.neib[i].type[0]=='O') group.db_type=3;
			 else if(group.neib[i].type[0]=='S') group.db_type=4;
			 else if(group.neib[i].type[0]=='P') group.db_type=5;
			 else continue;
			}
		 else if(!strcmp(group.bond[i].type,"2")||
		    	 !strcmp(group.neib[i].type,"O.co2")||
			 !strcmp(group.neib[i].type,"O.2")||
			 !strcmp(group.neib[i].type,"S.2"))
		 	{
                         if(group.neib[i].type[0]=='C') group.db_type=1;
                         else if(group.neib[i].type[0]=='N') group.db_type=2;
                         else if(group.neib[i].type[0]=='O') group.db_type=3;
                         else if(group.neib[i].type[0]=='S') group.db_type=4;
                         else if(group.neib[i].type[0]=='P') group.db_type=5;
                         else continue;
                        }
                 else continue;
                }

        group.valid=TRUE; return group;
}

int Molecule::Check_Atom_Type(Group &group) const 
// there are always some dilemma when assigning the atom type
{
	int i,mark;

	if(group.center.type[0]=='H')
		{
		 if(strcmp(group.center.type,"H"))
			{
			 strcpy(group.center.type,"H");
			 return FALSE;
			}
		}

	if(group.center.type[0]=='P')
                {
                 if(strcmp(group.center.type,"P.3"))
                        {
                         strcpy(group.center.type,"P.3");
                         return FALSE;
                        }
                }

	if(group.center.type[0]=='N')
		{
		 if(group.db_type==3)	// -NO, -NO2  and etc. 
			{
			 if(strcmp(group.center.type,"N.2"))
				{
				 strcpy(group.center.type,"N.2");
			 	 return FALSE;
				}
			}
		}

	if(group.center.type[0]=='S')
		{
		 if((group.db_type==3)&&(group.num_neib==3)) // -SO-
			{
			 if(strcmp(group.center.type,"S.o"))
				{
				 strcpy(group.center.type,"S.o");
				 return FALSE;
				}
			}
		 else if((group.db_type==3)&&(group.num_neib==4)) // -SO2-
			{
			 if(strcmp(group.center.type,"S.o2"))
                                {
                                 strcpy(group.center.type,"S.o2");
                                 return FALSE;
                                }
			}
		 else if(group.num_neib>=4)	// e.g. -SF5
			{
			 if(strcmp(group.center.type,"S.o2"))
                                {
                                 strcpy(group.center.type,"S.o2");
                                 return FALSE;
                                }
			}
		}

	if(!strcmp(group.center.type,"C.2"))
		{
		 mark=TRUE;

		 for(i=0;i<group.num_nonh;i++)
			{
			 if(!strcmp(group.bond[i].type,"2")) continue;
			 else {mark=FALSE; break;}
			} 

		 if(mark==TRUE&&group.num_nonh==2) 	// =C=
			{
			 strcpy(group.center.type,"C.1");
			 return FALSE;
			}
		}

	if(!strcmp(group.center.type,"N.2"))
		{
		 mark=TRUE;

                 for(i=0;i<group.num_nonh;i++)
                        {
                         if(!strcmp(group.bond[i].type,"2")) continue;
                         else {mark=FALSE; break;}
                        }

                 if(mark==TRUE&&group.num_nonh==2)         // =N=
                        {
                         strcpy(group.center.type,"N.1");
                         return FALSE;
                        }
		}

	return TRUE;
}

void Molecule::Atom_Typing()
{
	int i;
	Group group;

	for(i=0;i<num_atom;i++)
	{
	 group=Find_A_Group(atom[i].id);

	 strcpy(atom[i].hb,"N");

	 if(!strcmp(atom[i].type,"C.3"))
		{
		 if(group.num_nonh==1)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"C.3.h3 (pi=0)");
					 atom[i].valid=1;
					 continue;
					}
				 else
					{
					 strcpy(atom[i].xtype,"C.3.h3 (pi=1)");
					 atom[i].valid=2;
					 continue;
					}
				}
			 else
				{
                                 strcpy(atom[i].xtype,"C.3.h3.x");
                                 atom[i].valid=3;
				 continue;
				}
			}
		 else if(group.num_nonh==2)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"C.3.h2 (pi=0)");
					 atom[i].valid=4;
					 continue;
					}
				 else if(group.num_pi==1)
					{
					 strcpy(atom[i].xtype,"C.3.h2 (pi=1)");
                                         atom[i].valid=5;
					 continue;
					}
				 else
					{
					 strcpy(atom[i].xtype,"C.3.h2 (pi=2)");
                                         atom[i].valid=6;
					 continue;
					}
				}
			 else 
				{
				 if(group.num_pi==0)
                                        {
                                        strcpy(atom[i].xtype,"C.3.h2.x (pi=0)");
                                        atom[i].valid=7;
					continue;
                                        }
                                 else if(group.num_pi==1)
                                        {
                                        strcpy(atom[i].xtype,"C.3.h2.x (pi=1)");
                                        atom[i].valid=8;
					continue;
                                        }
                                 else
                                        {
                                        strcpy(atom[i].xtype,"C.3.h2.x (pi=2)");
                                        atom[i].valid=9;
					continue;
                                        }
				}
			}
		 else if(group.num_nonh==3)
			{
			 if(group.num_hetero==0)
				{
                                 if(group.num_pi==0)
                                        {
                                         strcpy(atom[i].xtype,"C.3.h (pi=0)");
                                         atom[i].valid=10;
					 continue;
                                        }
                                 else if(group.num_pi==1)
                                        {
                                         strcpy(atom[i].xtype,"C.3.h (pi=1)");
                                         atom[i].valid=11;
					 continue;
                                        }
                                 else
                                        {
                                         strcpy(atom[i].xtype,"C.3.h (pi>1)");
                                         atom[i].valid=12;
					 continue;
                                        }
				}
			 else 
				{
                                 if(group.num_pi==0)
                                        {
                                        strcpy(atom[i].xtype,"C.3.h.x (pi=0)");
                                        atom[i].valid=13;
					continue;
                                        }
                                 else if(group.num_pi==1)
                                        {
                                        strcpy(atom[i].xtype,"C.3.h.x (pi=1)");
                                        atom[i].valid=14;
					continue;
                                        }
                                 else
                                        {
                                        strcpy(atom[i].xtype,"C.3.h.x (pi>1)");
                                        atom[i].valid=15;
					continue;
                                        }
				}
			}
		 else if(group.num_nonh==4)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
                                       {
                                        strcpy(atom[i].xtype,"C.3 (pi=0)");
                                        atom[i].valid=16;
					continue;
                                       }
                                 else if(group.num_pi==1)
                                       {
                                        strcpy(atom[i].xtype,"C.3 (pi=1)");
                                        atom[i].valid=17;
					continue;
                                       }
                                 else
                                       {
                                        strcpy(atom[i].xtype,"C.3 (pi>1)");
                                        atom[i].valid=18;
					continue;
                                       }
				}
			 else 
				{
				 if(group.num_pi==0)
                                       {
                                        strcpy(atom[i].xtype,"C.3.x (pi=0)");
                                        atom[i].valid=19;
					continue;
                                       }
                                 else 
                                       {
                                        strcpy(atom[i].xtype,"C.3.x (pi>0)");
                                        atom[i].valid=20;
					continue;
                                       }
				}
			}
		 else	// a guess is better than nothing	
			{
			 strcpy(atom[i].xtype,"C.3.unknown");
			 atom[i].valid=1;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"C.2"))
		{
		 if(group.num_nonh==1)
			{
			 strcpy(atom[i].xtype,"C.2.h2");	
			 atom[i].valid=21;
			 continue;
			}
		 else if(group.num_nonh==2)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0) 
					{
					 strcpy(atom[i].xtype,"C.2.h (pi=0)");
					 atom[i].valid=22;
					 continue;
					}
				 else
					{
					 strcpy(atom[i].xtype,"C.2.h (pi=1)");
					 atom[i].valid=23;
					 continue;
					}
				}
			 else
				{
				 if(group.num_pi==0) 
                                        {
                                         strcpy(atom[i].xtype,"C.2.h.x (pi=0)");
                                         atom[i].valid=24;
                                         continue;
                                        }
                                 else
                                        {
                                         strcpy(atom[i].xtype,"C.2.h.x (pi=1)");
                                         atom[i].valid=25;
                                         continue;
                                        }
				}
			}
		 else if(group.num_nonh==3)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"C.2 (pi=0)"); 
                                         atom[i].valid=26;
                                         continue;
					}
				 else
					{
					 strcpy(atom[i].xtype,"C.2 (pi>0)");
					 atom[i].valid=27;
					 continue;
					}
				}
			 else if(group.num_hetero==1)
				{
				 if(group.num_pi==0)
                                        {
                                         strcpy(atom[i].xtype,"C.2.x (pi=0)"); 
                                         atom[i].valid=28;
                                         continue;
                                        }
                                 else
                                        {
                                         strcpy(atom[i].xtype,"C.2.x (pi>0)");
                                         atom[i].valid=29;
                                         continue;
                                        }
				}
			 else
				{
				 if(group.num_pi==0)
                                        {
                                         strcpy(atom[i].xtype,"C.2.x2 (pi=0)"); 
                                         atom[i].valid=30;
                                         continue;
                                        }
                                 else
                                        {
                                         strcpy(atom[i].xtype,"C.2.x2 (pi>0)");
                                         atom[i].valid=31;
                                         continue;
                                        }
				}
			}
		 else	// a guess is better than nothing
			{
			 strcpy(atom[i].xtype,"C.2.unknown");
			 atom[i].valid=26;
			 continue;
			}
		}

	 if(!strcmp(atom[i].type,"C.cat"))
		{
		 strcpy(atom[i].xtype,"C.cat");
		 atom[i].valid=31;
		 continue;
		}

	 if(!strcmp(atom[i].type,"C.ar"))
		{
		 if(group.num_nonh==2)
			{
			 if(group.num_nar==0)
				{
				 strcpy(atom[i].xtype,"C.ar.h");
				 atom[i].valid=32;
				 continue;
				}
			 else 
				{
				 strcpy(atom[i].xtype,"C.ar.h.(X)");
				 atom[i].valid=33;
				 continue;
				}
			}
		 else if(group.num_nonh==3)
			{
			 if(group.num_nar==0)
				{
				 if(group.num_hetero==0)
					{
					 strcpy(atom[i].xtype,"C.ar");
					 atom[i].valid=34;
					 continue;
					}
				 else 
					{
					 strcpy(atom[i].xtype,"C.ar.x");
					 atom[i].valid=35;
					 continue;
					}
				}
			 else 
				{
				 if(group.num_hetero==0)
                                        {
                                         strcpy(atom[i].xtype,"C.ar.(X)");
                                         atom[i].valid=36;
					 continue;
                                        }
                                 else 
                                        {
                                         strcpy(atom[i].xtype,"C.ar.(X).x");
                                         atom[i].valid=37;
					 continue;
                                        }
				}
			}
		 else	// a guess is better than nothing
			{
			 strcpy(atom[i].xtype,"C.ar.unknown");
			 atom[i].valid=34;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"C.1"))
		{
		 if(group.db_type!=0)
			{
			 strcpy(atom[i].xtype,"C.1.==");
			 atom[i].valid=40;
			 continue;
			}
		 else if(group.num_nonh==1)
			{
			 strcpy(atom[i].xtype,"C.1.h");
			 atom[i].valid=38;
			 continue;
			}
		 else if(group.num_nonh==2)
			{
			 strcpy(atom[i].xtype,"C.1");
                         atom[i].valid=39;
                         continue;
			}
		 else	// a guess is better than nothing
			{
			 strcpy(atom[i].xtype,"C.1.unknown");
			 atom[i].valid=39;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"H"))
		{
		 strcpy(atom[i].xtype,"H");
		 atom[i].valid=0;
		 continue;
		}

	if(!strcmp(atom[i].type,"N.4")||!strcmp(atom[i].type,"N.3")||
	   !strcmp(atom[i].type,"N.pl3"))
		{
		 if(group.num_nonh==1)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"N.3.h2 (pi=0)");
					 strcpy(atom[i].hb,"D");
					 atom[i].valid=41;
					 continue;
					}
				 else 
					{
					 strcpy(atom[i].xtype,"N.3.h2 (pi=1)");
					 strcpy(atom[i].hb,"D");
					 atom[i].valid=42;
					 continue;
					}
				}
			 else 
				{
				 strcpy(atom[i].xtype,"N.3.h2.x");
				 strcpy(atom[i].hb,"D");
				 atom[i].valid=43;
				 continue;
				}
			}
		 else if(group.num_nonh==2)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"N.3.h (pi=0)");
					 strcpy(atom[i].hb,"D");
					 atom[i].valid=44;
					 continue;
					}
				 else if(group.num_pi==1)
					{
					 strcpy(atom[i].xtype,"N.3.h (pi>0)");
					 strcpy(atom[i].hb,"D");
					 atom[i].valid=45;
					 continue;
					}
				 else if(atom[i].ring==0)
					{
					 strcpy(atom[i].xtype,"N.3.h (pi>0)");
					 strcpy(atom[i].hb,"D");
					 atom[i].valid=45;
					 continue;
					}
				 else
					{
					 strcpy(atom[i].xtype,"N.3.h (ring)");
					 strcpy(atom[i].hb,"D");
					 atom[i].valid=46;
					 continue;
					}
				}
                         else 
                                {
				 if(group.num_pi<=1)
                                        {
                                         strcpy(atom[i].xtype,"N.3.h.x");
					 strcpy(atom[i].hb,"D");
                                         atom[i].valid=47;
                                         continue;
                                        }
                                 else if(atom[i].ring==0)
                                        {
                                         strcpy(atom[i].xtype,"N.3.h.x");
					 strcpy(atom[i].hb,"D");
                                         atom[i].valid=47;
					 continue;
                                        }
                                 else
                                        {
                                         strcpy(atom[i].xtype,"N.3.h.x (ring)");
					 strcpy(atom[i].hb,"D");
                                         atom[i].valid=48;
					 continue;
                                        }
                                }
			}
		 else if(group.num_nonh==3)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"N.3 (pi=0)");
					 atom[i].valid=49;
					 continue;
					}
				 else if(group.num_pi==1)
					{
					 strcpy(atom[i].xtype,"N.3 (pi>0)");
					 atom[i].valid=50;
					 continue;
					}
				 else if(atom[i].ring==0)
					{
					 strcpy(atom[i].xtype,"N.3 (pi>0)");
					 atom[i].valid=50;
					 continue;
					}
				 else
					{
					 strcpy(atom[i].xtype,"N.3 (ring)");
					 atom[i].valid=51;
					 continue;
					}
				}
                         else 
                                {
                                 if(group.num_pi<=1)
                                        {
                                         strcpy(atom[i].xtype,"N.3.x");
                                         atom[i].valid=52;
					 continue;
                                        }
                                 else if(atom[i].ring==0)
                                        {
                                         strcpy(atom[i].xtype,"N.3.x");
                                         atom[i].valid=52;
					 continue;
                                        }
                                 else
                                        {
                                         strcpy(atom[i].xtype,"N.3.x (ring)");
                                         atom[i].valid=53;
					 continue;
                                        }
                                }
			}
		 else if(group.num_nonh==4)
			{
			 strcpy(atom[i].xtype,"N.4.unknown");
                         atom[i].valid=43;
                         continue;
			}
		 else
			{
			 strcpy(atom[i].xtype,"N.3.unknown");
			 atom[i].valid=41;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"N.am"))
		{
		 if(group.num_nonh==1)
			{
			 strcpy(atom[i].xtype,"N.am.h2");
			 strcpy(atom[i].hb,"D");
			 atom[i].valid=54;
			 continue;
			}
		 else if(group.num_nonh==2)
			{
			 if(group.num_hetero==0)
				{
				 strcpy(atom[i].xtype,"N.am.h");
				 strcpy(atom[i].hb,"D");
				 atom[i].valid=55;
				 continue;
				}
			 else
				{
				 strcpy(atom[i].xtype,"N.am.h.x");
				 strcpy(atom[i].hb,"D");
				 atom[i].valid=56;
				 continue;
				}
			}
		 else if(group.num_nonh==3)
			{
			 if(group.num_hetero==0)
				{
				 strcpy(atom[i].xtype,"N.am");
				 atom[i].valid=57;
				 continue;
				}
			 else
				{
				 strcpy(atom[i].xtype,"N.am.x");
				 atom[i].valid=58;
				 continue;
				}
			}
		 else
			{
			 strcpy(atom[i].xtype,"N.am.unknown");
			 atom[i].valid=54;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"N.2"))
		{
		 if(group.db_type==1||group.db_type==4||group.db_type==5)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					strcpy(atom[i].xtype,"N.2.(=C) (pi=0)");
					atom[i].valid=59;
					continue;
					}
				 else
					{
					strcpy(atom[i].xtype,"N.2.(=C) (pi=1)");
                                        atom[i].valid=60;
                                        continue;
					}
				}
			 else
				{
				 if(group.num_pi==0)
                                      {
                                      strcpy(atom[i].xtype,"N.2.(=C).x (pi=0)");
                                      atom[i].valid=61;
                                      continue;
                                      }
                                 else
                                      {
                                      strcpy(atom[i].xtype,"N.2.(=C).x (pi=1)");
                                      atom[i].valid=62;
                                      continue;
                                      }
				}
			}
		 else if(group.db_type==2)
			{
			 if(group.num_hetero==0)
				{
				 strcpy(atom[i].xtype,"N.2.(=N)");
				 atom[i].valid=63;
				 continue;
				}
			 else
				{
				 strcpy(atom[i].xtype,"N.2.(=N).x");
				 atom[i].valid=64;
				 continue;
				}
			}
		 else if(group.db_type==3)
			{
			 if(group.num_nonh==2)
				{
				 strcpy(atom[i].xtype,"N.2.o");
				 atom[i].valid=65;
				 continue;
				}
			 else if(group.num_nonh==3) 
				{
				 strcpy(atom[i].xtype,"N.2.o2");
				 atom[i].valid=66;
				 continue;
				}
			 else 
				{
				 strcpy(atom[i].xtype,"N.2.o");
                                 atom[i].valid=65;
                                 continue;
				}
			}
		 else
			{
			 strcpy(atom[i].xtype,"N.2.unknown");
			 atom[i].valid=59;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"N.ar"))
		{
		 strcpy(atom[i].xtype,"N.ar");
                 atom[i].valid=67;
                 continue;
		}

	if(!strcmp(atom[i].type,"N.1"))
		{
		 strcpy(atom[i].xtype,"N.1");
		 strcpy(atom[i].hb,"A");
		 atom[i].valid=68;
		 continue;
		}

	if(!strcmp(atom[i].type,"O.3"))
		{
		 if(group.num_nonh==1)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"O.3.h (pi=0)");
					 strcpy(atom[i].hb,"DA");
					 atom[i].valid=69;
					 continue;
					}
				 else 
					{
					 strcpy(atom[i].xtype,"O.3.h (pi=1)");
					 strcpy(atom[i].hb,"DA");
					 atom[i].valid=70;
					 continue;
					}
				}
			 else 
				{
                                 strcpy(atom[i].xtype,"O.3.h.x");
			 	 strcpy(atom[i].hb,"DA");
                                 atom[i].valid=71;
				 continue;
				}
			}
		 else if(group.num_nonh==2)
			{
			 if(group.num_hetero==0)
				{
				 if(group.num_pi==0)
					{
					 strcpy(atom[i].xtype,"O.3 (pi=0)");
					 atom[i].valid=72;
					 continue;
					}
				 else
					{
					 strcpy(atom[i].xtype,"O.3 (pi>0)");
					 atom[i].valid=73;
					 continue;
					}
				}
			 else
				{
				 strcpy(atom[i].xtype,"O.3.x");
				 atom[i].valid=74;
				 continue;
				}
			}
		 else
			{
			 strcpy(atom[i].xtype,"O.3.unknown");
			 atom[i].valid=69;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"O.2")||!strcmp(atom[i].type,"O.co2"))
		{
		 strcpy(atom[i].xtype,"O.2");
		 strcpy(atom[i].hb,"A");
		 atom[i].valid=75;
		 continue;
		}

	if(!strcmp(atom[i].type,"S.3"))
		{
		 if(group.num_nonh==1)
			{
			 strcpy(atom[i].xtype,"S.3.h");
			 atom[i].valid=76;
			 continue;
			}
		 else if(group.num_nonh==2)
			{
			 strcpy(atom[i].xtype,"S.3");
			 atom[i].valid=77;
			 continue;
			}
		 else
			{
                 	 strcpy(atom[i].xtype,"S.3.unknown");
                 	 atom[i].valid=76;
			 continue;
			}
		}

	if(!strcmp(atom[i].type,"S.2"))
		{
		 strcpy(atom[i].xtype,"S.2");
		 atom[i].valid=78;
		 continue;
		}

	if(!strcmp(atom[i].type,"S.o"))
		{
		 strcpy(atom[i].xtype,"S.o");
		 atom[i].valid=79;
		 continue;
		}

	if(!strcmp(atom[i].type,"S.o2"))
		{
		 strcpy(atom[i].xtype,"S.o2");
                 atom[i].valid=80;
                 continue;
		}

	if(!strcmp(atom[i].type,"P.3"))
		{
		 if(group.db_type==3)
			{
		 	 strcpy(atom[i].xtype,"P.3.(=O)");
		 	 atom[i].valid=81;
		 	 continue;
			}
		 else if(group.db_type==4)
			{
			 strcpy(atom[i].xtype,"P.3.(=S)");
                         atom[i].valid=82;
                         continue;
			}
		 else
			{
			 strcpy(atom[i].xtype,"P.3.unknown");
                         atom[i].valid=81;
                         continue;
			}
		}

	if(!strcmp(atom[i].type,"F"))
		{
                 if(group.num_pi==0)
                        {
                         strcpy(atom[i].xtype,"F (pi=0)");
                         atom[i].valid=83;
			 continue;
                        }
                 else if(group.num_pi==1) 
                        {
                         strcpy(atom[i].xtype,"F (pi=1)");
                         atom[i].valid=84;
			 continue;
                        }
		 else
			{
			 strcpy(atom[i].xtype,"F.unknown");
                         atom[i].valid=83;
                         continue;
			}
		}

	if(!strcmp(atom[i].type,"Cl"))
		{
                 if(group.num_pi==0) 
                        {
                         strcpy(atom[i].xtype,"Cl (pi=0)");
                         atom[i].valid=85;
			 continue;
                        }
                 else if(group.num_pi==1) 
                        {
                         strcpy(atom[i].xtype,"Cl (pi=1)");
                         atom[i].valid=86;
			 continue;
                        }
		 else
                        {
                         strcpy(atom[i].xtype,"Cl.unknown");
                         atom[i].valid=85;
                         continue;
                        }
		}

	if(!strcmp(atom[i].type,"Br"))
		{
                 if(group.num_pi==0) 
                        {
                         strcpy(atom[i].xtype,"Br (pi=0)");
                         atom[i].valid=87;
			 continue;
                        }
                 else if(group.num_pi==1) 
                        {
                         strcpy(atom[i].xtype,"Br (pi=1)");
                         atom[i].valid=88;
			 continue;
                        }
		 else
			{
                         strcpy(atom[i].xtype,"Br.unknown");
                         atom[i].valid=87;
                         continue;
                        }
		}

	if(!strcmp(atom[i].type,"I"))
		{
		 if(group.num_pi==0) 
			{
			 strcpy(atom[i].xtype,"I (pi=0)");
			 atom[i].valid=89;
			 continue;
			}
		 else if(group.num_pi==1)
			{
			 strcpy(atom[i].xtype,"I (pi=1)");
			 atom[i].valid=90;
			 continue;
			}
		 else
			{
			 strcpy(atom[i].xtype,"I.unknown");
                         atom[i].valid=89;
                         continue;
			}
		}

	 strcpy(atom[i].xtype,"Unknown"); atom[i].valid=0;
	}

	return;
}

void Molecule::Factor_Detecting()
{
	factor[0].num=Count_Hydrophobic_Carbon();
	factor[1].num=Count_Internal_HBond();
        factor[2].num=Count_Halogen_1_3_Pair();
        factor[3].num=Count_Nar_1_4_Pair();
        factor[4].num=Count_O3_1_4_Pair();
        factor[5].num=Count_Acceptor_1_5_Pair();
        factor[6].num=Count_Remote_Donor_Pair();
        factor[7].num=Count_Amino_Acid();
        factor[8].num=Count_Salicylic_Acid();
        factor[9].num=Count_Sulfonic_Acid();

	return;
}

float Molecule::Count_Hydrophobic_Carbon()
{
	int i,num;

	num=0;

        for(i=0;i<num_atom;i++)
       		{
                 if(strcmp(atom[i].type,"C.3")&&
                    strcmp(atom[i].type,"C.2")) continue;
                 else if(Hydrophobic_Neighbor_Check(atom[i].id)
			 ==FALSE) continue;
                 else {num++; atom[i].logp+=cb[0];}
                }

	if(num>=10) num/=2;

        return (float)num;
}

float Molecule::Count_Amino_Acid()
{
	int i,j,tmp,num,mark;

	num=0;

	for(i=0;i<num_atom;i++)
		{
		 if(strcmp(atom[i].type,"O.2")) continue;

                 tmp=atom[i].neib[0]-1;
                 if(atom[tmp].type[0]!='C') continue;
                 else if(atom[tmp].ring!=0) continue;

		 mark=FALSE;

                 for(j=0;j<num_atom;j++)
                        {
                         if(i==j) continue;
                         else if(strcmp(atom[j].type,"O.3")) continue;
			 else if(strcmp(atom[j].hb,"DA")) continue;
                         else if(atom[j].ring!=0) continue;
                         else if(Connection_1_3_Check(atom[i].id, atom[j].id)
                                 ==FALSE) continue;
                         else {mark=TRUE; break;}
                        }

                 if(mark==FALSE) continue;

		 for(j=0;j<num_atom;j++)
			{
			 if(strcmp(atom[j].xtype,"N.3.h2 (pi=0)")) continue;
			 else if(Connection_1_4_Check(atom[i].id, atom[j].id)
				 ==FALSE) continue;
			 else 
				{
				 num++; 
				 atom[i].logp+=(cb[7]/2.0);
				 atom[j].logp+=(cb[7]/2.0);
				 break;
				}
			}

		 if(num!=0) break;

		 for(j=0;j<num_atom;j++)
			{
			 if(strcmp(atom[j].type,"N.ar")) continue;
			 else if(Connection_1_4_Check(atom[i].id, atom[j].id)
                                 ==FALSE) continue; 
			 else 
				{
                                 num++; 
                                 atom[i].logp+=(cb[7]/2.0);
                                 atom[j].logp+=(cb[7]/2.0);
                                 break;
                                }
			}

		 if(num!=0) break;
		}

	return (float)num;
}

float Molecule::Count_Internal_HBond()
{
	int i,j,mark1,mark2;
	float num;
	int *record;

	record=new int[num_atom];
	if(record==NULL) Memory_Allocation_Error();

	for(i=0;i<num_atom;i++) record[i]=0;

	num=0;

	for(i=0;i<num_atom;i++)
		{
		 if(strcmp(atom[i].hb,"D")&&strcmp(atom[i].hb,"DA")) continue;
		 else if(atom[i].ring!=0) continue; // not allowed in ring

		 if(Adjacent_Ring_Check(atom[i].id)==FALSE) mark1=FALSE;
		 else mark1=TRUE;

		 for(j=0;j<num_atom;j++)
			{
			 if(i==j) continue;
			 if(strcmp(atom[j].hb,"A")&&
			    strcmp(atom[j].hb,"DA")) continue;
			 else if(atom[j].ring!=0) continue; // not in ring 

			 if(Adjacent_Ring_Check(atom[j].id)==FALSE) mark2=FALSE;
			 else mark2=TRUE;

			 if(mark1==TRUE&&mark2==TRUE)
				{
				 if(Connection_1_4_Check(atom[i].id,
				    atom[j].id)==FALSE) continue;
				 else 
					{
					 record[i]++; record[j]++; 
					}
				}
			 else if(mark1==TRUE&&mark2==FALSE)
			 	{
				 if(Connection_1_5_Check(atom[i].id,
                                    atom[j].id)==FALSE) continue;
				 else
                                        {
                                         record[i]++; record[j]++;
                                        }
				}
			 else if(mark2==FALSE&&mark2==TRUE)
				{
				 if(Connection_1_5_Check(atom[i].id,
                                    atom[j].id)==FALSE) continue;
				 else
                                        {
                                         record[i]++; record[j]++;
                                        }
				}
			 else continue;
			}
		}

	for(i=0;i<num_atom;i++)
		{
		 if(record[i]==0) continue;
		 else
			{
			 num+=0.500;
			 atom[i].logp+=(cb[1]/2.0);
			}
		}

	delete [] record;

	return num;
}

float Molecule::Count_Nar_1_4_Pair()
{
        int i,j,num,tmp1,tmp2,tmp3,tmp4;

        num=0;

        for(i=0;i<num_atom-1;i++)
                {
                 if(strcmp(atom[i].type,"N.ar")) continue;

		 tmp1=atom[i].neib[0]; tmp2=atom[i].neib[1];

                 for(j=i+1;j<num_atom;j++)
                        {
                         if(strcmp(atom[j].type,"N.ar")) continue;
                         else if(Connection_1_4_Check(atom[i].id, atom[j].id)
                                 ==FALSE) continue;
                         else 
				{
				 tmp3=atom[j].neib[0]; tmp4=atom[j].neib[1];

				 if(Connection_1_2_Check(tmp1,tmp3)==TRUE)
					{
					 if(Connection_1_2_Check(tmp2,tmp4)
					    ==TRUE) 
						{
						 num++;
						 atom[i].logp+=(cb[3]/2.0);
						 atom[j].logp+=(cb[3]/2.0);
						}
					 else continue;
					}
				 else if(Connection_1_2_Check(tmp1,tmp4)==TRUE)
					{
					 if(Connection_1_2_Check(tmp2,tmp3)
					    ==TRUE) 
						{
						 num++;
						 atom[i].logp+=(cb[3]/2.0);
                                                 atom[j].logp+=(cb[3]/2.0);
                                                }
					 else continue;
					}
				 else continue;
				}
                        }
                }

        return (float)num;
}

float Molecule::Count_Halogen_1_3_Pair()
{
        int i,j;
	int num_c,num_x,num1,num2;

        num_c=num_x=num1=num2=0;

        for(i=0;i<num_atom;i++)
                {
                 if(!strcmp(atom[i].type,"F")) num_x++;
                 else if(!strcmp(atom[i].type,"Cl")) num_x++;
                 else if(!strcmp(atom[i].type,"Br")) num_x++;
                 else if(!strcmp(atom[i].type,"I")) num_x++;
                 else if(atom[i].type[0]=='C') num_c++;
                 else continue;
                }

        for(i=0;i<num_atom-1;i++)
                {
                 if(strcmp(atom[i].type,"F")) continue;

		 for(j=i+1;j<num_atom;j++)
                        {
                         if(strcmp(atom[j].type,"F")) continue;
                         else if(Connection_1_3_Check(atom[i].id,atom[j].id)
                                 ==FALSE) continue;
                         else 
				{
				 num1++; 
				 atom[i].logp+=(cb[2]/2.0);
				 atom[j].logp+=(cb[2]/2.0);
				 continue;
				}
                        }
                }

        for(i=0;i<num_atom-1;i++)
                {
                 if(strcmp(atom[i].type,"Cl")&&
                    strcmp(atom[i].type,"Br")&&
                    strcmp(atom[i].type,"I")) continue;

                 for(j=i+1;j<num_atom;j++)
                        {
                         if(strcmp(atom[j].type,"Cl")&&
                            strcmp(atom[j].type,"Br")&&
                            strcmp(atom[j].type,"I")) continue;
                         else if(Connection_1_3_Check(atom[i].id,atom[j].id)
                                 ==FALSE) continue;
                         else 
				{
				 num2++; 
				 atom[i].logp+=(cb[2]/2.0);
                                 atom[j].logp+=(cb[2]/2.0);
				 continue;
				}
                        }
                }

	if(num_c<=num_x) return num2;
	else return (float)(num1+num2);
}

float Molecule::Count_Acceptor_1_5_Pair()
{
        int i,j,num,tmp1,tmp2;

        num=0;

        for(i=0;i<num_atom-1;i++)
                {
                 // if(strcmp(atom[i].xtype,"O.2")) continue;
		 if(strcmp(atom[i].hb,"A")) continue;

		 tmp1=atom[i].neib[0]-1;
		 if(atom[tmp1].type[0]=='S') continue;
		 else if(atom[tmp1].type[0]=='P') continue;
		 else if(atom[tmp1].ring!=0) continue;

                 for(j=i+1;j<num_atom;j++)
                        {
                         // if(strcmp(atom[j].xtype,"O.2")) continue;
			 if(strcmp(atom[j].hb,"A")) continue;

			 tmp2=atom[j].neib[0]-1;
			 if(atom[tmp2].type[0]=='S') continue;
			 else if(atom[tmp2].type[0]=='P') continue;
			 else if(atom[tmp2].ring!=0) continue;

                         if(Connection_1_5_Check(atom[i].id, atom[j].id)
                                 ==FALSE) continue;
			 else 
				{
				 num++; 
				 atom[i].logp+=(cb[5]/2.0);
				 atom[j].logp+=(cb[5]/2.0);
				 continue;
				}
                        }
                }

        return (float)num;
}

float Molecule::Count_Remote_Donor_Pair()
{
	int i,j,num;
	int mark1,mark2,tmp1,tmp2;

	num=0;

	for(i=0;i<num_atom-1;i++)
		{
		 if(strcmp(atom[i].hb,"D")&&strcmp(atom[i].hb,"DA")) continue;
		 else if(atom[i].ring!=0) continue;

		 tmp1=Adjacent_Aromatic_Check(atom[i].id);
		 if(tmp1==FALSE) continue;

		 for(j=i+1;j<num_atom;j++)
			{
			 if(strcmp(atom[j].hb,"D")&&strcmp(atom[j].hb,"DA")) 
				continue;
			 else if(atom[j].ring!=0) continue;

			 tmp2=Adjacent_Aromatic_Check(atom[j].id);
			 if(tmp2==FALSE) continue;

			 mark1=Connection_Check(atom[i].id,atom[j].id);
			 if(mark1<=5) continue; 

			 tmp1--; tmp2--;
			 mark2=Connection_Check(atom[tmp1].id,atom[tmp2].id);
			 if(mark2<mark1) 
				{
				 num++;
				 atom[i].logp+=(cb[6]/2.0);
				 atom[j].logp+=(cb[6]/2.0); 
				}
			 else continue;
			}
		}

	return (float)num;
}

float Molecule::Count_Cyclic_Ester()
{
	int i,j,num,tmp;

	num=0;

	for(i=0;i<num_atom;i++)
		{
		 if(strcmp(atom[i].type,"O.2")) continue;

		 tmp=atom[i].neib[0]-1;
		 if(atom[tmp].type[0]!='C') continue;
		 if(atom[tmp].ring==0) continue;

		 for(j=0;j<num_atom;j++)
			{
			 if(i==j) continue;
			 else if(strcmp(atom[j].type,"O.3")) continue;
			 else if(atom[j].ring==0) continue;
			 else if(Connection_1_3_Check(atom[i].id, atom[j].id)
				 ==FALSE) continue;
			 else {num=1; break;}
			}

		 if(num!=0) break;
		}

	return (float)num;
}

float Molecule::Count_Salicylic_Acid()
{
	int i,j,num,tmp,mark;

	num=0;

	for(i=0;i<num_atom;i++)
		{
		 if(strcmp(atom[i].type,"O.2")) continue;

		 tmp=atom[i].neib[0]-1;
		 if(atom[tmp].type[0]!='C') continue;
		 else if(atom[tmp].ring!=0) continue;
		 else if(Adjacent_Aromatic_Check(atom[tmp].id)==FALSE) continue;

		 mark=FALSE;

		 for(j=0;j<num_atom;j++)
                        {
                         if(i==j) continue;
                         else if(strcmp(atom[j].type,"O.3")) continue;
                         else if(atom[j].ring!=0) continue;
                         else if(Connection_1_3_Check(atom[i].id, atom[j].id)
                                 ==FALSE) continue;
                         else {mark=TRUE; break;}
                        }

		 if(mark==FALSE) continue;

		 mark=FALSE;

		 for(j=0;j<num_atom;j++)
			{
			 if(i==j) continue;
			 else if(strcmp(atom[j].type,"O.3")) continue;
			 else if(strcmp(atom[j].hb,"DA")) continue;
			 else if(Adjacent_Aromatic_Check(atom[j].id)==FALSE) 
				continue;
			 else if(Connection_1_5_Check(atom[i].id, atom[j].id)
				 ==FALSE) continue;
			 else 
				{
				 num++;
				 atom[i].logp+=(cb[8]/2.0);
				 atom[j].logp+=(cb[8]/2.0);
				 break;
				} 
			}

		 if(num!=0) break;
		}

	return (float)num;
}

float Molecule::Count_O3_1_4_Pair()
{
	int i,j;
	float num;
	int *record;

	record=new int[num_atom];
	if(record==NULL) Memory_Allocation_Error();

	for(i=0;i<num_atom;i++) record[i]=0;

	num=0;

	for(i=0;i<num_atom-1;i++)
		{
		 if(strcmp(atom[i].type,"O.3")) continue;
		 else if(strcmp(atom[i].hb,"N")) continue;
		 else if(atom[i].ring!=0) continue;
		 else if(Adjacent_Aromatic_Check(atom[i].id)==FALSE) continue;

		 for(j=i+1;j<num_atom;j++)
			{
			 if(strcmp(atom[j].type,"O.3")) continue;
			 else if(strcmp(atom[j].hb,"N")) continue;
			 else if(atom[j].ring!=0) continue;
                 	 else if(Adjacent_Aromatic_Check(atom[j].id)==FALSE) 
				continue;
			 else if(Connection_1_4_Check(atom[i].id,atom[j].id)
				 ==FALSE) continue;
			 else 
				{
				 record[i]++; record[j]++; 
				}
			}
		}

	for(i=0;i<num_atom;i++)
		{
		 if(record[i]==0) continue;
		 else
			{
			 num+=0.500;
			 atom[i].logp+=(cb[4]/2.0);
			}
		}

	delete [] record;

	return (float)num;
}

float Molecule::Count_Sulfonic_Acid()
{
	int i,j,num,tmp1,tmp2;

	num=0;

	for(i=0;i<num_atom;i++)
		{
		 if(strcmp(atom[i].type,"S.o2")) continue;
		 else if(atom[i].ring!=0) continue;

		 tmp1=Adjacent_Aromatic_Check(atom[i].id);
		 if(tmp1==FALSE) continue;

		 for(j=0;j<num_atom;j++)
			{
			 if(strcmp(atom[j].xtype,"N.3.h2 (pi=1)")) continue;

			 tmp2=Adjacent_Aromatic_Check(atom[j].id);
			 if(tmp2==FALSE) continue;

			 if(Connection_1_6_Check(atom[i].id,atom[j].id)
				 ==FALSE) continue;
			 else if(Connection_1_4_Check(tmp1,tmp2)
				 ==FALSE) continue;
			 else 
				{
				 num++; 
				 atom[i].logp+=(cb[9]/2.0);
				 atom[j].logp+=(cb[9]/2.0);
				 break;
				}
			}
		}

	return (float)num;
}

void Molecule::Get_Coefficients()
{
	// coefficients for 90 atom types

	ca[0] =  0.528;	 // C.3.h3 (pi=0)
	ca[1] =  0.267;  // C.3.h3 (pi=1)
	ca[2] = -0.032;  // C.3.h3.x
	ca[3] =  0.358;  // C.3.h2 (pi=0)
	ca[4] = -0.008;  // C.3.h2 (pi=1)
	ca[5] = -0.185;  // C.3.h2 (pi=2)
	ca[6] = -0.137;  // C.3.h2.x (pi=0)
	ca[7] = -0.303;  // C.3.h2.x (pi=1)
	ca[8] = -0.815;  // C.3.h2.x (pi=2)
	ca[9] =  0.127;  // C.3.h (pi=0)
	ca[10]= -0.243;  // C.3.h (pi=1)
	ca[11]= -0.499;  // C.3.h (pi>1)
	ca[12]= -0.205;  // C.3.h.x (pi=0)
	ca[13]= -0.305;  // C.3.h.x (pi=1)
	ca[14]= -0.709;  // C.3.h.x (pi>1)
	ca[15]= -0.006;  // C.3 (pi=0)
	ca[16]= -0.570;  // C.3 (pi=1)
	ca[17]= -0.317;  // C.3 (pi>1)
	ca[18]= -0.316;  // C.3.x (pi=0)
	ca[19]= -0.723;  // C.3.x (pi>0)
	ca[20]=  0.420;  // C.2.h2
	ca[21]=  0.466;  // C.2.h (pi=0)
	ca[22]=  0.136;  // C.2.h (pi=1)
	ca[23]=  0.001;  // C.2.h.x (pi=0)
	ca[24]= -0.310;  // C.2.h.x (pi=1)
	ca[25]=  0.050;  // C.2 (pi=0)
	ca[26]=  0.013;  // C.2 (pi>0)
	ca[27]= -0.030;  // C.2.x (pi=0)
	ca[28]= -0.027;  // C.2.x (pi>0)
	ca[29]=  0.005;  // C.2.x2 (pi=0)
	ca[30]= -0.315;  // C.2.x2 (pi>0)
	ca[31]=  0.337;  // C.ar.h
	ca[32]=  0.126;  // C.ar.h.(N)
	ca[33]=  0.296;  // C.ar
	ca[34]= -0.151;  // C.ar.x
	ca[35]=  0.174;  // C.ar.(N)
	ca[36]=  0.366;  // C.ar.(N).x
	ca[37]=  0.209;  // C.1.h
	ca[38]=  0.330;  // C.1
	ca[39]=  2.073;  // C.1.==
	ca[40]= -0.534;  // N.3.h2 (pi=0)
	ca[41]= -0.329;  // N.3.h2 (pi=1)
	ca[42]= -1.082;  // N.3.h2.x
	ca[43]= -0.112;  // N.3.h (pi=0)
	ca[44]=  0.166;  // N.3.h (pi>0)
	ca[45]=  0.545;  // N.3.h (ring)
	ca[46]=  0.324;  // N.3.h.x
	ca[47]=  0.153;  // N.3.h.x (ring)
	ca[48]=  0.159;  // N.3 (pi=0)
	ca[49]=  0.761;  // N.3 (pi>0)
	ca[50]=  0.881;  // N.3 (ring)
	ca[51]= -0.239;  // N.3.x
	ca[52]= -0.010;  // N.3.x (ring)
	ca[53]= -0.646;  // N.am.h2
	ca[54]= -0.096;  // N.am.h
	ca[55]= -0.044;  // N.am.h.x
	ca[56]=  0.078;  // N.am
	ca[57]= -0.118;  // N.am.x
	ca[58]=  0.007;  // N.2.(=C) (pi=0)
	ca[59]= -0.275;  // N.2.(=C) (pi=1)
	ca[60]=  0.366;  // N.2.(=C).x (pi=0)
	ca[61]=  0.251;  // N.2.(=C).x (pi=1)
	ca[62]=  0.536;  // N.2.(=N)
	ca[63]= -0.597;  // N.2.(=N).x
	ca[64]=  0.427;  // N.2.o
	ca[65]=  1.178;  // N.2.o2
	ca[66]= -0.493;  // N.ar
	ca[67]= -0.566;  // N.1
	ca[68]= -0.467;  // O.3.h (pi=0)
	ca[69]=  0.082;  // O.3.h (pi=1)
	ca[70]= -0.522;  // O.3.h.x
	ca[71]=  0.084;  // O.3 (pi=0)
	ca[72]=  0.435;  // O.3 (pi>0)
	ca[73]=  0.105;  // O.3.x
	ca[74]= -0.399;  // O.2
	ca[75]=  0.419;  // S.3.h
	ca[76]=  0.255;  // S.3
	ca[77]= -0.148;  // S.2
	ca[78]= -1.375;  // S.o
	ca[79]= -0.168;  // S.o2
	ca[80]= -0.447;  // P.3.(=O)
	ca[81]=  1.253;  // P.3.(=S)
	ca[82]=  0.375;  // F (pi=0)
	ca[83]=  0.202;  // F (pi=1)
	ca[84]=  0.512;  // Cl (pi=0)
	ca[85]=  0.663;  // Cl (pi=1)
	ca[86]=  0.850;  // Br (pi=0)
	ca[87]=  0.839;  // Br (pi=1)
	ca[88]=  1.050;  // I (pi=0)
	ca[89]=  1.109;  // I (pi=1)

	// coefficients for 10 correction factors

	cb[0]=  0.211; 	// Hydrophobic carbon
	cb[1]=  0.429;  // Internal H-bond
	cb[2]=  0.137;  // X_X_1_3
	cb[3]=  0.485;  // Nar_Nar_1_4
	cb[4]= -0.268;  // O3_O3_1_4
	cb[5]=  0.580;  // A_A_1_5
	cb[6]= -0.423;  // D_D_remote
	cb[7]= -2.166;  // Alpha amino acid
	cb[8]=  0.554;  // Salicylic acid
	cb[9]= -0.501;  // Sulfonic acid

	// assign descriptions for correction factors

	strcpy(factor[0].symbol,"Hydrophobic carbon");
	strcpy(factor[1].symbol,"Internal H-bond");
	strcpy(factor[2].symbol,"Halogen 1-3 pair");
	strcpy(factor[3].symbol,"Aromatic nitrogen 1-4 pair");
        strcpy(factor[4].symbol,"Ortho sp3 oxygen pair");
        strcpy(factor[5].symbol,"Acceptor 1-5 pair");
	strcpy(factor[6].symbol,"Paralleled donor pair");
        strcpy(factor[7].symbol,"Alpha amino acid");
        strcpy(factor[8].symbol,"Salicylic acid");
	strcpy(factor[9].symbol,"P-amino sulfonic acid");

	return;
}

void Molecule::Calculate_LogP()
{
	int i;

	logp=0.000;

	for(i=0;i<num_atom;i++)
		{
		 if(atom[i].valid==0) continue;
		 else 
			{
			 atom[i].logp+=ca[atom[i].valid-1];
			 logp+=ca[atom[i].valid-1];
			}
		}

	for(i=0;i<10;i++) logp+=(factor[i].num*cb[i]);

	return;
}

