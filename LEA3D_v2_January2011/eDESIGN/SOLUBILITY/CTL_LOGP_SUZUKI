 ***************************************************
 *                                                 *
 *                                                 *
 *            C H E M I C A L C - 2                *
 *                                                 *
 *                                                 *
 *      Log P & Log 1/S  Estimation Program        *
 *                                                 *
 *                                                 *
 *                                                 *
 *                 Ver. 1.0  (1991)                *
 *                                                 *
 ***************************************************
 This version is capable of estimating both log Pand
 the aqueous solubility from the structure.         
 The compounds containing C, H, N, O, S, or halogen 
 can be handled.                                    
                                                    
 Choose the mode for inputting structural formula.  
  MODE 1 => Input "1" 
  MODE 2 =>       "2" 
  MODE 3 =>       "3" 
          No.  =>  ? 
 The total number of atoms =   7
  < Structural Formula > 
   1  2  3  4  5  6  7
   C  C  C  C  C  C  O  
0  1- 2- 3- 4- 5- 6- 1
0  6- 7
                 
  Any change  ?  
  0 : No change. 
  1 : Element - C,O,N,S,F,Cl,Br,I 
             or CN,CO,CS,NO,NO2,NCS,SO,SO2 
  2 : Bond.      
  3 : Total atoms.
        No. =>  ? 
   All for the structure.  
 This estimation is based on the group-contribution 
 method proposed by Suzuki and Kudo [J. Comp.-Aided 
 Mol. Design, 4(1990)155-198 ].      Note that this 
 version  covers only  the calculation using  Basic 
 Group Set.  You should be aware of the propagation 
 of errors if you calculate  a structurally complex 
 molecule.                                         
                                                   
 ----<GROUP>------------------------------------------
  GROUP( 1) = CH2 -(C)(C) --(O)                                           
  GROUP( 2) = CH2 -(C)(C)                                                 
  GROUP( 3) = CH2 -(C)(C)                                                 
  GROUP( 4) = CH2 -(C)(C)                                                 
  GROUP( 5) = CH2 -(C)(C) --(O)                                           
  GROUP( 6) = CH -(O)(C)(C)                                               
  GROUP( 7) = OH -(C)                                                     
 -----------------------------------------------------
          Contribution  Frequency  Compounds
 Gi( 1) =   0.536          467        198
 Gi( 2) =   0.536          467        198
 Gi( 3) =   0.536          467        198
 Gi( 4) =   0.536          467        198
 Gi( 5) =   0.536          467        198
 Gi( 6) =  -0.087           18         17
 Gi( 7) =  -1.287           91         76
                                  
  ****  Estimated log P Value *****
     log P(octanol/water)=   1.306
  *********************************
 Do you want to calculate Aqueous Solubility ?
 Yes/No  IF Yes =>  Input "1" 
            No  =>        "0" 
                   No. =>  ? 
   Aqueous solubility is estimated via two pathways.
   The first is based  on  the combined handling of 
 two available group contribution methods of Irmann 
 [Chem.Ing.Tech., 37(1965)789-798] and Wakita et al.
 [Chem. Pharm. Bull., 34(1986)4663-4681].  According
 to the  compound  type,  this  program  selects  an
 appropriate estimetion method.  The molecules whose
 log 1/S values are not  available by  both  methods
 can be  estimated  by the zero-th order additivity 
 scheme.                                           
    The second is based on the following regression 
 equation that correlate the aqueous solubility with
 the 1-octanol/water partition coefficient.        
 [T.Suzuki, J. Comput.-Aided Mol. Design, 5(1991)149
 -166]                                             
                                                   
 log 1/S =1.050logP + 0.00956(Tm-25) + 0.515 [S:mol/L]
                                                   
 If the compound is Solid at 25C;
                     ====> Input Melting Point in C.
 If Liquid or Melting Point is unavailable;
                     ====> Input 0 (zero).
 (In such case log 1/S of the supercooled liquid will
  be estimated.)
           
  Tm ?  => 
                                 
  < W-Y-M-W Method >             
                             Contribution(mol/L)
 GROUP( 1) = CH2                    0.61
 GROUP( 2) = CH2                    0.61
 GROUP( 3) = CH2                    0.61
 GROUP( 4) = CH2                    0.61
 GROUP( 5) = CH2                    0.61
 GROUP( 6) = CH                     0.49
 GROUP( 7) = OH-(Csec)             -2.82
 Aliphatic ring                 1(-0.40)
 Aliphatic branch                1(-0.10)
                                       
  *** Estimated log 1/S Value ********* 
   log 1/S =   0.22    (S: mol/L)
  ************************************* 
                               
  < Estimation of S from Log P > 
  *** Estimated log 1/S value using Reg. Eq. *****
   log 1/S =   0.86    (S: mol/L) 
  ************************************************
                                            
 Do you want to estimate log P in other solvent/water sstems (Y/N)? 
 Do you want to calculate next molecule ?
 Yes/No  IF Yes =>  Input "1" 
            No  =>        "0" 
                   No. =>  ? 
