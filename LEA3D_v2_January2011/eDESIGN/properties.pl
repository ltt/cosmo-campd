#!/usr/bin/perl


$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

	local($filesdf,$optiondipole,$param_charges,$optionsolubility)=@ARGV;
	if($filesdf eq "" || $optiondipole eq ""){
		die "usage: prop <sdf file> <Dipole ? (0/1)> <Charge by AM1 or GAS> <Solubility ? (0/1) only if <=100 atoms>\n";
	};	


	# The default van der Waals radii are taken from A. Bondi (1964) "van der Waals Volumes and Radii" J.Phys.Chem. 68, 441-451
	#If an element does not appear in the table it is assigned a value of 2.0?
	
        #   Ag  1.72      Ar  1.88     As  1.85     Au  1.66
	#   Br  1.85      C   1.70     Cd  1.58     Cl  1.75
	#   Cu  1.40      F   1.47     Ga  1.87     H   1.20
	#   He  1.40      Hg  1.55     I   1.98     In  1.93
	#   K   2.75      Kr  2.02     Li  1.82     Mg  1.73
	#   N   1.55      Na  2.27     Ne  1.54     Ni  1.63
	#   O   1.52      P   1.80     Pb  2.02     Pd  1.63
	#   Pt  1.72      S   1.80     Se  1.90     Si  2.10
	#   Sn  2.17      Te  2.06     Tl  1.96     U  1.86
	#   Xe  2.16      Zn  1.39
	
        %tabR=(
                'C',1.70,
                'O',1.52,
		'N',1.55,
		'S',1.80,
		'P',1.80,
		'Br',1.85,
		'Cl',1.75,
		'I',1.98,
		'F',1.47,
		'H',1.2,
		'Hp',1.1,
		'Si',2.10,
	);
	
	%tabmm=(
		'C',12.011,
		'O',15.999,
		'N',14.007,
		'S',32.065,
		'P',30.974,
		'Br',79.904,
		'Cl',35.453,
		'I',126.9,
		'F',18.998,
		'H',1.0079,
		'Si',28.0855
	);

        %prop=(
                'psa',0.0,
                'volume',0.0,
                'area',0.0,
                'mm',0.0,
                'logp',0.0,
                'mr',0.0,
                'rg',0.0,
                'ix',0.0,
                'iy',0.0,
                'iz',0.0,
                'length',0.0,
                'nbhd',0.0,
                'nbha',0.0,
                'nbatom',0.0,
                'dipole',0.0,
                'rot',0.0,
                'nring',0.0,
                'nringa',0.0,
		'complexity',0.0,
		'solubility',0.0,
		'lipinski',0.0,
        );

	%param=(
		'WORKDIR',"",
		'CHARGES',"",
		'VERBOSITY',"",
	);
	$param{WORKDIR}=".";
	if($param_charges eq "AM1"){
		$param{CHARGES}="AM1";
	}
	else{
		$param{CHARGES}="GAS";
	};	
	$param{VERBOSITY}="1";

        # Drug like molecules: Lipinski rules + PSA
	$lip_psa=140;
	$lip_mw=500;
	$lip_nbah=10;
	$lip_nbdh=5;
	$lip_atom=50;
	$lip_logp=6;
	                                                 	
	$property="psa volume area mm logp mr rg ix iy iz length nbhd nbha nbatom nring nringa rot complexity lipinski";

	$exedipole=0;
	if($optiondipole == 1){
		$property=$property." dipole";	
		$exedipole=1;		
	};

	$exesolubility=0;
	if($optionsolubility == 1){
		$property=$property." solubility";
		$exesolubility=1;
	};

	@fprop=split(" ",$property);
	
	#print  "MOLECULAR PROPERTIES :\n";
	open(PPT,">properties.out");
		print PPT "#ID class ";
		foreach $propi (0..@fprop-1){
			print PPT " $fprop[$propi] ";
		};
		print PPT "\n";


	##### PROPRIETES FROM SDF
	
	$flagnew=1;
	$moli=0;
	open(MOL,"<$filesdf");
	while(<MOL>){
		if($flagnew){
			$istratom=0;
			$masse=0;
			$flagname=0;
                        $compt=0;
                        $ig=1;
                        $jg=0;
                        @strx='';
                        @stry='';
                        @strz='';
                        @atom='';
                        @coval='';
                        @fonc='';
                        @ifonc='';
                        @covfonc='';
                        $blanc=' ';
                        @radius='';
                        $atomlourd=0;
                        $flagnew=0;
                        $setchg=0;
			$namegeneric="";
			$namemdl="";
			unlink "tmpi.sdf" if(-e "tmpi.sdf");
			open(OUT,">tmpi.sdf");
                };
                @getstr = split(' ',$_);
		print OUT $_;
		
                $compt++;
                if (($compt > 4) && ($ig <= $istratom)){
                        $strx[$ig]=$getstr[0];
                        $stry[$ig]=$getstr[1];
                        $strz[$ig]=$getstr[2];
                        $atom[$ig]=$getstr[3];
                        $atomlourd ++ if($getstr[3] ne 'H');
                        $radius[$ig]=$tabR{$getstr[3]};
                        $masse=$masse+$tabmm{$getstr[3]};
                        $ig++;
                };

                if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
                        if ($jg == 0){
                                $jg++;
                        }
                        else{
                               @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller==4){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3];
                                        };
                                }
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };

                                $fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
                                $ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
                                $covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
                                $coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

                                $fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
                                $ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
                                $covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
                                $coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];

                                $jg++;
                        };
                };
                if ($compt == 4){
                        $istratom=$getstr[0];
                        $istrbond=$getstr[1];

                        @coller=split(' *',$istratom);
                        if(@coller>3 && @coller==6){
                                $istratom=$coller[0].$coller[1].$coller[2];
                                $istrbond=$coller[3].$coller[4].$coller[5];
                        }
                        elsif(@coller>3 && @coller==5){
                                if($_=~/^\s/){
                                        $istratom=$coller[0].$coller[1];
                                        $istrbond=$coller[2].$coller[3].$coller[4];
                                }
                                else{
                                        $istratom=$coller[0].$coller[1].$coller[2];
                                        $istrbond=$coller[3].$coller[4];
                                };
                        };

                        #$compt++;
                };
		if($flagname && $getstr[0] ne ''){
			$namemdl=$getstr[0] if($mdl);
			$namegeneric=$getstr[0] if($name);
			$mdl=0;
			$name=0;
			$flagname=0;
		};
		if ($_=~/^>/ && ($_=~/MDLNUMBER/ || $_=~/ZINC/ || $_=~/name/ || $_=~/NAME/)){
			$flagname=1;
			$mdl=1 if($_=~/MDLNUMBER/ || $_=~/ZINC/);
			$name=1 if($_=~/name/ || $_=~/NAME/);
		};	
                if ($_=~/\$\$\$\$/){
			close(OUT);
	
			$f4="tmpi.sdf";
			chop($tmpmol2 = `$leaexe/SDF_MOL2.pl tmpi.sdf ` );
			rename "tmpi_1.mol2", "tmpi.mol2";
			$f3="tmpi.mol2";

			$moli++;	
			
                        $flagnew=1;
			#print "$moli atomes = $istratom\n";

			# pb with XLOGP in c and SOLUBULITY in fortan if too much atoms
			#print "Warning $f4 has $istratom atoms (>150 atoms): skipped propperties !\n" if($istratom >= 100);
		       	#if($istratom < 150){	
			if($istratom > 0){
				&calprop;
			};	

			if($namemdl ne ""){
                       	 	print PPT " $namemdl $filesdf ";
			}
			else{
				$namegeneric="$moli" if($namegeneric eq "");
				print PPT " $namegeneric $filesdf ";
			};

        		$prop{lipinski}=1;
        		if(($prop{psa} > $lip_psa ) || ($prop{mm} >$lip_mw) || ($prop{nbha} >$lip_nbah) || ($prop{nbhd} >$lip_nbdh) || ($prop{nbatom} > $lip_atom) || ($prop{logp} > $lip_logp )){
	                	$prop{lipinski}=0;
		        };
			
                        foreach $propi (0..@fprop-1){
				$prop{$fprop[$propi]}="0.0" if($prop{$fprop[$propi]} eq "");
                        	print PPT " $prop{$fprop[$propi]} ";
                        };
                        print PPT "\n";

			unlink "tmpi.sdf";
			unlink "tmpi.mol2";
			unlink "chargetmpi.mol2";
                };

                if($_=~/^M CHG/ || $_=~/^M  CHG/){
                        @getmoli=split(' ',$_);
                        $countchg=4;
                        foreach $setchgi (4..@getmoli-1){
                                if($setchgi==$countchg && $getmoli[$setchgi] ne ""){
                                        $setchg=$setchg+$getmoli[$setchgi];
                                        $countchg=$countchg+2;
                                };
                        };
                };
        };
        close(MOL);

        close(PPT);



################################################################################
#
sub calprop{

	$exepsa=1;
	$exevolume=1;
	$exearea=1;
	$exemm=1;
	$exelogp=0; #LF, da Fehler und nicht required
	$exemr=1;
	$exerg=1;
	$exeix=1;
	$exeiy=1;
	$exeiz=1;
	$exelength=1;
	$exenbhd=1;
	$exenbha=1;
	$exenbatom=1;
	$exerot=1;
	$exenring=1;
	$exenringa=1;
	$execomplexity=1;
	
#================================= Surface Polaire = $surfpolaire
if($exepsa || $exevolume || $exearea || $exelipinski){
        unlink "psa.out";
        open(GP,">psa.in");
                printf GP "* XYZR\n";
                printf GP "%8s\n",$istratom;
                foreach $ig (1..$istratom){

                        $radius[$ig]=$tabR{Hp} if ($atom[$ig] eq 'H' && ($fonc[$ig]=~/ 1-O / || $fonc[$ig]=~/ 1-N /));
                        $strx[$ig]= sprintf "%5.4f",$strx[$ig];
                        $stry[$ig]= sprintf "%5.4f",$stry[$ig];
                        $strz[$ig]= sprintf "%5.4f",$strz[$ig];
                        printf GP"%9s%1s%9s%1s%9s%1s%6s%1s%6s\n",$strx[$ig],$blanc,$stry[$ig],$blanc,$strz[$ig],$blanc,$radius[$ig],$blanc,$atom[$ig];

                };

        close(GP);

	chop($tmplog=`$leaexe/ncs_linux $param{WORKDIR}/psa.in`);
	#system("$leaexe/ncs_linux $param{WORKDIR}/psa.in");

        $surfpolaire=0;
        $area=0;
        $volume=0;

	open(IN2,"<psa.out");
        while(<IN2>){
                @get=split(' ',$_);
                if($get[1] eq 'ATOM'){
                        if($atom[$get[0]] eq 'N' || $atom[$get[0]] eq 'O'){
                                $surfpolaire=$surfpolaire+$get[7];
                        };
                        if($atom[$get[0]] eq 'H' && ($fonc[$get[0]]=~/ 1-O / || $fonc[$get[0]] =~ /1-N /)){     
                                $surfpolaire=$surfpolaire+$get[7];
                        };
                }
                elsif ($get[0] eq 'area'){
                        $area=$get[2];
                }
                elsif ($get[0] eq 'volume'){
                        $volume=$get[2];
                };
        };
        close(IN2);

        $prop{psa}=$surfpolaire;
        $prop{area}=$area;
        $prop{volume}=$volume;

	unlink "psa.in";
	unlink "psa.out";
};

#================================= Complexity
if($execomplexity){
	chop($complexity = `$leaexe/complexity.pl $f4 | awk '{print $1}' ` );
	$complexity=~s/(.*)=(.*)/$1/;
	#print "complexity = $complexity\n";
	$prop{complexity}=$complexity;
};	
	
#================================= Masse
#       $masse
if($exemm || $exelipinski){
        $prop{mm}=$masse;
	#print "MM = $masse\n" if($param{VERBOSITY} >= 1);
};

#================================= Nbatom
if($exenbatom || $exelipinski){
        $prop{nbatom}=$atomlourd;
	#print "NB atom = $atomlourd\n" if($param{VERBOSITY} >= 1);
};

#================================= Rayon de giration

if($exeix || $exeiy || $exeiz || $exerg){
        $rgx=0;
        $rgy=0;
        $rgz=0;
        foreach $ig (1..$istratom){
                $rgx=$rgx+$strx[$ig]*$tabmm{$atom[$ig]};
                $rgy=$rgy+$stry[$ig]*$tabmm{$atom[$ig]};
                $rgz=$rgz+$strz[$ig]*$tabmm{$atom[$ig]};
        };

#center of mass:
        $rgx=$rgx/$masse;
        $rgy=$rgy/$masse;
        $rgz=$rgz/$masse;

        $rayongyration = 0;
        $raygx=0;
        $raygy=0;
        $raygz=0;
        $rayg=0;

        foreach $ig (1..$istratom){
                $raygx=$rgx-$strx[$ig];
                $raygx=$raygx*$raygx;
                $raygy=$rgy-$stry[$ig];
                $raygy=$raygy*$raygy;
                $raygz=$rgz-$strz[$ig];
                $raygz=$raygz*$raygz;
                $rayg=sqrt($raygx+$raygy+$raygz);
                $rayg=$rayg*$rayg;
                $rayongyration = $rayongyration + $rayg;
        };
        $rayongyration = (sqrt($rayongyration))/($istratom+1);
        $prop{rg}=sprintf"%4.2f",$rayongyration;
};

#================================= Length and Axes Inertie Ix, Iy, Iz

$oldfashion=0;

if($exelength && $oldfashion==0){
#length
	$biglength=-1;
	$strlengthimin="";
	$strlengthimax="";
	foreach $ig (1..$istratom){
		foreach $im ($ig+1..$istratom){
			$length=sqrt(($strx[$ig]-$strx[$im])**2 + ($stry[$ig]-$stry[$im])**2 + ($strz[$ig]-$strz[$im])**2);
			if($length > $biglength){
				$biglength=$length;
				$strlengthimin=$ig;
				$strlengthimax=$im;
			};
		};	
	};
	#print "length $strlengthimin -> $strlengthimax = $biglength\n";
	$prop{length}=sprintf"%4.2f",$biglength;
};


if(($exeix || $exeiy || $exeiz) && $oldfashion==0){

#diagonalized components Ixx, Iyy and Izz of the moment of inertia tensor

	#center of mass is calculated above ($rgx, $rgy, $rgz)
	$ixx=0;
	$iyy=0;
	$izz=0;
	foreach $ig (1..$istratom){
		$ixx=$tabmm{$atom[$ig]}*(($rgy-$stry[$ig])*($rgy-$stry[$ig]) + ($rgz-$strz[$ig])*($rgz-$strz[$ig]));
		$iyy=$tabmm{$atom[$ig]}*(($rgx-$strx[$ig])*($rgx-$strx[$ig]) + ($rgz-$strz[$ig])*($rgz-$strz[$ig]));
		$izz=$tabmm{$atom[$ig]}*(($rgx-$strx[$ig])*($rgx-$strx[$ig]) + ($rgy-$stry[$ig])*($rgy-$stry[$ig]));
	};

	if($ixx < $iyy){
		$itmp=$iyy;
		$iyy=$ixx;
		$ixx=$itmp;
	};
	if($ixx < $izz){
		$itmp=$izz;
		$izz=$ixx;
		$ixx=$itmp;
	};
	if($iyy < $izz){
		$itmp=$iyy;
		$iyy=$izz;
		$izz=$itmp;
	};

	#inverse for mimicking paper of akritopoulou-zanze et al, DDT, 12, 948-952
	$itmp=$ixx;	
	$ixx=$izz;
	$izz=$itmp;

	#Normalized for mimicking paper of akritopoulou-zanze et al, DDT, 12, 948-952
	$prop{ix}=sprintf"%4.2f",$ixx/$izz;
	$prop{iy}=sprintf"%4.2f",$iyy/$izz;
	$prop{iz}=sprintf"%4.2f",$izz/$izz;

}
elsif(($exeix || $exeiy || $exeiz || $exelength) && $oldfashion==1){

        $strlongxmin=$strx[1];
        $strlongxmax=$strx[1];
        $strlongymin=$stry[1];
        $strlongymax=$stry[1];
        $strlongzmin=$strz[1];
        $strlongzmax=$strz[1];

        $strlengthiminx=1;
        $strlengthimaxx=1;
        $strlengthiminy=1;
        $strlengthimaxy=1;
        $strlengthiminz=1;
        $strlengthimaxz=1;

        foreach $longxk (2..$istratom){
                if ($atom[$longxk] ne 'H'){
                        if ($strx[$longxk] > $strlongxmax){
                                $strlongxmax=$strx[$longxk];
                                $strlengthimaxx=$longxk;
                        };
                        if ($strx[$longxk] < $strlongxmin){
                                $strlongxmin=$strx[$longxk];
                                $strlengthiminx=$longxk;
                        };
                        if ($stry[$longxk] > $strlongymax){
                                $strlongymax=$stry[$longxk];
                                $strlengthimaxy=$longxk;
                        };
                        if ($stry[$longxk] < $strlongymin){
                                $strlongymin=$stry[$longxk];
                                $strlengthiminy=$longxk;
                        };
                        if ($strz[$longxk] > $strlongzmax){
                                $strlongzmax=$strz[$longxk];
                                $strlengthimaxz=$longxk;
                        };
                        if ($strz[$longxk] < $strlongzmin){
                                $strlongzmin=$strz[$longxk];
                                $strlengthiminz=$longxk;
                        };
                };
        };
        $longueurx=$strlongxmax - $strlongxmin;
        $longueurx=$longueurx*$longueurx;
        $longueurx=sqrt($longueurx);

        $longueury=$strlongymax - $strlongymin;
        $longueury=$longueury*$longueury;
        $longueury=sqrt($longueury);

        $longueurz=$strlongzmax - $strlongzmin;
        $longueurz=$longueurz*$longueurz;
        $longueurz=sqrt($longueurz);

        if (($longueurx >= $longueury) && ($longueury >= $longueurz)){
                $ix=$longueurx;
                $iy=$longueury;
                $iz=$longueurz;
                $strlengthimin=$strlengthiminx;
                $strlengthimax=$strlengthimaxx;
        }
        elsif (($longueurx >= $longueurz) && ($longueurz >= $longueury)){
                $ix=$longueurx;
                $iy=$longueurz;
                $iz=$longueury;
                $strlengthimin=$strlengthiminx;
                $strlengthimax=$strlengthimaxx;
        }
        elsif (($longueury >= $longueurx) && ($longueurx >= $longueurz)){
                $ix=$longueury;
                $iy=$longueurx;
                $iz=$longueurz;
                $strlengthimin=$strlengthiminy;
                $strlengthimax=$strlengthimaxy;
        }
        elsif (($longueury >= $longueurz) && ($longueurz >= $longueurx)){
                $ix=$longueury;
                $iy=$longueurz;
                $iz=$longueurx;
                $strlengthimin=$strlengthiminy;
                $strlengthimax=$strlengthimaxy;
        }
        elsif (($longueurz >= $longueurx) && ($longueurx >= $longueury)){
                $ix=$longueurz;
                $iy=$longueurx;
                $iz=$longueury;
                $strlengthimin=$strlengthiminz;
                $strlengthimax=$strlengthimaxz;
        }
       elsif (($longueurz >= $longueury) && ($longueury >= $longueurx)){
                $ix=$longueurz;
                $iy=$longueury;
                $iz=$longueurx;
                $strlengthimin=$strlengthiminz;
                $strlengthimax=$strlengthimaxz;
        };

	#length is defined by the atoms used to calculate the biggest inertial axis (and not the 2 atoms that are the most far away from each other)
	
        $lengthix=($strx[$strlengthimax]-$strx[$strlengthimin]);
        $lengthiy=($stry[$strlengthimax]-$stry[$strlengthimin]);
        $lengthiz=($strz[$strlengthimax]-$strz[$strlengthimin]);
        $lengthix=$lengthix*$lengthix;
        $lengthiy=$lengthiy*$lengthiy;
        $lengthiz=$lengthiz*$lengthiz;
        $length = $lengthix + $lengthiy + $lengthiz;
        $length = sqrt($length);

        $prop{length}=sprintf"%4.2f",$length;
        $prop{ix}=sprintf"%4.2f",$ix;
        $prop{iy}=sprintf"%4.2f",$iy;
        $prop{iz}=sprintf"%4.2f",$iz;
};

#================================= Nombre de Ha et Hd
if($exenbha || $exenbhd || $exelipinski){

        $nbha=0;
        $nbhd=0;
        foreach $ig (1..$istratom){
                $nbhd++ if ($atom[$ig] eq 'H' && ($fonc[$ig]=~/ 1-O /||$fonc[$ig]=~/ 1-N /));
                $nbha++ if ($atom[$ig] eq 'N' && $coval[$ig] <= 3);
                $nbha++ if ($atom[$ig] eq 'O');
        };

        $prop{nbha}=$nbha;
        $prop{nbhd}=$nbhd;
};

#================================= Moment Dipolaire et ses composantes si les charges sont definies
if($exedipole){

# cette methode est une "Approximation dipolaire"

	$dipole=0;
	 	
        if(-e $f3 && ($param{CHARGES} eq 'GAS' || $param{CHARGES} eq 'AM1')){

                $typechg="gas" if($param{CHARGES} eq 'GAS');
                $typechg="bcc" if($param{CHARGES} eq 'AM1');

		chop($tmplog=`$leaexe/charge.pl $f3 $typechg $setchg`);
		#print "charge $tmplog\n"; 

		if(-e "charge$f3"){
			#print "read charge$f3 for getting charges\n";
                        open(INP,"<$param{WORKDIR}/charge$f3");
                        @stratomchg="";
                        $flagatomchg=0;
                        $chgi=1;
                        while(<INP>){
                                $flagatomchg=0 if($_=~/^\@<TRIPOS>BOND/);
                                if($flagatomchg){
                                        @getchg=split(' ',$_);
                                        $stratomchg[$chgi]=$getchg[8] if($getchg[8] ne '');
					#print "$stratomchg[$chgi]\n";
					$chgi++;
                                };
                                $flagatomchg=1 if($_=~/^\@<TRIPOS>ATOM/);
                        };
                        close(INP);
                        $chgi=$chgi-1;
                        print "problem $istratom atoms != $chgi atoms\n" if($istratom != $chgi);
                };

                if ($stratomchg[1] ne ''){

# debye(�), which is defined so that a single negative charge seperated by 100pm from a single positive charge has a dipole moment of 4.80D. If something had a dipole moment of 1.2D it would have 25% of an electrons charge on one atom with an equal positive charge on the other.
			 
			$facteur=4.8048; # to convert into debye
			
			# Calcul du barycentre des charges + et -
			$chggpx=0;
		       	$chggpy=0;
			$chggpz=0;
			$chggnx=0;
			$chggny=0;
			$chggnz=0;
			$chttp=0;
			$chttn=0;
			$chggx=0;
			$chggy=0;
			$chggz=0;
			$chtt=0;
			foreach $rgyrk (1..$istratom){
				$chggx=$chggx+$strx[$rgyrk]*$stratomchg[$rgyrk];
				$chggy=$chggy+$stry[$rgyrk]*$stratomchg[$rgyrk];
				$chggz=$chggz+$strz[$rgyrk]*$stratomchg[$rgyrk];
				$chtt=$chtt+$stratomchg[$rgyrk];
		       		if ($stratomchg[$rgyrk] > 0 ){
					$chggpx=$chggpx+$strx[$rgyrk]*$stratomchg[$rgyrk];
					$chggpy=$chggpy+$stry[$rgyrk]*$stratomchg[$rgyrk];
					$chggpz=$chggpz+$strz[$rgyrk]*$stratomchg[$rgyrk];
					$chttp=$chttp+$stratomchg[$rgyrk];
				}
				else{
					$chggnx=$chggnx+$strx[$rgyrk]*$stratomchg[$rgyrk]*-1;
					$chggny=$chggny+$stry[$rgyrk]*$stratomchg[$rgyrk]*-1;
					$chggnz=$chggnz+$strz[$rgyrk]*$stratomchg[$rgyrk]*-1;
					$chttn=$chttn+$stratomchg[$rgyrk]*-1;
				};
			};			
			$chggpx=sprintf"%4.3f",$chggpx/$chttp;
			$chggpy=sprintf"%4.3f",$chggpy/$chttp;
			$chggpz=sprintf"%4.3f",$chggpz/$chttp;
			$chggnx=sprintf"%4.3f",$chggnx/$chttn;
			$chggny=sprintf"%4.3f",$chggny/$chttn;
			$chggnz=sprintf"%4.3f",$chggnz/$chttn;

			#point medium entre + et - donc barycentre des charges
			#point ou la charge est nulle !
			$medx=sprintf"%4.3f",$chggnx-($chggnx-$chggpx)/2;
			$medy=sprintf"%4.3f",$chggny-($chggny-$chggpy)/2;
			$medz=sprintf"%4.3f",$chggnz-($chggnz-$chggpz)/2;
			#print "medium $medx, $medy, $medz\n";
			
			$dip=0;
			
			#point � equidistance de + et -
			#donc le calcul suivant doit donner 0
			#foreach $rgyrk (1..$istratom){
			#	$dip=$dip+sqrt(($medx-$strx[$rgyrk])**2+($medy-$stry[$rgyrk])**2+($medz-$strz[$rgyrk])**2)*$stratomchg[$rgyrk];
			#};
			#print "$dip\n";	
			
			#Ci-apres : ceci est en fait la distance entre les barycentre + et - (ex: dans H-Cl H(bary+) et Cl(bary-))
			#cela ne correspond pas au moment dipolaire
			#$dip=sqrt(($chggnx-$chggpx)**2+($chggny-$chggpy)**2+($chggnz-$chggpz)**2);
			#$dip=$dip*$facteur;
			#print "$dip\n";
			
			# Dipole : sum of vectors between gravity center (which one +, - or all ?) times the partial charge
			$dipole=0;
			$dipx=0;
			$dipy=0;
			$dipz=0;
			foreach $rgyrk (1..$istratom){
				# vector between center of gravity of all charges
				$dipx=$dipx+($medx-$strx[$rgyrk])*$stratomchg[$rgyrk];
				$dipy=$dipy+($medy-$stry[$rgyrk])*$stratomchg[$rgyrk];
				$dipz=$dipz+($medz-$strz[$rgyrk])*$stratomchg[$rgyrk];
			};
			$dip=sqrt($dipx**2+$dipy**2+$dipz**2)*$facteur;

			open(OUT1,">vector_dipole.pdb");
				printf OUT1 "REMARK barycenter of + (Na) and - (Cl)\n";
				printf OUT1 "REMARK C barycentre of charges\n";
				printf OUT1 "HETATM    1   C  dip     1    %8s%8s%8s\n",$medx,$medy,$medz;
				printf OUT1 "HETATM    1  Na  dip     1    %8s%8s%8s\n",$chggpx,$chggpy,$chggpz;
				printf OUT1 "HETATM    2  Cl  dip     1    %8s%8s%8s\n",$chggnx,$chggny,$chggnz;
			close(OUT1);

			$dipole=sprintf"%4.3f",$dip;

                };
		
        };
	#print "$dipole\n";
        $prop{dipole}=sprintf"%4.2f",$dipole;
	
};

#================================= MR (atomic contribution by Ghose and Crippen)
if($exemr){

	if(-e $f3){
                chop($tmplog=`$leaexe/GHOSE.pl $param{WORKDIR}/$f3`);

                $mr=$tmplog;
                $mr=~s/(.*)=(.*) (.*)MR=(.*)/$4/;
                $mr=~s/ //g;

                $prop{mr}=$mr;
        }
        else{
                $prop{mr}=0.0;
        };
};
#================================= LogP by XLOGP (atomic contribution by Wang et al.)
if($exelogp || $exelipinski){

	 
        if(-e $f4 && $istratom < 150){

                system("cp $param{WORKDIR}/$f4 $param{WORKDIR}/inxlogp.sdf");
                ## translate sdf in mol2 with XLOGP speciffically atom type (five atom rings)
		#system("$leaexe/XLOGP_SDF_MOL2.pl inxlogp.sdf");
		
                chop($tmplog=`$leaexe/XLOGP_SDF_MOL2.pl inxlogp.sdf`);
		
		## Execute XLOGP
                chop($logp=`$leaexe/XLOGP/xlogp2.1/xlogp/xlogp inxlogp_1.mol2`);

                $logp=~s/LogP =(.*) (.*)/$1/;
                $logp=~s/ //g;
                $prop{logp}=$logp;

		unlink "inxlogp_1.mol2";
		unlink "inxlogp.sdf";
		unlink "xlogp.mol2";
		unlink "xlogp.log";
        }
        else{
                $prop{logp}=0.0;
        };
};

#================================= SOLUBILITY
if($exesolubility){

	$prop{solubility}=0;

	if($istratom <= 100){
		unlink "$param{WORKDIR}/solubility" if(-e "$param{WORKDIR}/solubility");
		chop($tmpsolub=`$leaexe/SOLUBILITY/main $f3`);
		open(INP,"<$param{WORKDIR}/solubility");
		while(<INP>){
		if($_=~/solubility/){
			@getsoluble=split(' ',$_);
			$getsoluble[2]=$getsoluble[2]/1000 if($_ =~/ mg/);
			$getsoluble[2]=$getsoluble[2]*1000 if($_ =~/ Kg/);
			$prop{solubility}=sprintf"%4.2f",$getsoluble[2] if($getsoluble[2] < $prop{solubility} || $prop{solubility} == 0);
		};
		};
		close(INP);
		unlink "$param{WORKDIR}/solubility";
	}
	else{
		print "Solubility: Number of atoms >= 100 (maximum admits by CHEMICALC-2\n ";
	};	
};

#================================= rotatable bonds (between heaby atoms, except amide C-N bond because of high energy barrier and execpt ring bond)

if($exerot || $exenring || $exenringa || $exepharm){

        if(-e $f4){

                $prop{nringa}=0;
                $prop{nring}=0;
                $prop{rot}=0;

                if(-e "acyclic.sdf" || -e "fused_rings.sdf" || -e "linker.sdf" || -e "ring.sdf" || -e "substituent.sdf" || -e "special.sdf"){
                        print "WARNING: acyclic.sdf fused_rings.sdf linker.sdf ring.sdf special.sdf and substituent.sdf are reserved file names ! they will be overwrited !\n";
                };

		#system("$leaexe/MAKE_FGTS.pl $f4");
                chop($tmplog=`$leaexe/MAKE_FGTS.pl $f4`);
		
		$nrot=0;
                $nring=0;
                $nringa=0;
                @centerx="";
                @centery="";
                @centerz="";
                @centertype="";
                $centeri=0;
                if(!-z "ring.sdf"){
                        $readnext=0;
                        #$readnext2=0;
                        #$readnext3=0;
                        $readnext4=0;
                        $ncyclesnb=0;
                        #$cyclearomnb=0;
                        #$natomescyclenb=0;
                        $arcenter=0;
                        open(INP,"<$param{WORKDIR}/ring.sdf");
                        while(<INP>){
                                if($_=~/(\$\$\$\$)/){
                                        $nring=$nring+$ncyclesnb;
                                        #$nringa=$nringa+1 if(($natomescyclenb==6 && $cyclearomnb==3) || ($natomescyclenb==5 && $cyclearomnb==2));
                                        $nringa++ if($arcenter); # forcement +1 cycle aromatique
                                        $arcenter=0;
                                        $ncyclesnb=0;
                                        #$cyclearomnb=0;
                                        #$natomescyclenb=0;
                                };
                                if($readnext){
                                        @getsoluble=split(' ',$_);
                                        $ncyclesnb=$getsoluble[0] if($getsoluble[0] ne "" && $getsoluble[0] > 0);
                                        $readnext=0;
                                };
                                if($readnext4){
                                        @getsoluble=split(' ',$_);
                                        $centerx[$centeri]=$getsoluble[0];
                                        $centery[$centeri]=$getsoluble[1];
                                        $centerz[$centeri]=$getsoluble[2];
                                        $readnext4=0;
                                        $centeri++;
                                };
                                if($_=~/ncycles/){
                                        $readnext=1;
                                };
                                if($_=~/ARcenter/ || $_=~/LIPcenter/){
                                        $centertype[$centeri]="AR" if($_=~/ARcenter/);
                                        $centertype[$centeri]="LIP" if($_=~/LIPcenter/);
                                        $readnext4=1;
                                        $arcenter=1 if($_=~/ARcenter/);
                                };
                        };
                        close(INP);
                };
                if(!-z "fused_rings.sdf"){
                        $readnext=0;
                        #$readnext2=0;
                        #$readnext3=0;
                        $readnext4=0;
                        $ncyclesnb=0;
                        #$cyclearomnb=0;
                        #$natomescyclenb=0;
                        $arcenter=0;
                        $readnext5=0;
                        $nbringar=0;
                        open(INP,"<$param{WORKDIR}/fused_rings.sdf");
                        while(<INP>){
                                if($_=~/(\$\$\$\$)/){
                                        $nring=$nring+$ncyclesnb;

                                        # limitation si seulement certains cycles sont aromatiques dans la molecules ... ils ne sont pas comptabilises
                                        $nringa=$nringa+$nbringar;# detect in MAKE_FGT.pl if smallest fused rings are 5 or 6 atoms

                                        $ncyclesnb=0;
                                        $nbringar=0;
                                        #$cyclearomnb=0;
                                        #$natomescyclenb=0;
                                        $arcenter=0;
                                };
                                if($readnext){
                                        @getsoluble=split(' ',$_);
                                        $ncyclesnb=$getsoluble[0] if($getsoluble[0] ne "" && $getsoluble[0] > 0);
                                        $readnext=0;
                                };
                                if($readnext4){
                                        @getsoluble=split(' ',$_);
                                        $centerx[$centeri]=$getsoluble[0];
                                        $centery[$centeri]=$getsoluble[1];
                                        $centerz[$centeri]=$getsoluble[2];
                                        $readnext4=0;
                                        $centeri++;
                                };
                                if($readnext5){
                                        @getsoluble=split(' ',$_);
                                        $nbringar=$getsoluble[0] if($getsoluble[0] ne "" && $getsoluble[0] > 0);
                                        $readnext5=0;
                                };
                                if($_=~/ncycles/){
                                        $readnext=1;
                                };
                                if($_=~/nbringar/){
                                        $readnext5=1;
                                };
                                if($_=~/ARcenter/ || $_=~/LIPcenter/){
                                        $centertype[$centeri]="AR" if($_=~/ARcenter/);
                                        $centertype[$centeri]="LIP" if($_=~/LIPcenter/);
                                        $readnext4=1;
                                        $arcenter=1 if($_=~/ARcenter/);
                                };
                        };
                        close(INP);
                };
                if(!-z "special.sdf"){
                        $readnext=0;
                        $readnext4=0;
                        $readnext5=0;
                        $ncyclesnb=0;
                        $arcenter=0;
                        $nbringar=0;
                        open(INP,"<$param{WORKDIR}/special.sdf");
                        while(<INP>){
                                if($_=~/(\$\$\$\$)/){
                                        $nring=$nring+$ncyclesnb;

                                        #$nringa=$nringa+$ncyclesnb if($arcenter);# si system aromatique alors tous les cycles sont aromatiques
                                        # limitation si seulement certains cycles sont aromatiques dans la molecules ... ils ne sont pas comptabilises
                                        $nringa=$nringa+$nbringar;# detect in MAKE_FGT.pl if smallest fused rings are 5 or 6 atoms

                                        $nbringar=0;
                                        $ncyclesnb=0;
                                        $arcenter=0;
                                };
                                if($readnext5){
                                        @getsoluble=split(' ',$_);
                                        $nbringar=$getsoluble[0] if($getsoluble[0] ne "" && $getsoluble[0] > 0);
                                        $readnext5=0;
                                };
                                if($readnext){
                                        @getsoluble=split(' ',$_);
                                        $ncyclesnb=$getsoluble[0] if($getsoluble[0] ne "" && $getsoluble[0] > 0);
                                        $readnext=0;
                                };
                                if($readnext4){
                                        @getsoluble=split(' ',$_);
                                        $centerx[$centeri]=$getsoluble[0];
                                        $centery[$centeri]=$getsoluble[1];
                                        $centerz[$centeri]=$getsoluble[2];
                                        $readnext4=0;
                                        $centeri++;
                                };
                                if($_=~/ncycles/){
                                        $readnext=1;
                                };
                                if($_=~/nbringar/){
                                        $readnext5=1;
                                };
                                if($_=~/ARcenter/ || $_=~/LIPcenter/){
                                        $centertype[$centeri]="AR" if($_=~/ARcenter/);
                                        $centertype[$centeri]="LIP" if($_=~/LIPcenter/);
                                        $readnext4=1;
                                        $arcenter=1 if($_=~/ARcenter/);
                                };
                        };
                        close(INP);
                };
                if(!-z "acyclic.sdf"){
                        $readnext=0;
                        $nbrotat=0;
                        open(INP,"<$param{WORKDIR}/acyclic.sdf");
                        while(<INP>){
                                if($_=~/(\$\$\$\$)/){
                                        $nrot=$nrot+$nbrotat;
                                        $nbrotat=0;
                                };
                                if($readnext){
                                        @getsoluble=split(' ',$_);
                                        $nbrotat=$getsoluble[0] if($getsoluble[0] ne "" && $getsoluble[0] > 0);
                                        $readnext=0;
                                };
                                if($_=~/nrotatable/){
                                        $readnext=1;
                                };
                        };
                        close(INP);
                };
                $prop{nringa}=$nringa;
                $prop{nring}=$nring;
                $prop{rot}=$nrot;

                unlink "ring.sdf";
                unlink "fused_rings.sdf";
                unlink "acyclic.sdf";
                unlink "linker.sdf";
                unlink "substituent.sdf";
                unlink "special.sdf";

        }
        else{
                $prop{nringa}=0;
                $prop{nring}=0;
                $prop{rot}=0;
        };
};



};
