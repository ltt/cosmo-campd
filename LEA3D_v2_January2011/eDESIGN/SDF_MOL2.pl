#!/usr/bin/perl


	$name="";

        local($fileopen,$opt,$name)=@ARGV;

	$leaexe=$ENV{LEA3D};
	die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");
	
	require "$leaexe/CYCLE_SDF.pl";
	

        if ($fileopen eq ''){
		die "usage: sdfmol2 <file.sdf> <1 if multiple file> <name to put after \@<TRIPOS>MOLECULE> \n";
	}


	if(!-e "$fileopen"){
		die "$fileopen does not exist !\n";
	};
	
        unlink "molconcat.mol2" if($opt==1);

				
	$nombre_molecule=0;
	$flagnew=1;
	open(MOL,"<$fileopen");
	while(<MOL>){
		if($flagnew){
			$moli="";
			$masse=0;
			$getinfo=0;
			$info='';
			$compt=0;
			$ig=1;
			$jg=0;
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			@coval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			@bond='';
			@listb='';
			@typeb='';
			$blanc=' ';	
			@radius='';
			@lignebond='';		
			$atomlourd=0;
			$flagnew=0;
			$nombre_molecule++;
		};
		@getstr = split(' ',$_);
		$compt++;
		
		if($getinfo){
			$getinfo=0;
			$jointxt=join(' ',@getstr);
			$info=$info.":".$jointxt;	
		};
		
		if (($compt > 4) && ($ig <= $istratom)){
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];
			$atomlourd ++ if($getstr[3] ne 'H');
			$radius[$ig]=$tabR{$getstr[3]};
			$masse=$masse+$tabmm{$getstr[3]};
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{
				@coller=split(' *',$getstr[0]);
				@coller2=split(' *',$getstr[1]);
				if(@coller==6 && $getstr[1] ne ""){
					$getstr[0]=$coller[0].$coller[1].$coller[2];
					$getstr[2]=$getstr[1];
					$getstr[1]=$coller[3].$coller[4].$coller[5];
				}
				elsif(@coller==6 && $getstr[1] eq ""){
					$getstr[0]=$coller[0].$coller[1];
					$getstr[1]=$coller[2].$coller[3].$coller[4];
					$getstr[2]=$coller[5];
				}
				elsif(@coller==5){
					if($_=~/^\s/){
						$getstr[0]=$coller[0].$coller[1];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[2].$coller[3].$coller[4];
					}
					else{
						$getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[3].$coller[4];
					};
                                }
                                elsif(@coller==4){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3];
                                        };					
				}
				elsif(@coller2==4){
					$getstr[1]=$coller2[0].$coller2[1].$coller2[2];
					$getstr[2]=$coller2[3];
				}
				elsif(@coller==7){
					$getstr[0]=$coller[0].$coller[1].$coller[2];
					$getstr[1]=$coller[3].$coller[4].$coller[5];
					$getstr[2]=$coller[6];
				};

				$bond[$getstr[0]]=$bond[$getstr[0]].$blanc.$getstr[1].$blanc.$getstr[2];
				$listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];
				$typeb[$getstr[0]]=$typeb[$getstr[0]].$blanc.$getstr[2];

				$bond[$getstr[1]]=$bond[$getstr[1]].$blanc.$getstr[0].$blanc.$getstr[2];
				$listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];
				$typeb[$getstr[1]]=$typeb[$getstr[1]].$blanc.$getstr[2];


				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
				$lignebond[$jg]=$_;
				$jg++;
			};
		};
		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];

			@coller=split(' *',$istratom);
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

			#$compt++;
		};
		if ($_=~/\$\$\$\$/){
			$flagnew=1;
			$prop{atomlourd}=$atomlourd;
			
			&convert;
			
			if($opt==1){
				print "$f5 into molconcat.mol2 DONE\n";	
			}
			else{	
				print "$f5 DONE\n";
			};	
			
		};
		
		
		if ($_=~/>/ && $_=~/<ID>/){
			@getmoli=split(' ',$_);
			$moli=$getmoli[2];
			$moli=~s/\(//;
			$moli=~s/\)//;
			#print "moli = $moli\n";
		};
		if ($_=~/>/ && ($_=~/name/ || $_=~/CAS/ || $_=~/cas/ || $_=~/ZINC/ || $_=~/MDLNUMBER/ || $_=~/DB_ID/ || $_=~/ENTRY/ || $_=~/code_number/ || $_=~/REGNO/ || $_=~/COMP_NAME/ || $_=~/NOM/ || $_=~/NAME/)){
			    $getinfo=1;
			    $info=$info." $getstr[1] $getstr[2]";
		};	
		
	};
	close(MOL);


###################################################################################
###################################################################################


sub convert{

	@type='';
	foreach $bi (1..$istratom){
#		print"$bi _ $atom[$bi] $fonc[$bi] \n";
#		print"$bi _ $atom[$bi] $ifonc[$bi] \n";

		#july 2008
		if($atom[$bi] eq 'X'){
			$type[$bi]="X";
		};

		if($atom[$bi] eq 'C'){
			$car=$fonc[$bi];
			$nocc=$car=~s/ 2-/ 2-/g;
			if($fonc[$bi] =~/ 2-/){
				$type[$bi]="C.2";
			}
			elsif($fonc[$bi] =~/ 3-/){
				$type[$bi]="C.1";
			}
			elsif($nocc==2){
				$type[$bi]="C.1";
			}
			elsif($fonc[$bi] =~/ 4-/){
				$type[$bi]="C.ar";	
			}
			else{
				$type[$bi]="C.3";
			};
		};
		
		if($atom[$bi] eq 'N'){
			if($fonc[$bi] =~/ 2-/){
				$type[$bi]="N.2";
			}
			elsif($fonc[$bi] =~/ 3-/){
				$type[$bi]="N.1";
			}
			elsif($fonc[$bi] =~/ 4-/){
				$type[$bi]="N.ar";	
			}
			else{
				$type[$bi]="N.3";
			};
			$type[$bi]="N.4" if($coval[$bi] == 4 && $fonc[$bi] !~/ 2-/ && $fonc[$bi] !~/ 3-/);
			
			$type[$bi]="N.pl3" if($coval[$bi] == 5 && @fonc[$bi]==3); #si NO2
		};
		
		if($atom[$bi] eq 'O'){
		#print "fonc $fonc[$bi]\n";
			if($fonc[$bi] =~/ 2-/){
				$type[$bi]="O.2";
			}
			else{
				$type[$bi]="O.3";
			};
		};
		if($atom[$bi] eq 'S'){
		#print "fonc $fonc[$bi]\n";
			if($fonc[$bi] =~/ 2-/){
				$type[$bi]="S.2";
			}
			else{
				$type[$bi]="S.3";
			};
			if($fonc[$bi] =~/ 2-O /){
				$car=$fonc[$bi];
				$nocc=$car=~s/ 2-O / 2-O /g;
				$type[$bi]="S.o" if($nocc==1);
				$type[$bi]="S.o2" if($nocc==2);
			}
		};
		if($atom[$bi] eq 'P'){
			$type[$bi]="P.3";
		};
		if($atom[$bi] eq 'H'){
			$type[$bi]="H";
		};
		if($atom[$bi] eq 'F'){
			$type[$bi]="F";
		};
		if($atom[$bi] eq 'Cl'){
			$type[$bi]="Cl";
		};
		if($atom[$bi] eq 'Br'){
			$type[$bi]="Br";
		};
		if($atom[$bi] eq 'I'){
			$type[$bi]="I";
		};
		if($atom[$bi] eq 'Si'){
			$type[$bi]="Si";
		};
	};

	foreach $bi (1..$istratom){
		if($atom[$bi] eq 'C'){

# Fonction acide (O.co2)
			if($fonc[$bi] =~/ 2-O / && $fonc[$bi] =~/ 1-O / ){
			$car=$fonc[$bi];
			$nocc=$car=~s/ 1-O / 1-O /g;
			if ($nocc == 1){ 
				@det=split(' ',$fonc[$bi]);
				$p=0;
				foreach $k (0..@det-1){
					$p=$k if($det[$k] =~/1-O/);
				};
				@det1=split(' ',$ifonc[$bi]);
				$iatom=$det1[$p];
				@det2=split(' ',$ifonc[$iatom]);

# if ($fonc[$iatom] =~/ 1-H /) O.3 et O.2 sinon O.co2 sur les 2 O
				if(@det2==1){
					$type[$iatom]="O.co2";
					$p=0;
					foreach $k (0..@det-1){
						$p=$k if($det[$k] =~/2-O/);
					};
					@det1=split(' ',$ifonc[$bi]);
					$iatom1=$det1[$p];
					$type[$iatom1]="O.co2";
				};
			};
			};
			
# C.cat in guanidinium			
			if($fonc[$bi] =~/ 2-N / && $fonc[$bi] =~/ 1-N / ){
				$car=$fonc[$bi];
				$nocc=$car=~s/ 1-N / 1-N /g;
				if($nocc == 2){
					@det=split(' ',$fonc[$bi]);
					@det1=split(' ',$ifonc[$bi]);
					foreach $k (0..@det-1){
						if($det[$k] =~/2-N/){
							$iatom=$det1[$k];
							$car2=$fonc[$iatom];
							$nocc2=$car2=~s/ 1-H / 1-H /g;
							if($nocc2==2){ #means N is protonated
								$type[$bi]="C.cat";
							};
						};	
					};	
				};
			};
			

# Fonction amide (N.am)
			if($fonc[$bi] =~/ 2-O / && $fonc[$bi] =~/ 1-N / ){
				@det=split(' ',$fonc[$bi]);
				foreach $k (0..@det-1){
					if($det[$k] =~/1-N/){
						@det1=split(' ',$ifonc[$bi]);
						$type[$det1[$k]]="N.am" if($atom[$det1[$k]] eq 'N');
					};
				};
			};
		};
		
		if($atom[$bi] eq 'P'){
		
# Fonction phosphate une seule charge - (P et O.co2)

			if($fonc[$bi] =~/ 2-O / && $fonc[$bi] =~/ 1-O / ){
			$car=$fonc[$bi];
			$nocc=$car=~s/ 1-O / 1-O /g;
			$noccn=$car=~s/ 1-N / 1-N /g;
			$dejavu=0;
			$p=-1;
			if ($nocc == 3 || ($nocc == 2 && $noccn==1)){
				@det=split(' ',$fonc[$bi]);
				@det1=split(' ',$ifonc[$bi]);
				foreach $k (0..@det-1){
					if($det[$k] =~/1-O/){
						@det2=split(' ',$ifonc[$det1[$k]]);
						if(@det2==1){
							$type[$det1[$k]]="O.co2";
							$p=$det1[$k];
						};
						$dejavu++ if($fonc[$det1[$k]] =~/ 1-H /);
					};
				};
				
				#if($dejavu>=1 && $p != -1){
				if($p != -1){                     # ici met o.co2 sur tout atom ionis� et =O du phosphate
				        foreach $k (0..@det-1){
					if($det[$k] =~/2-O/){
						@det2=split(' ',$ifonc[$det1[$k]]);
						if(@det2==1){
							$type[$det1[$k]]="O.co2";
							$type[$p]="O.co2";
						};
					};
					};
				};
				
			};
			};			
              };		

	};


				
	
# AROMATIQUES N.ar et C.ar

		#print"Mol$moli\n";
		$nbatom=$istratom;
		&cyclesdf2;

# N.pl3

		foreach $bi (1..$istratom){
			if($atom[$bi] eq 'N' && $type[$bi] eq 'N.3'){
				
				@det=split(' ',$ifonc[$bi]);
				$p=0;
				foreach $k (0..@det-1){
					$p++ if($type[$det[$k]] eq 'C.cat');
					$p++ if($type[$det[$k]] eq 'C.ar' || $type[$det[$k]] eq 'C.2');
					$p++ if($type[$det[$k]] eq 'C.1');
					$p++ if($type[$det[$k]] eq 'N.2' || $type[$det[$k]] eq 'N.ar'|| $type[$det[$k]] eq 'S.o' || $type[$det[$k]] eq 'S.o2');
				};
				
				$car=$fonc[$bi];
				$nocc=$car=~s/ 1-H / 1-H /g;
				$noccb=$car=~s/ 1-C / 1-C /g;
				
				$noncycle=1;
				$sicycle=0;
				foreach $lc (0..@cyclef-1){
					$tp=' '.$cyclef[$lc].' ';
					$sicycle=1  if($tp=~/ $bi /);
					$noncycle=0  if(($tp=~/ $bi /) && $cyclefar[$lc] ne "ar");
				};
				#print "$bi $type[$bi]\n";
				#print "$p $sicycle $noncycle $noccb $nocc @arom\n";
				#modif @arom not defined here use N.ar
				$type[$bi]="N.pl3" if($p >=1 && $sicycle==1 && $noncycle==0 && $nocc==1 && $type[$bi] eq "N.ar");
				
				$type[$bi]="N.pl3" if($p >= 2);
				$type[$bi]="N.pl3" if($p==1 && $nocc==2 && $sicycle==0);
				$type[$bi]="N.pl3" if($p==1 && $noccb>=1 && $nocc==1 && $sicycle==0);
				$type[$bi]="N.pl3" if($p==1 && $noccb>=2 && $sicycle==0);
				
				$type[$bi]="N.pl3" if($p >=1 && $sicycle==1 && $noncycle==1);
				
				#$type[$bi]="N.pl3" if($p >=1 && $sicycle==1 && $noncycle==0 && $nocc==1 && @arom==6);
				#$type[$bi]="N.pl3" if($p >=1 && $sicycle==1 && $noncycle==0 && $nocc==1 && @arom==5);
				#$type[$bi]="N.pl3" if($p >=1 && $sicycle==1 && $noncycle==0 && $noccb>=2);
			#print "$bi $type[$bi]\n";	
				
			}  #si NO2
			elsif($atom[$bi] eq 'N' && $type[$bi] eq 'N.2'){
				$car=$fonc[$bi];
			        $nocc=$car=~s/ 2-O / 2-O /g;
			        $noccb=$car=~s/ 1-O / 1-O /g;
			        $type[$bi]="N.pl3" if($nocc == 1 && $noccb== 1);
			
			        @det=split(' ',$fonc[$bi]);
	                        foreach $k (0..@det-1){
	                        	if($det[$k] =~/1-O/){
	                        		@det1=split(' ',$ifonc[$bi]);
	                        		$type[$det1[$k]]="O.2" if($atom[$det1[$k]] eq 'O');
	                        	};
	                        };
			};
		};

# S.2 si S.3 pres de C.ar ou C.2

		foreach $bi (1..$istratom){
			if($atom[$bi] eq 'S' && $type[$bi] eq 'S.3'){
				@det=split(' ',$ifonc[$bi]);
				foreach $k (0..@det-1){
					$type[$bi]="S.2" if($type[$det[$k]] eq 'C.ar' || $type[$det[$k]] eq 'C.2' || $type[$det[$k]] eq 'C.1' || $type[$det[$k]] eq 'N.ar' || $type[$det[$k]] eq 'N.2' );
				};
			};
		};


### CONVERSION

	#Variables en vue du classement des FGTS
	$class_c3=0;
	$class_c2=0;
	$class_c1=0;
	$class_car=0;
	$class_cat=0;
	$class_n3=0;
	$class_n2=0;
	$class_n1=0;
        $class_nar=0;
        $class_nam=0;
        $class_npl3=0;
        $class_n4=0;
        $class_o3=0;
        $class_o2=0;
        $class_oco2=0;

        $class_ospc=0;
        $class_ot3p=0;

        $class_s3=0;
        $class_s2=0;
        $class_so=0;
        $class_so2=0;
        $class_p3=0;
        $class_h=0;

        $class_hspc=0;
        $class_ht3p=0;

        $class_f=0;
        $class_cl=0;
        $class_br=0;
        $class_i=0;
        $class_si=0;

        $class_lp=0;
        $class_du=0;
        $class_na=0;
        $class_k=0;
        $class_ca=0;
        $class_li=0;
        $class_al=0;


	$blanc=' ';

	if($moli ne '' && ($fileopen eq "mol.sdf" || $fileopen eq "molco.sdf")){
		$f4="mol$moli.mol2";
	}
	else{	
		$f4=$fileopen;
		$moli=$nombre_molecule;
		#if($nombre_molecule > 1){
			$f4=~s/\.sdf/_$moli\.mol2/;
		#}
		#else{
		#	$f4=~s/\.sdf/\.mol2/;
		#};
	};
		
	$f5=$f4;
	
	if($opt==1){
		$f4="molconcat.mol2";
		open(OUTC,">>$f4");
	}	
	else{
		open(OUTC,">$f4");
	};	
	
		if($info ne ''){
			print OUTC "#	  Creating by LEA \n";
			print OUTC "#  $info \n";
			print OUTC "#\n";
		}
		else{
			print OUTC "#\n";
			print OUTC "#	  Creating by LEA \n";
			print OUTC "#\n";
		};
		
		print OUTC "\n";
		print OUTC "@<TRIPOS>MOLECULE\n";
		if($name eq ""){
			print OUTC "mol$moli\n";
		}
		else{
			print OUTC "$name\n";
		};	
		printf OUTC "%4s%1s%4s\n",$istratom,$blanc,$istrbond;
		print OUTC "SMALL\n";
		print OUTC "NO_CHARGES\n\n\n";
		print OUTC "@<TRIPOS>ATOM\n";
		foreach $bi (1..$istratom){
			$a1=$bi;
			$a2="$atom[$bi]$bi";
			@lena6b=split('',$a2);
			$lena6=@lena6b;
			$a2="$a2   " if($lena6 == 1);
			$a2="$a2  " if($lena6 == 2);
			$a2="$a2 " if($lena6 == 3);
			$a3=sprintf "%5.4f",$strx[$bi];
			$a4=sprintf "%5.4f",$stry[$bi];
			$a5=sprintf "%5.4f",$strz[$bi];
			$a6=$type[$bi];
			@lena6b=split('',$a6);
			$lena6=@lena6b;
			$a6="$a6    " if($lena6 == 1);
                        $a6="$a6   " if($lena6 == 2);
                        $a6="$a6  " if($lena6 == 3);
			$a6="$a6 " if($lena6 == 4);
                        $a7="1";
                        $a8="lig";
                        $a9="0.000";

			printf OUTC "%4s %4s%5s%10s %10s %10s %5s %6s %3s %8s\n",$a1,$a2,$blanc,$a3,$a4,$a5,$a6,$a7,$a8,$a9;
			
			$class_c3++ if($type[$bi]=~/C\.3/);
			$class_c2++ if($type[$bi]=~/C\.2/);
			$class_c1++ if($type[$bi]=~/C\.1/);
			$class_car++ if($type[$bi]=~/C\.ar/);
			$class_cat++ if($type[$bi]=~/C\.cat/);
			$class_n3++ if($type[$bi]=~/N\.3/);
			$class_n2++ if($type[$bi]=~/N\.2/);
			$class_n1++ if($type[$bi]=~/N\.1/);
       	 		$class_nar++ if($type[$bi]=~/N\.ar/);
        		$class_nam++ if($type[$bi]=~/N\.am/);
        		$class_npl3++ if($type[$bi]=~/N\.pl3/);
        		$class_n4++ if($type[$bi]=~/N\.4/);
        		$class_o3++ if($type[$bi]=~/O\.3/);
        		$class_o2++ if($type[$bi]=~/O\.2/);
        		$class_oco2++ if($type[$bi]=~/O\.co2/);

       		 	$class_ospc++ if($type[$bi]=~/O\.spc/);
        		$class_ot3p++ if($type[$bi]=~/O\.t3p/);

        		$class_s3++ if($type[$bi]=~/S\.3/);
        		$class_s2++ if($type[$bi]=~/S\.2/);
        		$class_so++ if($type[$bi]=~/S\.o/);
        		$class_so2++ if($type[$bi]=~/S\.o2/);
        		$class_p3++ if($type[$bi]=~/P\.3/);
        		$class_h++ if($type[$bi]=~/H/);

        		$class_hspc++ if($type[$bi]=~/H\.spc/);
        		$class_ht3p++ if($type[$bi]=~/H\.t3p/);

       	 		$class_f++ if($type[$bi]=~/F/);
        		$class_cl++ if($type[$bi]=~/Cl/);
        		$class_br++ if($type[$bi]=~/Br/);
        		$class_i++ if($type[$bi]=~/I/);
        		$class_si++ if($type[$bi]=~/Si/);

        		$class_lp++ if($type[$bi]=~/LP/);
        		$class_du++ if($type[$bi]=~/Du/);
        		$class_na++ if($type[$bi]=~/Na/);
       	 		$class_k++ if($type[$bi]=~/K/);
        		$class_ca++ if($type[$bi]=~/Ca/);
        		$class_li++ if($type[$bi]=~/Li/);
        		$class_al++ if($type[$bi]=~/Al/);

			
		};
		
		$class_ar=0;
		$class_1=0;
		$class_2=0;
		$class_3=0;
		$class_am=0;
		
		@typebondmol2='';
		print OUTC "@<TRIPOS>BOND\n";
		foreach $bi (1..$istrbond){

			@extract=split(' ',$lignebond[$bi]);

				@coller=split(' *',$extract[0]);
				@coller2=split(' *',$extract[1]);
				if(@coller==6 && $extract[1] ne ""){
					$extract[0]=$coller[0].$coller[1].$coller[2];
					$extract[2]=$extract[1];
					$extract[1]=$coller[3].$coller[4].$coller[5];
				}
				elsif(@coller==6 && $extract[1] eq ""){
					$extract[0]=$coller[0].$coller[1];
					$extract[1]=$coller[2].$coller[3].$coller[4];
					$extract[2]=$extract[5];
				}
				elsif(@coller==5){
					if($lignebond[$bi]=~/^\s/){
						$extract[0]=$coller[0].$coller[1];
						$extract[2]=$extract[1];
						$extract[1]=$coller[2].$coller[3].$coller[4];
					}
					else{
						$extract[0]=$coller[0].$coller[1].$coller[2];
						$extract[2]=$extract[1];
						$extract[1]=$coller[3].$coller[4];
					};
                                }
                                elsif(@coller==4){
                                        if($lignebond[$bi]=~/^\s/){
                                                $extract[0]=$coller[0];
                                                $extract[2]=$extract[1];
                                                $extract[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $extract[0]=$coller[0].$coller[1].$coller[2];
						$extract[2]=$extract[1];
                                                $extract[1]=$coller[3];
                                        };					
				}
                                elsif(@coller2==4){
					$extract[1]=$coller2[0].$coller2[1].$coller2[2];
					$extract[2]=$coller2[3];
				}
                                elsif(@coller==7){
					$extract[0]=$coller[0].$coller[1].$coller[2];
					$extract[1]=$coller[3].$coller[4].$coller[5];
					$extract[2]=$coller[6];
				};

			$a1=$bi;
			$a2=$extract[0];
			$a3=$extract[1];
			$a4=$extract[2];

			if(($type[$a2] eq 'N.am') && ($type[$a3] eq 'C.2')){
				$a4="am" if($fonc[$a3]=~/ 2-O /);
			};			
			if(($type[$a3] eq 'N.am') && ($type[$a2] eq 'C.2')){
				$a4="am" if($fonc[$a2]=~/ 2-O /);
			};
			
			if(($type[$a2] eq 'O.2') && ($type[$a3] eq 'N.pl3')){
				$a4="2" if($a4=1);
			};
			if(($type[$a2] eq 'N.pl3') && ($type[$a3] eq 'O.2')){
				$a4="2" if($a4=1);
			};
			
			# phosphate
			if(($type[$a2] eq 'P.3') && ($type[$a3] eq 'O.co2')){
				$a4="ar";
			};
			
			# carboxylate
			if(($type[$a2] eq 'C.2') && ($type[$a3] eq 'O.co2')){
				$a4="ar";
			};
			
			# si bond de type 4 dans le sdf alors ar
			if($type[$a3] =~ /.ar/ && $type[$a2] =~ /.ar/ && $fonc[$a3]=~/ 4-/ && $fonc[$a2]=~/ 4-/){
				@det=split(' ',$fonc[$a3]);
				@det1=split(' ',$ifonc[$a3]);
	                        foreach $k (0..@det-1){
	                        	if($det[$k] =~/4-/){
	                        		$a4="ar" if($det1[$k]=$a2);
	                        	};
	                        };
			};
			
			
			if(($type[$a3] =~ /.ar/) && ($type[$a2] =~ /.ar/) ){
				foreach $lc (0..@cyclef-1){
					$tp=' '.$cyclef[$lc].' ';
					$a4="ar" if(($tp=~/ $a2 /) && ($tp=~/ $a3 /) && $cyclefar[$lc] eq "ar");
				};
			};
			if((($type[$a3] =~ /S.2/) && ($type[$a2] =~ /.ar/)) || (($type[$a2] =~ /S.2/) && ($type[$a3] =~ /.ar/))){
				foreach $lc (0..@cyclef-1){
					$tp=' '.$cyclef[$lc].' ';
					$a4="ar" if(($tp=~/ $a2 /) && ($tp=~/ $a3 /)&& $cyclefar[$lc] eq "ar");
				};
			};
			if((($type[$a3] =~ /O.2/) && ($type[$a2] =~ /.ar/)) || (($type[$a2] =~ /O.2/) && ($type[$a3] =~ /.ar/))){
				foreach $lc (0..@cyclef-1){
					$tp=' '.$cyclef[$lc].' ';
					$a4="ar" if(($tp=~/ $a2 /) && ($tp=~/ $a3 /) && $cyclefar[$lc] eq "ar");
				};
			};
			printf OUTC "%4s%5s%5s $a4\n",$a1,$a2,$a3;
			
			$class_ar++ if($a4=~/ar/);
			$class_1++ if($a4=~/1/);
			$class_2++ if($a4=~/2/);
			$class_3++ if($a4=~/3/);
			$class_am++ if($a4=~/am/);
			
			$typebondmol2[$bi]=$a4;
			

		};
		print OUTC "@<TRIPOS>SUBSTRUCTURE\n";
		print OUTC "      1 mol$moli             1 ****\n";
	close(OUTC);
	
	
	
};



######################################################################
######################################################################



sub cyclesdf2{

	&cyclesdf;

	# check aromaticity
	 	
	@cyclefar='';
	foreach $lc (0..@cyclef-1){
		#print "$cyclef[$lc]\n";
		@arom=split(' ',$cyclef[$lc]);
		if(@arom == 5 || @arom == 6){
			$longcycle=@arom;
			$nbatomarpartial=0;
			foreach $lc2 (0..@arom-1){
				$narp=" ".$fonc[$arom[$lc2]]." ";
				$nocc1=$narp=~s/1-/1-/g;
				$nocc2=$narp=~s/2-/2-/g;
				if($atom[$arom[$lc2]] eq 'C'){
					$nbatomarpartial++ if($nocc1==2 && $nocc2==1);
				}
				elsif($atom[$arom[$lc2]] eq 'O'){
					$nbatomarpartial++ if($nocc1==2 && @arom==5);
				}
				elsif($atom[$arom[$lc2]] eq 'N'){
					if($nocc1==1 && $nocc2==1){
						$nbatomarpartial++;
					}
					elsif($nocc1==3 && @arom==5){
						$nbatomarpartial++;
					};
				}
				elsif($atom[$arom[$lc2]] eq 'S'){
					$nbatomarpartial++ if($nocc1==2 && @arom==5);
				};
			};
			if($nbatomarpartial==$longcycle){
				$cyclefar[$lc]="ar";
				foreach $lc2 (0..@arom-1){
					$type[$arom[$lc2]]="N.ar" if($atom[$arom[$lc2]] eq 'N' && $type[$arom[$lc2]] ne 'N.am');
					$type[$arom[$lc2]]="C.ar" if($atom[$arom[$lc2]] eq 'C');
					$type[$arom[$lc2]]="S.2" if($atom[$arom[$lc2]] eq 'S');
					$type[$arom[$lc2]]="O.2" if($atom[$arom[$lc2]] eq 'O');
				};
			};
		};
	};	
};

###################################################################################################################


