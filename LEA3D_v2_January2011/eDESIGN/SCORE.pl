#!/usr/bin/perl

###################################################################################################
#------------------------------------------- SCORE.pl---------------------------------------------#
# 												  #
# Scoring Code for LEA 3D, calling COSMOconf and MATLAB to calculate the score		  	  #
#												  #
# Updated and Extended by Lorenz Fleitmann, supervised by Jan Scheffcyzk 	  		  #
# Lehrstuhl f�r Technische Thermodynamik (LTT), RWTH Aachen University,           Juni, 2016      #
#												  #
###################################################################################################

print"SUBROUTINE SCORE OK \n";

sub score{

	local($file,$nbchildevaluer)=@_;

	@score="";
	foreach $sj (1..$nbchildevaluer){
		$score[$sj]=0;
		$properties[$sj]="";
	};

########## Fonction � �valuer

print "COMPUTE ENERGY\n" if($param{VERBOSITY} >= 1);
	
# a molecule must have >= $percentproperties % of the properties to go through docking
$percentproperties=80;

       
if($nbpre_fonction > 0){

#########################################################################################################################################################################
# COSMO-CAMD evaluation
#########################################################################################################################################################################

        if($evaluate_cosmo){
			print "\n";
            print ('#'x80);	
            print "\nHello LEA: This is Generation No. $memo_gen[$memoi] Molecule No. $memo_gi[$gi]\n";
            print "Current molecule code: ";
            print "$memomol[$gi]\n";

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Molecules: Get-USMILES-Code and check for nbatom-constraint, desired-moles constraint and atom limit

			# Molecular properties:
			$USMILES = $Generation_USMILES[$gi];
			$mwSolvent = $Generation_mwSolvent[$gi];
			$nbatom = $Generation_nbatom[$gi];
			$targetingnbatom = $Generation_nbatomtarget[$gi];
			$desiredtype = $Generation_desiredtype[$gi];
			$atom_limit_ok = $Generation_atom_limit[$gi];
			$fragprecalc = $Generation_fragprecalc[$gi];
	
			print "\nUSMILES: $USMILES\n";
			print "  nbatom = $nbatom\n";
			print "  mol weight = $mwSolvent\n\n";
#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
# If inside the desired nbatom region and containing no undesired functional groups, display results from Property Prediction and Process Model evaluation
            if ($targetingnbatom and $desiredtype and $atom_limit_ok and $fragprecalc){
					
				&results_of_mol($gi);						
	
			}

#-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
# Molecules which do not fulfill nbatom-target, desired atom type or atom_limit
			else{
		 	
				print "\n\nMolecule does not meet nbatom target\n" if(!$targetingnbatom);
				print "\n\nMolecule is of undesired type\n" if(!$desiredtype);
				print "\n\nMolecule does not meet n_max_$atom_to_limit\n" if (!$atom_limit_ok);
				print "\n\nMolecule's COSMOfrag precalculation score is too low\n"  if (!$fragprecalc);
				$cosmoscore[$gi+1]= 0;
				&writetoFails;
			};

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                # For statistical purpose add USMILES on summary list
                &summary_of_SMILES($USMILES,$cosmoscore[$gi+1]);
        };

		


	#Chemical Function occurences: use the first molecule of the sdf file (no mol2 file)
	if($evaluate_function){
		$pointeur_function=-1;
		foreach $prop (0..@fprop-1){
			$pointeur_function=$prop if($fprop[$prop] eq "function");
		};
		
		$tmpprop="";
		$tmpprop=&chemfunction($file); # apply at the first molecule only
		#print "tmpprop=$tmpprop\n";
		
		$prop3=0;
		@listfonc=split('_',$listefonction);
		$llistfonc=@listfonc;
		$fract=1/$llistfonc;
		foreach $lfi (0..@listfonc-1){
			if($tmpprop =~ /$listfonc[$lfi]/){
				$prop3=$prop3+$fract;
				print"$listfonc[$lfi] OK ($prop3)\n" if($param{VERBOSITY} >= 1);
			};
		};	
		print "Function (search for $listefonction) Score=$prop3 for all conformers (1 means full match)\n";

		# fill @score for each conformers = same score for each
		foreach $sj (1..$nbchildevaluer){ 
		 	$score[$sj]=$score[$sj]+(&composite($prop3,$fmin[$pointeur_function],$fmax[$pointeur_function]))*$fw[$pointeur_function];
			$properties[$sj]=$properties[$sj]."FUNCTIONS conformer $sj = $tmpprop\n";
		};	
	};


	#Pharmacophore: separate tmpi.sdf and creates tmpi.mol2 for each molecule in sdf 
	if($evaluate_pharm){
		$pointeur_function=-1;
		foreach $prop (0..@fprop-1){
			$pointeur_function=$prop if($fprop[$prop] eq "pharm");
		};

		$tmpprop="";
		$tmpprop=&pharmacophore; # match=1 else 0
		print "PHARMACOPHORE ($pharmk constraints):\n";

		@listfonc=split(' ',$tmpprop);
		foreach $sj (1..$nbchildevaluer){
			@listfonc2=split('=',$listfonc[$sj-1]);
			$score[$sj]=$score[$sj]+(&composite($listfonc2[1],$fmin[$pointeur_function],$fmax[$pointeur_function]))*$fw[$pointeur_function];
			$properties[$sj]=$properties[$sj]."PHARMACOPHORE conformer $sj = $listfonc2[1]\n";
			print "PHARMACOPHORE conformer $sj = $listfonc2[1]\n";
		};	
	};



	#120 GC Fingerprint: the conversion by sdfmol2 makes that only the first molecule of $file is analysed
	if($evaluate_finger){
		$pointeur_function=-1;
		foreach $prop (0..@fprop-1){
			$pointeur_function=$prop if($fprop[$prop] eq "fingerprint");
		};

		$fingerprint=0;
		# in similarities_GC.pl the conversion by sdfmol2 makes that only the first molecule of $file is analysed
		chop($tmpfinger=`$leaexe/similarities_GC.pl $simmeasure $simdescriptor $fingerref $file`);
		$tmpfinger=~s/(.*)= (.*)/$2/;
		$fingerprint=sprintf"%3.2f",$tmpfinger;

		print "GC SIMILARITIES with $fingerref ($simmeasure on $simdescriptor representation) = $fingerprint for all conformers\n";

		# fill @score for each conformers = same score for each
		foreach $sj (1..$nbchildevaluer){
			$score[$sj]=$score[$sj]+(&composite($fingerprint,$fmin[$pointeur_function],$fmax[$pointeur_function]))*$fw[$pointeur_function];
			$properties[$sj]=$properties[$sj]."GC SIMILARITIES conformer $sj with $fingerref ($simmeasure on $simdescriptor representation) = $fingerprint\n";
		};
	};


};


	if($nbpre_fonction > 0 && $evaluate_dock){
		$flagdock=0;
		foreach $sj (1..$nbchildevaluer){
			#print "$sj $score[$sj]\n";
			# $sumnbpre_fonction is the portion of non docking score
			$tmpscore=sprintf"%3.2f",($score[$sj]/$sumnbpre_fonction)*100;
			$flagdock=1 if($tmpscore >= $percentproperties);
			#one conformers with score > percentproperties is enough to launch docking on all conformers
		};
		if($flagdock){
			print "Conformers have properties ($tmpscore) >= $percentproperties \% => docking step accepted\n";
		}
		else{
			print "Conformers have not required properties ($tmpscore) < $percentproperties \% => docking step rejected\n";
		};	
	}
	elsif($evaluate_dock){
		$flagdock=1;
	};

	if($flagdock){
		
		#Docking
		$dockres=&docking($file);
		print "\nsub SCORE get:\n$dockres\n";
	
		$sj=1;	
		@listfonc=split('\n',$dockres);
		foreach $prop (0..@listfonc-1){
			@getline=split(' ',$listfonc[$prop]);
			if($getline[0]!~/^#/){
				foreach $prop (0..@fprop-1){
					if($fprop[$prop] eq "dock"){

					# CASE FLEXX
					if($docking_program eq "FLEXX"){ 
						if($getline[1]<0){ 
							$val = abs($getline[1]/($fmin[$prop]));
							$val=1.0 if($val > 1);
							$score[$sj]=$score[$sj]+($val*$fw[$prop]);
						};	
						
						#old calculation in LEA
						#$val = abs($prop{$fprop[$propi]}/(-60));
						#$val = abs($prop{$fprop[$propi]}/($fmin[$propi]));
						#$val=0.0 if($val < 0);
						#$val=1.0 if($val > 1);
						
						# disabled: ne permet pas un classement des scores car 100% pour
						# tout ce qui est entre $fmin et $fmax puis degressif
						#if($getline[1]<0){
						#	$score[$sj]=$score[$sj]+(&composite($getline[1],$fmin[$prop],$fmax[$prop]))*$fw[$prop];
							#print "conformer $sj global score= $getline[1]\n";
						#};	
					}
					elsif($docking_program eq "SURFLEX"){
						if($getline[1]>0){
							$val = abs($getline[1]/($fmax[$prop]));	
							$val=1.0 if($val > 1);
							$score[$sj]=$score[$sj]+($val*$fw[$prop]);
						};	
					}
					elsif($docking_program eq "PLANTS"){
						if($getline[1]<0){
							$val = abs($getline[1]/($fmin[$prop]));
							$val=1.0 if($val > 1);
							$score[$sj]=$score[$sj]+($val*$fw[$prop]);
						};	
					};
					
					};
				};
				$properties[$sj]=$properties[$sj].$listtitle."\n".$listfonc[$prop];
				$sj++;
			}
			else{
				$listtitle=$listfonc[$prop];
			};	
		};	
	};	


########## Convert total score in percentage
## LF: New Score calculation:
        print "-------------------\nSCORE:\n-------------------\n";
        print "nbatom Score: $score[1]\n" if ($sumw == 2);
	#$score[1]=0; #uncomment for nbatom hard constraint
	#$sumw = 2;  #for nbatom hard constraint set to 1
        foreach $sj (1..$nbchildevaluer){
                print "COSMO-Score: $cosmoscore[$gi+1]\n";
                $score[$sj] = $score[$sj]+$cosmoscore[$gi+1]*$fw[0];
                $totalscore = $score[$sj]/$sumw;
                print "TOTAL SCORE: $totalscore\n-------------------\n\n";
        }
        foreach $sj (1..$nbchildevaluer){
        print "sj= $sj score[sj]= $score[$sj] und sumw= $sumw \n";
                $score[$sj]=sprintf"%3.2f",($score[$sj]/$sumw)*100;
                print "$sj $score[$sj]\n";
        };

        @scoretmp="";
        @scoretmp=@score;
        &decreasing_order;
        print "Conformers: decreasing order of scores:\n";
        foreach $sj (1..$nbchildevaluer){
                print "$sj $score[$ranking[$sj]] mol$ranking[$sj]\n";
        };

};

################################################################################
################################################################################

sub decreasing_order{

	@ranking="";
	$debj=0;
	$debj=1 if($scoretmp[0] eq "");
	foreach $sj ($debj..@scoretmp-1){
		$ranking[$sj]=$sj;
	};	
	foreach $sj ($debj..@scoretmp-1){
		$maxclass=$scoretmp[$sj];
		$maxi=$sj;
		foreach $k ($sj..@scoretmp-1){
			if ($maxclass < $scoretmp[$k]){
				$maxclass=$scoretmp[$k];
				$maxi=$k;
			};
		};
		if($maxi != $sj){
			$tmp=$scoretmp[$maxi];
			$scoretmp[$maxi]=$scoretmp[$sj];
			$scoretmp[$sj]=$tmp;
			$tmp=$ranking[$sj];
			$ranking[$sj]=$ranking[$maxi];
			$ranking[$maxi]=$tmp;
		};	
	};
};

#################################################################################
#################################################################################

sub composite{

        local($valeur,$compmin,$compmax)=@_;

# Fournit un score entre 0 (unfitted) et 1 (perfect fit)

        $flagcompmin=0;
        $flagcompmax=0;


        if ($compmax eq '' && $compmin ne ''){
                $flagcompmin=1;
        }
        elsif($compmin eq '' && $compmax ne ''){
                $flagcompmax=1;
        };

        $val=0;
        if ($flagcompmin){
                $val=1 if ($valeur >= $compmin);
        }
        elsif($flagcompmax){
                $val=1 if ($valeur <= $compmax);
        }
        else{

		# in order to give a score to value far from required
		# $ecartminmax=($compmax-$compmin)/4; #hard
		$ecartminmax=($compmax-$compmin); #smooth

       	 	$sqecartminmax=$ecartminmax*$ecartminmax;

        	if ($compmin == $compmax){
                	  if ($valeur != $compmin){
	
				# in order to give a score to value far from required
				#   $ecartmin=$compmin*3/4; # hard
				#   0.9 means that val will not be zero from 10% to 100%
				$ecartmin=$compmin*0.9; # smooth give score from 10% of the target value

                        	$sqecartmin=$ecartmin*$ecartmin;
                        	if ($valeur < $compmin){
                                	$val=$sqecartmin-(($compmin-$valeur)*($compmin-$valeur));
                                	if($val < 0){# in case ($compmin-$valeur)**2 is greater than $sqecartmin
						$val=0;
					}
					else{
						$val=$val/$sqecartmin;
					};	
                        	}
                        	else{
                                	$val=$sqecartmin-(($valeur-$compmin)*($valeur-$compmin));
                                	if($val < 0){# in case ($compmin-$valeur)**2 is greater than $sqecartmin
						$val=0;
					}
					else{
						$val=$val/$sqecartmin;
					};	
                        	};
                  	}
                  	else{
                        	$val=1;
                  	};
		}
        	else{
                	if ($valeur < $compmin){
                       		#$val=$sqecartminmax-(($compmin-$valeur)*($compmin-$valeur));
				#$val=$val/$sqecartminmax;
				
                        	$ecartmin=$compmin*0.9;
				$sqecartmin=$ecartmin*$ecartmin;
				$val=$sqecartmin-(($compmin-$valeur)*($compmin-$valeur));

				$val=$val/$sqecartmin;
                  	}
                 	elsif ($valeur > $compmax){
                        	#$val=$sqecartminmax-(($valeur-$compmax)*($valeur-$compmax));
                        	#$val=$val/$sqecartminmax;
				
				$ecartmin=$compmax*0.9;
				$sqecartmin=$ecartmin*$ecartmin;
				$val=$sqecartmin-(($valeur-$compmin)*($valeur-$compmin));

				$val=$val/$sqecartmin;
                  	}
                  	else{# between or == $compmax or == $compmin
                        	$val=1;
                  	};
		};
	};

        if(($val>1)||($val<0)|| $valeur eq ''){
        	$val=0;
        }
	else{
		$val=sprintf"%4.3f",($val);
	};
	$val;
};

###################################################################################

######################################################################################
sub changesdf{
#-------------------------------------------------------------------------------------
# changesdf subroutine
# Execute RDKIT (part of the COSMOquick programme) to create a sdf-file, which is
# readable by COSMOconf.
#-------------------------------------------------------------------------------------
		# Replace '+' by '#' in the USMILES to get back to valid USMILES-Code
		$USMILES_sharp = $USMILES;
		$USMILES_sharp =~ tr/+/#/;

		# Same for brackets []
		$USMILES_sharp =~ tr/^/[/;
		$USMILES_sharp =~ tr/_/]/;

		# Write USMILES-Code in currentSMILES.smi-file
		open(SMI,">currentSMILES.smi") or die "I cannot open the SMILES-file currentSMILES.smi";
	 	print SMI "$USMILES_sharp $USMILES";	
		close(SMI);
			
		# Call RDKIT to create a new sdf file
		system("$generalFolder/$rdkitdir currentSMILES.smi mol.sdf >log_RDKIT");
}
######################################################################################

######################################################################################
sub getUSMILES{
#-------------------------------------------------------------------------------------
# getUSMILES subroutine
# Execute COSMOfrag to retrieve USMILES-Code of current molecule. The USMILES is used
# throughout the whole programme to identify the molecule.
# The variable $USMILES is globally accessable by every subroutine of LEA3D.
#-------------------------------------------------------------------------------------
		local $number = $_[0];

		&writeFindUSMILESinput($number);
		system("$generalFolder/$cosmofragdir frag_USMILES_$number.inp >log_frag_$number"); # cosmo call
                open(USMILES_FILE, "$param{WORKDIR}/frag_USMILES_$number.out");
                $searchfor = "USMI:";
                local @line_string = grep{/$searchfor/}<USMILES_FILE>;
                local @USMILES_string = split ( /\s+/, $line_string[0]);
                local $USMILES = $USMILES_string[2];
                close USMILES_FILE;
		unlink("frag_USMILES_$number.out");
		unlink("frag_USMILES_$number.inp");
		unlink("frag_USMILES_$number.tab");
		unlink("log_frag_$number");
	
		# Replace '#' by '+' because COSMOtherm and COSMOconf cant handle '#' in the filename
                $USMILES =~ tr/#/+/;

		# Same for cling brackets
		$USMILES =~ tr/[/^/;
		$USMILES =~ tr/]/_/;

		if ($cosmofrag_precalc  or $cosmomethod eq 'COSMOfrag'){
			system("cp $param{WORKDIR}/mol_$number.mcos $param{WORKDIR}/$gatecosmo/'$USMILES'.mcos");
		}
		unlink("mol_$number.mcos");
		return $USMILES;
}	
######################################################################################

######################################################################################
sub getproperties{
#-------------------------------------------------------------------------------------
# getproperties subroutine:
# LEA built-in subroutine determining molecular properties of current molecule
# For COSMO-CAMD: 
#                 - get molecular weight (mm)
#                 - get number of heavy atoms (nbatom)
#-------------------------------------------------------------------------------------
if($evaluate_prop){
		local $file = $_[0];
                if($param{CHARGES} eq 'GAS' || $param{CHARGES} eq 'AM1'){
                        chop($tmpprop=`$leaexe/properties.pl $file 1 $param{CHARGES} 0`);
                        unlink "vector_dipole.pdb" if (-e "vector_dipole.pdb");
                }
                else{
                        chop($tmpprop=`$leaexe/properties.pl $file 0 - 0`);
                };

                open(MOL,"<properties.out");
                $sj=1;
                while(<MOL>){
                        @getline=split(' ',$_);
                        if($getline[0]!~/^#/){
                                $propertiestitle="";
                                $propertiesvalue="";
                                foreach $k (2..@gettitle-1){
                                        foreach $prop (0..@fprop-1){
                                                if($fprop[$prop] eq $gettitle[$k]){
                                                        # composite score score[$sj]
                                                        #print "conformer $sj $gettitle[$k] = $getline[$k]\n";
                                                        # give a score between 0 and 1
                                                        $score[$sj]=$score[$sj]+(&composite($getline[$k],$fmin[$prop],$fmax[$prop]))*$fw[$prop];
                                                        $propertiestitle=$propertiestitle."$gettitle[$k] ";
                                                        $propertiesvalue=$propertiesvalue."$getline[$k] ";
                                                };
					};

#### LF Get molecular weight and nbatom
					if($gettitle[$k] eq "mm"){
                                                $mwSolvent = $getline[$k];
                                                #print "conformer 1 mm = $mwSolvent\n";
					};
                                        if($gettitle[$k] eq "nbatom"){
	                                         $nbatom = $getline[$k];
						 #print "conformer 1 nbatom = $nbatom\n"
					};
## LF						

                                };
                                #To retreive all properties:
                                #$properties[$sj]=$properties[$sj]."#".$listtitle.$_;   
                                #or to retreive selected properties (in function)
                                $properties[$sj]=$properties[$sj]."#".$propertiestitle."\n".$propertiesvalue."\n";
                                $sj++;
                        }
                        else{
                                $_ =~s/^#//;
                                $listtitle=$_;
                                @gettitle=split(' ',$_);
                        };
                };
                close(MOL);
                unlink "properties.out";

		return ($mwSolvent, $nbatom);
        };
}
######################################################################################

######################################################################################
sub writetoFails{
#------------------------------------------------------------------------------------
# writetoFails subroutine:
# Write all USMILES of molecules not meeting the molecular constraints (nbatom,
# atom-limit, desired type) or failing process requirements (LLE, P_Fructose etc.)
#------------------------------------------------------------------------------------
                     # Print header (only once after LEA started, the variable 
                     # $firstfail was added to MAIN.pl and is set to 1, when LEA starts)       
                     if ($firstfail){
                        open(FAILS,"+>failed_molecules_log.txt");
                        $text ="
##########################################################################
  LOGFILE OF FAILED MOLECULES
##########################################################################


Starting time: $current_time\n\n\n";
                        print FAILS $text;
			$firstfail = 0;
                        }
                     else{
                        open(FAILS,">>failed_molecules_log.txt");
			}
			
			print FAILS "$USMILES\t\t";
			print FAILS "undesired type   " if(!$desiredtype);
			print FAILS "out of nbatom range   " if(!$targetingnbatom);
			print FAILS "Number of $atom_to_limit too high   " if (!$atom_limit_ok);
			print FAILS "Scoring unsuccessful" if ($scoring_unsuccessful);
			print FAILS "\n";
			
			close FAILS;
}
######################################################################################

#####################################################################################
sub call_matlab{
# -----------------------------------------------------------------------------------
# call_matlab  
# subroutine calls MATLAB for property prediction and process model evaluation
# -----------------------------------------------------------------------------------
			local $jobtype = $_[0];
			local $precalc = $_[1];
			local $noofcalc =  $_[2];
			local $parametrization = $_[3];
			local $USMILES = $_[4];

			if ($jobtype eq 'Scoring'){
				system("matlab -nosplash -nodisplay -logfile log_matlab_$noofcalc.log -singleCompThread -r \"chdir\('$param{WORKDIR}'\), Gate2LEA('Scoring', {'$param{WORKDIR}/$Gatedir';'$param{WORKDIR}/$gatecosmo';'$param{WORKDIR}/$gatemolstruct';'$param{WORKDIR}/$gateout2lea'},'$USMILES', '$parametrization', '$noofcalc', $precalc), quit\" " );
			}
			elsif ($jobtype eq 'Sum_molStructs'){
				system("matlab -nojvm -nosplash -nodisplay -logfile log_matlab.log -singleCompThread -r \"chdir\('$param{WORKDIR}'\), Gate2LEA('Sum_molStructs', {'$param{WORKDIR}/$Gatedir';'$param{WORKDIR}/$gatecosmo';'$param{WORKDIR}/$gatemolstruct';'$param{WORKDIR}/$gateout2lea'}, '', '', '', $precalc), quit\"" );
			unlink("log_matlab.log");
			}
			elsif ($jobtype eq 'Write_specs'){
                                system("matlab -nojvm -nosplash -nodisplay -singleCompThread -r \"chdir\('$param{WORKDIR}'\), Gate2LEA\('Write_specs', {'$param{WORKDIR}/$Gatedir';'$param{WORKDIR}/$gatecosmo';'$param{WORKDIR}/$gatemolstruct';'$param{WORKDIR}/$gateout2lea'}, 'Solvent X', '$parametrization', 'writing_specs'\), quit\"" );
			}

			chdir($param{WORKDIR});
}
######################################################################################

#####################################################################################
sub results_of_mol{
# -----------------------------------------------------------------------------------
# results_of_mol  
# Display results of process model evaluation done in MATLAB
# 
# -----------------------------------------------------------------------------------
			local $molno = $_[0];

                        chdir($gateout2lea);			

                        # Grep values from result file out_LEA_#.txt
                        open(Outputmatlab, "<out_LEA_$molno.txt") or print "out_LEA_$molno.txt did not exist. Error in process model evaluation!\n\n";
                        while($reihe = <Outputmatlab>){ chomp $reihe;
                                                        $results[$.-1] = $reihe;
                        }

		 	$cosmoscore[$gi+1] = $results[0];
			$ConCheck = $results[1];

			foreach $w (2..$#results){
              			print "$results[$w] \n";
			}
                        close Outputmatlab;

			# Remove temporary result files
			unlink("out_LEA_$molno.txt");
                        chdir("$param{WORKDIR}");

			# Add to fails if Score = 0
                        if (($ConCheck == 0) and !($molno eq 'child')){
                                        $scoring_unsuccessful = 1;
                                        &writetoFails;
                                        $no_of_failed_mols_unqualified++;
                                        $scoring_unsuccessful = 0;
                        }

			@results=(0,0,0,0);
}
######################################################################################

######################################################################################
sub evaluate_atom_limit{
# -----------------------------------------------------------------------------------
# evaluate_atom_limit
# Count atoms of type specified in $atom_to_limit in the molecule and check if 
# number is smaller than limit specified by $atom_limit.
# $atom_to_limit and $atom_limit are defined in LEA_run_options.pl
# Upper and lower case letters are counted.
# -----------------------------------------------------------------------------------
			$USMILES = $_[0];

			# Uppercase
                        $atom_count = eval "'$USMILES' =~ tr/$atom_to_limit//";
			
			# Lowercase
			$atom_to_limit_lc = lc $atom_to_limit;

			$atom_count_2 = eval "'$USMILES' =~ tr/$atom_to_limit_lc//";
			$atom_count = $atom_count + $atom_count_2;
			
                        print "Atom limit evaluation: n_$atom_to_limit =  $atom_count\n";
			
			if ($atom_count > $atom_limit){
				$atom_count_ok = 0;
				#print "Too many $atom_to_limit -atoms in the molecule ($atom_count > $atom_limit) \n";
			} 
			else {
				$atom_count_ok = 1;
				#print "Atom limit ok ($atom_count =< $atom_limit).\n";
			}
			return $atom_count_ok;
}
######################################################################################

######################################################################################
sub summary_of_SMILES{
# -----------------------------------------------------------------------------------
# summary_of_SMILES
# Create and maintain an array of all molecules scored and built with corresponding
# score.  
# -----------------------------------------------------------------------------------
		$USMILES = $_[0];
		$USMILES_Score = $_[1];
		
		my $length_array = @list_of_all_SMILES;
	
		if ($length_array eq 0){
					$list_of_all_SMILES[0] = $USMILES;
					$list_of_all_SMILES_Scores[0] = $USMILES_Score;
		}
		else{
                	for(@list_of_all_SMILES){ 
                                         if($USMILES eq $_){ 	$present = 1;
								last;
					 }
                                         else { $present = 0;
                                         }
                	}
			if (!$present){ $list_of_all_SMILES[$length_array] = $USMILES;
                                        $list_of_all_SMILES_Scores[$length_array] = $USMILES_Score;
			}
		}
}
######################################################################################

