#!/usr/bin/perl


$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

require "$leaexe/CYCLE_SDF.pl";

$filealire='';
$phos='';
$d2='';
$ncassy='';
$metal='';
$ftox='';
$ruleslip='';
$flagionis='';

local($filealire,$phos,$d2,$ncassy,$metal,$ftox,$ruleslip,$flagionis,$verbosity)=@ARGV;

if ($filealire eq '' || $phos eq '' || $d2 eq '' || $ncassy eq '' || $metal eq '' || $ftox eq '' || $ruleslip eq '' || $flagionis eq '' ){
	die "\n
	usage: drug <file.sdf>    <'P' (keep P) or not '-'>  <'3D' (keep 3D only) or not '-'>  <nb (maximal number of C* or no constraints '-'>  <'M' (keep metal) or not '-'>  <'T' or '-' (remove molecules with reactive functions)>  < 'L' (remove if not Lipinski like) or '-'> <'I' (ionisation) or not '-'> < 0 or 1 (verbosity mode)>
	\n Usually: \n
	drug <file.sdf> P 3D - - T - I 0  \n
	Keep molecules with phosphorus atoms, only 3D converted, no constraint on the number of C*, remove metals, warned if reactive functions and if not Lipinski like, ionize molecules and no verbosity mode
	
	\n\n";
};

chop($workdir = `pwd` );

if($verbosity eq ''){
	$verbosity=0;
};

$ncassy=10000 if($ncassy eq '-');
$phos='' if($phos eq '-');
$d2='' if($d2 eq '-');
if($flagionis eq 'I' || $flagionis eq 'i'){
	$flagionis=1;
}
else{
	$flagionis=0;
};

# Reactive function
$listefonction="halp_hals_thiocyanate_isocyanate_dilmid_nitrosamine_acylhalide_halocarbonyl_azo_thiosulfate_trialkylphosphine_hydrazine_cyanohydrine_oximester_anhydride_perhaloketone_thioester_osimpleo_pp_ss_aldehyde_imine_epoxydre_aziridine_dicarbonyl_dicarbonylt_michaelacc_haln_so2f_foso_thioepoxyde";


system("/bin/rm -f $workdir/notdruglike.sdf");
system("/bin/rm -f $workdir/druglike.sdf");

	%tabR=(
		'C',1.90,
		'O',1.74,
		'N',1.82,
		'S',2.11,
		'P',2.05,
		'Br',2.18,
		'Cl',2.03,
		'I',2.32,
		'F',1.65,
		'H',1.5,
		'Hp',1.1,
	);

	%tabmm=(
		'C',12,
		'O',16,
		'N',14,
		'S',32,
		'P',31,
		'Br',80,
		'Cl',35.4,
		'I',127,
		'F',19,
		'H',1,
	);

	%prop=(
		'psa',0.0,
		'mm',0.0,
		'logp',0.0,
		'nbhd',0.0,
		'nbha',0.0,
		'nbatom',0.0,
		'fonction',0.0,
		'lipinski',0.0,
	);

	%maxvalence=(
		'O',2,
		'C',4,
		'H',1,
		'N',3,
		'S',6,
		'P',5,
		'Br',1,
		'Cl',1,
		'I',1,
		'F',1,
	);

	$debug=0;	
	$rasmol=0;

        # Drug like molecules: Lipinski rules + PSA
        $lip_psa=140;
        $lip_mw=500;
        $lip_nbah=10;
        $lip_nbdh=5;
        $lip_atom=50;
        $lip_logp=6;
	

if($verbosity > 0){	
	if( $ruleslip eq 'L' ){	
	print "\nFiltering:\n\tPSA < $lip_psa A**2 \n\tMW < $lip_mw \n\tNB AH < $lip_nbah \n\tNB DH < $lip_nbdh \n\tNB ATOMS < $lip_atom \n\tXLOGP < $lip_logp\n\n";
	};

	if($metal eq '-'){
		print "\tRemove molecules containing: Tc Pt Ga Co Cu Te Zn B Si Ge Fe Se At Au Al As Hg Sn K Na\n";
	};

	if($phos ne 'P'){
		print "\tRemove molecules containing phosphorous atoms\n";
	};

	if( $ncassy < 10000){
		print "\tRemove molecules containing more than $ncassy asymetric carbons\n";
	};

	if($d2 eq '3D'){
		print "\tRemove molecules not converted in 3D\n";
	};

	if($ftox eq '-'){
		print "\tRemove molecules containing reactive functions: $listefonction\n";
	};

	if($flagionis){
		print "\tIonize molecules\n";
	};

	print "\nMolecules:\n\n";
};

#**********************************************************************************************
##### PROPRIETES FROM SDF
	
	$moli=0;
	$flagnew=1;
	$flagmend=0;
	
	$elem=0;
	$nbmolassy=0;
	$nd2=0;
	$blanc=' ';
	$nbmol=0;
	$nblip=0;
	$nbftox=0;
	
	open(MOL,"<$filealire");
	while(<MOL>){
	
		if($flagnew){
			
			$oneatome=0;
			$oneatomep=0;
			$eliminemol=0;
			$elemold=$elem;
			$pas3d=0;
			$nbmol++;
			
			$masse=0;
			@tabfilesdf='';
			$compt=0;
			$ig=1;
			$jg=0;
			$moli++;
			
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			@coval='';
			@nbcoval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			@radius='';
			@listb='';
			
			$blanc=' ';	
			$atomlourd=0;
			
			$lignecharge="";
			@getlignechg="";

			$flagnew=0;
		};
		
		$tabfilesdf[$compt]=$_;
		
		@getstr = split(' ',$_);
		$compt++;
		
		if (($compt > 4) && ($ig <= $istratom)){
		
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];
			$group[$ig]="";	
			$atomlourd++ if($getstr[3] ne 'H');
			$radius[$ig]=$tabR{$getstr[3]};
			$masse=$masse+$tabmm{$getstr[3]};
			
			#Elements non organiques rejet�s
			if($getstr[3] eq 'Tc' || $getstr[3] eq 'Pt' || $getstr[3] eq 'Ga' || $getstr[3] eq 'Co' || $getstr[3] eq 'Cu'){
				$oneatome=1;
				$elem++;
			};
			if($getstr[3] eq 'Te' || $getstr[3] eq 'Zn' || $getstr[3] eq 'B' || $getstr[3] eq 'Si' || $getstr[3] eq 'Ge'){
				$oneatome=1;
				$elem++;
			};			
			if($getstr[3] eq 'Fe' || $getstr[3] eq 'Se' || $getstr[3] eq 'At' || $getstr[3] eq 'Au' || $getstr[3] eq 'Al'){
				$oneatome=1;
				$elem++;
			};				
			if($getstr[3] eq 'As' || $getstr[3] eq 'Hg' || $getstr[3] eq 'Sn' || $getstr[3] eq 'K' || $getstr[3] eq 'Na'){
				$oneatome=1;
				$elem++;
			};			
			if ($getstr[3] eq 'P' && $phos ne 'P'){
			      	$oneatomep=1;
				$elem++;
			};
			
			
			#verifie si 3ieme dimension
                	$pas3d=$pas3d+($getstr[2]*$getstr[2]);
                	
                	
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{
                               @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
				elsif(@coller==4){
					if($_=~/^\s/){
						$getstr[0]=$coller[0];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[1].$coller[2].$coller[3];
					}
					else{
						$getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
						$getstr[1]=$coller[3];
					};
				}	
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };

                                $listb[$getstr[0]]=$listb[$getstr[0]].$blanc.$getstr[1];

                                $listb[$getstr[1]]=$listb[$getstr[1]].$blanc.$getstr[0];

				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];
                                $nbcoval[$getstr[0]]=$nbcoval[$getstr[0]]+1;

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];
                                $nbcoval[$getstr[1]]=$nbcoval[$getstr[1]]+1;

####### Elimine les covalences bizarres
				$eliminemol=1 if($coval[$getstr[0]] > $maxvalence{$atom[$getstr[0]]} && $atom[$getstr[0]] ne "N");
				$eliminemol=1 if($coval[$getstr[1]] > $maxvalence{$atom[$getstr[1]]} && $atom[$getstr[1]] ne "N");
				$eliminemol=1 if($getstr[2] == 0);

				print "liaison $getstr[0] et $getstr[1] de type nul !\n" if($verbosity && $getstr[2] == 0); 
				
				print "atome $getstr[0] valence $coval[$getstr[0]] > max = $maxvalence{$atom[$getstr[0]]}\n" if($verbosity && $coval[$getstr[0]] > $maxvalence{$atom[$getstr[0]]} && $atom[$getstr[0]] ne "N");
				print "atome $getstr[1] valence $coval[$getstr[1]] > max = $maxvalence{$atom[$getstr[1]]}\n" if($verbosity && $coval[$getstr[1]] > $maxvalence{$atom[$getstr[1]]} && $atom[$getstr[1]] ne "N");
				$jg++;
			};
		};
		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];
                        $eliminemol=1 if ($istratom == 1);

			@coller=split(' *',$istratom);
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

		
		};
		if($_=~/^M  CHG/ || $_=~/^M CHG/){
			$lignecharge=$_;
			$lignecharge=~s/M//;
			$lignecharge=~s/CHG//;
			@getlignechg=split(' ',$lignecharge);
		};
		if ($_=~/\$\$\$\$/){
			
			$flagnew=1;
			$flagmend=0;
			print "\tMOL $moli " if($verbosity);
			
			#elimine les metaux
			if($oneatome && $metal eq '-'){
				$eliminemol=1;
			};
			
			#pour le phosphore
			if($oneatomep && $phos ne 'P'){
				$eliminemol=1;
			};
			
			#Recherche le nombre de C assymetriques
			$molassy=&cassy;
			$eliminemol=1 if($molassy > $ncassy);
			$nbmolassy++ if($molassy > $ncassy);
			
			#Si pas 3D comme requis
			$eliminemol=1 if ($d2 eq '3D' && $pas3d==0.0);
			$nd2++ if ($d2 eq '3D' && $pas3d==0.0);
			
			$elemoldn=$elem-$elemold;
               		print "\t   [$molassy c* ; $elemoldn elements non organiques ; 3D] " if($eliminemol && $pas3d!=0.0 && $verbosity);
               		print "\t   [$molassy c* ; $elemoldn elements non organiques ; 2D] " if($eliminemol && $pas3d==0.0 && $verbosity);
               		

			if( $ruleslip eq 'L' ){
				#Lipinski
				&calprop;
				$eliminemol=1 if($prop{lipinski}==0);

			};
			$nblip++ if($prop{lipinski}==0);

			#used by &fonction for ammonium and by $ionise...
			$ammonium=0;
			$nbchg=0;
			$sdfchg="";

			if($ftox eq '-' || $flagionis){
				#Recherche des cycles
				@cyclef='';
				&cyclesdf;

				$prop{fonction}=&fonction;
				$eliminemol=1 if($prop{fonction}==0 && $ftox eq '-');
			};			
			$nbftox++ if($prop{fonction}==0);
		
	
			if(($ioniseacide ne '' || $ionisenitrogen ne '') && $flagionis){
			        print "\t   ionisation 1 (charge -) (carboxylic acid, phosphate, phosphonate and cyclic phosphate) by removing H$ioniseacide " if($verbosity && $ioniseacide ne '');
		         	print "\tionisation 1 (charge +) (amidinium and guanidinium) by adding 2H minus old one" if($verbosity && $ionisenitrogen ne '');
				
				@hionis=split(' ',$ioniseacide);
				$nbhionis=@hionis;
		
				$ionisenitrogen=~s/^;//;
				@hionisp=split(';',$ionisenitrogen);
				$nbhionisp=@hionisp;
				@hionisp2=split(' ',$ionisenitrogen2);
			        
				$longa=$istratom-$nbhionis+$nbhionisp;
			        $longb=$istrbond-$nbhionis+$nbhionisp;
				
				if($eliminemol){
					open(OUG,">>notdruglike.sdf");
				}
				else{
					open(OUG,">>druglike.sdf");
				};
					foreach $p (0..2){
						printf OUG "$tabfilesdf[$p]";
					};
					printf OUG "%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$longa,$longb;
					
					$niveaubond=$istratom+3;
					#coordinates
					$longacount=0;	
					foreach $rt (4..$niveaubond){
						#print "$rt $tabfilesdf[$rt]\n";
						$ecrit=1;
						foreach $p (0..@hionis-1){
							$ecrit=0 if(($hionis[$p]+3) == $rt);
							#print "enleve $rt $tabfilesdf[$rt]\n"  if(($hionis[$p]+3) == $rt);
						};
						if($ecrit){
							printf OUG "$tabfilesdf[$rt]";
							$longacount++;
						};
					};
					
					# Add H coordinates if protonation
					$ionisenitrogen3=""; 
					foreach $p (0..@hionisp-1){
						printf OUG "$hionisp[$p]";
						$longacount++;
						$ionisenitrogen3=$ionisenitrogen3." $longacount ";
					};
					@hionisp3=split(' ',$ionisenitrogen3);

					## BOND renumbering des numero un par un
					foreach $p (0..@hionis-1){
					#print "\nhionis@hionis\n";
					        $arret=0;
						foreach $rt ($niveaubond+1..@tabfilesdf-1){
						
							$arret=1 if($tabfilesdf[$rt] eq '' || $tabfilesdf[$rt]=~/^>/ || $tabfilesdf[$rt]=~/^M /);
						
							if($arret == 0){
						
							if($tabfilesdf[$rt] ne "enleve"){
							
							@recupbond=split(' ',$tabfilesdf[$rt]);
						
							@coller=split(' *',$recupbond[0]);
							@coller2=split(' *',$recupbond[1]);
							if(@coller==6 && $recupbond[1] ne ""){
								$recupbond[0]=$coller[0].$coller[1].$coller[2];
								$recupbond[2]=$recupbond[1];
								$recupbond[1]=$coller[3].$coller[4].$coller[5];
							}
							elsif(@coller==6 && $recupbond[1] eq ""){
								$recupbond[0]=$coller[0].$coller[1];
								$recupbond[1]=$coller[2].$coller[3].$coller[4];
								$recupbond[2]=$coller[5];
							}
							elsif(@coller==5){
								if($tabfilesdf[$rt]=~/^\s/){
									$recupbond[0]=$coller[0].$coller[1];
									$recupbond[2]=$recupbond[1];
									$recupbond[1]=$coller[2].$coller[3].$coller[4];
								}
								else{
									$recupbond[0]=$coller[0].$coller[1].$coller[2];
									$recupbond[2]=$recupbond[1];
									$recupbond[1]=$coller[3].$coller[4];
								};
							}
							elsif(@coller==4){
								if($tabfilesdf[$rt]=~/^\s/){
									$recupbond[0]=$coller[0];
									$recupbond[2]=$recupbond[1];
									$recupbond[1]=$coller[1].$coller[2].$coller[3];
								}
								else{
									$recupbond[0]=$coller[0].$coller[1].$coller[2];
									$recupbond[2]=$recupbond[1];
									$recupbond[1]=$coller[3];
								};
							}
							elsif(@coller2==4){
								$recupbond[1]=$coller2[0].$coller2[1].$coller2[2];
								$recupbond[2]=$coller2[3];
							}
							elsif(@coller==7){
								$recupbond[0]=$coller[0].$coller[1].$coller[2];
								$recupbond[1]=$coller[3].$coller[4].$coller[5];
								$recupbond[2]=$coller[6];
							};
		
									$bond1=$recupbond[0];
									$bond2=$recupbond[1];
						
									$ecrit=1;
									if($bond1 == $hionis[$p] || $bond2 == $hionis[$p]){
										$ecrit=0;
									
										if($ionisenitrogen2!~/ $bond1 / && $ionisenitrogen2!~/ $bond2 /){
										# car enleve 1 H
										$nbchg++;
										if($bond1 == $hionis[$p]){
											$schg=sprintf "%3s",$bond2;
											$sdfchg=$sdfchg."$schg  -1";
										}
										else{
											$schg=sprintf "%3s",$bond1;
											$sdfchg=$sdfchg."$schg  -1";
										};
										};
										
										#print "$tabfilesdf[$rt]\n";
										$tabfilesdf[$rt]="enleve";
									};
						
									$bond1=$bond1-1 if($bond1 > $hionis[$p]);
									$bond2=$bond2-1 if($bond2 > $hionis[$p]);
							
									$bond3=$recupbond[2];
						
					 				$z="  0  0  0  0";
        	                					#$ligneb=pack("A3A3A3A12",$bond1,$bond2,$bond3,$z);
									$ligneb=sprintf "%3s%3s%3s$z",$bond1,$bond2,$bond3;
        	                		
        	                					$tabfilesdf[$rt]="$ligneb\n" if($ecrit);
        	                			
        	                					#printf OUG "$ligneb\n" if($ecrit);
        	                			
        	                				};
        	                			};
        	                  		};
        	                  		
        	                  		foreach $pppb ($p+1..@hionis-1){
        	                			$hionis[$pppb]=$hionis[$pppb]-1 if($hionis[$pppb] > $hionis[$p]);
						};
						foreach $pppb2 (0..@hionisp2-1){
							$hionisp2[$pppb2]=$hionisp2[$pppb2]-1 if($hionisp2[$pppb2] > $hionis[$p]);
						};
					};
				
					$flagaddhrogen=0;	
					$flagaddM=0;
					foreach $rt ($niveaubond+1..@tabfilesdf-1){
        	                			if($tabfilesdf[$rt] ne "enleve"){
								if($flagaddhrogen == 0 && ($tabfilesdf[$rt] eq '' || $tabfilesdf[$rt]=~/^>/ || $tabfilesdf[$rt]=~/^M / || $tabfilesdf[$rt]=~/\$\$\$\$/)){
									$flagaddhrogen=1;
									# Add H bonds if protonation
			                                        	foreach $p (0..@hionisp2-1){
										if($sdfchg !~/ $hionisp2[$p] /){
											$nbchg++;
											$schg=sprintf "%3s",$hionisp2[$p];
											$sdfchg=$sdfchg."$schg  +1";
										};	
										$bond1=$hionisp2[$p];	
										$bond2=$hionisp3[$p];
										$bond3=1;
										$z="  0  0  0  0";
										#$ligneb=pack("A3A3A3A12",$bond1,$bond2,$bond3,$z);
										$ligneb=sprintf "%3s%3s%3s$z",$bond1,$bond2,$bond3;
                          			                      		printf OUG "$ligneb\n";
                                        				};
								};
								if($flagaddM ==0 && $sdfchg ne "" && ($tabfilesdf[$rt] eq '' || $tabfilesdf[$rt]=~/^>/ || $tabfilesdf[$rt]=~/^M / || $tabfilesdf[$rt]=~/\$\$\$\$/)){
									$flagaddM=1;
									#printf OUG "M  CHG %2s $sdfchg\n",$nbchg;
									
									#modif oct 2008
									if($getlignechg[0] ne ""){
										$getlignechg[0]=$getlignechg[0]+$nbchg;
										printf OUG "M  CHG %2s",$getlignechg[0];
										$mchg=1;
										while($mchg <= @getlignechg-1){
											printf OUG " %3s %3s",$getlignechg[$mchg],$getlignechg[$mchg+1];
											$mchg=$mchg+2;
										};
										printf OUG " $sdfchg\n";
									}
									else{
										printf OUG "M  CHG %2s $sdfchg\n",$nbchg;
									};	

								};

        	                				printf OUG "$tabfilesdf[$rt]" if($tabfilesdf[$rt]!~/^M  CHG/ && $tabfilesdf[$rt]!~/^M CHG/);
        	                			};
        	                  	};
					close(OUG);	
			}
			else{
				if($eliminemol){
					open(OUG,">>notdruglike.sdf");
					foreach $rt (0..@tabfilesdf-1){
						printf OUG "$tabfilesdf[$rt]";
					};
					close(OUG);	
				}
				else{
					open(OUG,">>druglike.sdf");
					$flagaddM=0;
					foreach $rt (0..@tabfilesdf-1){
						if($flagaddM ==0 && $ammonium == 1 && $getlignechg[0] ne "" && ($tabfilesdf[$rt] eq '' || $tabfilesdf[$rt]=~/^>/ || $tabfilesdf[$rt]=~/^M / || $tabfilesdf[$rt]=~/\$\$\$\$/)){
							$flagaddM=1;
							printf OUG "M  CHG %2s",$getlignechg[0];
							$mchg=1;
							while($mchg <= @getlignechg-1){
							#for $mchg (1..@getlignechg-1,$mchg+2){
								printf OUG " %3s %3s",$getlignechg[$mchg],$getlignechg[$mchg+1];
								$mchg=$mchg+2;
							};
							printf OUG "\n";
						}
						else{	
							printf OUG "$tabfilesdf[$rt]";
						};	
					};
					close(OUG);
				};			
			};
			
			print "\n" if($verbosity);
		};
		
		if ($_=~/>/ && $_=~/<ID>/){
			@getmoli=split(' ',$_);
			$moli=$getmoli[2];
			$moli=~s/\(//;
			$moli=~s/\)//;
		};	
			
	};
	close(MOL);
	
	if ($verbosity){
		print "\nResults:\n\n";
		print "\tInput: $nbmol molecules (output: see druglike.sdf and notdruglike.sdf)\n";
		print "\tNon organic molecules: $elem\n" if($metal eq '-');
		print "\tNumber of molecules containing more than $ncassy C*: $nbmolassy\n" if($ncassy < 10000);
		print "\tNumber of unconverted molecules: $nd2\n" if($d2 eq '3D');
		print "\tNumber of non Lipinski like molecules: $nblip\n" if($ruleslip eq 'L');
		print "\tNumber of molecules containing reactive functions: $nbftox\n" if($ftox eq '-');
		print "\tIonisation\n" if($flagionis);	
	};
	


###################################################################################
###################################################################################

sub egaux{

	local($fv1,$fv2)=@_;

 	$equal=0;
	if($nbcoval[$fv1] == $nbcoval[$fv2] && $atom[$fv1] eq $atom[$fv2]){	
		@voisa=split(' ',$fonc[$fv1]);
		@voisb=split(' ',$fonc[$fv2]);

		$nbequal=0;	   			
		foreach $vi (0..@voisa-1){	
			foreach $vi2 (0..@voisb-1){
				if($voisa[$vi] eq $voisb[$vi2] && $voisa[$vi] ne '' && $voisb[$vi2] ne ''){
					$voisa[$vi]='';
					$voisb[$vi2]='';
					$nbequal++;
				};
			};
		};
		$equal=1 if($nbequal == @voisa);
	};


	$equal;						   								   			
};


###################################################################################

sub cassy{

# Recherche c*   si 1 fois substituants identiques alors pas de c*	
$subdiff2=0;

@coupletest='';
@valctest='';
$vali=0;
	
foreach $ty (1..$istratom){

	if($atom[$ty] eq 'C' && $nbcoval[$ty] == 4){
			 	
		print "\t C $ty $nbcoval[$ty] voisins $fonc[$ty]\n" if($debug);
		print "\t C $ty $nbcoval[$ty] voisins $ifonc[$ty]\n" if($debug);
		
		@pilea='';
		@pileb='';
		@tpilea='';
		@tpileb='';
		@cpile='';
					
		$pi=0;
		$subdiff=0; # C non assymetrique
					
		@gh='';
		@ghi='';
		@gh=split(' ',$fonc[$ty]);
		@ghi=split(' ',$ifonc[$ty]);
		foreach $tp (0..@gh-2){
			foreach $tp2 ($tp+1..@gh-1){
				if(&egaux($ghi[$tp],$ghi[$tp2])){  # egaux donc il faut aller plus loin
					$pilea[$pi]=$ghi[$tp];
					$tpilea[$pi]=$ty;
					#print "a @pilea\n";
					$pileb[$pi]=$ghi[$tp2];
					$tpileb[$pi]=$ty;
					#print "b @pileb\n";
					$cpile[$pi]='';
					$pi++;
				};
			};
		};
					
					
		$pos='';
		foreach $io (0..@pilea-1){
			print "\t $pilea[$io]\t$pileb[$io]\t$tpilea[$io]\t$tpileb[$io]\t$cpile[$io]\n" if($debug);
			$pos=$io if($cpile[$io] eq '' && $pilea[$io] ne '');
		};
					
		while ($pos ne ''){
			print "pos $pos\n" if($debug);
			$at1=$pilea[$pos];
			$at2=$pileb[$pos];
					
			$pasdesuite=1;
			while($pasdesuite){
						
				$long= @pilea-$pos-2;
				#print "long $long\n" if($debug);
				if($long > 0){
							
					@cass='';
					$csi=0;
					foreach $r ($pos+1..@pilea-1){
						if($pilea[$pos] == $tpilea[$r] && $pileb[$pos] == $tpileb[$r]){
							print "elimine $pilea[$r] $pileb[$r]\n" if($debug);
							$cass[$csi]=" $pilea[$r] $pileb[$r] -$cpile[$r]- ";
							$csi++;
							$pilea[$r]='';
							$pileb[$r]='';
							$tpilea[$r]='';
							$tpileb[$r]='';
							$cpile[$r]='';
							$pi=$pi-1;
						};
					};
						
					if($cass[0] ne ''){
						$cpile[$pos]=1;
						foreach $r (0..@cass-1){
							@getmul=split(' ',$cass[$r]);
								
							$flagmul=0;
							$mul1=$getmul[0];
							foreach $t (0..@cass-1){
								$flagmul=1 if($cass[$t]=~/ $mul1 / && $cass[$t]=~/ -1- /);
							};
							$cpile[$pos]=0 if($flagmul==0);
								
							$flagmul=0;
							$mul2=$getmul[1];
							foreach $t (0..@cass-1){
								$flagmul=1 if($cass[$t]=~/ $mul2 / && $cass[$t]=~/ -1- /);
							};
							$cpile[$pos]=0 if($flagmul==0);	
							print "$pilea[$pos] & $pileb[$pos] differents donc $tpilea[$pos]==$tpileb[$pos] est assym�trique\n" if($flagmul==0 && $debug);									
						};
					};

					$pasdesuite=0 if($cass[0] eq '');
							
					$pos='';
					foreach $io (0..@pilea-1){
						print "\t $pilea[$io]\t$pileb[$io]\t$tpilea[$io]\t$tpileb[$io]\t$cpile[$io]\n" if($debug);
						$pos=$io if($cpile[$io] eq '' && $pilea[$io] ne '');
					};
						
				}
				else{
					$pasdesuite=0;
				};
				$pasdesuite=0 if($pos eq '');
			};
						
			$at1=$pilea[$pos];
			$at2=$pileb[$pos];
						
			$ctest=' ';
			foreach $io (0..@pilea-1){
				$ctest=$ctest."$pilea[$io]-$pileb[$io] ";	
			};
						
			if($pos ne ''){
				@vois1=split(' ',$ifonc[$at1]);
				@vois2=split(' ',$ifonc[$at2]);
						
				print "etudie  $at1 -  $at2\n" if($debug);
				$find=3;
				foreach $jh (0..@vois1-1){
					$piold = $pi;	
					if($vois1[$jh] != $tpilea[$pos] && $vois1[$jh] != $at2){    #&& $vois1[$jh] != $at2
						$find=0; 	
						foreach $jh2 (0..@vois2-1){
							print "$vois1[$jh] & $vois2[$jh2]\n" if($debug);
							if($vois2[$jh2] != $tpileb[$pos] && $vois2[$jh2] != $at1){  #&& $vois2[$jh2] != $at1
								if($vois2[$jh2] == $vois1[$jh]){
									$pilea[$pi]=$vois1[$jh];
									$tpilea[$pi]=$at1;
									$pileb[$pi]=$vois2[$jh2];
									$tpileb[$pi]=$at2;
									$cpile[$pi]=1;
									$pi++;
										
							 		print "\najoute \t$pilea[$pi-1]\t$pileb[$pi-1]\t$tpilea[$pi-1]\t$tpileb[$pi-1]\t$cpile[$pi-1]\n" if($debug);
									print "find1 $find\n" if($debug);
								}
								elsif(&egaux($vois1[$jh],$vois2[$jh2])){  # egaux donc il faut aller plus loin
												
									if($vois1[$jh] ne $tpileb[$pi] && $vois2[$jh2] ne $tpilea[$pi] && $ctest!~/ $vois1[$jh]-$vois2[$jh2] / ){
										#print "$vois1[$jh] & $vois2[$jh2]\n";
										$pilea[$pi]=$vois1[$jh];
										#print "a @pilea\n";
										$tpilea[$pi]=$at1;
										$pileb[$pi]=$vois2[$jh2];
										#print "b @pileb\n";
										$tpileb[$pi]=$at2;
										$cpile[$pi]='';
										
										$coupt="$pilea[$pi]-$pileb[$pi]";
										$couptinv="$pileb[$pi]-$pilea[$pi]";
										$found=-1;
										foreach $vb (0..@coupletest-1){
											$found=$vb if($coupletest[$vb] eq $coupt || $coupletest[$vb] eq $couptinv);
										};
										if($found > -1){
											$cpile[$pi]=$valctest[$found];
										};
										
										$pi++;
									};
												
									$find=1;
									print "\najoute\t$pilea[$pi-1]\t$pileb[$pi-1]\t$tpilea[$pi-1]\t$tpileb[$pi-1]\t$cpile[$pi-1]\n" if($debug);		
									print "find2 $find\n" if($debug);		
												
								}
								else{
									$pilea[$pi]=$vois1[$jh];
									$tpilea[$pi]=$at1;
									$pileb[$pi]=$vois2[$jh2];
									$tpileb[$pi]=$at2;
									$cpile[$pi]=0;
									$pi++;
							 		print "\najoute \t$pilea[$pi-1]\t$pileb[$pi-1]\t$tpilea[$pi-1]\t$tpileb[$pi-1]\t$cpile[$pi-1]\n" if($debug);
									print "$find\n" if($debug);		
								};
							};
						};
						if($find && $piold == $pi){
							$cpile[$pos]=1;
						#print "ici\n";
						};
					};
				};
						
				$cpile[$pos]=1 if($cpile[$pos] eq '' && $find==3);
						
				$pos='';
				foreach $io (0..@pilea-1){
					print "\t$pilea[$io]\t$pileb[$io]\t$tpilea[$io]\t$tpileb[$io]\t$cpile[$io]\n" if($debug);
					$pos=$io if($cpile[$io] eq '' && $pilea[$io] ne '');
					
					$coupt="$pilea[$io]-$pileb[$io]";
					$couptinv="$pileb[$io]-$pilea[$io]";
					$found=-1;
					foreach $vb (0..@coupletest-1){
						$found=$vb if($coupletest[$vb] eq $coupt || $coupletest[$vb] eq $couptinv);
					};
					if($found > -1){
						$valctest[$found]= $cpile[$io];
					}
					else{
						$coupletest[$vali]=$coupt;
						$valctest[$vali]=$cpile[$io];
						$vali++;
					};
				};
				if($debug){
					foreach $vb (0..@coupletest-1){
						print "COUPLE $coupletest[$vb] \tVALEUR $valctest[$vb]\n"
					};
				};	
			};
		};
				
					
	if($debug){
		print "\n";
		foreach $io (0..@pilea-1){
			print "\t$pilea[$io]\t$pileb[$io]\t$tpilea[$io]\t$tpileb[$io]\t$cpile[$io]\n"; 		
		};
	};
											
	@cass='';
	$csi=0;
	foreach $r (0..@pilea-1){
		if($ty == $tpilea[$r] && $ty == $tpileb[$r]){
			$cass[$csi]=" $pilea[$r] $pileb[$r] -$cpile[$r]- ";
			$csi++;
		};
	};
	print "cass @cass\n" if($debug);
					
	$mul=0;
	if($cass[0] ne ''){
		foreach $r (0..@cass-1){
			@getmul=split(' ',$cass[$r]);
								
			$flagmul=0;
			$mul1=$getmul[0];
			foreach $t (0..@cass-1){
				$flagmul=1 if($cass[$t]=~/ $mul1 / && $cass[$t]=~/ -1- /);
			};
			$mul=1 if($flagmul);
								
			$flagmul=0;
			$mul2=$getmul[1];
			foreach $t (0..@cass-1){
				$flagmul=1 if($cass[$t]=~/ $mul2 / && $cass[$t]=~/ -1- /);
			};
			$mul=1 if($flagmul);	
		};				
	};
	
	print "C $ty assymetrique\n" if($mul==0 && $debug);
				
	if($mul==0 && $rasmol){
		open(OUTC,">char.sdf");
		foreach $k (0..@tabfilesdf-1){
			printf OUTC "$tabfilesdf[$k]";
		};
		close(OUTC);
		system("rasmol -mdl char.sdf");
	};					
						 		
	$subdiff=0 if($mul);
	$subdiff=1 if($mul==0);
	$subdiff2++ if($subdiff);
};	
};

     $subdiff2;
};


###################################################################################

sub calprop{

	$elimine=0;
	$prop{nbatom}=$atomlourd;
	
#================================= Surface Polaire = $surfpolaire
	
	open(GP,">psa.in");
		printf GP "* XYZR\n";
		printf GP "%8s\n",$istratom;
		foreach $ig (1..$istratom){
			$radius[$ig]=$tabR{Hp} if ($atom[$ig] eq 'H' && ($fonc[$ig]=~/ 1-O / || $fonc[$ig]=~/ 1-N /));
			$strx[$ig]= sprintf "%5.4f",$strx[$ig];
			$stry[$ig]= sprintf "%5.4f",$stry[$ig];
			$strz[$ig]= sprintf "%5.4f",$strz[$ig];
			printf GP"%9s%1s%9s%1s%9s%1s%6s%1s%6s\n",$strx[$ig],$blanc,$stry[$ig],$blanc,$strz[$ig],$blanc,$radius[$ig],$blanc,$atom[$ig];  	
		};  	
	 	 
	close(GP);

	if($param{UNIX}){	
		system("$leaexe/ncs_unix $workdir/psa.in");
	}
	elsif($param{LINUX}){
		system("$leaexe/ncs_linux $workdir/psa.in");
	};

	$surfpolaire=0;
	$area=0;
	$volume=0;

	open(IN2,"<psa.out");
	while(<IN2>){
		@get=split(' ',$_);
		if($get[1] eq 'ATOM'){
			if($atom[$get[0]] eq 'N' || $atom[$get[0]] eq 'O'){
				$surfpolaire=$surfpolaire+$get[7];
			};
			if($atom[$get[0]] eq 'H' && ($fonc[$get[0]]=~/ 1-O / || $fonc[$get[0]] =~ /1-N /)){  		
				$surfpolaire=$surfpolaire+$get[7];
			};
		}
		elsif ($get[0] eq 'area'){
			$area=$get[2];
		}
		elsif ($get[0] eq 'volume'){
			$volume=$get[2];
		};
	};
	close(IN2);
	
	$prop{psa}=$surfpolaire;
	$prop{area}=$area;


#================================= Masse
#	$masse
	$prop{mm}=$masse;
	

#================================= Nombre de Ha et Hd

	$nbha=0;
	$nbhd=0;
	foreach $ig (1..$istratom){
		$nbhd++ if ($atom[$ig] eq 'H' && ($fonc[$ig]=~/ 1-O /||$fonc[$ig]=~/ 1-N /));
		$nbha++ if ($atom[$ig] eq 'N' && $coval[$ig] <= 3);
		$nbha++ if ($atom[$ig] eq 'O');
	};

	$prop{nbha}=$nbha;
	$prop{nbhd}=$nbhd;
	
#================================= LogP by XLOGP (atomic contribution by Wang et al.)

        $f3="mol$moli.mol2";

        if(-e $f3){
                chop($logp=`$leaexe/xlogp.pl $workdir/$f3 `);
                $logp=~s/LogP =(.*) (.*)/$1/;
                $logp=~s/ //g;
                $prop{logp}=$logp;
        }
        else{
                $prop{logp}=0.0;
        };


#================================= Calcul de Lipinski
	
	$prop{lipinski}=1;
	if(($prop{psa} > $lip_psa ) || ($prop{mm} >$lip_mw) || ($prop{nbha} >$lip_nbah) || ($prop{nbhd} >$lip_nbdh) || ($prop{nbatom} > $lip_atom) || ($prop{logp} > $lip_logp )){
		$prop{lipinski}=0;
		if($verbosity){
		        print "\t   NOT DRUG LIKE    ";
		 	print "\tPSA ($prop{psa}) > $lip_psa    " if($prop{psa} > $lip_psa );
	       	 	print "\tMM ($prop{mm}) > $lip_mw    " if($prop{mm} > $lip_mw );
	       	 	print "\tNB HA ($prop{nbha}) > $lip_nbah    " if($prop{nbha} > $lip_nbah );
	        	print "\tNB HD ($prop{nbhd}) > $lip_nbdh    " if($prop{nbhd} > $lip_nbdh );
	        	print "\tNB atoms ($prop{nbatom}) > $lip_atom    " if($prop{nbatom} > $lip_atom );
	        	print "\tLOGP ($prop{logp}) > $lip_logp    " if($prop{logp} > $lip_logp );
		};
	};
	
	$elimine=1 if($prop{lipinski} == 0);
	$elimine;
};


################################################################################



sub fonction{

## SEARCH FUNCTIONS

$function='';
$toxicity='';
$ioniseacide='';
$ionisenitrogen='';
$ionisenitrogen2='';
	
#print"Mol($moli) = ";
foreach $l (1..$istratom){

	
#		print "$l _ $atom[$l] $fonc[$l] \n";
#		print "$l _ $atom[$l] $ifonc[$l] \n";
        $belongcycle=0;
	foreach $cyc (0..@cyclef-1){
		$car=" ".$cyclef[$cyc]." ";
		if($car=~/ $l /){
			#print "$l match $cyclef[$cyc] donc appartient a un cycle\n";
			$belongcycle=1;
		};
	};
	
############################################# Carbone

	if($atom[$l] eq 'C'){

		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-O / && $fonc[$l] =~/ 1-N /){
			$function=$function.$blanc.'carbamate'.$blanc;
#			print"carbamate ";
		};
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-O / ){
			$car=$fonc[$l];
			$nocc=$car=~s/ 1-O / 1-O /g;
#			print"nb occ = $nocc\n";
			if ($nocc == 1){ 
				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k (0..@det-1){
					$p=$k if($det[$k] =~/1-O/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				if($fonc[$iatom] =~/ 1-H /){
					$function=$function.$blanc.'acid'.$blanc;
#					print"acid ";
					@ionis=split(' ',$fonc[$iatom]);
					$p=0;
					foreach $k (0..@ionis-1){
					     	$p=$k if($ionis[$k] =~/1-H/);
					};
					@ionis=split(' ',$ifonc[$iatom]);
					$ioniseacide=$ioniseacide." $ionis[$p] ";
				}
				else{
					$function=$function.$blanc.'ester'.$blanc;
#					print"ester ";
				};
			}
			else{
#				print"OC(=O)O ";
			};
		};
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-N / ){
			@det=split(' ',$fonc[$l]);
			$p=0;
			foreach $k (0..@det-1){
				$p=$k if($det[$k] =~/1-N/);
			};
			@det=split(' ',$ifonc[$l]);
			$iatom=$det[$p];
			$car=$fonc[$iatom];
			$nocc=$car=~s/ 1-H / 1-H /g;
			if($nocc == 2){
				$function=$function.$blanc.'amide-ter'.$blanc;
#				print"amide-ter ";
			}
			else{
				$function=$function.$blanc.'amide'.$blanc;
#				print"amide ";
			};
		};

		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-H / && $fonc[$l] =~/ 1-C /){
			$function=$function.$blanc.'aldhehyde'.$blanc;
#			print"aldhehyde ";
		};


		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-C /){
			$car=$fonc[$l];
			$nocc=$car=~s/ 1-C / 1-C /g;
#			print"nb occ = $nocc\n";
#			print"keto " if ($nocc == 2); 
			$function=$function.$blanc.'keto'.$blanc if ($nocc == 2);
		};

		if($fonc[$l] =~/ 2-N / && $fonc[$l] =~/ 1-N / && $belongcycle==0){
			$amidinium=0;

			@det=split(' ',$fonc[$l]);
			$p=0;
			foreach $k (0..@det-1){
				if($det[$k] =~/1-N/){
					$p=$k;
					@det2=split(' ',$ifonc[$l]);
					$iatom=$det2[$p];
					$car=$fonc[$iatom];
					$belongcycle2=0;
					foreach $cyc (0..@cyclef-1){
						$car2=" ".$cyclef[$cyc]." ";
						if($car2=~/ $iatom /){
							$belongcycle2=1;
						};
					};
					$nocc=$car=~s/ 1-H / 1-H /g;
					if($nocc == 2 && $belongcycle2==0){
						$iatom4=$det2[$p];
						last;
					};
				};
			};	
			if($nocc == 2 && $belongcycle2==0){
				$amidinium++;
			};
			
			@det=split(' ',$fonc[$l]);
			$p=0;
			foreach $k (0..@det-1){
				$p=$k if($det[$k] =~/2-N/);
			};
			@det=split(' ',$ifonc[$l]);
			$iatom2=$det[$p];
			$car=$fonc[$iatom2];
			$belongcycle2=0;
			foreach $cyc (0..@cyclef-1){
				$car2=" ".$cyclef[$cyc]." ";
				if($car2=~/ $iatom2 /){
					$belongcycle2=1;
				};
			};	
			$nocc=$car=~s/ 1-H / 1-H /g;
			if($nocc == 1 && $belongcycle2==0){
				$amidinium++;
				@det=split(' ',$fonc[$iatom2]);
				$p=0;
				foreach $k (0..@det-1){
					$p=$k if($det[$k] =~/1-H/);
				};
				@det=split(' ',$ifonc[$iatom2]);
				$iatom3=$det[$p];
				#print "H $iatom3\n";
			};	
			
			#Protonate
			if($amidinium == 2){
				#			print "protonate\n";
				$protonateval=&protonate($l,$iatom2,$iatom3,$iatom4);
				$ionisenitrogen=$ionisenitrogen.";".$protonateval;
				$ioniseacide=$ioniseacide." $iatom3 " if($protonateval ne "");
				$ionisenitrogen2=$ionisenitrogen2." $iatom2  $iatom2 ";
			};
		};

	};

#####################################################################
############################################# Oxygene

	if($atom[$l] eq 'O'){
		if($fonc[$l] =~/ 1-C /){
			$car=$fonc[$l];
			$nocc=$car=~s/ 1-C / 1-C /g;
			if($nocc == 2){
				$ok=1;

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k (0..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$ok=0 if($fonc[$iatom] =~/ 2-O / || $fonc[$iatom] =~/ 2-S /);
				$pold=$p;

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k ($pold+1..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$ok=0 if($fonc[$iatom] =~/ 2-O /  || $fonc[$iatom] =~/ 2-S /);

				if($ok){
					$function=$function.$blanc.'ether'.$blanc;
#					print"ether ";
				};
			}
			elsif($nocc == 1 && $fonc[$l] =~/ 1-H /){	

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k (0..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$nocc2=$car=~s/ 1-H / 1-H /g;
                                if($nocc2 == 2){
                                        $function=$function.$blanc.'alcohol1'.$blanc;
#                                       print"alcohol1 ";
                                }
                                elsif($nocc2 == 1 && $fonc[$iatom] !~/ 2-O / && $fonc[$iatom] !~/ 2-S /){
                                        $function=$function.$blanc.'alcohol2'.$blanc;
#                                       print"alcohol2 ";
                                }
                                elsif($nocc2 == 0 && $fonc[$iatom] !~/ 2-O / && $fonc[$iatom] !~/ 2-S /){
                                        $function=$function.$blanc.'alcohol3'.$blanc;
#                                       print"alcohol3 ";
                                };

			};
		};
	};
	
#####################################################################
############################################# Azote

	if($atom[$l] eq 'N'){

	#oct 2008
	# Ammonium quaternaire : check if charge +1 in sdf (CHG)
	# N with 4 voisins autres que H
	# N with 3 voisins dont 1 avec double liaison (peut etre dans un cycle eg thiamine)
	# N with 4 voisins dont H+ (ici la forme non protonn�e existe aussi)
	 
		if($coval[$l] > 4){
			$eliminemol=1;
			print "atome $l valence $coval[$l] > 4 (ammonium) \n" if($verbosity);
		};	
		if($coval[$l] == 4 && $nbcoval[$l]==4){
			
			print "atom $l ammonium with charge +1\n" if($verbosity);
			$ammonium=1;

			#check if charge +1 in sdf (CHG)
			if($getlignechg[0] ne ""){
				$vuchg=0;
				$mchg=1;
				while($mchg <= @getlignechg-1){
				#for $mchg (1..@getlignechg-1,$mchg+2){
					if($getlignechg[$mchg] == $l){
						$vuchg=1;
						if($getlignechg[$mchg+1] eq "1" || $getlignechg[$mchg+1] eq "+1"){
							$vuchg=2;
						}
						else{
							$getlignechg[$mchg+1]="+1";
						};	
					};
					$mchg=$mchg+2;
				};
				if($vuchg==0){
					$getlignechg[0]=$getlignechg[0]+1;
					$getlignechg[@getlignechg]=$l;
					$getlignechg[@getlignechg+1]="+1";
				};
			}
			else{
				$getlignechg[0]=1;
				$getlignechg[1]=$l;
				$getlignechg[2]="+1";
			};	
		}
		elsif($coval[$l] == 4 && $nbcoval[$l] == 3){

			#if($belongcycle==1 && $coval[$l]>3 && $nbcoval[$l]>2){
			#	$eliminemol=1;
			#	print "atome $l in ring, 3 voisins et une double liaison: protonation prossible mais quant meme elimine...\n" if($verbosity); 
			#};

			if($fonc[$l] =~/ 1-O / && $fonc[$l] =~/ 2-O /){
				$car=$fonc[$l];
				$nocc=$car=~s/ 2-O / 2-O /g;
                        	$noccb=$car=~s/ 1-O / 1-O /g;
				if($nocc == 1 && $noccb== 1){
					$group[$l]=5; # NO2
				}
				elsif($nocc == 1 && $noccb==2){
					# nitrooxy (isosorbide)
				}
				else{
					$eliminemol=1 if($coval[$l] > $maxvalence{$atom[$l]});
					print "atome $l (pas NO2) valence $coval[$l] > max = $maxvalence{$atom[$l]}\n" if($verbosity && $coval[$l] > $maxvalence{$atom[$l]});

				};
			}
			else{
				print "atom $l ammonium with charge +1\n" if($verbosity);
				$ammonium=1;

				#check if charge +1 in sdf (CHG)
				if($getlignechg[0] ne ""){
					$vuchg=0;
					$mchg=1;
					while($mchg <= @getlignechg-1){
					#for $mchg (1..@getlignechg-1,$mchg+2){
						if($getlignechg[$mchg] == $l){
							$vuchg=1;
							if($getlignechg[$mchg+1] eq "1" || $getlignechg[$mchg+1] eq "+1"){
								$vuchg=2;
							}
							else{
								$getlignechg[$mchg+1]="+1";
							};
						};
						$mchg=$mchg+2;	
					};
					if($vuchg==0){
						$getlignechg[0]=$getlignechg[0]+1;
						$getlignechg[@getlignechg]=$l;
						$getlignechg[@getlignechg+1]="+1";
					};
				}
				else{
					$getlignechg[0]=1;
					$getlignechg[1]=$l;
					$getlignechg[2]="+1";
				};	
				#print "$vuchg\n$getlignechg[0] $getlignechg[1] $getlignechg[2]\n"; 
			};
		};

		if($fonc[$l] =~/ 1-C /){
			$car=$fonc[$l];
			$nocc=$car=~s/ 1-C / 1-C /g;
			if($nocc == 3){
				$ok=1;

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k (0..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$ok=0 if($fonc[$iatom] =~/ 2-O / || $fonc[$iatom] =~/ 2-S /);
				$pold=$p;

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k ($pold+1..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$ok=0 if($fonc[$iatom] =~/ 2-O / || $fonc[$iatom] =~/ 2-S /);
				$pold=$p;

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k ($pold+1..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$ok=0 if($fonc[$iatom] =~/ 2-O/ || $fonc[$iatom] =~/ 2-S /);

				if($ok){
					$function=$function.$blanc.'amine3'.$blanc;
#					print"amine3 ";
				};
				
			}
			elsif($nocc == 2 && $fonc[$l] =~/ 1-H /){
				$ok=1;

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k (0..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$ok=0 if($fonc[$iatom] =~/ 2-O / || $fonc[$iatom] =~/ 2-S /);
				$pold=$p;

				@det=split(' ',$fonc[$l]);
				$p=0;
				foreach $k ($pold+1..@det-1){
					$p=$k if($det[$k] =~/1-C/);
				};
				@det=split(' ',$ifonc[$l]);
				$iatom=$det[$p];
				$car=$fonc[$iatom];
				$ok=0 if($fonc[$iatom] =~/ 2-O / || $fonc[$iatom] =~/ 2-S /);

				if($ok){
					$function=$function.$blanc.'amine2'.$blanc;
#					print"amine2 ";
				};
			}
			elsif($nocc == 1 && $fonc[$l] =~/ 1-H /){	
				$car=$fonc[$l];
				$nocc2=$car=~s/ 1-H / 1-H /g;
				if($nocc2 == 2){
					$ok=1;

					@det=split(' ',$fonc[$l]);
					$p=0;
					foreach $k (0..@det-1){
						$p=$k if($det[$k] =~/1-C/);
					};
					@det=split(' ',$ifonc[$l]);
					$iatom=$det[$p];
					$car=$fonc[$iatom];
					$ok=0 if($fonc[$iatom] =~/ 2-O / || $fonc[$iatom] =~/ 2-S /);

					if($ok){
						$function=$function.$blanc.'amine1'.$blanc;
#						print"amine1 ";
					};
				};
			};
		};
	};

#####################################################################
############################################# Phosphore

# phosphates : 4 oxygens
# phosphonate : 3 oxygens and one another R
# Dont treat phosphinic acid like (one 2-O and only one 1-O)

	if($atom[$l] eq 'P'){

		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-O /){
			$car=$fonc[$l];
			$nocc=$car=~s/ 1-O / 1-O /g;
			$noccn=$car=~s/ 1-N / 1-N /g;
			#print"nb occ = $nocc\n";
			#if ($nocc == 3 || ($nocc == 2 && $noccn==1)){
			if ($nocc == 3 || $nocc == 2){
				@det=split(' ',$fonc[$l]);
				@det2=split(' ',$ifonc[$l]);
				$deux=0;
				foreach $k (0..@det-1){
					if($det[$k] =~/1-O/){
						if($fonc[$det2[$k]] =~/ 1-H /){
							$deux++;
							#print "deux $deux\n";
							if($deux<=2){
								@ionis=split(' ',$fonc[$det2[$k]]);
								$p=0;
								foreach $kp (0..@ionis-1){
									$p=$kp if($ionis[$kp] =~/1-H/);
						        	};
						       	 	@ionis=split(' ',$ifonc[$det2[$k]]);
						        	$ioniseacide=$ioniseacide." $ionis[$p] ";
						        };
						};
					};
					
				};
			};
		
		};

	};
	
	
	
#####################################################################
############################################# TOXICITY


#### O
	if($atom[$l] eq 'O' && $belongcycle==0){
		
		$car=$fonc[$l];
		$nocc2=$car=~s/ 1-C / 1-C /g;
		$nocc3=$car=~s/ 1-S / 1-S /g;
		$nocc2=$nocc2+$nocc3;
		$nocc4=0;
		if($nocc2 == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
				if($det[$k] =~/1-C/ || $det[$k] =~/1-S/){
			 		if($fonc[$det2[$k]] =~/ 2-O /){
						$nocc4++;
					};
			   	};
			};			
		};
		if($nocc4 == 2){
			$toxicity=$toxicity.$blanc.'anhydride'.$blanc;
		};
		
		if($fonc[$l] =~/ 1-O / && $nbcoval[$l] == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
				if($det[$k] =~/1-O/){
			 		if($nbcoval[$det2[$k]] == 2){
						$toxicity=$toxicity.$blanc.'osimpleo'.$blanc;
					};
			   	};
			};	
		};
		
		if($fonc[$l] =~/ 1-C / && $nbcoval[$l] == 2){
		$car=$fonc[$l];
		$nocc2=$car=~s/ 1-C / 1-C /g;
		if($nocc2 == 2){
		
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			$epox=' ';
			foreach $k (0..@det-1){
				if($det[$k] =~/1-C/){
					$epox=$epox."$det2[$k] ";
			   	};
			};
			@getepox=split(' ',$epox);
			$ok=0;
			foreach $ke (0..@getepox-1){
				if($ok==0){
					@det=split(' ',$fonc[$getepox[$ke]]);
					@det2=split(' ',$ifonc[$getepox[$ke]]);
					foreach $k (0..@det-1){
						if($det[$k] =~/1-C/){
							$ok=1 if($epox=~/ $det2[$k] /);
						};
			   		};
				};
			};
			$toxicity=$toxicity.$blanc.'epoxydre'.$blanc if($ok);
		};	
		};		
						
	};
	
#### P

	if($atom[$l] eq 'P' && $belongcycle==0){
		if($fonc[$l] =~/ 1-P /){
			$toxicity=$toxicity.$blanc.'pp'.$blanc;
		};	
		if($fonc[$l] =~/ 1-Cl / || $fonc[$l] =~/ 1-Br / || $fonc[$l] =~/ 1-I / || $fonc[$l] =~/ 1-F /){
			$toxicity=$toxicity.$blanc.'halp'.$blanc;
		};
		if($fonc[$l] =~/ 1-C / && $nbcoval[$l] == 3){
		        $car=$fonc[$l];
			$nocc2=$car=~s/ 1-C / 1-C /g;	
			if($nocc2 == 3){
				$toxicity=$toxicity.$blanc.'trialkylphosphine'.$blanc;
			};		
		};				
	};

#### S
	
	if($atom[$l] eq 'S' && $belongcycle==0){
		
		if($fonc[$l] =~/ 1-Cl / || $fonc[$l] =~/ 1-Br / || $fonc[$l] =~/ 1-I /){
			$toxicity=$toxicity.$blanc.'hals'.$blanc;
		};
		
		if($fonc[$l] =~/ 2-S / && $nbcoval[$l] == 1){
			$toxicity=$toxicity.$blanc.'ss'.$blanc;
		};
				
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 2-S / && $nbcoval[$l] == 4){
			$car=$fonc[$l];
			$nocc2=$car=~s/ 2-O / 2-O /g;	
			if($nocc2 == 2){
				$toxicity=$toxicity.$blanc.'thiosulfate'.$blanc;
			};				
		};
			
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-F / && $nbcoval[$l] == 4){
			$car=$fonc[$l];
			$nocc2=$car=~s/ 2-O / 2-O /g;	
			if($nocc2 == 2){
				$toxicity=$toxicity.$blanc.'so2f'.$blanc;
			};				
		};
			
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-O / && $nbcoval[$l] == 3){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
				if($det[$k] =~/1-O/){
			 		if($fonc[$det2[$k]] =~/ 1-F /){
						$toxicity=$toxicity.$blanc.'foso'.$blanc;
					};
			   	};
			};										
		};
		
		if($fonc[$l] =~/ 1-C / && $nbcoval[$l] == 2){
		$car=$fonc[$l];
		$nocc2=$car=~s/ 1-C / 1-C /g;
		if($nocc2 == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			$epox=' ';
			foreach $k (0..@det-1){
				if($det[$k] =~/1-C/){
					$epox=$epox."$det2[$k] ";
			   	};
			};
			@getepox=split(' ',$epox);
			$ok=0;
			foreach $ke (0..@getepox-1){
				if($ok==0){
					@det=split(' ',$fonc[$getepox[$ke]]);
					@det2=split(' ',$ifonc[$getepox[$ke]]);
					foreach $k (0..@det-1){
						if($det[$k] =~/1-C/){
							$ok=1 if($epox=~/ $det2[$k] /);
						};
			   		};
				};
			};
			$toxicity=$toxicity.$blanc.'thioepoxyde'.$blanc if($ok);
		};	
		};		
						
	};
	
#### N	

	if($atom[$l] eq 'N' && $belongcycle==0){

		if($fonc[$l] =~/ 1-Cl / || $fonc[$l] =~/ 1-Br / || $fonc[$l] =~/ 1-I / || $fonc[$l] =~/ 1-F /){
			$toxicity=$toxicity.$blanc.'haln'.$blanc;
		};	
		
		if($fonc[$l] =~/ 1-C / && $nbcoval[$l] == 3 && $fonc[$l] =~/ 1-H /){
		$car=$fonc[$l];
		$nocc2=$car=~s/ 1-C / 1-C /g;
		if($nocc2 == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			$epox=' ';
			foreach $k (0..@det-1){
				if($det[$k] =~/1-C/){
					$epox=$epox."$det2[$k] ";
			   	};
			};
			@getepox=split(' ',$epox);
			$ok=0;
			foreach $ke (0..@getepox-1){
				if($ok==0){
					@det=split(' ',$fonc[$getepox[$ke]]);
					@det2=split(' ',$ifonc[$getepox[$ke]]);
					foreach $k (0..@det-1){
						if($det[$k] =~/1-C/){
							$ok=1 if($epox=~/ $det2[$k] /);
						};
			   		};
				};
			};
			$toxicity=$toxicity.$blanc.'aziridine'.$blanc if($ok);
		};	
		};		
	
		if($fonc[$l] =~/ 2-C / && $fonc[$l] !~/ 1-O / && $nbcoval[$l] == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
				if($det[$k] =~/2-C/){
					$belongcycle2=0;
					foreach $cyc (0..@cyclef-1){
						$car2=" ".$cyclef[$cyc]." ";
						if($car2=~/ $det2[$k] /){
							#print "$det2[$k] match $cyclef[$cyc] donc appartient a un cycle\n";
							$belongcycle2=1;
						};
					};
			 		if($nbcoval[$det2[$k]]==3 && $belongcycle2==0){
						$toxicity=$toxicity.$blanc.'imine'.$blanc;
					};
			   	};
			};		
		};		
	
		if($fonc[$l] =~/ 1-H / && $fonc[$l] =~/ 1-N / && $nbcoval[$l] == 3){
			$car=$fonc[$l];
			$nocc2=$car=~s/ 1-H / 1-H /g;	
			if($nocc2 == 2){
				@det=split(' ',$fonc[$l]);
				@det2=split(' ',$ifonc[$l]);
				foreach $k (0..@det-1){
			        	if($det[$k] =~/1-N/){
			        		if($fonc[$det2[$k]] =~/ 1-H /){
							$toxicity=$toxicity.$blanc.'hydrazine'.$blanc;
						};
			        	};
				};
			};	
			
		};
				
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-N / && $nbcoval[$l] == 2){
			$toxicity=$toxicity.$blanc.'nitrosamine'.$blanc;
		};
			
		if($fonc[$l] =~/ 2-N / && $nbcoval[$l] == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/2-N/){
			        	if($nbcoval[$det2[$k]] == 2){
						$toxicity=$toxicity.$blanc.'azo'.$blanc;
					};
			        };
			};		
		};
		
		if($fonc[$l] =~/ 2-C / && $fonc[$l] =~/ 1-O / && $nbcoval[$l] == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/1-O/){
			        	if($fonc[$det2[$k]] =~/ 1-C / || $fonc[$det2[$k]] =~/ 1-S /){
						@det3=split(' ',$fonc[$det2[$k]]);
						@det4=split(' ',$ifonc[$det2[$k]]);
						foreach $k2 (0..@det3-1){
							if($det3[$k2] =~/1-C/ || $det3[$k2] =~/1-S/){
								if($fonc[$det4[$k2]] =~/ 2-O /){
									$toxicity=$toxicity.$blanc.'oximester'.$blanc;
								};
							};
						};
					};
			        };
			};					
		};			
	};
	
#### C	
			
	if($atom[$l] eq 'C' && $belongcycle==0){
		
		if($fonc[$l] =~/ 3-N / && $fonc[$l] =~/ 1-C / && $nbcoval[$l] == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/1-C/){
			        	if($fonc[$det2[$k]] =~/ 1-O /){
						@det3=split(' ',$fonc[$det2[$k]]);
						@det4=split(' ',$ifonc[$det2[$k]]);
						foreach $k2 (0..@det3-1){
							if($det3[$k2] =~/1-O/){
								if($fonc[$det4[$k2]] =~/ 1-H /){
									$toxicity=$toxicity.$blanc.'cyanohydrine'.$blanc;
								};
							};
						};
					};
			        };
			};			
		};	
	
		if($fonc[$l] =~/ 2-O / && ($fonc[$l] =~/ 1-Cl / || $fonc[$l] =~/ 1-Br / || $fonc[$l] =~/ 1-I / || $fonc[$l] =~/ 1-F /)){
			$toxicity=$toxicity.$blanc.'acylhalide'.$blanc;
		};			
		
		if($fonc[$l] =~/ 1-H / && ($fonc[$l] =~/ 1-Cl / || $fonc[$l] =~/ 1-Br / || $fonc[$l] =~/ 1-I /) && $fonc[$l] =~/ 1-C /){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/1-C/){
			        	if($fonc[$det2[$k]] =~/ 2-O / || $fonc[$det2[$k]] =~/ 2-S /){
						$toxicity=$toxicity.$blanc.'halocarbonyl'.$blanc;
					};
			        };
			};
		};
			
		if($fonc[$l] =~/ 2-S / && $fonc[$l] =~/ 2-N /){
			$toxicity=$toxicity.$blanc.'thiocyanate'.$blanc;
		};		
		
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 2-N /){
			$toxicity=$toxicity.$blanc.'isocyanate'.$blanc;
		};
		
		if(($fonc[$l] =~/ 2-O / || $fonc[$l] =~/ 2-S / || $fonc[$l] =~/ 2-N /) && $fonc[$l] =~/ 1-S / && $nbcoval[$l] == 3){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/1-S/){
			        	if($nbcoval[$det2[$k]] == 2){
						$toxicity=$toxicity.$blanc.'thioester'.$blanc;
					};
			        };
			};			
		};		
		
		$car=$fonc[$l];
		$nocc2=$car=~s/ 2-N / 2-N /g;	
		if($nocc2 == 2){
			$toxicity=$toxicity.$blanc.'dilmid'.$blanc;
		};
				
		if($fonc[$l] =~/ 1-C / && ($fonc[$l] =~/ 1-Br / || $fonc[$l] =~/ 1-I /) && $nbcoval[$l] == 4){
		        $car=$fonc[$l];
			$nocc2=$car=~s/ 1-Br / 1-Br /g;	
			$nocc3=$car=~s/ 1-I / 1-I /g;
			$nocc2=$nocc2+$nocc3;
			if($nocc2 == 3){
			   	@det=split(' ',$fonc[$l]);
				@det2=split(' ',$ifonc[$l]);
				foreach $k (0..@det-1){
			        	if($det[$k] =~/1-C/){
			        		if($fonc[$det2[$k]] =~/ 2-O /){
							$toxicity=$toxicity.$blanc.'perhaloketone'.$blanc;
						};
			        	};
				};
			};
		};
							
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-H / && $nbcoval[$l] == 3){
			$toxicity=$toxicity.$blanc.'aldehyde'.$blanc;			
		};
		
		if($fonc[$l] =~/ 3-N / && $fonc[$l] =~/ 1-C / && $nbcoval[$l] == 2){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/1-C/){
			        	if($fonc[$det2[$k]] =~/ 2-O / && $nbcoval[$det2[$k]]== 3){
						$toxicity=$toxicity.$blanc.'michaelacc'.$blanc;
					};
			        };
			};							
		};		
		
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-C / && $nbcoval[$l] == 3){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/1-C/){
			        	if($fonc[$det2[$k]] =~/ 2-O / && $nbcoval[$det2[$k]]== 3){
						$toxicity=$toxicity.$blanc.'dicarbonyl'.$blanc;
					};
			        };
			};							
		};	
		
		if($fonc[$l] =~/ 2-O / && $fonc[$l] =~/ 1-C / && $nbcoval[$l] == 3){
			@det=split(' ',$fonc[$l]);
			@det2=split(' ',$ifonc[$l]);
			foreach $k (0..@det-1){
			        if($det[$k] =~/1-C/){
			        	if($fonc[$det2[$k]] =~/ 2- / && $nbcoval[$det2[$k]]== 3 && $fonc[$det2[$k]] =~/ 1-O /){
						@det3=split(' ',$fonc[$det2[$k]]);
						@det4=split(' ',$ifonc[$det2[$k]]);
						foreach $k2 (0..@det3-1){
							if($det3[$k2] =~/1-O/){
								if($fonc[$det4[$k2]] =~/ 1-H /){
									$toxicity=$toxicity.$blanc.'dicarbonylt'.$blanc;
								};
							};
						};
					};
			        };
			};							
		};				
						
	};	
	
#####################################################################


};


	$prop2='';

	$car=$function;
	$nocc=$car=~s/ acid / acid /g;
	$prop2=$prop2.'_'.$nocc.'acid' if ($nocc > 0);
	
	$car=$function;
	$nocc=$car=~s/ ester / ester /g;
	$prop2=$prop2.'_'.$nocc.'ester' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ carbamate / carbamate /g;
	$prop2=$prop2.'_'.$nocc.'carbamate' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ amide / amide /g;
	$prop2=$prop2.'_'.$nocc.'amide' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ amide-ter / amide-ter /g;
	$prop2=$prop2.'_'.$nocc.'amide-ter' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ aldhehyde / aldhehyde /g;
	$prop2=$prop2.'_'.$nocc.'aldhehyde' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ keto / keto /g;
	$prop2=$prop2.'_'.$nocc.'keto' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ amine1 / amine1 /g;
	$prop2=$prop2.'_'.$nocc.'amine1' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ amine2 / amine2 /g;
	$prop2=$prop2.'_'.$nocc.'amine2' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ amine3 / amine3 /g;
	$prop2=$prop2.'_'.$nocc.'amine3' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ alcohol1 / alcohol1 /g;
	$prop2=$prop2.'_'.$nocc.'alcohol1' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ alcohol2 / alcohol2 /g;
	$prop2=$prop2.'_'.$nocc.'alcohol2' if ($nocc > 0);

        $car=$function;
        $nocc=$car=~s/ alcohol3 / alcohol3 /g;
        $prop2=$prop2.'_'.$nocc.'alcohol3' if ($nocc > 0);

	$car=$function;
	$nocc=$car=~s/ ether / ether /g;
	$prop2=$prop2.'_'.$nocc.'ether' if ($nocc > 0);

	#print"$prop2\n";


	$prop3=1;
	@listfonc=split('_',$listefonction);
	
	foreach $lfi (0..@listfonc-1){
		
		if($prop2 =~ /$listfonc[$lfi]/){
			print "\t  MATCH FONC $listfonc[$lfi]  " if($verbosity);
		};

  		if($toxicity =~ /$listfonc[$lfi]/){
			$prop3=0;
			print "\t  MATCH TOX $listfonc[$lfi]  " if($verbosity);
		};
	};
	
	$rasmolf=0;
	if($rasmolf && $prop3==0){
		open(OUTC,">char.sdf");
		foreach $k (0..@tabfilesdf-1){
			printf OUTC "$tabfilesdf[$k]";
		};
		close(OUTC);
		system("rasmol -mdl char.sdf");
	};	
	
	$prop3;
};


############################################################################
############################################################################

sub protonate{

	local($originmol,$pointemol,$hydrogen,$secondn)=@_;

	$newatoms="";

## molecule(C=N) with C in 0, 0, 0
	
	$originmol=$pointemol;# N in C=N
	$pointemol=$secondn;# N in NH2-C

	@corxmol=@strx;
	@corymol=@stry;
	@corzmol=@strz;

        $corx=$corxmol[$originmol];
	$cory=$corymol[$originmol];
	$corz=$corzmol[$originmol];

	#print "C $originmol : $corxmol[$originmol] $corymol[$originmol] $corzmol[$originmol]\n";
	#print "N $pointemol : $corxmol[$pointemol] $corymol[$pointemol] $corzmol[$pointemol]\n";
        foreach $k (1..$istratom){
                $corxmol[$k]=$corxmol[$k]-$corx;
		$corymol[$k]=$corymol[$k]-$cory;
		$corzmol[$k]=$corzmol[$k]-$corz;
        };

# template amidinium (C=N) with N in 0, 0, 0

	@corxtemp="";
	@corytemp="";
	@corztemp="";

# C
	$corxtemp[0]=0.0298;
	$corytemp[0]=0.1124;
	$corztemp[0]=0.1710;

#=N
	$corxtemp[1]=1.1639;
	$corytemp[1]=-0.4292;
	$corztemp[1]=0.4225;

#NH1
	$corxtemp[2]=2.0022;
        $corytemp[2]=0.1228;
        $corztemp[2]=0.4022;

#NH2
	$corxtemp[3]=1.2257;
        $corytemp[3]=-1.4036;
        $corztemp[3]=0.6447;

#second N connected to C (the NH2)
	$corxtemp[4]=-0.0336;
	$corytemp[4]=1.5307;
	$corztemp[4]=-0.1443;

	#$origintemp=0;
	#$pointetemp=1;
	
	$origintemp=1;# N in C=N
	$pointetemp=4;# N in NH2-C

	$corxt=$corxtemp[$origintemp];
	$coryt=$corytemp[$origintemp];
	$corzt=$corztemp[$origintemp];
	foreach $k (0..@corxtemp-1){
		$corxtemp[$k]=$corxtemp[$k]-$corxt;
		$corytemp[$k]=$corytemp[$k]-$coryt;
		$corztemp[$k]=$corztemp[$k]-$corzt;
	};


## Vectors

# template
	$ux=$corxtemp[$pointetemp]-$corxtemp[$origintemp];
	$uy=$corytemp[$pointetemp]-$corytemp[$origintemp];
	$uz=$corztemp[$pointetemp]-$corztemp[$origintemp];
	$nu=sqrt($ux**2+$uy**2+$uz**2);

# molecule
	$vx=$corxmol[$pointemol]-$corxmol[$originmol];
	$vy=$corymol[$pointemol]-$corymol[$originmol];
	$vz=$corzmol[$pointemol]-$corzmol[$originmol];
	$nv=sqrt($vx**2+$vy**2+$vz**2);


## angle before
	$cosangle=(($ux*$vx)+($uy*$vy)+($uz*$vz))/($nu * $nv);
	$testcos=1-$cosangle*$cosangle;
	$testcos=sqrt($testcos*$testcos);
	$teta=atan2(sqrt($testcos),$cosangle);
	$tetab=atan2(sqrt($testcos),$cosangle)*180.0/3.14159;
	#print "Angle(vecteur1,vecteur2) = $tetab\n";

# Vector normalization

        $ux=$ux/$nu;
        $uy=$uy/$nu;
        $uz=$uz/$nu;

        $vx=$vx/$nv;
        $vy=$vy/$nv;
        $vz=$vz/$nv;

# orthogonal vector

        $wx=($uy*$vz)-($uz*$vy);
        $wy=($uz*$vx)-($ux*$vz);
        $wz=($ux*$vy)-($uy*$vx);
	$nw=sqrt($wx**2+$wy**2+$wz**2);

	if($nw != 0){
		$wx=$wx/$nw;
	        $wy=$wy/$nw;
        	$wz=$wz/$nw;

		$kx=($wy*$uz)-($wz*$uy);
		$ky=($wz*$ux)-($wx*$uz);
	        $kz=($wx*$uy)-($wy*$ux);
        	$nk=sqrt($kx**2+$ky**2+$kz**2);

		$kx=$kx/$nk;
        	$ky=$ky/$nk;
	        $kz=$kz/$nk;

		#Rotation
		$cosangle=(($vx*$kx)+($vy*$ky)+($vz*$kz))/($nv * $nk);
		$teta=sqrt($teta*$teta);
		$teta=-1*$teta if($cosangle < 0);

	 	foreach $k (0..@corxtemp-1){
				$ncorx=$corxtemp[$k];
				$ncory=$corytemp[$k];
				$ncorz=$corztemp[$k];
				
				# changement de repere
				$x=$ncorx * $ux + $ncory * $uy + $ncorz * $uz;
	                	$y=$ncorx * $kx + $ncory * $ky + $ncorz * $kz;
        		        $z=$ncorx * $wx + $ncory * $wy + $ncorz * $wz;

				# Rotation
				$xrot= $x*cos($teta) - $y*sin($teta);
				$yrot= $x*sin($teta) + $y*cos($teta);
				$zrot=$z;
				
				# rechangement de repere
				$corxtemp[$k]=$xrot*$ux + $yrot*$kx + $zrot*$wx;
				$corytemp[$k]=$xrot*$uy + $yrot*$ky + $zrot*$wy;
				$corztemp[$k]=$xrot*$uz + $yrot*$kz + $zrot*$wz;
	        };
	}
	else{
	       foreach $k (0..@corxtemp-1){
		# inverse coordinates
			$corxtemp[$k]=$corxtemp[$k]*-1;
			$corytemp[$k]=$corytemp[$k]*-1;
			$corztemp[$k]=$corztemp[$k]*-1;
		};
        };

## VERIFIE ANGLE
# template
        $ux=$corxtemp[$pointetemp]-$corxtemp[$origintemp];
        $uy=$corytemp[$pointetemp]-$corytemp[$origintemp];
        $uz=$corztemp[$pointetemp]-$corztemp[$origintemp];
        $nu=sqrt($ux**2+$uy**2+$uz**2);

# molecule
        $vx=$corxmol[$pointemol]-$corxmol[$originmol];
        $vy=$corymol[$pointemol]-$corymol[$originmol];
        $vz=$corzmol[$pointemol]-$corzmol[$originmol];
        $nv=sqrt($vx**2+$vy**2+$vz**2);

## angle after
        $cosangle=(($ux*$vx)+($uy*$vy)+($uz*$vz))/($nu * $nv);
        $testcos=1-$cosangle*$cosangle;
        $testcos=sqrt($testcos*$testcos);
        $teta=atan2(sqrt($testcos),$cosangle);
        $tetab=atan2(sqrt($testcos),$cosangle)*180.0/3.14159;
        #print "Angle(vecteur1,vecteur2) = $tetab\n";


# re-translate in molecule	
	foreach $k (0..@corxtemp-1){
		$corxtemp[$k]=sprintf "%4.4f",$corxtemp[$k]+$corx;
		$corytemp[$k]=sprintf "%4.4f",$corytemp[$k]+$cory;
		$corztemp[$k]=sprintf "%4.4f",$corztemp[$k]+$corz;
	#	print "$k $corxtemp[$k] $corytemp[$k] $corztemp[$k]\n";
	};


	$newatoms=sprintf"%10s%10s%10s H   0  0  0  0  0  0  0  0  0  0  0  0\n",$corxtemp[2],$corytemp[2],$corztemp[2];
	$newatoms=$newatoms.";".sprintf"%10s%10s%10s H   0  0  0  0  0  0  0  0  0  0  0  0\n",$corxtemp[3],$corytemp[3],$corztemp[3];

};

############################################################################
###########################################################################
