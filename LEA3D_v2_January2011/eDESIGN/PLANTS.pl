#!/usr/bin/perl


$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

### VARIABLES
$rms=5; # recherche dans centre du site actif

#lance PLANTS et met les resultats dans plants.out

$list_mol2='';
$prot='';
$plantsexe='';

# Coordonnées d'un point dans le site actif
$refx='';
$refy='';
$refz='';

local($plantsexe,$prot,$refx,$refy,$refz,$rms,$list_mol2,$com)=@ARGV;

if ($plantsexe eq '' || $list_mol2 eq '' || $prot eq '' || $refx eq '' || $refy eq '' || $refz eq ''){
	die " \n
	usage: PLANTS.pl <plants executable> <protein.mol2> <refx> <refy> <refz> <$rms> <list_mol2> <user defined command file .com>\n";
};
	if($com ne "" && !-e $com){
		die "file $com not found\n";
	};	

	if($com eq ''){
		$com=$prot;
		$com=~s/\.mol2$/\.com/;
		$dirplants=$prot;
		$dirplants=~s/\.mol2$//;
		$dirplants="plants_".$dirplants;
		
		open(DOC,">$com");
		print DOC "protein_file $prot\n";
		print DOC "ligand_list $list_mol2\n\n";
		print DOC "search_speed speed1\n\n";
		print DOC "bindingsite_center $refx $refy $refz\n";
		print DOC "bindingsite_radius 12\n\n";
		print DOC "cluster_rmsd 1.5\n";
		print DOC "cluster_structures 10\n\n";
		print DOC "scoring_function chemplp # plp, plp95  or chemplp  (standard: chemplp)\n\n";
		print DOC "output_dir $dirplants\n\n";
		print DOC "write_multi_mol2 0\n";	
		close(DOC);
	};

	if($com ne ''){
		print  "PLANTS: (use $com as input file without modifications)\n";
	}
	else{
		print  "PLANTS:\n";
	};

	system("$plantsexe --mode screen $com > plants_log ");
	

#*************************************************************************
# extrait les resultats

@list='';
@name='';
$i=0;
open(DOC,"<$dirplants/ligand.log");
while(<DOC>){

## versions may vary by '_' : mol1_entry_00001_conf_01 or mol_1_entry_00001_conf_01
#
	@getnomb=split(' ',$_);
	if($_=~/^rank /){
		@getnomb=split(' ',$_);
		
		$mol=$getnomb[5];
		$score=$getnomb[3];
		$no=$getnomb[1];

		$molecule=$mol;
		$molecule=~s/(.*)entry(.*)/$1/i;
		$molecule2=$molecule;

		#generic name: same for dock solutions but different for 2 corina conformer 	
		$name[$i]=$molecule;
		$name[$i]=~s/_$//;
		
		if($name[$i]!~/_/){
			$name[$i]=~s/mol/mol_/;
			$molecule=~s/mol/mol_/;
		};

		$molecule="$molecule".$no.".mol2";
		rename "$dirplants/$mol.mol2", $molecule;
		
		$list[$i]="$molecule $score 0.0 0.0";
		
		#print "PLANTS.pl reads generic $name[$i] ($list[$i] and copy $mol.mol2 locally)\n";
		$i++;
	};
};	
close(DOC);

%tabmm=(
	'C',12,
	'O',16,
	'N',14,
	'S',32,
	'P',31,
	'Br',80,
	'Cl',35,
	'I',127,
	'F',19,
	'H',1,
);

# contains different rms solutions for each molecule/conformer
open(KEE,">plants.tmp"); 
printf KEE "#nom score rms_1er dist_ref \n";
@result='';
$diff="";
foreach $i (0..@list-1){
	@get=split(' ',$list[$i]);
	if (-e "$get[0]" && $get[1] < 0.0){
	
		&readmol2("$get[0]");
		
		$rgyrmm=0;
		$rgx=0;
		$rgy=0;
		$rgz=0;
		foreach $rgyrk (1..$strrgyrno){
			@sepstr = split('\.',$stratomtype[$rgyrk]);
			$rgyrmm=$rgyrmm+$tabmm{$sepstr[0]};
			$rgx=$rgx+$strx[$rgyrk]*$tabmm{$sepstr[0]};
			$rgy=$rgy+$stry[$rgyrk]*$tabmm{$sepstr[0]};
			$rgz=$rgz+$strz[$rgyrk]*$tabmm{$sepstr[0]};
		};
		if($rgyrmm != 0){
			$rgx=$rgx/$rgyrmm;
			$rgy=$rgy/$rgyrmm;
			$rgz=$rgz/$rgyrmm;
		};

		#take coordinates of the first one to calculate rms_first
		if($i==0 || $name[$i] ne $name[$i-1]){
			@strx0=@strx;
			@stry0=@stry;
			@strz0=@strz;
			$rmsfirst=0.0;
		}
		else{
			$rmsfirst=0.0;
			foreach $rgyrk (1..$strrgyrno){
				$rmsfirst=$rmsfirst+(($strx[$rgyrk]-$strx0[$rgyrk])**2+($stry[$rgyrk]-$stry0[$rgyrk])**2+($strz[$rgyrk]-$strz0[$rgyrk])**2);	
			};	
			$rmsfirst=sqrt($rmsfirst);
		};	
		$rmsfirst=$rmsfirst/$istratom;

#		print "La masse moleculaire $rgyrmm\n";
#		print "Coordonnees du centre de masse ($rgx ; $rgy ; $rgz)\n";

		if($refx ne "-" && $refy ne "-" && $refz ne "-"){
#			calcul de la distance entre le centre de masse de 2 molecules
			$distance=sqrt(($rgx-$refx)**2+($rgy-$refy)**2+($rgz-$refz)**2);
		#	print "$get[0] $rmsfirst\n";
		}
		else{
		        $distance="-";
			#print "Distance / REF =  $distance\n";
                };

		$rmsfirst=sprintf"%3.2f",$rmsfirst;
		$test=int($rmsfirst);
		$distance=sprintf"%3.2f",$distance;

		if($i==0 || $name[$i] ne $name[$i-1]){
			$diff=" 0 ";
                	$result[$i]="$get[0] $get[1] $rmsfirst $distance";
			print KEE "$get[0] $get[1] $rmsfirst $distance\n";
		}
		elsif($diff!~/ $test /){
			$diff=$diff." $test ";
			$result[$i]="$get[0] $get[1] $rmsfirst $distance";
			print KEE "$get[0] $get[1] $rmsfirst $distance\n";
		};	
	}
	else{
		$result[$i]="$get[0] 0.0 0.0 -";
		printf KEE "$get[0] 0.0 0.0 -\n";
	};
	
};
close(KEE);

# all with the correct rms < dist_ref 
 
open(KEE,">plants.out");
printf KEE "#nom score rms_1er dist_ref\n";
foreach $i (0..@result-1){
	@get=split(' ',$result[$i]);

	if($get[3] <= $rms && $get[0] ne ""){
		print KEE "$result[$i]\n";
	};	
};	
close(KEE);

	

###############################################
###############################################


sub readmol2{
	local($chemin)=@_;

	open(KEN,"<$chemin");

	$flagatom=0;
	$flagbond=0;
	$istratom=1;
	$istrbond=1;
	$fn=0;
		while (<KEN>){
			@getstr = split(' ',$_);

			if ($fn){
				#	$name=$getstr[0];
#print"$name\n";
			};

			if ($getstr[0] eq '@<TRIPOS>SUBSTRUCTURE'){
				$flagbond=0;
			};
			if ($getstr[0] eq ''){
				$flagbond=0;
			};

			if (($flagbond)&&($getstr[0] ne '')){
				@strbond1[$istrbond]=$getstr[1];
				@strbond2[$istrbond]=$getstr[2];
				$getstr[3]="am" if (($stratomtype[$getstr[1]]=~/N.am/) && ($stratomtype[$getstr[2]]=~/C.2/));
				$getstr[3]="am" if (($stratomtype[$getstr[2]]=~/N.am/) && ($stratomtype[$getstr[1]]=~/C.2/));
				@strbondtype[$istrbond]=$getstr[3];
				$istrbond++;
			};

			if ($getstr[0] eq '@<TRIPOS>BOND'){
				$flagatom=0;
				$flagbond=1;
			};

			if (($flagatom )&&($getstr[0] ne '')){
				@strx[$istratom]=	$getstr[2];
				@stry[$istratom]=	$getstr[3];
				@strz[$istratom]=	$getstr[4];
				$getstr[5]="N.am" if ($getstr[5]=~/N.3/);
				$stratomtype[$istratom]=$getstr[5];
				@ma=split('\.',$getstr[5]);
				$masse=$masse+$tabmm{$ma[0]};
				$istratom++;
			};

			if ($getstr[0] eq '@<TRIPOS>ATOM'){
				$flagatom=1;
			};
			$fn=0;
			if ($getstr[0] eq '@<TRIPOS>MOLECULE'){
				$fn=1;
			};

		};

	close(KEN);
	$istrbond=$istrbond-1;
	$istratom=$istratom-1;
	$strrgyrno=$istratom;
#print"$strrgyrno, $istrbond\n";

};


#*************************************************************************
#*************************************************************************


