#!/usr/bin/perl


local($pdb)=@ARGV;


if ($pdb eq ''){
	die "\n
	usage: pdbmol2 <protonated pdb from tleap>\n\nParameters for PH=7\nATOM processed only\nCheck SS bonds\n";
};

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

	$mol2=$pdb;
	$mol2=~s/\.pdb$/\.mol2/;
	
	%order_sybyl_elt=(
		'C','C.3',  # elt
		'O','O.3',  # elt
		'N','N.am', # elt
		'S','S.3',  # elt
		'H','H',    # elt
	);
	
	%order_sybyl=(
		'C','C.2', # ALL
		'CA','C.3', 
		'O','O.2',
		'N','N.am',
		'H','H',
		
		'OXT','O.co2', # Cter
		'Octer','O.co2',
		
		'Nnter','N.4', # Nter for all
		
		'CB','C.3', # except GLY
		'CG','C.3', # MOST
		
		'CD','C.3', # MOST
		'CE','C.3', # MOST
		
		'SD','S.3', # MET
		'SG','S.3', # CYS
		'OG','O.3', # THR and SER
		
		'NZ','N.4', # LYS charged
		
		'NE','N.pl3', # ARG 		
	        'NH','N.pl3',# ARG N trigonal planar sur NH1 et NH2 !
		'CZ','C.2',

		'OE','O.2', # GLN
		
		'OD','O.2', # ASN
		'ND','N.am',
		
		'GLNCD','C.2', # GLN
		'GLNNE','N.am',
	
		'ASNCG','C.2', # ASN

		'ASPCG','C.2', # ASP
		'ASPOD','O.co2',

		'GLUCD','C.2', # GLU
		'GLUOE','O.co2',

		'HISCG','C.2', # HIS
		'HISND','N.2',
		'HISCE','C.2',
		'HISNE','N.pl3',
		'HISCD','C.2',
	
		'PHECG','C.ar', # PHE
		'PHECD','C.ar', # D1 et D2
		'PHECE','C.ar',
		'PHECZ','C.ar',

		'TYROH','O.3', # TYR
		'TYRCG','C.ar',
		'TYRCD','C.ar',
		'TYRCE','C.ar',
		'TYRCZ','C.ar',
		
		'TRPCG','C.ar', #TRP
		'TRPCD','C.ar',
		'TRPNE','N.ar',
		'TRPCE','C.ar',
		'TRPCZ','C.ar',
		'TRPCH','C.ar',		
	);	

	
########### read pdbtomol2.dat
	@resname="";
	@aa1="";
	@aa2="";
	@aa3="";# bond type
	$bi=0;
	open(IN,"<$leaexe/pdbmol2.dat");
	while(<IN>){
		@get=split(' ',$_);
		if($get[0] ne "#" && $get[0] ne ""){
			$resname[$bi]=$get[0];
			$aa1[$bi]=$get[1];
			$aa2[$bi]=$get[2];
			$aa3[$bi]=$get[3];
			$bi++;
		};	
	};
	close(IN);
	
	
########## READ PDB	
	open(IN,"<$pdb");
	open(OUT,">$mol2");
	print OUT "#\n";
	print OUT "#     Creating by LEA \n";
	print OUT "#\n";
	print OUT "#\n";
	print OUT "@<TRIPOS>MOLECULE\n";
	print OUT "$pdb\n";
	
	$istratom=0;
	$istrbond=1;
	@coord="";
	@bond="";
	$old=-1000;
	$resold="";
	$nter="";
	$ntercount=0;
	$cter="";
	@aa1no="";
	@aa2no="";
	$pointeur=1;

	$tmpn="";
	$tmpo="";
	$tmpc="";
	
	$a7=0;

	while(<IN>){
	if($_ =~/^ATOM/){
		
		@getp=split('',$_);
		
		$residu=$getp[17].$getp[18].$getp[19];
		$residu=~s/ //g;	
		$polcha=$getp[21];
		$polcha=~s/ //g;
		$no=$getp[22].$getp[23].$getp[24].$getp[25].$getp[26];
		$no=~s/ //g;
		$atom=$getp[12].$getp[13].$getp[14].$getp[15];
		$atom=~s/ //g;
		$atom2=$atom;
		
		$istratom++;
		
		$nter="nter" if($atom eq "H1" || $atom eq "H2" || $atom eq "H3");
		$ntercount++ if($atom eq "H1");
		$ntercount++ if($atom eq "H2");
		$ntercount++ if($atom eq "H3");
		
		$cter="cter" if($atom eq "OXT");
		
		######### Fill @aa1no and @aa2no for BOND setting	
		if($no != $old && $resold ne ""){
			
			# Cter case, Useless HERE, OXT last atom in pdb file, replace O atom type
			if($cter ne ""){
				foreach $bi ($pointeur..($istratom-1)){
					if($coord[$bi]=~/ O /){
						$coord[$bi]=~s/ O.2  / O.co2/; 
					};	
				};
			};
			
			# Nter, Useless here
			if($nter ne "" && $ntercount==3){
				foreach $bi ($pointeur..($istratom-1)){
					if($coord[$bi]=~/ N /){
						$coord[$bi]=~s/ N.am / N.4  /;
					};
				};
			};	
			
			foreach $bi (0..@aa1-1){
				if($resname[$bi] =~/^$resold/ && $aa1no[$bi] ne "" && $aa2no[$bi] ne ""){
					$a10=$istrbond;		
					$a11=$aa1no[$bi];
					$a12=$aa2no[$bi];
					$a13=$aa3[$bi];
					if($a11 >= 10000 || $a10 >=10000 || $a12 >=10000){
						$bond[$istrbond]=sprintf "$a10 $a11 $a12 $a13\n";
					}
					else{
						$bond[$istrbond]=sprintf "%4s%5s%5s $a13\n",$a10,$a11,$a12;
					};
					$istrbond++;
				};	
			};
			
			#connect next aa if no = old + 1
			if($no == ($old+1)){
				$a10=$istrbond;
			
				$a11=$tmpc; # previous residue old C = $tmpc
				$a12=$istratom; # must be N 
			
				$a13="am";

				if($a11 >= 10000 || $a10 >=10000 || $a12 >=10000){
					$bond[$istrbond]=sprintf "$a10 $a11 $a12 $a13\n";
				}
				else{
					$bond[$istrbond]=sprintf "%4s%5s%5s $a13\n",$a10,$a11,$a12;
				};
				$istrbond++;
			};

			$pointeur=$istratom;	
			@aa1no="";
			@aa2no="";
			
			$nter="";
			$cter="";
			
			$tmpc="";
			$tmpo="";
			$tmpn="";
		};
			
		$tmpn=$istratom if($atom eq "N");
		$tmpo=$istratom if($atom eq "O");
		$tmpc=$istratom if($atom eq "C");
		
		# Check if $atom and $elt are present in parameter file
		$atom5=$atom;
		$atom5=~s/\d//g;
		@lena6b=split('',$atom5);
		$elt=$lena6b[0];
		
		$vu=0;	
		foreach $bi (0..@aa1-1){

			if($resname[$bi] eq $residu && $aa1[$bi] eq $atom){
				$aa1no[$bi]=$istratom;
				$vu=1;
				#print "$resname[$bi] =  $residu et $aa1[$bi] = $atom\n";
			}
			elsif($resname[$bi] eq $residu && $aa2[$bi] eq $atom){
			        $aa2no[$bi]=$istratom;
			        $vu=1;
			        #print "$resname[$bi] =  $residu et $aa2[$bi] = $atom\n";
			};
			
			if($cter ne ""){ # move RES C-O to REScter C-O and set REScter C
				if($tmpo eq "" || $tmpc eq ""){
					foreach $ci (0..@aa1-1){
						if( $resname[$ci] eq $residu && $aa1[$ci] eq "O"){
							$tmpo=$aa1no[$ci];
						}
						elsif($resname[$ci] eq $residu && $aa2[$ci] eq "O"){		
							$tmpo=$aa2no[$ci];
						};
						if( $resname[$ci] eq $residu && $aa1[$ci] eq "C"){
							$tmpc=$aa1no[$ci];
						}
						elsif($resname[$ci] eq $residu && $aa2[$ci] eq "C"){
							$tmpc=$aa2no[$ci];
						};	
					};
				};	
				foreach $ci (0..@aa1-1){
					if( $resname[$ci] eq $residu.$cter && $aa1[$ci] eq "O"){
						$aa1no[$ci]=$tmpo;
					}
					elsif(  $resname[$ci] eq $residu.$cter && $aa2[$ci] eq "O"){
						$aa2no[$ci]=$tmpo;
					};
					if( $resname[$ci] eq $residu.$cter && $aa1[$ci] eq "C"){
						$aa1no[$ci]=$tmpc;
					}
					elsif(  $resname[$ci] eq $residu.$cter && $aa2[$ci] eq "C"){
						$aa2no[$ci]=$tmpc;
					};
				};	
				if($resname[$bi] eq $residu.$cter && $aa1[$bi] eq "OXT" && $atom eq "OXT"){
					$aa1no[$bi]=$istratom;
					$vu=1;
				}
				elsif($resname[$bi] eq $residu.$cter && $aa2[$bi] eq "OXT" && $atom eq "OXT"){
					$aa2no[$bi]=$istratom;
					$vu=1;
				};
			};	
			if($nter ne ""){
				if($tmpn eq ""){
				foreach $ci (0..@aa1-1){#search for N
					if($aa1[$ci] eq "N"){
						$tmpn=$aa1no[$ci];
					}
					elsif($aa2[$ci] eq "N"){
						$tmpn=$aa2no[$ci];
					};
				};	
				};
				
				if($resname[$bi] eq $residu.$nter && $aa1[$bi] eq "N"){
					$aa1no[$bi]=$tmpn;
				}	
				elsif($resname[$bi] eq $residu.$nter && $aa2[$bi] eq "N"){ 
					$aa2no[$bi]=$tmpn;		
				};
				
				if($resname[$bi] eq $residu.$nter && $aa1[$bi] eq $atom){
					$aa1no[$bi]=$istratom;
					$vu=1;
				}
				elsif($resname[$bi] eq $residu.$nter && $aa2[$bi] eq $atom){
					$aa2no[$bi]=$istratom;
					$vu=1;
				};	
			};
		};
		if($vu==0){
			print "WARNING $residu $no $atom  not present in pdbmol2.dat ?\n";
		};	       
		
		
		# ATOM TYPE remove number for sybyl atom type
		$atom=~s/\d//g;
		$a6="";
		
		$atom3=$residu.$atom;
		$atom4=$atom.$nter if($nter ne "");
		$atom4=$atom.$cter if ($cter ne "");
		if($order_sybyl{$atom4} ne "" && ($nter ne "" || $cter ne "")){
			#print "Nter $residu $no $atom set !\n";
			$a6=$order_sybyl{$atom4};
		}	
		elsif($order_sybyl{$atom3} ne ""){
			#print "special $residu $no $atom set !\n";
			$a6=$order_sybyl{$atom3};
		}	
		elsif($order_sybyl{$atom} ne ""){
			#print "standart $residu $no $atom set !\n";
			$a6=$order_sybyl{$atom};
		}
		else{
			$a6=$order_sybyl_elt{$elt};
			print "Warning set default $residu $no $atom => $elt ($a6)\n " if($elt ne "H");
		};	
		
		$strx=$getp[30].$getp[31].$getp[32].$getp[33].$getp[34].$getp[35].$getp[36].$getp[37];
		$strx=~s/ //g;
		$stry=$getp[38].$getp[39].$getp[40].$getp[41].$getp[42].$getp[43].$getp[44].$getp[45];
		$stry=~s/ //g;
		$strz=$getp[46].$getp[47].$getp[48].$getp[49].$getp[50].$getp[51].$getp[52].$getp[53];
		$strz=~s/ //g;

		if($a6 ne ""){
				$a1=$istratom;
				$a2=$atom2;
				@lena6b=split('',$a2);
				$lena6=@lena6b;
				$a2="$a2   " if($lena6 == 1);
				$a2="$a2  " if($lena6 == 2);
				$a2="$a2 " if($lena6 == 3);
				$a3=sprintf "%5.4f",$strx;
				$a4=sprintf "%5.4f",$stry;
				$a5=sprintf "%5.4f",$strz;
				@lena6b=split('',$a6);
				$lena6=@lena6b;
				$a6="$a6    " if($lena6 == 1);
				$a6="$a6   " if($lena6 == 2);
				$a6="$a6  " if($lena6 == 3);
				$a6="$a6 " if($lena6 == 4);
				
#PLANTS special writing
				if($no ne $old){
					$a7++;
				};
				$a7=$no;

				$a8=$residu.$no;
				@lena6b=split('',$a8);
				$lena6=@lena6b;
				$a8="$a8   " if($lena6 == 4);
				$a8="$a8  " if($lena6 == 5);
				$a8="$a8 " if($lena6 == 6);

				#$a7="1";
				#$a8="pdb";

				$a9="0.0000";
				if($atom eq "O" || $atom eq "C" || $atom eq "N" || $atom eq "CA"){
					#$coord[$istratom]=sprintf "%4s %4s%5s%10s %10s %10s %5s %6s %3s %8s BACKBONE\n",$a1,$a2,$blanc,$a3,$a4,$a5,$a6,$a7,$a8,$a9;
					$coord[$istratom]=sprintf "%7d %4s%5s%9s %9s %9s %5s %5s %7s     %6s BACKBONE\n",$a1,$a2,$blanc,$a3,$a4,$a5,$a6,$a7,$a8,$a9; 
				}
				else{
					#$coord[$istratom]=sprintf "%4s %4s%5s%10s %10s %10s %5s %6s %3s %8s\n",$a1,$a2,$blanc,$a3,$a4,$a5,$a6,$a7,$a8,$a9;
					$coord[$istratom]=sprintf "%7d %4s%5s%9s %9s %9s %5s %5s %7s     %6s\n",$a1,$a2,$blanc,$a3,$a4,$a5,$a6,$a7,$a8,$a9;
				};
		}
		else{
			print "Unable to set Mol2 parameter for $polcha $residu $no $atom ?\n";	
		};
		$resold=$residu;
		$old=$no;
        };		
   	};
	close(IN);

	# Cter case, OXT last atom in pdb file, replace O atom type
        if($cter ne ""){
        	foreach $bi ($pointeur..$istratom){
        		if($coord[$bi]=~/ O /){
        			$coord[$bi]=~s/ O.2  / O.co2/;
        		};
        	};
        };

	# Nter case
	if($nter ne "" && $ntercount==3){
	       foreach $bi ($pointeur..$istratom){
	       		if($coord[$bi]=~/ N /){
		 		$coord[$bi]=~s/ N.am / N.4  /;
			};
		};
	};		
	
	# BOND settings
	foreach $bi (0..@aa1-1){
		if($resname[$bi] =~/^$resold/ && $aa1no[$bi] ne "" && $aa2no[$bi] ne ""){
			$a10=$istrbond;
			$a11=$aa1no[$bi];
			$a12=$aa2no[$bi];
			$a13=$aa3[$bi];
			if($a11 >= 10000 || $a10 >=10000 || $a12 >=10000){
				$bond[$istrbond]=sprintf "$a10 $a11 $a12 $a13\n";
			}
			else{
				$bond[$istrbond]=sprintf "%4s%5s%5s $a13\n",$a10,$a11,$a12;
			};
			$istrbond++;
		};						
	};	

	
# PRINT MOL2 FILE
	 
	$istrbond=$istrbond-1;	
	$blanc=" ";
	printf OUT "%4s%1s%4s\n",$istratom,$blanc,$istrbond;
	print OUT "SMALL\n";
	print OUT "NO_CHARGES\n\n\n";
	print OUT "@<TRIPOS>ATOM\n";
	foreach $bi (1..$istratom){
		print OUT "$coord[$bi]";
	};
	print OUT "@<TRIPOS>BOND\n";
	foreach $bi (1..$istrbond){
		print OUT "$bond[$bi]";
	};
	print OUT "@<TRIPOS>SUBSTRUCTURE\n";
	print OUT "      1 pdb             1 ****\n";
        close(OUT);

	
	print "see file $mol2\n";

	
