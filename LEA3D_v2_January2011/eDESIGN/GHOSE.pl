#!/usr/bin/perl

$path='';
$workdir='';

chop($workdir = `pwd` );

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable(setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

$path=$leaexe;

$inputfile='';
my($inputfile)=@ARGV;
if ($inputfile eq ''){
	die "usage: logp <file.mol2>\n";
}

#print "GHOSE $path $inputfile $workdir\n";
	
###################################################
#######################################    Masse

		%tabmm=(
		'C',12,
		'O',16,
		'N',14,
		'S',32,
		'P',31,
		'Br',80,
		'Cl',35,
		'I',127,
		'F',19,
		'H',1,
		);

		
	$masse=0;
###################################################
########################	LOGP et MR

	$numerologp=0;
	open(IN1,"<$path/LOGP");
	while (<IN1>){
		@get = split(' ',$_);
		$numerologp++;
		$LOGPVAL[$numerologp]=$get[0];
	};
	close(IN1);

	$numeroMR=0;
	open(IN1,"<$path/MR");
	while (<IN1>){
		@get = split(' ',$_);
		$numeroMR++;
		$MRVAL[$numeroMR]=$get[0];
	};
	close(IN1);

###################################################
####################################  READ MOL2

	$flagatom=0;
	$flagbond=0;
	$istratom=1;
	$istrbond=1;
	$flagCOO=0;
		open(PI,"<$inputfile");
		while (<PI>){
			@getstr = split(' ',$_);

			if ($fn){
				$name=$getstr[0];
			};

			if ($getstr[0] eq '@<TRIPOS>SUBSTRUCTURE'){
				$flagbond=0;
			};
			if ($getstr[0] eq ''){
				$flagbond=0;
			};

			if ($flagbond){
				@strbond1[$istrbond]=$getstr[1];
				@strbond2[$istrbond]=$getstr[2];
				$getstr[3]="am" if (($stratomtype[$getstr[1]]=~/N.am/) && ($stratomtype[$getstr[2]]=~/C.2/));
				$getstr[3]="am" if (($stratomtype[$getstr[2]]=~/N.am/) && ($stratomtype[$getstr[1]]=~/C.2/));
				@strbondtype[$istrbond]=$getstr[3]; 
				$istrbond++;
			};

			if ($getstr[0] eq '@<TRIPOS>BOND'){
				$flagatom=0;
				$flagbond=1;
			};

			if ($flagatom ){
				$getstr[5]="N.am" if ($getstr[5]=~/N.3/);
				$stratomtype[$istratom]=$getstr[5];
				@ma=split('\.',$getstr[5]);
				$masse=$masse+$tabmm{$ma[0]};
				$istratom++;
			};

			if ($getstr[0] eq '@<TRIPOS>ATOM'){
				$flagatom=1;
			};
			$fn=0;
			if ($getstr[0] eq '@<TRIPOS>MOLECULE'){
				$fn=1;
			};

		};
                close(PI);

	$istrbond=$istrbond-1;
	$istratom=$istratom-1;
	$strrgyrno=$istratom;


###################################################
###################################################

	$mr=0;
	$mrinterm=0;
	$refract=0;

	foreach $mri (1..$strrgyrno){
		$nomr=0;
		$nbvoisins=0;
		$nbH=0;
		$nbC=0;
		$nbsimple=0;
		$nbdouble=0;
		$nbar=0;
		$nbX2ar=0;
		$nbNpyrol=0;
		$nbNpyrolbis=0;
		$nbsimplebisar=0;
		$nbCar=0;
		$nbC2=0;
		$nbNp=0;
		$nb72=0;
		$nb72bis=0;
		$nbCyne=0;
		$nbCene=0;
		$nbNar=0;
		$nbO=0;
		$nbOd=0;
		$nbNdouble=0;
		$nbSd=0;
		$nbNsimple=0;
		$nbXarCar=0;
		$nbXC2=0;

		foreach $mrib (1..$istrbond){
			if (($strbond1[$mrib]==$mri) || ($strbond2[$mrib]==$mri)){
				$numvois=$strbond1[$mrib] if $strbond2[$mrib]== $mri;
				$numvois=$strbond2[$mrib] if $strbond1[$mrib]== $mri;
				$nbvoisins++;
				@numvoisin[$nbvoisins]=$numvois;
				$typevoisin=$stratomtype[$numvois];

				$nbH++ 		if ($typevoisin=~/H/);
				$nbC++ 		if $typevoisin=~/C/;
				$nbCar++ 	if $typevoisin=~/C.ar/;
				$nbC2++ 	if $typevoisin=~/C.2/;
				$nbNp=1 	if $typevoisin=~/N.pl3/;
				$nbNp=1 	if $typevoisin=~/N.2/;
				$nbO++		if $typevoisin=~/O/;
				$nbXC2++	if $typevoisin=~/N.2/;
				$nbXC2++	if $typevoisin=~/N.am/;
				$nbXC2++	if $typevoisin=~/P.2/;

				$nbXarCar++	if (($strbondtype[$mrib]==/ar/)	&&($typevoisin=~/O/));
				$nbXarCar++	if (($strbondtype[$mrib]==/ar/)	&&($typevoisin=~/N/));
				$nbXarCar++	if (($strbondtype[$mrib]==/ar/)	&&($typevoisin=~/S/));
				$nbXarCar++	if (($strbondtype[$mrib]==/ar/)	&&($typevoisin=~/P/));

				$nbOd++		if (($strbondtype[$mrib]==2)	&&($typevoisin=~/O/));
				$nbsimple++ 	if (($strbondtype[$mrib]==1) 	&& ($typevoisin=~/C/));
				$nbdouble++ 	if (($strbondtype[$mrib]==2) 	&& ($typevoisin=~/C/));
				$nbar++ 	if (($strbondtype[$mrib]=~/ar/) && ($typevoisin=~/C/));

				$nbX2ar++ 	if (($strbondtype[$mrib]=~/ar/) && ($typevoisin=~/O.2/));
				$nbX2ar++ 	if (($strbondtype[$mrib]=~/ar/) && ($typevoisin=~/S.2/));

				$nbsimplebisar++ if (($strbondtype[$mrib]==1) 	&& ($typevoisin=~/C.ar/));

				$nbNar++ 	if (($strbondtype[$mrib]=~/ar/));
				$nbNdouble++ 	if (($strbondtype[$mrib]=~/2/));
				$nbNsimple++ 	if (($strbondtype[$mrib]=~/1/));


				if (($typevoisin=~/N/) && ($strbondtype[$mrib]=~ /ar/)){
					foreach $mric (1..$istrbond){
						if (($strbond1[$mric]==$numvois)||($strbond2[$mric]==$numvois)){
						if (($strbond1[$mric]!=$mri)&&($strbond2[$mric]!=$mri)){
							$strN=$strbond1[$mric] if ($strbond2[$mric]==$numvois);
							$strN=$strbond2[$mric] if ($strbond1[$mric]==$numvois);
#							$nbNpyrol++ if ($stratomtype[$strN]=~/H/);
#							$nbNpyrol++ if (($stratomtype[$strN]=~/C/)&&($strbondtype[$mric]==1));
							$nbNpyrol++ if ($strbondtype[$mric]==1);
						};
						};
					};
				};

				if ($stratomtype[$mri]=~/N/){
				if (($typevoisin=~/C/) && ($strbondtype[$mrib]==1)|| ($strbondtype[$mrib]=~/am/)){
					foreach $mric (1..$istrbond){
						if (($strbond1[$mric]==$numvois)||($strbond2[$mric]==$numvois)){
						if (($strbond1[$mric]!=$mri)&&($strbond2[$mric]!=$mri)){
							$str72=$strbond1[$mric] if ($strbond2[$mric]==$numvois);
							$str72=$strbond2[$mric] if ($strbond1[$mric]==$numvois);
							$nb72++ if (($stratomtype[$str72]=~/O/)&&($strbondtype[$mric]==2));
						};
						};
					};
				};
				};

				if ($stratomtype[$mri]=~/N/){
				if (($typevoisin=~/N/)||($typevoisin=~/O/)||($typevoisin=~/S/)||($typevoisin=~/P/)&&($strbondtype[$mrib]==1)){
					foreach $mric (1..$istrbond){
						if (($strbond1[$mric]==$numvois)||($strbond2[$mric]==$numvois)){
						if (($strbond1[$mric]!=$mri)&&($strbond2[$mric]!=$mri)){
							$str72b=$strbond1[$mric] if ($strbond2[$mric]==$numvois);
							$str72b=$strbond2[$mric] if ($strbond1[$mric]==$numvois);
							$nb72bis++ if (($stratomtype[$str72b]=~/O/)&&($strbondtype[$mric]==2));
							$nb72bis++ if (($stratomtype[$str72b]=~/N/)&&($strbondtype[$mric]==2));
							$nb72bis++ if (($stratomtype[$str72b]=~/S/)&&($strbondtype[$mric]==2));
							$nb72bis++ if (($stratomtype[$str72b]=~/P/)&&($strbondtype[$mric]==2));
						};
						};
					};
				};
				};

				if ($stratomtype[$mri]=~/N/){
				if (($typevoisin=~/C/) && ($strbondtype[$mrib]=~/ar/)){
					$nbNpyrolbis++;
				};
				};
				if ($stratomtype[$mri]=~/N/){
				if (($typevoisin=~/C/) && ($strbondtype[$mrib]=~/3/)){
					$nbCyne++;
				};
				};
				if ($stratomtype[$mri]=~/N/){
				if (($typevoisin=~/C/) && ($strbondtype[$mrib]=~/2/)){
					$nbCene++;
				};
				};
				if ($stratomtype[$mri]=~/S/){
				if (($typevoisin=~/S/) && ($strbondtype[$mrib]=~/1/)){
					$nbSd++;
				};
				};

			};

		};



#####################################################  HYDROGENE OU HALOGENE lie a C lie a X voisins....
# H lie a un seul voisin:

if (($stratomtype[$mri]=~/H/)||($stratomtype[$mri]=~/F/)||($stratomtype[$mri]=~/I/)||($stratomtype[$mri]=~/Br/)||($stratomtype[$mri]=~/Cl/)){
			$voisi=$numvoisin[1];


			$csp3=0;
			$csp2=0;
			$csp=0;
			$hetat=0;

			$csp3=1 	if $stratomtype[$voisi]=~/C.3/;
			$csp2=1 	if $stratomtype[$voisi]=~/C.2/;
			$csp=1 		if $stratomtype[$voisi]=~/C.1/;
			$csp2=1 	if $stratomtype[$voisi]=~/C.ar/;

			$hetat=1 	if $stratomtype[$voisi]=~/O/;
			$hetat=1 	if $stratomtype[$voisi]=~/N/;
			$hetat=1 	if $stratomtype[$voisi]=~/P/;
			$hetat=1 	if $stratomtype[$voisi]=~/S/;
			$hetat=1 	if $stratomtype[$voisi]=~/F/;
			$hetat=1 	if $stratomtype[$voisi]=~/Cl/;
			$hetat=1 	if $stratomtype[$voisi]=~/Br/;
			$hetat=1 	if $stratomtype[$voisi]=~/I/;

			$acd=0;
			$nbsdC=0;
			$nbsdX=0;

			$nbsdC=1 if ($stratomtype[$mri]=~/F/);
			$nbsdC=1 if ($stratomtype[$mri]=~/Cl/);
			$nbsdC=1 if ($stratomtype[$mri]=~/Br/);
			$nbsdC=1 if ($stratomtype[$mri]=~/I/);

			$nbsdv=0;
			foreach $mrid (1..$istrbond){
				if (($strbond1[$mrid]== $voisi) || ($strbond2[$mrid]== $voisi)){
				if (($strbond1[$mrid]!= $mri) && ($strbond2[$mrid]!= $mri)){
					$nbsdv++;
					@sdvoisin[$nbsdv]= $strbond2[$mrid] if ($strbond1[$mrid]== $voisi) ;
					@sdvoisin[$nbsdv]= $strbond1[$mrid] if ($strbond2[$mrid]== $voisi) ;
					@sdbond[$nbsdv]=$mrid;
					
					if ($stratomtype[$sdvoisin[$nbsdv]]=~/O/){
						$nbsdC=$nbsdC+1 if $strbondtype[$mrid]=~/1/;
						$nbsdC=$nbsdC+2 if $strbondtype[$mrid]=~/2/;
						if (($strbondtype[$mrid]=~/ar/)&&($stratomtype[$sdvoisin[$nbsdv]]=~/O.2/)){
							$nbsdC=$nbsdC+1;
						}
						elsif ($strbondtype[$mrid]=~/ar/){
							$nbsdC=$nbsdC+2;
						};
					};

					if ($stratomtype[$sdvoisin[$nbsdv]]=~/N/){
					$nbNpyrol=0;
					foreach $mric (1..$istrbond){
						if (($strbond1[$mric]==$sdvoisin[$nbsdv])||($strbond2[$mric]==$sdvoisin[$nbsdv])){
						if (($strbond1[$mric]!=$voisi)&&($strbond2[$mric]!=$voisi)){
							$strN=$strbond1[$mric] if ($strbond2[$mric]==$sdvoisin[$nbsdv]);
							$strN=$strbond2[$mric] if ($strbond1[$mric]==$sdvoisin[$nbsdv]);
#							$nbNpyrol++ if ($stratomtype[$strN]=~/H/);
#							$nbNpyrol++ if (($stratomtype[$strN]=~/C/)&&($strbondtype[$mric]==1));
							$nbNpyrol++ if ($strbondtype[$mric]==1);
						};
						};
					};
						$nbsdC=$nbsdC+1 if $strbondtype[$mrid]=~/1/;
						$nbsdC=$nbsdC+2 if $strbondtype[$mrid]=~/2/;
						$nbsdC=$nbsdC+3 if $strbondtype[$mrid]=~/3/;
						if (($strbondtype[$mrid]=~/ar/)&&($nbNpyrol==1)){
							$nbsdC=$nbsdC+1;
						}
						elsif ($strbondtype[$mrid]=~/ar/) {
							$nbsdC=$nbsdC+2;
						};

					};

					if ($stratomtype[$sdvoisin[$nbsdv]]=~/S/){
						$nbsdC=$nbsdC+1 if $strbondtype[$mrid]=~/1/;
						$nbsdC=$nbsdC+2 if $strbondtype[$mrid]=~/2/;
						if (($strbondtype[$mrid]=~/ar/)&&($stratomtype[$sdvoisin[$nbsdv]]=~/S.2/)){
							$nbsdC=$nbsdC+1;
						}
						elsif ($strbondtype[$mrid]=~/ar/) {
							$nbsdC=$nbsdC+2;
						};
					};

					if ($stratomtype[$sdvoisin[$nbsdv]]=~/P/){
						$nbsdC=$nbsdC+1 if $strbondtype[$mrid]=~/1/;
						$nbsdC=$nbsdC+2 if $strbondtype[$mrid]=~/2/;
						$nbsdC=$nbsdC+2 if $strbondtype[$mrid]=~/ar/;
						$nbsdC=$nbsdC+3 if $strbondtype[$mrid]=~/3/;
					};

					$nbsdC++ if $stratomtype[$sdvoisin[$nbsdv]]=~/Cl/;
					$nbsdC++ if $stratomtype[$sdvoisin[$nbsdv]]=~/Br/;
					$nbsdC++ if $stratomtype[$sdvoisin[$nbsdv]]=~/I/;
					$nbsdC++ if $stratomtype[$sdvoisin[$nbsdv]]=~/F/;
				};	
				};
			};
							
				foreach $mrie (1..$nbsdv){
					foreach $mrif (1..$istrbond){
					if (($strbond1[$mrif]==$sdvoisin[$mrie]) || ($strbond2[$mrif]== $sdvoisin[$mrie])){
						if (($strbond1[$mrif]!= $voisi) && ($strbond2[$mrif]!= $voisi)){
							$sdsdvoisin=$strbond2[$mrif] if ($strbond1[$mrif]==$sdvoisin[$mrie]);
							$sdsdvoisin=$strbond1[$mrif] if ($strbond2[$mrif]==$sdvoisin[$mrie]);

							$nbsdXflag=0;
							$nbsdXflag=1 if $stratomtype[$sdsdvoisin]=~/O/;
							$nbsdXflag=1 if $stratomtype[$sdsdvoisin]=~/N/;
							$nbsdXflag=1 if $stratomtype[$sdsdvoisin]=~/S/;
							$nbsdXflag=1 if $stratomtype[$sdsdvoisin]=~/P/;

#							$nbsdX++ if $stratomtype[$sdsdvoisin]=~/Cl/;
#							$nbsdX++ if $stratomtype[$sdsdvoisin]=~/Br/;
#							$nbsdX++ if $stratomtype[$sdsdvoisin]=~/I/;
#							$nbsdX++ if $stratomtype[$sdsdvoisin]=~/F/;

							$nbsdX++ if $nbsdXflag==1;

						if ($stratomtype[$voisi]=~/C/){
				if (($stratomtype[$sdvoisin[$mrie]]=~/C.ar/)&&($nbsdXflag)&&($strbondtype[$mrif] eq 'ar')){
					$nbNpyrol=0;
					foreach $mric (1..$istrbond){
						if (($strbond1[$mric]==$sdsdvoisin)||($strbond2[$mric]==$sdsdvoisin)){
						if (($strbond1[$mric]!=$sdvoisin[$mrie])&&($strbond2[$mric]!=$sdvoisin[$mrie])){
							$strN=$strbond1[$mric] if ($strbond2[$mric]==$sdsdvoisin);
							$strN=$strbond2[$mric] if ($strbond1[$mric]==$sdsdvoisin);
#							$nbNpyrol++ if ($stratomtype[$strN]=~/H/);
#							$nbNpyrol++ if (($stratomtype[$strN]=~/C/)&&($strbondtype[$mric]==1));
							$nbNpyrol++ if ($strbondtype[$mric]==1);
						};
						};
					};



	if (($stratomtype[$sdsdvoisin] ne 'O.2')&&($stratomtype[$sdsdvoisin] ne 'S.2')&&($nbNpyrol==0)){;
								$acd++ if ($strbondtype[$sdbond[$mrie]]==1);
							};
							};
							if (($stratomtype[$sdvoisin[$mrie]]=~/C.1/)&&($nbsdXflag)){
								$acd++ if ($strbondtype[$sdbond[$mrie]]==1);
							};
					if (($stratomtype[$sdvoisin[$mrie]]=~/C.2/)&&($nbsdXflag)&&($strbondtype[$mrif]==2)){
								$acd++ if ($strbondtype[$sdbond[$mrie]]==1);
							};
					if (($stratomtype[$sdvoisin[$mrie]]=~/C.2/)&&($nbsdXflag)&&($strbondtype[$mrif]=~/ar/)){
								$acd++ if ($strbondtype[$sdbond[$mrie]]==1);
							};
						};

						};
						};
					};
				};


};

#####################################################  OXYGENE lie a C lie a ....

			$voisiO=$numvoisin[1];
			$nbsdCO=0;
			if ($stratomtype[$voisiO]=~/C.2/){
				foreach $mrih (1..$istrbond){
					if (($strbond1[$mrih]== $voisiO) || ($strbond2[$mrih]== $voisiO)){
						if (($strbond1[$mrih]!=$mri)&&($strbond2[$mrih]!=$mri)){
							$voisiOBOND=$strbond1[$mrih] if $strbond2[$mrih]==$voisiO;
							$voisiOBOND=$strbond2[$mrih] if $strbond1[$mrih]==$voisiO;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/O/;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/N/;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/S/;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/P/;
						};
					};
				};
			};
			$voisiO=$numvoisin[2];
			if ($stratomtype[$voisiO]=~/C.2/){
				foreach $mrih (1..$istrbond){
					if (($strbond1[$mrih]== $voisiO) || ($strbond2[$mrih]== $voisiO)){
						if (($strbond1[$mrih]!=$mri)&&($strbond2[$mrih]!=$mri)){
							$voisiOBOND=$strbond1[$mrih] if $strbond2[$mrih]==$voisiO;
							$voisiOBOND=$strbond2[$mrih] if $strbond1[$mrih]==$voisiO;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/O/;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/N/;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/S/;
							$nbsdCO++ if $stratomtype[$voisiOBOND]=~/P/;
						};
					};
				};
			};


########################################################################################################################


		if (($stratomtype[$mri] eq 'C.3') && ($nbvoisins==4)){
			$nomr=1 if (($nbC==1) && ($nbH==3));
			$nomr=1 if ($nbH==4);
			$nomr=2 if  (($nbC==2) && ($nbH==2));
			$nomr=3 if  (($nbC==3) && ($nbH==1));
			$nomr=4 if  ($nbC==4);
			$nomr=5 if  (($nbC==0) && ($nbH==3));
			$nomr=6 if  (($nbC==1) && ($nbH==2));
			$nomr=7 if  (($nbC==0) && ($nbH==2));
			$nomr=8 if  (($nbC==2) && ($nbH==1));
			$nomr=9 if  (($nbC==1) && ($nbH==1));
			$nomr=10 if  (($nbC==0) && ($nbH==1));
			$nomr=11 if  (($nbC==3) && ($nbH==0));
			$nomr=12 if  (($nbC==2) && ($nbH==0));
			$nomr=13 if  (($nbC==1) && ($nbH==0));
			$nomr=14 if  (($nbC==0) && ($nbH==0));
			
		};
		if (($stratomtype[$mri] eq 'C.2') && ($nbvoisins==3)){
			$nomr=15 if  ($nbH==2);
			$nomr=16 if  (($nbH==1) && ($nbsimple==1));
			$nomr=17 if  (($nbH==0) && ($nbsimple==2));
			$nomr=18 if  (($nbH==1) && ($nbsimple==0));
			$nomr=19 if  (($nbH==0) && ($nbsimple==1));
			$nomr=20 if  (($nbH==0) && ($nbsimple==0));
			$nomr=36 if  (($nbH==1) && ($nbsimple==1) && ($nbC==1));
			$nomr=37 if  (($nbH==1) && ($nbsimple==1) && ($nbC==1) && ($nbsimplebisar==1));
			$nomr=38 if  (($nbH==0) && ($nbsimple==2) && ($nbC==2));
			$nomr=39 if  (($nbH==0) && ($nbsimple==2) && ($nbC==2) && ($nbsimplebisar>=1));
			$nomr=40 if  (($nbH==0) && ($nbsimple==1) && ($nbC==1));
			$nomr=41 if  (($nbH==0) && ($nbC==0));
	
		};
		if (($stratomtype[$mri] eq 'C.1') && ($nbvoisins==2)){
			$nomr=21 if ($nbH==1);
			$nomr=22 if (($nbH==0) && ($nbsimple==1));
			$nomr=22 if (($nbH==0) && ($nbdouble==2));
			$nomr=23 if (($nbH==0) && ($nbsimple==0) && ($nbdouble==0));
			$nomr=40 if  (($nbH==0) && ($nbsimple==1) && ($nbC==1));
			$nomr=40 if  (($nbH==0)  && ($nbC==0) && ($nbNdouble==2));
			
		};
		if (($stratomtype[$mri] eq 'C.ar') && ($nbvoisins==3)){
			$nomr=24 if (($nbH==1) && ($nbar==2));
			$nomr=25 if (($nbH==0) && ($nbar>=2) && ($nbC==3));
			$nomr=26 if (($nbH==0) && ($nbar==2) && ($nbC==2));
			$nomr=27 if (($nbH==1) && ($nbar==1) && ($nbC==1)&& ($nbNpyrol==0)&&($nbX2ar==0)&&($nbXarCar==1));
			$nomr=28 if (($nbH==0) && ($nbC==2)&& ($nbNpyrol==0)&& ($nbX2ar==0)&&($nbar>=1)&&($nbXarCar==1));
			$nomr=29 if (($nbH==0) && ($nbar==1) && ($nbC==1)&&($nbNpyrol==0)&&($nbX2ar==0)&&($nbXarCar>=1));
			$nomr=30 if (($nbH==1) && ($nbar==0) && ($nbC==0)&&($nbNpyrol==0)&&($nbX2ar==0)&&($nbXarCar==2));
			$nomr=31 if (($nbH==0) && ($nbC==1)&& ($nbNpyrol==0)&&($nbX2ar==0)&&($nbXarCar==2));
			$nomr=32 if (($nbH==0) && ($nbar==0) && ($nbC==0)&&($nbXarCar>=2));
			$nomr=33 if (($nbH==1) && ($nbar==1) && ($nbC==1) && ($nbX2ar==1));
			$nomr=33 if (($nbH==1) && ($nbar==1) && ($nbC==1) && ($nbNpyrol==1));
			$nomr=34 if (($nbH==0) && ($nbC==2) && ($nbX2ar==1));
#print "$nbNpyrol\n";
			$nomr=34 if (($nbH==0) && ($nbC==2) && ($nbNpyrol==1));
			$nomr=35 if (($nbH==0) && ($nbar==1) && ($nbC==1) && ($nbX2ar>=1));
			$nomr=35 if (($nbH==0) && ($nbar==1) && ($nbC==1) && ($nbNpyrol>=1));
			$nomr=42 if (($nbH==1) && ($nbC==0)  && ($nbX2ar==1)&&($nbNar==2)&&($nbXarCar==2));
			$nomr=42 if (($nbH==1) && ($nbC==0) && ($nbNpyrol==1)&&($nbNar==2)&&($nbXarCar==2));
			$nomr=43 if (($nbH==0) && ($nbC==1) && ($nbX2ar==1)&&($nbNar>=2)&&($nbXarCar==2));
			$nomr=43 if (($nbH==0) && ($nbC==1) && ($nbNpyrol==1)&&($nbNar>=2)&&($nbXarCar==2));
			$nomr=44 if (($nbH==0) && ($nbC==0) && ($nbX2ar==1)&&($nbNar>=2)&&($nbXarCar>=2));
			$nomr=44 if (($nbH==0) && ($nbC==0) && ($nbNpyrol==1)&&($nbNar>=2)&&($nbXarCar>=2));

		};
		if ($stratomtype[$mri] eq 'H'){
			$nomr=46 if (($csp3) && ($nbsdC==0) && ($nbsdX==0));
			$nomr=47 if (($csp3) && ($nbsdC==1));
			$nomr=47 if (($csp2) && ($nbsdC==0));
			$nomr=48 if (($csp3) && ($nbsdC==2));
			$nomr=48 if (($csp2) && ($nbsdC==1));
			$nomr=48 if (($csp) && ($nbsdC==0));
			$nomr=49 if (($csp3) && ($nbsdC==3));
			$nomr=49 if (($csp2) && ($nbsdC==2));
			$nomr=49 if (($csp2) && ($nbsdC==3));
			$nomr=49 if (($csp) && ($nbsdC==3));
			$nomr=50 if ($hetat);
			$nomr=52 if (($csp3) && ($nbsdC==0) && ($nbsdX==1));
			$nomr=53 if (($csp3) && ($nbsdC==0) && ($nbsdX==2));
			$nomr=54 if (($csp3) && ($nbsdC==0) && ($nbsdX==3));
			$nomr=55 if (($csp3) && ($nbsdC==0) && ($nbsdX>=4));
			$nomr=51 if ($acd >=1);
		};
		if ($stratomtype[$mri]=~/O/){
			$nomr=56 if ($nbH==1);

			$nomr=57 if (($nbH==1)&&($nbCar==1));
			$nomr=57 if (($nbH==1)&&($nbC2==1));
			$nomr=57 if (($nbH==1)&&($stratomtype[$mri]=~/O.co2/));
			$nomr=57 if (($nbH==1)&&($nbXC2==1));

			$nomr=58 if (($nbH==0)&&($nbC==1)&&($nbC2==1));
			$nomr=59 if (($nbH==0)&&($nbC==2));
			$nomr=60 if (($nbH==0)&&($nbC==2)&&($nbCar>=1));
			$nomr=60 if (($nbH==0)&&($nbC==2)&&($nbsdCO>=1));
			$nomr=60 if (($nbH==0)&&($nbC==1)&&($stratomtype[$mri]=~/O.3/));

			$nomr=61 if (($nbNp)&&($stratomtype[$mri]=~/O.2/));
#			$nomr=61 if (($nbNp)&&($stratomtype[$mri]=~/O.3/)&&($nbH==1));
		};
		if ($stratomtype[$mri]=~/N/){
			$nomr=66 if (($nbH==2)&&($nbC==1));
			$nomr=67 if (($nbH==1)&&($nbC==2));
			$nomr=68 if (($nbH==0)&&($nbC==3));
			$nomr=69 if (($nbH==2)&&($nbCar==1));
			$nomr=69 if (($nbH==2)&&($nbC==0));
			$nomr=70 if (($nbH==1)&&($nbC==2)&&($nbCar==1));
			$nomr=71 if (($nbH==0)&&($nbC==3)&&($nbCar==1));
			$nomr=72 if ($nb72>=1);
			$nomr=72 if ($nb72bis>=1);
			$nomr=73 if (($nbH==1)&&($nbC==2)&&($nbCar==2));
			$nomr=73 if (($nbH==0)&&($nbC==3)&&($nbCar==3));
			$nomr=73 if (($nbH==0)&&($nbC==3)&&($nbCar==2));
			$nomr=73 if (($nbH==1)&&($nbC==2)&&($nbCar==2)&&($nbNpyrolbis==2));
			$nomr=74 if (($nbH==0)&&($nbC==1)&&($nbCyne>=1));
			$nomr=74 if (($nbC>=1)&&($nbCene==1));
			$nomr=75 if (($nbH==0)&&($nbC==1)&&($nbCar==1)&&($nbNpyrolbis==1));
			$nomr=75 if (($nbH==0)&&($nbC==2)&&($nbCar==2)&&($nbNpyrolbis==2));

			$nomr=76 if (($nbH==0)&&($nbC==1)&&($nbCar==1)&&($nbNpyrolbis==0)&&($stratomtype[$mri]=~/N.pl3/));
			$nomr=76 if (($nbH==0)&&($nbC==2)&&($nbCar==2)&&($nbNpyrolbis==2)&&($nbO==1));
			$nomr=76 if (($nbH==0)&&($nbC==0)&&($nbO==3)&&($stratomtype[$mri]=~/N.pl3/));
			$nomr=77 if (($nbsimple==1)&&($nbH==0)&&($nbC==1)&&($nbO==2)&&($stratomtype[$mri]=~/N.pl3/));
			$nomr=78 if (($nbH==0)&&($nbC==1)&&($nbCar==1)&&($nbNpyrolbis==0)&&($nbNdouble==1));
			$nomr=78 if (($nbH==0)&&($nbC==0)&&($nbCar==0)&&($nbNdouble==1));
		};
		if ($stratomtype[$mri] eq 'F'){
#print "$nbsdC\n";
			$nomr=81 if (($csp3) && ($nbsdC==1));
			$nomr=82 if (($csp3) && ($nbsdC==2));
			$nomr=83 if (($csp3) && ($nbsdC==3));
			$nomr=84 if (($csp2) && ($nbsdC==1));
			$nomr=85 if (($csp2) && ($nbsdC==2));
			$nomr=85 if (($csp2) && ($nbsdC==3));
			$nomr=85 if (($csp2) && ($nbsdC==4));
			$nomr=85 if (($csp) && ($nbsdC==1));
			$nomr=85 if (($csp) && ($nbsdC==4));
			$nomr=85 if ($hetat);
			
		};
		if ($stratomtype[$mri] eq 'Cl'){
			$nomr=86 if (($csp3) && ($nbsdC==1));
			$nomr=87 if (($csp3) && ($nbsdC==2));
			$nomr=88 if (($csp3) && ($nbsdC==3));
			$nomr=89 if (($csp2) && ($nbsdC==1));
			$nomr=90 if (($csp2) && ($nbsdC==2));
			$nomr=90 if (($csp2) && ($nbsdC==3));
			$nomr=90 if (($csp2) && ($nbsdC==4));
			$nomr=90 if (($csp) && ($nbsdC==1));
			$nomr=90 if (($csp) && ($nbsdC==4));
			$nomr=90 if ($hetat);
			
		};
		if ($stratomtype[$mri] eq 'Br'){
			$nomr=91 if (($csp3) && ($nbsdC==1));
			$nomr=92 if (($csp3) && ($nbsdC==2));
			$nomr=93 if (($csp3) && ($nbsdC==3));
			$nomr=94 if (($csp2) && ($nbsdC==1));
			$nomr=95 if (($csp2) && ($nbsdC==2));
			$nomr=95 if (($csp2) && ($nbsdC==3));
			$nomr=95 if (($csp2) && ($nbsdC==4));
			$nomr=95 if (($csp) && ($nbsdC==1));
			$nomr=95 if (($csp) && ($nbsdC==4));
			$nomr=95 if ($hetat);
			
		};
		if ($stratomtype[$mri] eq 'I'){
			$nomr=96 if (($csp3) && ($nbsdC==1));
			$nomr=97 if (($csp3) && ($nbsdC==2));
			$nomr=98 if (($csp3) && ($nbsdC==3));
			$nomr=99 if (($csp2) && ($nbsdC==1));
			$nomr=100 if (($csp2) && ($nbsdC==2));
			$nomr=100 if (($csp2) && ($nbsdC==3));
			$nomr=100 if (($csp2) && ($nbsdC==4));
			$nomr=100 if (($csp) && ($nbsdC==1));
			$nomr=100 if (($csp) && ($nbsdC==4));
			$nomr=100 if ($hetat);
			
		};

		if ($stratomtype[$mri]=~/S/){
			$nomr=106 if (($nbH==1)&&($nbC==1));
			$nomr=107 if (($nbH==0)&&($nbC==2));
			$nomr=107 if (($nbH==0)&&($nbC==1)&&($nbSd==1));
			$nomr=108 if (($nbH==0)&&($nbC==1)&&($nbdouble==1));
			$nomr=109 if (($nbH==0)&&($nbC==2)&&($nbsimple==2)&&($nbOd==1));
			$nomr=109 if (($nbH==0)&&($nbC==2)&&($nbsimple==2)&&($nbOd==2));
		};
		if ($stratomtype[$mri]=~/P/){
			$nomr=115 if (($nbH==0)&&($nbC==4)&&($nbdouble==1)&&($nbsimple==3));
			$nomr=116 if (($nbH==0)&&($nbC==3)&&($nbsimple==3)&&($nbNdouble==1));
			$nomr=117 if (($nbH==0)&&($nbC==0)&&($nbNsimple==3)&&($nbNdouble==1));
			$nomr=118 if (($nbH==0)&&($nbC==0)&&($nbNsimple==3));
			$nomr=119 if (($nbH==0)&&($nbC==3)&&($nbsimple==3));
			$nomr=120 if (($nbvoisins==4)&&($nbH==0)&&($nbC==1)&&($nbsimple==1)&&($nbNdouble==1));
		};


	$mrinterm=$mrinterm + $LOGPVAL[$nomr];
	$mrinterm=sprintf"%3.2f",($mrinterm);		
	$refract=$refract + $MRVAL[$nomr];
	$refract=sprintf"%3.2f",($refract);

#	print "$mri, $LOGPVAL[$nomr], $MRVAL[$nomr] \n";

	};


#        log S - log P regression equation
#        S    ..... Solubility in Water [mol/L]
#        log P - log 1/S Regression Equations Used in This Program --------
#        log 1/S = 1.050 log P + 0.00956(TM-25.0) - 0.515   [S:mol/L]
#            ( n = 497, r = 0.976, s = 0.505 )
#            ( Regression Range: -1.05<=log P<=8.85)
#       [Ref.] Suzuki,T., J. Comput.-Aided Mol. Design, 5(1991)149.
# 	TM =25 C si on ne connait pas le temperature de fusion

	$sol= 1.050 * $mrinterm - 0.515;
	$solm=1/(10**$sol)*1000*$masse;

#	print "\n";
#	print "Masse	$masse\n";
#	print "\n";
#	print "Ghose et al. :\n";
#	print "MR   		$refract\n";
#	print "LogP		$mrinterm\n";
#	print "WARNING LogP hors range [ -1.05 ; 8.85 ]\n" if (($mrinterm > 8.85) || ($mrinterm < -1.05));
#	print "Solubility Log 1/S =  	$sol (ou $solm mg/l)\n";
#	print "\n";

	print "LOGP_Ghose_and_Crippen= $mrinterm   MR= $refract \n";

#	open(OUT,">$workdir/RANK");
#	printf OUT "$mrinterm $refract\n";
#	close(OUT);
