/****************************************************
  Jmol interaction functions
*****************************************************/

function affiche_align(file){
	  top.jmol_right.location.href="jmol?file="+file+"&level=deb&cmd="
	}


function none(name,loop){
	loop = parseInt(loop)
	if (loop<= 0) {return;}
    for(var i = 0;i < top.jmol_right.document.getElementsByTagName("span").length;i++){
		top.jmol_right.document.getElementsByTagName("span")[i].className = '';
	}
	loop = loop-1;
	loop.toString()
    top.jmol_right.document.getElementById(name).className = 'black_inv';
    setTimeout("color_align('"+name+"',"+loop+")",300);
}

function color_align(name,loop){
	loop = parseInt(loop)
	if (loop<= 0) {return;}
	for(var i = 0;i < top.jmol_right.document.getElementsByTagName("span").length;i++){
		top.jmol_right.document.getElementsByTagName("span")[i].className = '';
	}
	loop = loop-1;
	loop.toString()
	top.jmol_right.document.getElementById(name).className = 'white';
	setTimeout("none('"+name+"',"+loop+")",300);
	}

function color_align_dist(name,num){
	if (num == 1){
	for(var i = 0;i < top.jmol_right.document.getElementsByTagName("span").length;i++){
		top.jmol_right.document.getElementsByTagName("span")[i].className = '';
	    }
	}
	top.jmol_right.document.getElementById(name).className = 'bleu';
}


function docolor(pos){
var cmd = document.options.color.value+"select "+pos+";color green;";
mycallback("user", "residu "+pos+" colored")
document.applets["jmol"].script(cmd);
}

function execute(cmd){
//mycallback("user", cmd);
document.applets["jmol"].script(cmd);
}

function execute_right(cmd){
if (parent.jmol_left.document.options.color.value != "select;color amino;color backbone amino;")
	{
	cmd = parent.jmol_left.document.options.color.value+cmd;
	}
else {
	cmd = "select;color structure;"+cmd;
	}
parent.jmol_left.document.applets["jmol"].script(cmd);
}

function executepick(cmd){
mypickcallback("user", cmd);
parent.jmol_right.document.applets["jmol"].script(cmd);
}

function doacc(){
	if (parent.jmol_right.document.cmd.com.value == "none") {
	setTimeout("doacc()", 1000);
	}
	else {
	com = parent.jmol_right.document.cmd.com.value;
	parent.jmol_right.document.cmd.com.value = "none";
	execute(com);
	}
}

function docmd(cmd){
parent.jmol_right.location.href=cmd;
return;
}

function docmdacc(cmd){
top.jmol_right.location.href=cmd;
doacc();
}

function mycallback(n,what){
	var s = document.options.infos.value;
	   /*var file = document.options.file.value;
	   pos = file.indexOf("_",0)*/
       if(s.length>10000) s="";
 	if (n=="user") {
        document.options.infos.value=n+": "+what+"\n"+s
	}
 	else {
 		var mot= new RegExp("[0-9]+ atoms",'g');
		if (what.substring(0,6)== "Script"){ what = "" }
		else if (what.substring(0,4)== "Jmol"){ what = "" }
		if (what.substring(0,7)== "Atom #1"){
			pos = what.indexOf(":",8);
			sel_aa = what.substring(8,pos+2);
			pos =sel_aa.indexOf(":",0);
			pos_aa = sel_aa.substring(5,pos);
			pos_aa.toString();
			chain = sel_aa.substring(pos+1,pos+2);
			color_align_dist(pos_aa+":"+chain,1);
			/*else {
				for(var i = 0;i < top.jmol_right.document.getElementsByTagName("span").length;i++){
					top.jmol_right.document.getElementsByTagName("span")[i].className = '';
					}
				}*/

			 msg = document.options.color.value+"select "+sel_aa+";color blue;";
			 document.options.infos.value= sel_aa+" selected\n"+s;
			 document.applets["jmol"].script(msg);
			 what = what+"\n";
			 }
		if (what.substring(0,7)== "Atom #2"){
		     pos = what.indexOf(":",8);
			 sel_aa = what.substring(8,pos+2);
			 pos =sel_aa.indexOf(":",0);
			 pos_aa = sel_aa.substring(5,pos);
			 pos_aa.toString();
			 chain = sel_aa.substring(pos+1,pos+2);
			 color_align_dist(pos_aa+':'+chain,0);
			  /* else {
			       for(var i = 0;i < top.jmol_right.document.getElementsByTagName("span").length;i++){
			       top.jmol_right.document.getElementsByTagName("span")[i].className = '';
			       }
			   }*/
			 msg = "select "+sel_aa+";color blue;";
			 document.options.infos.value= sel_aa+" selected\n"+s;
			 document.applets["jmol"].script(msg);
			 what = what+"\n";
			 }
		if (what.substring(0,8)== "Distance"){
		         pos1 = what.indexOf(":",0);
			pos_c = what.indexOf("[",0);
			 aa1 = what.substring(pos_c,pos1+2);
			 pos2 = what.indexOf(":",18);
			pos2_c = what.indexOf("[",18);
			aa2 = what.substring(pos2_c,pos2+2);
	        pos3 = what.indexOf(":",40);
			dist = what.substring(pos3+2);
			 dist = dist.toString();
			 what = "Distance between "+aa1+" and "+aa2 + " = " + dist + " Angst\n";
			 } 
			  if (what.match(mot)) {
			 what = "";
			 }	      
      document.options.infos.value=what+s;
 	}
}


function mypickcallback(n,what){
       var s = document.options.infos.value;
	   var options = ""
	   
	   /*var file = document.options.file.value;
	   pos = file.indexOf("_",0)
	   chain_f = file.substring(pos+5,pos+6)*/
	   chain_f = 'A';
       if(s.length>10000) s="";
       what = what.substring(0,11);
       pos = what.indexOf(":",0)
       sel_aa = what.substring(0,pos+2);
       aa = what.substring(1,4);
       pos_aa = what.substring(5,pos);
	   chain = sel_aa.substring(pos+1,pos+2);
       msg = document.options.color.value+";select "+sel_aa+";color green;select";
       document.options.infos.value= sel_aa+" selected\n"+s;
       document.applets["jmol"].script(msg);
	if (document.options.tools[0].checked == true && document.options.aa_info.checked == false){
	     pos_aa.toString();
		 color_align(pos_aa+':'+chain,3);
	   }

	   else if (document.options.tools[0].checked == true && document.options.aa_info.checked == true){
		cmd = "jmol?file="+document.options.file.value+"&level=deb&cmd="+what;
		docmd(cmd);
		pos_aa.toString();
		color_align(pos_aa+':'+chain,1);
		}

	   else if (document.options.tools[1].checked == true && document.options.aa_info.checked == true){
			    top.jmol_right.document.open();
			    top.jmol_right.document.write("<HTML><HEAD></head><body>Please wait...<form name='cmd'><input name='com' type='hidden' value='none'></form></body></html>");
				top.jmol_right.document.close();
				cmd = "aa_acc?aa="+aa+"&pos="+pos_aa+"&file=" + document.options.file.value + "&lim=" + document.options.lim.value + "&typecol=" + document.options.typecol.value+"&what="+what;
				docmdacc(cmd);
		}
	else if (document.options.tools[1].checked == true && document.options.aa_info.checked == false){
			    top.jmol_right.document.open();
			    top.jmol_right.document.write("<HTML><HEAD></head><body>Please wait...<form name='cmd'><input name='com' type='hidden' value='none'></form></body></html>");
				top.jmol_right.document.close();
				cmd = "aa_acc?aa="+aa+"&pos="+pos_aa+"&file=" + document.options.file.value + "&lim=" + document.options.lim.value + "&typecol=" + document.options.typecol.value;
				docmdacc(cmd);
		}

}