			# script to dock a list of ligand
			# flexx -b dock_list.bat -a '$(p)=rarg;$(list)=list_mol2;$(n)=15' -o dbflexx.out &

			output " >> docking " $(l) " into " $(p)

			set verbosity 			3
			set assign_formal_charges 	1
			set place_particles		0

			receptor
				read $(p)
			end

	for_each   $(l) in $(list)
       			lig
       				read   $(l)
       							# bond atom
				MAPREF   map_reference.mol2  n y
       			end

       			
			docking
			selbas   r
			placebas p
			complex

			info y
			listsol $(n)
			end

			lig
  	 			write ./$(l) n n 1-$(n)
			end
			output ">>written:  $(l)_xxx.mol2"
	end_for

			delall y
			quit y	
			
			
