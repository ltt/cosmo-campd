#!/usr/bin/perl


$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

$leaflexx=$leaexe;

$radius="";
$water="";
$chainold="";
$limitdist="";
local($pdb,$cx,$cy,$cz,$radius,$water,$limitdist)=@ARGV;


if ($pdb eq '' || $cx eq '' || $cy eq '' || $cz eq '' || $radius eq ''){
	die "\n
	usage: makeflexx  <protein.pdb>  <Residue_name> <CHAIN name or '-'> <Residue number> <radius (eg: 10 A)> <'water' or empty ''> <radius_water (eg: 3.1 A keep water molecules close to the Residue) or empty ''>
\nOR
	usage: makeflexx  <protein.pdb>  <x> <y> <z> <radius (eg: 10 A)> <'water' or empty ''> <radius_water (eg: 3.1 A keep water molecules>\n\n"
};


################################################ config.dat a jour !

	$flexx_config=$ENV{FLEXX_HOME};
        if (($flexx_config eq "" || !-e "$flexx_config/config.dat") && !-e "config.dat"){
                system("cp $leaflexx/FLEXX/config.dat .");
                print "Warning : config.dat have been copied from $leaflexx/FLEXX\n";
                print "Copy Failed !\n" if(!-e "config.dat");
        };

	chop($workdir = `pwd` );
	
 system("rm -f wat_*_a*");
 system("rm -f wat_*_b*");
 system("rm -f wat_*_c*");
 system("rm -f wat_*_d*");
 system("rm -f water*");

################################################ creation du site actif et des water.mol2

$refx=0.0;
$refy=0.0;
$refz=0.0;

$site="site_".$pdb;
$rec="rec_".$pdb;

if($cy!~/\d/){

	$chain=$cy;
	$chainold=$chain;
	$chain='' if($chain eq '-');
	
	$res=$cx;
	
	$nores=$cz;
	
	$limitdist=3.2 if($limitdist eq '');
	
	print "pdb=$pdb chain=$chain residue=$res no=$nores radius=$radius water=$water($limitdist A)\n";
	if($radius eq ""){
		die "radius ?\n";
	};
	
	@typepdb='';
	@serial='';
	@cox='';
	@coy='';
	@coz='';
	$coi=0;
	@new_coord='';
	
	open(IN,"<$workdir/$pdb");
	open(OUT,">$workdir/$res.pdb");
	open(OUTC,">$workdir/$res.sdf");
	$new_coord[0]="\n";
	$new_coord[1]="  -ISIS-  08029913582D\n";
	$new_coord[2]="\n";
	$longa=0;
	$longb=0;
	$serial_list=' ';
	$serial_listb=' ';	
	while(<IN>){
	
	       if($_ =~/^ATOM/ || $_ =~/^HETATM/){
	
		@getp=split('',$_);
	        @get=split(' ',$_);
	
		$type_atom=$getp[12].$getp[13].$getp[14].$getp[15];
	        $type_atom=~s/ //g;
	
	        $type_atombis='';
	        $type_atombis=$getp[76].$getp[77];
	        $type_atombis=~s/\n//g;
	        $type_atombis=~s/ //g;
	        $type_atombis=$getp[12].$getp[13] if($type_atombis eq '');
	        $type_atombis=~s/ //g;
	
		$residu=$getp[17].$getp[18].$getp[19];	
		$residu=~s/ //g;
		$polcha=$getp[21];
		$no=$getp[22].$getp[23].$getp[24].$getp[25].$getp[26];
		$no=~s/ //g;
		$id=$getp[70].$getp[71].$getp[72].$getp[73].$getp[74].$getp[75];
	
	        $serial_tmp=$getp[6].$getp[7].$getp[8].$getp[9].$getp[10];
	        $serial_tmp=~s/ //g;
	
		if($residu eq $res && ($polcha eq $chain || $chain eq '') && ($no eq $nores)){
	       	 	
			#print en sdf
	       	 	
	       	 	$serial_list=$serial_list." $serial_tmp ";
	       	 	$coib=$coi+1;
	       	 	$serial_listb=$serial_listb." $coib ";
	       	 	
	       	 	print OUT "$_";
	        	$serial[$coi]=$serial_tmp;
	        	$typepdb[$coi]=$type_atom;
			
			$cox[$coi]=$getp[30].$getp[31].$getp[32].$getp[33].$getp[34].$getp[35].$getp[36].$getp[37];
			$coy[$coi]=$getp[38].$getp[39].$getp[40].$getp[41].$getp[42].$getp[43].$getp[44].$getp[45];
			$coz[$coi]=$getp[46].$getp[47].$getp[48].$getp[49].$getp[50].$getp[51].$getp[52].$getp[53];
			
			$zero=0;
			$new_coord[4]=$new_coord[4].sprintf"%10s%10s%10s%1s%1s%1s%3s%3s%3s\n",$cox[$coi],$coy[$coi],$coz[$coi],$blanc,$type_atombis,$blanc,$zero,$zero,$zero;
			
			$coi++;
		};
		};
		
		if($_=~/^CONECT/ && $serial_list=~/ $get[1] /){
			print OUT "$_";
			
			@g1=split(' ',$serial_list);
			@g2=split(' ',$serial_listb);
			foreach $r1 (0..@g1-1){
				$coi1=$g2[$r1] if($get[1] == $g1[$r1]);
			};
			
			foreach $r (2..@get-1){
				foreach $r1 (0..@g1-1){
					if($get[$r] == $g1[$r1]){
						$coi2=$g2[$r1];
						
						$test1=sprintf"%3s%3s%3s  0  0  0  0\n",$coi2,$coi1,1;
						$test2=sprintf"%3s%3s%3s  0  0  0  0\n",$coi1,$coi2,1;
						if($new_coord[5]!~/$test1/ && $new_coord[5]!~/$test2/){
							$new_coord[5]=$new_coord[5].$test1;
                                        		$longb++;
       						};
      					};
				};
			};
			
		};
	};
	close(IN);
	close(OUT);
	
	if($new_coord[5] eq ''){
	         foreach $k (0..@cox-2){
	         	foreach $k2 ($k+1..@cox-1){
	         	 	if($k != $k2){
	         	 		$dx=($cox[$k] - $cox[$k2])*($cox[$k] - $cox[$k2]);
					$dy=($coy[$k] - $coy[$k2])*($coy[$k] - $coy[$k2]);
					$dz=($coz[$k] - $coz[$k2])*($coz[$k] - $coz[$k2]);
		                	
					$dist=sprintf"%3.3f",sqrt($dx + $dy +$dz);
		
					if($dist <= 1.8){
					#print "$k $k2\n";
				        	$coi2=$k+1;
				        	$coi1=$k2+1;
						$test1=sprintf"%3s%3s%3s  0  0  0  0\n",$coi2,$coi1,1;
						$new_coord[5]=$new_coord[5].$test1;
						$longb++;
					};
	   			};
	         	};
		};
	};
	
	print OUTC "$new_coord[0]";
	print OUTC "$new_coord[1]";
	print OUTC "$new_coord[2]";
	
	$longa=@coz;
	$new_coord[3]=sprintf"%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$longa,$longb;
	print OUTC "$new_coord[3]";
	print OUTC "$new_coord[4]";
	print OUTC "$new_coord[5]";
	print OUTC "M  END\n";
	print OUTC "\$\$\$\$\n";
	close(OUTC);
	
	chop($mol2test=`$leaexe/SDF_MOL2.pl $res.sdf`);
	rename "$workdir/mol_1.mol2", "$workdir/$res.mol2";
	
	if($coi==0){
		die "Residue $res $chain does not exist ?\n";
	};
	
	#### Calcul du centre de masse du ligand

	%tabmm=(
		'C',12,
		'O',16,
		'N',14,
		'S',32,
		'P',31,
		'Br',80,
		'Cl',35,
		'I',127,
		'F',19,
		'H',1,
	);

	$rgyrmm=0;
	$rgx=0;
	$rgy=0;
	$rgz=0;
	foreach $k (0..@cox-1){
		$rgyrmm=$rgyrmm+$tabmm{C};
		$rgx=$rgx+$cox[$k]*$tabmm{C};
		$rgy=$rgy+$coy[$k]*$tabmm{C};
		$rgz=$rgz+$coz[$k]*$tabmm{C};
	};
	if($rgyrmm != 0){
		$rgx=sprintf"%4.3f",($rgx/$rgyrmm);
		$rgy=sprintf"%4.3f",($rgy/$rgyrmm);
		$rgz=sprintf"%4.3f",($rgz/$rgyrmm);
	};
		
        $refx=$rgx;
        $refy=$rgy;
        $refz=$rgz;

	open(OUT,">>$workdir/$res.pdb");
		#printf OUT  "HETATM    3  As  wat     1     %7s %7s %7s\n",$rgx,$rgy,$rgz;
		print OUT "REMARK CENTER $rgx $rgy $rgz\n";
	close(OUT);


}
else{

        print "pdb=$pdb Ref($cx, $cy, $cz,) radius=$radius water=$water\n";
	if($radius eq ""){
		die "radius ?\n";
	};
	
	@cox='';
	@coy='';
	@coz='';
	$coi=0;

	$cox[$coi]=$cx;
	$coy[$coi]=$cy;
	$coz[$coi]=$cz;
	
	$refx=$cx;
        $refy=$cy;
        $refz=$cz;

        $limitdist=$radius if($limitdist eq '');

};




@keepres='';
@keepno='';
@keepchain='';
$ki=0;

open(IN,"<$workdir/$pdb");
open(OUR,">$workdir/$rec");
while(<IN>){

	@getp=split('',$_);
	
	$residu=$getp[17].$getp[18].$getp[19];
	$residu=~s/ //g;	
	$polcha=$getp[21];
	$no=$getp[22].$getp[23].$getp[24].$getp[25].$getp[26];
	$no=~s/ //g;
	$id=$getp[70].$getp[71].$getp[72].$getp[73].$getp[74].$getp[75];
	
	$x=$getp[30].$getp[31].$getp[32].$getp[33].$getp[34].$getp[35].$getp[36].$getp[37];
	$y=$getp[38].$getp[39].$getp[40].$getp[41].$getp[42].$getp[43].$getp[44].$getp[45];
	$z=$getp[46].$getp[47].$getp[48].$getp[49].$getp[50].$getp[51].$getp[52].$getp[53];
	
	print OUR $_ if($_ =~/^ATOM/ && ($residu!~/H2O/ && $residu!~/WAT/ && $residu!~/HOH/ && $residu!~/OH2/));
	
	#If residu $res is an aa from protein then include it in rec and site	
	$templateaa=" LYS ARG GLN GLU ASN ASP HIS PRO ILE LEU ALA VAL GLY CYS MET TRP PHE TYR SER THR ";

	if($residu eq $res && $templateaa!~/$res/ && ($polcha eq $chain || $chain eq '') && ($no eq $nores)){
		print "Caution residu $res $no is considered as a ligand not a residue. It will not be in site.pdb\n";
	}
	else{
	
	if($water eq ''){
		if(($_ =~/^ATOM/ || $_ =~/^HETATM/) && ($residu!~/H2O/ && $residu!~/WAT/ && $residu!~/HOH/ && $residu!~/OH2/)){
			
				foreach $k (0..@cox-1){
					$dx=($x - $cox[$k])*($x - $cox[$k]);
					$dy=($y - $coy[$k])*($y - $coy[$k]);
					$dz=($z - $coz[$k])*($z - $coz[$k]);
		
					$dist=sprintf"%3.3f",sqrt($dx + $dy +$dz);
		
					if($dist <= $radius && $keepno[$ki-1] ne $no){
						#print "d=$_";
						$keepres[$ki]=$residu;
						$keepno[$ki]=$no;
						print "RES $no\n";
						$keepchain[$ki]=$polcha;
						$ki++;
						last;
					};
				};
			
       		 };
   	}
   	else{
		if($_ =~/^ATOM/ || $_ =~/^HETATM/){
			
				foreach $k (0..@cox-1){
					$dx=($x - $cox[$k])*($x - $cox[$k]);
					$dy=($y - $coy[$k])*($y - $coy[$k]);
					$dz=($z - $coz[$k])*($z - $coz[$k]);
		
					$dist=sprintf"%3.3f",sqrt($dx + $dy +$dz);
		
		                        #print "ALA 13 $dist\n" if($residu eq "ALA" && $no == 13);
		                        #print "$_\n" if($residu eq "ALA" && $no == 13);
		
					if($dist <= $radius && $keepno[$ki-1] ne $no){
						if($residu=~/H2O/ || $residu=~/WAT/ || $residu=~/HOH/ || $residu=~/OH2/){
							if($dist <= $limitdist){
								#print "d=$dist $_";
								$keepres[$ki]=$residu;
								$keepno[$ki]=$no;
								$keepchain[$ki]=$polcha;
								$ki++;
								last;
							};
						}
						else{
						    	#print "d=$dist $_";
							$keepres[$ki]=$residu;
							$keepno[$ki]=$no;
							print "RES $no\n";
							$keepchain[$ki]=$polcha;
							$ki++;
							last;
						};
					};
				};
			
       		 };   	
   	};
   	};

};
close(IN);
close(OUR);


#foreach $l (0..@keepres-1){
#	print"$keepres[$l] $keepno[$l]\n";
#};

$nbaa=@keepres;
$nom=$site;
$nom_cofacteur="cofactor_".$pdb;

$aai=0;
$aabold='';

$histidine='';
$metaux='';
$metaux_include='';
$metaux_pdb='';
$h2o='';

@watero='';
$wi=0;
	
open(OUT,">$workdir/$nom");
open(OUM,">$workdir/$nom_cofacteur");
open(IN,"<$workdir/$pdb");
while(<IN>){

	@getp=split('',$_);
	
	$residu=$getp[17].$getp[18].$getp[19];	
	$residu=~s/ //g;
	$polcha=$getp[21];
	$atomname=$getp[12].$getp[13].$getp[14].$getp[15];
	$atomname=~s/ //g;
	
	$polcham=$polcha;
	$polcham=~s/ //g;
	$polcham="*" if($polcham eq '');
	
	#print "\nWARNING ambiguite entre $residu $no et atom name $atomname\n\n" if ($atomname=~/MG/ && $residu!~/MG/);
	
	$no=$getp[22].$getp[23].$getp[24].$getp[25].$getp[26];
	$no=~s/ //g;
	$id=$getp[70].$getp[71].$getp[72].$getp[73].$getp[74].$getp[75];

	foreach $l (0..@keepres-1){
		if($water eq ''){
			if(($_ =~/^ATOM/ || $_ =~/^HETATM/) && ($keepchain[$l] eq $polcha) && ($residu!~/H2O/ && $residu!~/WAT/ && $residu!~/HOH/ && $residu!~/OH2/)){
		        	if($keepno[$l] == $no && $keepres[$l] eq $residu){
		        		
		        		if($residu =~ /MG/ || $residu =~ /MN/ || $residu =~ /ZN/ || $residu =~ /CA/ || $residu =~ /CO/){
		        			print "WARNING : $residu mis en ATOM !" if($_ =~/^ATOM/);
		        			$metaux=$metaux."_$residu $polcham $no _zn\n" if($residu =~/ZN/);
		        			$metaux=$metaux."_$residu $polcham $no _mn\n" if($residu =~ /MN/);
		        			$metaux=$metaux."_$residu $polcham $no _mg\n" if($residu =~ /MG/);
		        			$metaux=$metaux."_$residu $polcham $no _ca\n" if($residu =~ /CA/);
		        			$metaux=$metaux."_$residu $polcham $no _co\n" if($residu =~ /CO/);
		        			$metaux_include=$metaux_include."include _$residu $polcham $no\n";
		        			$metaux_pdb=$metaux_pdb.$_;
		        		};
		        		if($residu =~ /FE2/ || $residu =~ /FE3/){
		        			print "WARNING : $residu mis en ATOM !" if($_ =~/^ATOM/);
		        			$metaux=$metaux."$residu $polcham $no _fe2\n" if($residu =~ /FE2/);
		        			$metaux=$metaux."$residu $polcham $no _fe3\n" if($residu =~ /FE3/);
		        			$metaux_include=$metaux_include."include $residu $polcham $no\n";
		        			$metaux_pdb=$metaux_pdb.$_;
		        		};
		        		
		        		if($_ =~/^ATOM/){
		        			print OUT "$_";
		        			$histidine=$histidine."$residu $no $polcha\n" if($residu =~ /HIS/ && $histidine!~/ $no /);
		        		};
		        		
		        		if($_ =~/^HETATM/){
		        			print OUM "$_";
		        		};
		        		
		        		$aai++ if($aabold ne $no);
		        		$aabold=$no;
		        	};
			};
		}
		else{
		      if(($_ =~/^ATOM/ || $_ =~/^HETATM/) && ($keepchain[$l] eq $polcha)){
		
		        	if($keepno[$l] == $no && $keepres[$l] eq $residu){
		        	
		        	
		        		if(($residu=~/H2O/ || $residu=~/WAT/ || $residu=~/HOH/ || $residu=~/OH2/)&& $atomname =~ /O/){
		        			print "WARNING : $residu mis en ATOM !" if($_ =~/^ATOM/);
		        			$x=$getp[30].$getp[31].$getp[32].$getp[33].$getp[34].$getp[35].$getp[36].$getp[37];
						$y=$getp[38].$getp[39].$getp[40].$getp[41].$getp[42].$getp[43].$getp[44].$getp[45];
						$z=$getp[46].$getp[47].$getp[48].$getp[49].$getp[50].$getp[51].$getp[52].$getp[53];
		        		
						$watero[$wi]="$x $y $z wat_$no";
						$wi++;
						
		        	 		$h2o=$h2o."#wat_$no"."_a.mol2 #wat_$no"."_b.mol2 #wat_$no"."_c.mol2 #wat_$no"."_d.mol2 ";
		        	 	};
		        	
		        	        if($residu =~ /MG/ || $residu =~ /MN/ || $residu =~ /ZN/ || $residu =~ /CA/ || $residu =~ /CO/){
		        			print "WARNING : $residu mis en ATOM !" if($_ =~/^ATOM/);
		        			$metaux=$metaux."_$residu $polcham $no _zn\n" if($residu =~ /ZN/);
		        			$metaux=$metaux."_$residu $polcham $no _mn\n" if($residu =~ /MN/);
		        			$metaux=$metaux."_$residu $polcham $no _mg\n" if($residu =~ /MG/);
		        			$metaux=$metaux."_$residu $polcham $no _ca\n" if($residu =~ /CA/);
		        			$metaux=$metaux."_$residu $polcham $no _co\n" if($residu =~ /CO/);
		        			
		        			$metaux_include=$metaux_include."include _$residu $polcham $no\n";
		        			$metaux_pdb=$metaux_pdb.$_;
		        		};
		        		if($residu =~ /FE2/ || $residu =~ /FE3/){
		        		        print "WARNING : $residu mis en ATOM !" if($_ =~/^ATOM/);
		        			$metaux=$metaux."$residu $polcham $no _fe2\n" if($residu =~ /FE2/);
		        			$metaux=$metaux."$residu $polcham $no _fe3\n" if($residu =~ /FE3/);
		        			
		        			$metaux_include=$metaux_include."include $residu $polcham $no\n";
		        			$metaux_pdb=$metaux_pdb.$_;
		        		};
		        		
		        	        if($_ =~/^ATOM/ && ($residu!~/H2O/ && $residu!~/WAT/ && $residu!~/HOH/ && $residu!~/OH2/)){
		        			print OUT "$_";
		        			$histidine=$histidine."$residu $no $polcha\n" if($residu =~ /HIS/ && $histidine!~/ $no /);
		        		};
		        		
		        		if($_ =~/^HETATM/){
		        			print OUM "$_";
		        		};
		        	
		        		$aai++ if($aabold ne $no);
		        		$aabold=$no;
		        	};
			};
		};	
	};	

};
close(IN);
#print OUT "END";
close(OUM);
close(OUT);

open(IN,">>$rec");
print IN $metaux_pdb;
close(IN);

open(IN,">>$site");
print IN $metaux_pdb;
close(IN);

######################################## cree les molecules d'eau et repere la plus probable ie H le plus pres d'un heteroatome du ligand

if($water ne '' && $watero[0] ne ''){

open(FGH,">water.pdb");
open(OUTC,">water.mol2");
	print OUTC "#\n";
	print OUTC "#	  Creating by LEA \n";
	print OUTC "#\n";
	print OUTC "\n";
	print OUTC "@<TRIPOS>MOLECULE\n";
	print OUTC "water\n";

	$istratom=@watero*3;
	$istrbond=@watero*2;
        printf OUTC "%4s%1s%4s\n",$istratom,$blanc,$istrbond;
	print OUTC "SMALL\n";
	print OUTC "NO_CHARGES\n\n\n";
        $a1=0;
        $a2=0;

$bondeau='';
$coordeau='';
foreach $l (0..@watero-1){
	system("$leaexe/poswat $watero[$l]");
	
	#if($chainold ne ''){
		$radius_ref=1000;
	 	$l_ref='';
	        @get=split(' ',$watero[$l]);
	        @eautmp='';
	        @coordeau_tmp='';
	        @a1_tmp='';
	        @a1h2='';
	        @a1h1='';
	        @a1o[$p]='';
	
	        foreach $p (1..4){
			$part="a" if($p==1);
			$part="b" if($p==2);
			$part="c" if($p==3);
			$part="d" if($p==4);
			$moldeau="$get[@get-1]"."_"."$part".".pdb";
			$a1_tmp[$p]=$a1;
			
			open(JKL,"<$workdir/$moldeau");
			while(<JKL>){
				
				@getp=split('',$_);
		                $eautmp[$p]=$eautmp[$p].$_ if($_=~/^ATOM/ || $_=~/^HETATM/);
		
		 		$residu=$getp[17].$getp[18].$getp[19];
				$residu=~s/ //g;	
				$polcha=$getp[21];
				$no=$getp[22].$getp[23].$getp[24].$getp[25].$getp[26];
				$no=~s/ //g;
				$id=$getp[70].$getp[71].$getp[72].$getp[73].$getp[74].$getp[75];
				
				$x=$getp[30].$getp[31].$getp[32].$getp[33].$getp[34].$getp[35].$getp[36].$getp[37];
				$y=$getp[38].$getp[39].$getp[40].$getp[41].$getp[42].$getp[43].$getp[44].$getp[45];
				$z=$getp[46].$getp[47].$getp[48].$getp[49].$getp[50].$getp[51].$getp[52].$getp[53];
					
	                	$atomname=$getp[12].$getp[13].$getp[14].$getp[15];
	                	$atomname=~s/ //g;
	                	
	                	if($_=~/^ATOM/ || $_=~/^HETATM/){
	                		$a1_tmp[$p]++;
	                		$atomnameb=$atomname.$a1_tmp[$p];
	                		if($atomname=~/H/){
	                			$coordeau_tmp[$p]=$coordeau_tmp[$p].sprintf"%4s%5s%6s%11s%11s%11s H\n",$a1_tmp[$p],$atomnameb,$blanc,$x,$y,$z;
	                			$a1h2[$p]=$a1_tmp[$p] if($a1h1[$p] ne '');
	                			$a1h1[$p]=$a1_tmp[$p] if($a1h1[$p] eq '');
	                		}
	                		elsif($atomname=~/O/){
	                			$coordeau_tmp[$p]=$coordeau_tmp[$p].sprintf"%4s%5s%6s%11s%11s%11s O.spc\n",$a1_tmp[$p],$atomnameb,$blanc,$x,$y,$z;
	                			$a1o[$p]=$a1_tmp[$p];
	                		};
	                	
	                		if($atomname=~/H/){
	                	
			        		foreach $k (0..@cox-1){
							$dx=($x - $cox[$k])*($x - $cox[$k]);
							$dy=($y - $coy[$k])*($y - $coy[$k]);
							$dz=($z - $coz[$k])*($z - $coz[$k]);
		
							$dist=sprintf"%3.3f",sqrt($dx + $dy +$dz);
		
							
							if($dist <= $radius_ref && ($typepdb[$k]=~/N/ || $typepdb[$k]=~/O/ || $typepdb[$k]=~/S/)){
								#print "$typepdb[$k] $dist\n";
								$radius_ref=$dist;
								$l_ref=$p;
							};
						};
					};
				};
				
			};
			close(JKL);
		};
		
		if($chainold ne ''){
			$part="a" if($l_ref==1);
			$part="b" if($l_ref==2);
			$part="c" if($l_ref==3);
			$part="d" if($l_ref==4);
		
			print "decommente $get[@get-1]_$part hydrogene a $radius_ref A\n";
			print FGH "$eautmp[$l_ref]";
		
			$a1=$a1+3;
			$coordeau=$coordeau.$coordeau_tmp[$l_ref];
		
			$a2++;
			$bondeau=$bondeau.sprintf"%4s%5s%5s 1\n",$a2,$a1o[$l_ref],$a1h1[$l_ref];
			$a2++;
			$bondeau=$bondeau.sprintf"%4s%5s%5s 1\n",$a2,$a1o[$l_ref],$a1h2[$l_ref];
		
			$h2o=~s/#$get[@get-1]_$part/$get[@get-1]_$part/;
		}
		else{
			$l_ref=1;
		    	$part="a" if($l_ref==1);
			$part="b" if($l_ref==2);
			$part="c" if($l_ref==3);
			$part="d" if($l_ref==4);
		
			print "decommente au hasard $get[@get-1]_$part\n";
			print FGH "$eautmp[$l_ref]";
		
			$a1=$a1+3;
			$coordeau=$coordeau.$coordeau_tmp[$l_ref];
		
			$a2++;
			$bondeau=$bondeau.sprintf"%4s%5s%5s 1\n",$a2,$a1o[$l_ref],$a1h1[$l_ref];
			$a2++;
			$bondeau=$bondeau.sprintf"%4s%5s%5s 1\n",$a2,$a1o[$l_ref],$a1h2[$l_ref];
		
			$h2o=~s/#$get[@get-1]_$part/$get[@get-1]_$part/;
		};
	#};
};
close(FGH);

	print OUTC "@<TRIPOS>ATOM\n";
	print OUTC "$coordeau";
	print OUTC "@<TRIPOS>BOND\n";
	print OUTC "$bondeau";
	
close(OUTC);


$h2o=~s/ /\n/g;

# EN TEST
if($h2o ne ''){
	system("mkdir WATER");
	system("mv wat*_* WATER");
	
	#open(OUTC,">WATER/list_wat");
	#open(OUTC,">list_wat");
	#	print OUTC "$h2o";
	#close(OUTC);
	#$h2o=""; 
	
	$h2o="water.mol2\n";
};

	#$h2o=$h2o."#water.mol2\n" if(-e "water.mol2" && !-z "water.mol2");
};

########################################

@sprdf=split('\.',$rec);

$rdf=$sprdf[0].".rdf";
$tmp=$sprdf[0].".tmp";

print "\n====================== OUTPUT files ======================================\n";
print "\nPDB $rec\nSITE $site = $aai residues (sur $nbaa reperes)\n";

print "\n===================== FLEXX input Files ================================\n";
print "\nInput file pour FlexX :\n\t$rec\n\t$rdf\n";
print "\n Check bond type before any processsing !\n \t $res.pdb \n\t $res.sdf \n\t $res.mol2 \n" if(-e "$res.pdb" && !-z "$res.pdb");

print "\n===================== COFACTORS files ================================\n";
print "\ncofacteur(s) eventuel : $nom_cofacteur A vous de les ajouter si necessaire !\n" if(-e "$nom_cofacteur" && !-z "$nom_cofacteur");
unlink "$nom_cofacteur" if(-z "$nom_cofacteur");

print "\n===================== WATER MOLECULES ================================\n";
print "\nLes molecules H2O sont deja inclues dans $rdf avec 4 possibilites pour la position des hydrogenes (celles qui semblent les meilleures sont decommentees et sont concatenees dans le fichier water.pdb et water.mol2): \n$h2o A vous de verifier !\n" if($h2o ne '');

print "\n===================== METAL ===============================\n";
print "\nMetaux (deja inclus dans $rdf) :\n$metaux\n" if($metaux ne '');


print "\n===================== HISTIDINES =====================\n";
print "\nVerifier la protonation des Histidines suivantes (modifier le fichier $rdf en consequence):\n$histidine\n" if($histidine ne '');


print "\n===================== OTHER FILES =====================\n";
print "\n ligand.in (LEA3D input file)\n\t ligand.func (LEA3D input file)\n\t fixe_scaffold.bat (FlexX input file)\n";


print "\n=====================================================\n";
	
################################################ RDF


system("cp $leaflexx/FLEXX/protein.rdf $tmp");

$sitegen=$site;
$sitegen=~s/\.ent$//;
$recgen=$rec;
$recgen=~s/\.ent$//;

open(IN,"<$tmp");
open(OUT,">$rdf");
while(<IN>){
	if($_=~/^\@pdb_file/){
		print OUT "\@pdb_file $recgen\n";
	}
	elsif($_=~/^site7A/){
		print OUT "site7A $sitegen\n";	
	}
	elsif($_=~/^#_MG/ && $metaux ne ''){
		print OUT "$metaux\n";	
		print OUT $_;	
	}	
	elsif($_=~/#include _MG/ && $metaux ne ''){
		print OUT $_;
		print OUT "$metaux_include\n";	
	}
	elsif($_=~/^#h2o\.mol2/ && $h2o ne ''){	
		print OUT "$h2o\n";
	}
	else{
		print OUT $_;
	};
};
close(IN);
close(OUT);

unlink "$tmp";


################################################ ligand.in


system("cp $leaflexx/ligand.in ligand.in");


open(IN,"<ligand.in");
open(OUT,">tmp");
while(<IN>){
	
	if($_=~/^FUNCTION/){
		print OUT "FUNCTION ligand.func\n";
	}
	elsif($_=~/^SCAFFOLD/){
		print OUT "SCAFFOLD 0\n";	
	}
	elsif($_=~/^FILTER/){
		print OUT "FILTER P  3D  -  -  T  -  I 0\n";			
	}
	elsif($_=~/^GET_POOL/){
		print OUT "GET_POOL 0\n";		
	}
	elsif($_=~/^OPTIMIS/){
		print OUT "OPTIMIS 1\n";	
	}
	elsif($_=~/^NBCONF/){
		print OUT "NBCONF 3\n";					
	}
	else{
		print OUT $_;
	};
};
close(IN);
close(OUT);
rename "tmp","ligand.in";


################################################ ligand.func

system("cp $leaflexx/ligand.func ligand.func");
open(IN,"<ligand.func");
open(OUT,">tmp");
while(<IN>){
	@get=split(' ',$_);
	if($get[0] eq "dock" && ($_=~/^dock/ || $_=~/^#dock/)){
		$get[4]=$rec;
		$get[5]=$site;
		$get[6]=$refx;
		$get[7]=$refy;
		$get[8]=$refz;
		$line=join(' ',@get);
		print OUT "$line\n";
	}
	else{
		print OUT $_;
	};
};
close(IN);
close(OUT);
rename "tmp","ligand.func";


system("cp $leaflexx/FLEXX/fixe_scaffold.bat fixe_scaffold.bat");

################################################ FIN




