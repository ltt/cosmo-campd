#!/usr/bin/perl


	local($mol2,$max)= @ARGV;

        if($mol2 eq "" || $max eq ""){
                die "usage (FlexX format): splitmol2flexx <file.mol2> <number by file>\n";
        };
	$nom=$mol2;
	$nom=~s/\.mol2$//;

# DECOUPE UN MOL2 MULTIPLe EN MOL2 SIMPLE

$new=0;
$k=1;
$nbmol=0;
$field=0;

open(IN,"<$mol2");
while(<IN>){
	
	@get=split(' ',$_);

	if($new){
		if($nbmol > ($max*$k) ){
			$k++;
		};
		
		$name=$nom."_$k.mol2";
		system("touch $name") if( !-e "$name");
		open(OUT,">>$name");
		foreach $j (0..@ligne-1){
			printf OUT "$ligne[$j]";
		};
		close(OUT);
		@ligne='';
		$i=0;
		$field=0;
		$new=0;
	};

	if ( ($get[1] eq 'End' || $get[0] eq "") && $field == 2 ){
		$new=1;
		$nbmol++;
	}
	else{
		if($nbmol >= 1 && $i==0){
			$ligne[$i]="\n";
			$i++;
		};	
		$ligne[$i]=$_;
		$i++;
		$field++ if($get[0] eq '@<TRIPOS>MOLECULE');
		$field++ if($get[0] eq '@<TRIPOS>BOND');
	};

};
close(IN);

	$nbmol++ if($new==0);
	if($field != 0){
		if($nbmol > ($max*$k)){
			$k++;
		};	
		$name=$nom."_$k.mol2";
		system("touch $name") if( !-e "$name");
                open(OUT,">>$name");
                foreach $j (0..@ligne-1){
                        printf OUT "$ligne[$j]";
                };
                close(OUT);
	};
	print "$nbmol structures splitted into $k files (each containing $max molecules maximum)\n";
	
