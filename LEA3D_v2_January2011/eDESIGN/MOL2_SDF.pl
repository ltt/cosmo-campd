#!/usr/bin/perl



	local($mol2)=@ARGV;

	if ($mol2 eq ''){
		die "usage: mol2sdf <file.mol2>\n";
	};

	#chop($workdir = `pwd` );
	
	if(!-e "$mol2"){
		die "$mol2 does not exist !\n";
	};
	
	
	
        @new_coord='';
	$new_coord[0]="\n";
	$new_coord[1]="  -ISIS-  08029913582D\n";
	$new_coord[2]="\n";
	
	$longa=0;
	$longb=0;

	&readmol2("$mol2");

	$mol2=~s/\// /g;
	@gety=split(' ',$mol2);
	$mol2=$gety[@gety-1];	
	#&readmol2("$mol2");
	
	$longa=$istratom;
	$longb=$istrbond;
	$blanc=" ";
	$zero=0;
		
	$nom=$mol2;
	$nom=~s/\.mol2/\.sdf/;
	$nom3=$nom;
	$nom="$nom";
	
	@modifbondtype='';
	@coval_old=@coval;
	
	#foreach $k (1..$istrbond){
	#        if($strbondtype[$k] eq "ar"){
	#        	print "$k $strbond1[$k]-$coval[$strbond1[$k]] $strbond2[$k]-$coval[$strbond2[$k]] $modifbondtype[$k]\n";
	#        };
	#};
	
	 #print "test\n";
	foreach $k (1..$istrbond){
	
	 	
	        if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]==2 && $coval[$strbond2[$k]]==2){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
		 }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	       		if($coval[$strbond1[$k]]==2 && $coval[$strbond2[$k]]==3){
				$modifbondtype[$k]="2";
				$coval[$strbond1[$k]]++;
				$coval[$strbond2[$k]]++;
			}
			else{
				$modifbondtype[$k]="1";
			};		
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==2){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
	 	}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && ($typepdb[$strbond1[$k]] eq "O" || $typepdb[$strbond2[$k]] eq "O" || $typepdb[$strbond1[$k]] eq "S" || $typepdb[$strbond2[$k]] eq "S")){
	 		$modifbondtype[$k]="1";	 	
		};
        };

        #print "\n";
        #foreach $k (1..$istrbond){
	#        if($strbondtype[$k] eq "ar"){
	#        	print "$k $strbond1[$k]-$coval[$strbond1[$k]] $strbond2[$k]-$coval[$strbond2[$k]] $modifbondtype[$k]\n";
	#        };
	#};

	 	
	 	
	
	# tautomerie si un atome de C a > 4 liaison et/ou N a > 3 liaisons
        $tournem=0;
        foreach $k (1..$istrbond){
		if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]] > 4 || $coval[$strbond2[$k]] > 4){
	        		$tournem=$k;
	        		last;
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]>3 && $coval[$strbond2[$k]] > 4){
	        		$tournem=$k;
	        		last;
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]] > 4 && $coval[$strbond2[$k]] > 3){
	        		$tournem=$k;
	        		last;
	        	};
	 	};	
        };

        $tournep=0;
        foreach $k (1..$istrbond){
		if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]] < 4 || $coval[$strbond2[$k]] < 4){
	        		$tournep=$k;
	        		last;	
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]] < 3 && $coval[$strbond2[$k]]<4 && $coval_old[$strbond1[$k]] < 3){
	        	
	        		$tournep=$k;
	        		last;	
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]<4 && $coval[$strbond2[$k]]<3 && $coval_old[$strbond2[$k]] < 3){
	        	
	        		$tournep=$k;
	        		last;
	        	};
	 	};	
        };
	
	#print "tour=$nbtour  moins=>$strbond1[$tournem]-$strbond2[$tournem]  plus=>$strbond1[$tournep]-$strbond2[$tournep]\n";
	
$nbtour=0;
$dejavuplus='';
$dejavumoins='';
while($nbtour <= 10 && ($tournem > 0 || $tournep > 0)){
	
       # print "tour=$nbtour  moins=>$strbond1[$tournem]-$strbond2[$tournem]  plus=>$strbond1[$tournep]-$strbond2[$tournep]\n";

        @coval=@coval_old;
	
        if($tournep > 0){

                $dejavuplus=$dejavuplus." $strbond1[$tournep]-$strbond2[$tournep] ";
        	
        	if($modifbondtype[$tournep] eq "1"){
	        	$modifbondtype[$tournep]="2";
	        	$coval[$strbond1[$tournep]]++;
	        	$coval[$strbond2[$tournep]]++;
	        };

		foreach $k (($tournep+1)..$istrbond){
		if($k!=$tournep){
	        if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==2 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==2){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };	
		};
		};	
        	};
        	
        	foreach $k (1..($tournep-1)){
		if($k!=$tournep){
	        if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==2 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==2){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };	
		};
		};	
        	};

        }
        elsif($tournem > 0){

                $dejavumoins=$dejavumoins." $strbond1[$tournem]-$strbond2[$tournem] ";

        	if($modifbondtype[$tournem] eq "2"){
	        	$modifbondtype[$tournem]="1";
	        };
	        	
		foreach $k (($tournem+1)..$istrbond){
		if($k!=$tournem){
	        if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==2 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==2){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };	
		};
		};	
        	};
        	
        	foreach $k (1..($tournem-1)){
		if($k!=$tournem){
	        if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]==2 && $coval[$strbond2[$k]]==3){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]==3 && $coval[$strbond2[$k]]==2){
	        		$modifbondtype[$k]="2";
	        		$coval[$strbond1[$k]]++;
	        	        $coval[$strbond2[$k]]++;
	        	 }
	        	 else{
	        	 	$modifbondtype[$k]="1";
	        	 };	
		};
		};	
        	};



        };

        # tautomerie si un atome de C a > 4 liaison et/ou N a > 3 liaisons
        $tournem=0;
        foreach $k (1..$istrbond){
		if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]] > 4 || $coval[$strbond2[$k]] > 4){
	        		$tournem=$k;
	        		last;
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]>3 && $coval[$strbond2[$k]] > 4){
	        		$tournem=$k;
	        		last;
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]] > 4 && $coval[$strbond2[$k]] > 3){
	        		$tournem=$k;
	        		last;
	        	};
	 	};	
        };

        $tournep=0;
        foreach $k (1..$istrbond){
		if(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]<4 || $coval[$strbond2[$k]] < 4){
	        		$tournep=$k;
	        		last;	
	        	 };
		}
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "N" && $typepdb[$strbond2[$k]] eq "C"){
	        	if($coval[$strbond1[$k]]<3 && $coval[$strbond2[$k]]<4  && $coval_old[$strbond1[$k]] < 3){
	        		$tournep=$k;
	        		print "ok\n";	
	        		last;
	        	 };
	        }
		elsif(($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR") && $typepdb[$strbond1[$k]] eq "C" && $typepdb[$strbond2[$k]] eq "N"){
	        	if($coval[$strbond1[$k]]<4 && $coval[$strbond2[$k]]<3  && $coval_old[$strbond2[$k]] < 3){
	        		$tournep=$k;
	        		print "ok2\n";
	        		last;
	        	};
	 	};	
        };

        if($tournep > 0){
        	$testbond="$strbond1[$tournep]-$strbond2[$tournep]";
        	if($dejavuplus=~/ $testbond /){
        		# change de sens
        		foreach $k (1..$istrbond){
        			$testbond="$strbond1[$k]-$strbond2[$k]";
        			if($k!=$tournep && $dejavuplus!~/ $testbond / && $modifbondtype[$k] eq "1"){
        				$tournep=$k;
        				last;
        			};
        		};
        	};
        };

        if($tournem > 0){
        	$testbond="$strbond1[$tournem]-$strbond2[$tournem]";
        	if($dejavumoins=~/ $testbond /){
        		# change de sens
        		foreach $k (1..$istrbond){
        			$testbond="$strbond1[$k]-$strbond2[$k]";
        			if($k!=$tournem && $dejavumoins!~/ $testbond / && $modifbondtype[$k] eq "2"){
        				$tournem=$k;
        				last;
        			};
        		};
        	};
        };


	$nbtour++;
};
	
	if($tournep>0 || $tournem>0){
		print "WARNING : AROMATICITE MAL DECRITE ! pour $nom\n";
	}
	else{
		# print "$nom3 DONE\n";
	};
	
	foreach $k (1..$istrbond){
			
	        if($strbondtype[$k] eq "am" || $strbondtype[$k] eq "AM"){
	        	$strbondtype[$k]="1";
	        }
	        elsif($strbondtype[$k] eq "ar" || $strbondtype[$k] eq "AR"){
			#	print "$k $strbond1[$k],$strbond2[$k],$strbondtype[$k]\n";
	        	if($modifbondtype[$k] eq ""){
				 $modifbondtype[$k]=1;
				 print "bond $k forced bond = 1: $strbond1[$k] $strbond2[$k] $strbondtype[$k] --> $modifbondtype[$k] \n";
			 };	 
			$strbondtype[$k]=$modifbondtype[$k];
	        };

        	$test1=sprintf"%3s%3s%3s  0  0  0  0\n",$strbond1[$k],$strbond2[$k],$strbondtype[$k];
		$new_coord[5]=$new_coord[5].$test1;
        };

        	
	

        open(DRM,">$nom");
	
	print DRM "$new_coord[0]";
	print DRM "$new_coord[1]";
	print DRM "$new_coord[2]";
	
	$new_coord[3]=sprintf"%3s%3s  0  0  0  0  0  0  0  0999 V2000\n",$longa,$longb;
	print DRM "$new_coord[3]";
	print DRM "$new_coord[4]";
	print DRM "$new_coord[5]";
	if($sdfchg ne ''){
		printf DRM "M  CHG %2s $sdfchg\n",$nbcharges;
	};
	print DRM "M  END\n";
	print DRM "\$\$\$\$\n";
	close(DRM);
	

print "$nom DONE\n";

###############################################
###############################################


sub readmol2{
	local($chemin)=@_;

	open(OUT,"<$chemin");

	$flagatom=0;
	$flagbond=0;
	$istratom=1;
	$istrbond=1;
	@coval=0;
	
	@strbond1='';
	@strbond2='';
	@strbondtype='';
	@strx='';
	@stry='';
	@strz='';
	@stratomtype='';
	@typepdb='';
	@masse='';
	
	@ionisation='';
	$nbcharges=0;
	
	$nbatomarom=0;
	$nbbondarom=0;
	
	$fn=0;
		while (<OUT>){
			@getstr = split(' ',$_);

			if ($fn){
				$name=$getstr[0];
#print"$name\n";
			};

			if ($getstr[0] eq '@<TRIPOS>SUBSTRUCTURE'){
				$flagbond=0;
			};
			if ($getstr[0] eq ''){
				$flagbond=0;
			};

			if (($flagbond)&&($getstr[0] ne '')){
			
				$strbond1[$istrbond]=$getstr[1];
				$strbond2[$istrbond]=$getstr[2];
				$getstr[3]="am" if (($stratomtype[$getstr[1]]=~/N.am/) && ($stratomtype[$getstr[2]]=~/C.2/));
				$getstr[3]="am" if (($stratomtype[$getstr[2]]=~/N.am/) && ($stratomtype[$getstr[1]]=~/C.2/));
				$strbondtype[$istrbond]=$getstr[3];
				$strbondtype[$istrbond]="ar" if($strbondtype[$istrbond]=~/AR/);
				
				
				if($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[1]]=~/C.2/ && $stratomtype[$getstr[2]]=~/O.co2/ && $ionisation[$getstr[1]] eq ''){
					$strbondtype[$istrbond]="2";
					$ionisation[$getstr[1]]=1;
				}
				elsif($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[2]]=~/C.2/ && $stratomtype[$getstr[1]]=~/O.co2/ && $ionisation[$getstr[2]] eq ''){
					$strbondtype[$istrbond]="2";
					$ionisation[$getstr[2]]=1;
				}
				elsif($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[1]]=~/C.2/ && $stratomtype[$getstr[2]]=~/O.co2/ && $ionisation[$getstr[1]] ne ''){
					$strbondtype[$istrbond]="1";
					$schg=sprintf "%3s",$getstr[2];
					$sdfchg=$sdfchg."$schg  -1";
					$nbcharges++;
				}
				elsif($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[2]]=~/C.2/ && $stratomtype[$getstr[1]]=~/O.co2/ && $ionisation[$getstr[2]] ne ''){
					$strbondtype[$istrbond]="1";
					$schg=sprintf "%3s",$getstr[1];
					$sdfchg=$sdfchg."$schg  -1";
					$nbcharges++;
				};
				
				
				if($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[1]]=~/P.3/ && $stratomtype[$getstr[2]]=~/O.co2/ && $ionisation[$getstr[1]] eq ''){
					$strbondtype[$istrbond]="2";
					$ionisation[$getstr[1]]=1;
				}
				elsif($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[2]]=~/P.3/ && $stratomtype[$getstr[1]]=~/O.co2/ && $ionisation[$getstr[2]] eq ''){
					$strbondtype[$istrbond]="2";
					$ionisation[$getstr[2]]=1;
				}
				elsif($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[1]]=~/P.3/ && $stratomtype[$getstr[2]]=~/O.co2/ && $ionisation[$getstr[1]] ne ''){
					$strbondtype[$istrbond]="1";
					$schg=sprintf "%3s",$getstr[2];
					$sdfchg=$sdfchg."$schg  -1";
					$nbcharges++;
				}
				elsif($strbondtype[$istrbond]=~/ar/ && $stratomtype[$getstr[2]]=~/P.3/ && $stratomtype[$getstr[1]]=~/O.co2/ && $ionisation[$getstr[2]] ne ''){
					$strbondtype[$istrbond]="1";
					$schg=sprintf "%3s",$getstr[1];
					$sdfchg=$sdfchg."$schg  -1";
					$nbcharges++;
				};
								
				if($strbondtype[$istrbond] eq "1" || $strbondtype[$istrbond] eq "2" || $strbondtype[$istrbond] eq "3"){
					$coval[$getstr[1]]=$coval[$getstr[1]]+$strbondtype[$istrbond];
					$coval[$getstr[2]]=$coval[$getstr[2]]+$strbondtype[$istrbond];
				}
				elsif($strbondtype[$istrbond] eq "ar" || $strbondtype[$istrbond] eq "AR" || $strbondtype[$istrbond] eq "AM" || $strbondtype[$istrbond] eq "am"){
					$coval[$getstr[1]]=$coval[$getstr[1]]+1;
					$coval[$getstr[2]]=$coval[$getstr[2]]+1;
				};
				
				 $istrbond++;
			};

			if ($getstr[0] eq '@<TRIPOS>BOND'){
				$flagatom=0;
				$flagbond=1;
			};

			if (($flagatom )&&($getstr[0] ne '')){
				$strx[$istratom]=sprintf "%4.3f",$getstr[2];
				$stry[$istratom]=sprintf "%4.3f",$getstr[3];
				$strz[$istratom]=sprintf "%4.3f",$getstr[4];
			
				$getstr[5]="N.am" if ($getstr[5]=~/N.3/);
				$stratomtype[$istratom]=$getstr[5];
				@ma=split('\.',$getstr[5]);
				$typepdb[$istratom]=$ma[0];
				$masse=$masse+$tabmm{$ma[0]};
				
				$new_coord[4]=$new_coord[4].sprintf"%10s%10s%10s%1s%1s%1s%3s%3s%3s\n",$strx[$istratom],$stry[$istratom],$strz[$istratom],$blanc,$typepdb[$istratom],$blanc,$zero,$zero,$zero;
	
	                        $istratom++;
			};

			if ($getstr[0] eq '@<TRIPOS>ATOM'){
				$flagatom=1;
			};
			$fn=0;
			if ($getstr[0] eq '@<TRIPOS>MOLECULE'){
				$fn=1;
			};

		};

	close(OUT);
	$istrbond=$istrbond-1;
	$istratom=$istratom-1;
	$strrgyrno=$istratom;
#print"$strrgyrno, $istrbond\n";

};
