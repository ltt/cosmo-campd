#!/usr/bin/perl


print "SUBROUTINE PHARMACOPHORE \n";

sub pharmacophore{

	local($filesdf)=@_;

	# The default van der Waals radii are taken from A. Bondi (1964) "van der Waals Volumes and Radii" J.Phys.Chem. 68, 441-451
	#If an element does not appear in the table it is assigned a value of 2.0?

	#   Ag  1.72      Ar  1.88     As  1.85     Au  1.66
	#   Br  1.85      C   1.70     Cd  1.58     Cl  1.75
	#   Cu  1.40      F   1.47     Ga  1.87     H   1.20
	#   He  1.40      Hg  1.55     I   1.98     In  1.93
	#   K   2.75      Kr  2.02     Li  1.82     Mg  1.73
	#   N   1.55      Na  2.27     Ne  1.54     Ni  1.63
	#   O   1.52      P   1.80     Pb  2.02     Pd  1.63
	#   Pt  1.72      S   1.80     Se  1.90     Si  2.10
	#   Sn  2.17      Te  2.06     Tl  1.96     U  1.86
	#   Xe  2.16      Zn  1.39

	%tabR=(
		'C',1.70,
		'O',1.52,
		'N',1.55,
		'S',1.80,
		'P',1.80,
		'Br',1.85,
		'Cl',1.75,
		'I',1.98,
		'F',1.47,
		'H',1.2,
		'Hp',1.1,
	);

	%tabmm=(
		'C',12,
		'O',16,
		'N',14,
		'S',32,
		'P',31,
		'Br',80,
		'Cl',35.4,
		'I',127,
		'F',19,
		'H',1,
	);

	##### PROPRIETES FROM SDF

	$moli=0;
	$result_confs="";
	$flagnew=1;
	open(MOL,"<$filesdf");
	while(<MOL>){
		if($flagnew){
			$moli++;
			$masse=0;
			$compt=0;
			$ig=1;
			$jg=0;
			@strx='';
			@stry='';
			@strz='';
			@atom='';
			@coval='';
			@fonc='';
			@ifonc='';
			@covfonc='';
			$blanc=' ';	
			@radius='';
			$atomlourd=0;
			$flagnew=0;
			$setchg=0;
			open(OUT,">tmpi.sdf");
		};
		@getstr = split(' ',$_);
		print OUT $_;

		$compt++;
		if (($compt > 4) && ($ig <= $istratom)){
			$strx[$ig]=$getstr[0];
			$stry[$ig]=$getstr[1];
			$strz[$ig]=$getstr[2];
			$atom[$ig]=$getstr[3];
			$atomlourd ++ if($getstr[3] ne 'H');
			$radius[$ig]=$tabR{$getstr[3]};
			$masse=$masse+$tabmm{$getstr[3]};
			$ig++;
		};
		if (($compt > 4) && ($ig > $istratom) && ($jg <=$istrbond)){
			if ($jg == 0){
				$jg++;
			}
			else{
                               @coller=split(' *',$getstr[0]);
                                @coller2=split(' *',$getstr[1]);
                                if(@coller==6 && $getstr[1] ne ""){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[2]=$getstr[1];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                }
                                elsif(@coller==6 && $getstr[1] eq ""){
                                        $getstr[0]=$coller[0].$coller[1];
                                        $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        $getstr[2]=$coller[5];
                                }
                                elsif(@coller==5){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0].$coller[1];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[2].$coller[3].$coller[4];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3].$coller[4];
                                        };
                                }
                                elsif(@coller==4){
                                        if($_=~/^\s/){
                                                $getstr[0]=$coller[0];
                                                $getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[1].$coller[2].$coller[3];
                                        }
                                        else{
                                                $getstr[0]=$coller[0].$coller[1].$coller[2];
						$getstr[2]=$getstr[1];
                                                $getstr[1]=$coller[3];
                                        };					
                                }
                                elsif(@coller2==4){
                                        $getstr[1]=$coller2[0].$coller2[1].$coller2[2];
                                        $getstr[2]=$coller2[3];
                                }
                                elsif(@coller==7){
                                        $getstr[0]=$coller[0].$coller[1].$coller[2];
                                        $getstr[1]=$coller[3].$coller[4].$coller[5];
                                        $getstr[2]=$coller[6];
                                };

				$fonc[$getstr[0]]=$fonc[$getstr[0]].$blanc.$getstr[2].'-'.$atom[$getstr[1]].$blanc;
				$ifonc[$getstr[0]]=$ifonc[$getstr[0]].$blanc.$getstr[1].$blanc;
				$covfonc[$getstr[0]]=$covfonc[$getstr[0]].$blanc.$getstr[2];
				$coval[$getstr[0]]=$coval[$getstr[0]]+$getstr[2];

				$fonc[$getstr[1]]=$fonc[$getstr[1]].$blanc.$getstr[2].'-'.$atom[$getstr[0]].$blanc;
				$ifonc[$getstr[1]]=$ifonc[$getstr[1]].$blanc.$getstr[0].$blanc;
				$covfonc[$getstr[1]]=$covfonc[$getstr[1]].$blanc.$getstr[2];
				$coval[$getstr[1]]=$coval[$getstr[1]]+$getstr[2];

				$jg++;
			};
		};
		if ($compt == 4){
			$istratom=$getstr[0];
			$istrbond=$getstr[1];

			@coller=split(' *',$istratom);
			if(@coller>3 && @coller==6){
				$istratom=$coller[0].$coller[1].$coller[2];
				$istrbond=$coller[3].$coller[4].$coller[5];
			}
			elsif(@coller>3 && @coller==5){
				if($_=~/^\s/){
					$istratom=$coller[0].$coller[1];
					$istrbond=$coller[2].$coller[3].$coller[4];
				}
				else{
					$istratom=$coller[0].$coller[1].$coller[2];
					$istrbond=$coller[3].$coller[4];
				};
			};

			#$compt++;
		};
		if ($_=~/\$\$\$\$/){
			close(OUT);

			$f4="tmpi.sdf";
			chop($tmpmol2 = `$leaexe/SDF_MOL2.pl tmpi.sdf ` );
			rename "tmpi_1.mol2", "tmpi.mol2";
			$f3="tmpi.mol2";

			$flagnew=1;
			print "atomes = $istratom\n" if($param{VERBOSITY} >= 1);
			$result_search=&pharm;
			$result_confs=$result_confs." $moli=$result_search ";

			unlink $f4 if(-e $f4);
			unlink $f3 if(-e $f3);
		};
	};
	close(MOL);

	$result_confs;
};

########################################################

sub pharm{

# must retrieve centroids from conformer
 
	chop($tmplog=`$leaexe/MAKE_FGTS.pl $f4`);

	@centerx="";
	@centery="";
	@centerz="";
	@centertype="";
	$centeri=0;

	foreach $cbi (1..3){

		$ouvrir="";
		$ouvrir="ring.sdf" if($cbi==1 && !-z "ring.sdf");
		$ouvrir="fused_rings.sdf" if($cbi==2 && !-z "fused_rings.sdf");
		$ouvrir="special.sdf" if($cbi==3 && !-z "special.sdf");

		if(-e "$ouvrir" && !-z "$ouvrir"){
		$readnext4=0;
		open(INP,"<$param{WORKDIR}/$ouvrir");
		while(<INP>){
			if($readnext4){
				@getsoluble=split(' ',$_);
				$centerx[$centeri]=$getsoluble[0];
				$centery[$centeri]=$getsoluble[1];
				$centerz[$centeri]=$getsoluble[2];
				$readnext4=0;
				print "Centroid $centertype[$centeri] $centerx[$centeri] $centery[$centeri] $centerz[$centeri]\n" if($param{VERBOSITY} >= 1 );
				$centeri++;
			};
			if($_=~/ARcenter/ || $_=~/LIPcenter/){
				$centertype[$centeri]="AR" if($_=~/ARcenter/);
				$centertype[$centeri]="LIP" if($_=~/LIPcenter/);
				$readnext4=1;
			};
		};
		close(INP);
		};
	};
		
	unlink "ring.sdf" if(-e "ring.sdf");
	unlink "fused_rings.sdf" if(-e "fused_rings.sdf");
	unlink "acyclic.sdf" if(-e "acyclic.sdf");
	unlink "linker.sdf" if(-e "linker.sdf");
	unlink "substituent.sdf" if(-e "substituent.sdf");
	unlink "special.sdf" if (-e "special.sdf");


########################################################
	$pharm_match=1;

	#@getvacpharm1; name pharm 1
	#@getvacpharm2; name phrarm 2
	##@getvacmin; dist_min
	#@getvacmax; dist_max

# Sybyl atom type center

	@atommol2="";
	@atommol2x="";
	@atommol2y="";
	@atommol2z="";
	if(-e $f3){
		$flagatom=0;
		$chgi=1;
		open(INP,"<$param{WORKDIR}/$f3");
                while(<INP>){
			$flagatom=0 if($_=~/^\@<TRIPOS>BOND/);
                        if($flagatom){
                        	@getchg=split(' ',$_);
                                $atommol2x[$chgi]=sprintf "%4.3f",$getchg[2];
				$atommol2y[$chgi]=sprintf "%4.3f",$getchg[3];
				$atommol2z[$chgi]=sprintf "%4.3f",$getchg[4];
				$getchg[5]="N.am" if ($getchg[5]=~/N.3/);
				$atommol2[$chgi]=$getchg[5];
                                $chgi++;
                        };
                       	$flagatom=1 if($_=~/^\@<TRIPOS>ATOM/);
             	};
                close(INP);
	};	

# Screen pharmacophores
	
	@getvacpharms="";
	foreach $pharmi (0..@getvacpharm1-1){

print "check pharm no $pharmi [$getvacpharm1[$pharmi] $getvacpharm2[$pharmi] $getvacmin[$pharmi] <= d() <= $getvacmax[$pharmi]]\n" if($param{VERBOSITY} >= 1);
		
		$getvacpharms[$pharmi]=0;
		if($getvacmin[$pharmi] > $getvacmax[$pharmi]){
			print "pharmacophore Dmin must be <= Dmax ! and Dmin must be > 0\n";
		}
		else{
		if($getvacpharm1[$pharmi] eq "AH" || $getvacpharm1[$pharmi] eq "DH"){
			foreach $l (1..$istratom){
				&pharlie($getvacpharm1[$pharmi],$l);
				if ($flagpharverif){
					if($getvacpharm1[$pharmi] eq $getvacpharm2[$pharmi] && $getvacmin[$pharmi] == $getvacmax[$pharmi] && $getvacmin[$pharmi] == 0){
						# Atom or Center just exist !
						$getvacpharms[$pharmi]=1;
					}
					else{
						$pharm1x=$strx[$l];
						$pharm1y=$stry[$l];
						$pharm1z=$strz[$l];
						if($getvacpharm2[$pharmi] eq "AH" || $getvacpharm2[$pharmi] eq "DH"){
							foreach $k (1..$istratom){
									&pharlie($getvacpharm2[$pharmi],$k);
									if($flagpharverif){
										$pharm2x=$strx[$k];
										$pharm2y=$stry[$k];
										$pharm2z=$strz[$k];
										$raygx=$pharm1x-$pharm2x;
										$raygx=$raygx*$raygx;
										$raygy=$pharm1y-$pharm2y;
										$raygy=$raygy*$raygy;
										$raygz=$pharm1z-$pharm2z;
										$raygz=$raygz*$raygz;
										$rayg=sqrt($raygx+$raygy+$raygz);
										if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
											$getvacpharms[$pharmi]=1;
											last;
										};
									};
							};
						}
						elsif($atommol2[1] ne "" && $getvacpharm1[$pharmi] !~/LIP/ && $getvacpharm1[$pharmi] !~/AR/){
							foreach $k (1..@atommol2-1){
								&pharlie($getvacpharm2[$pharmi],$k);
								if($flagpharverif){
									$pharm2x=$atommol2x[$k];
									$pharm2y=$atommol2y[$k];
									$pharm2z=$atommol2z[$k];
									$raygx=$pharm1x-$pharm2x;
									$raygx=$raygx*$raygx;
									$raygy=$pharm1y-$pharm2y;
									$raygy=$raygy*$raygy;
									$raygz=$pharm1z-$pharm2z;
									$raygz=$raygz*$raygz;
									$rayg=sqrt($raygx+$raygy+$raygz);
									if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
										$getvacpharms[$pharmi]=1;
										last;
									};
								};
							};
						}
						elsif($centertype[0] ne ""){
							foreach $k (0..@centertype-1){
								&pharlie($getvacpharm2[$pharmi],$k);
								if($flagpharverif){
									$pharm2x=$centerx[$k];
									$pharm2y=$centery[$k];
									$pharm2z=$centerz[$k];
									$raygx=$pharm1x-$pharm2x;
									$raygx=$raygx*$raygx;
									$raygy=$pharm1y-$pharm2y;
									$raygy=$raygy*$raygy;
									$raygz=$pharm1z-$pharm2z;
									$raygz=$raygz*$raygz;
									$rayg=sqrt($raygx+$raygy+$raygz);
									if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
										$getvacpharms[$pharmi]=1;
										last;
									};
								};
							};
						};		
		
					};
					if($getvacpharms[$pharmi]){
						print "pharm no $pharmi [$getvacpharm1[$pharmi] $getvacpharm2[$pharmi] $getvacmin[$pharmi] <= d() <= $getvacmax[$pharmi]] found\n" if($param{VERBOSITY} >= 1);
						last;
					};
				};
			};
		}
		elsif($atommol2[1] ne "" && $getvacpharm1[$pharmi] !~/LIP/ && $getvacpharm1[$pharmi] !~/AR/){
			foreach $l (1..@atommol2-1){
				&pharlie($getvacpharm1[$pharmi],$l);
				if ($flagpharverif){
					if($getvacpharm1[$pharmi] eq $getvacpharm2[$pharmi] && $getvacmin[$pharmi] == $getvacmax[$pharmi] && $getvacmin[$pharmi] == 0){
						# Atom or Center just exist
						$getvacpharms[$pharmi]=1;
					}
					else{
						$pharm1x=$atommol2x[$l];
						$pharm1y=$atommol2y[$l];
						$pharm1z=$atommol2z[$l];
						if($getvacpharm2[$pharmi] eq "AH" || $getvacpharm2[$pharmi] eq "DH"){
							foreach $k (1..$istratom){
								&pharlie($getvacpharm2[$pharmi],$k);
								if($flagpharverif){
									$pharm2x=$strx[$k];
									$pharm2y=$stry[$k];
									$pharm2z=$strz[$k];
									$raygx=$pharm1x-$pharm2x;
									$raygx=$raygx*$raygx;
									$raygy=$pharm1y-$pharm2y;
									$raygy=$raygy*$raygy;
									$raygz=$pharm1z-$pharm2z;
									$raygz=$raygz*$raygz;
									$rayg=sqrt($raygx+$raygy+$raygz);
									if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
										$getvacpharms[$pharmi]=1;
										last;
									};
								};
							};
						}
						elsif($atommol2[1] ne "" && $getvacpharm1[$pharmi] !~/LIP/ && $getvacpharm1[$pharmi] !~/AR/){
							foreach $k (1..@atommol2-1){
								&pharlie($getvacpharm2[$pharmi],$k);
								if($flagpharverif){
									$pharm2x=$atommol2x[$k];
									$pharm2y=$atommol2y[$k];
									$pharm2z=$atommol2z[$k];
									$raygx=$pharm1x-$pharm2x;
									$raygx=$raygx*$raygx;
									$raygy=$pharm1y-$pharm2y;
									$raygy=$raygy*$raygy;
									$raygz=$pharm1z-$pharm2z;
									$raygz=$raygz*$raygz;
									$rayg=sqrt($raygx+$raygy+$raygz);
									if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
										$getvacpharms[$pharmi]=1;
										last;
									};
								};
							};
                                                }
                                                elsif($centertype[0] ne ""){
                                                        foreach $k (0..@centertype-1){
                                                                &pharlie($getvacpharm2[$pharmi],$k);
                                                                if($flagpharverif){
                                                                        $pharm2x=$centerx[$k];
                                                                        $pharm2y=$centery[$k];
                                                                        $pharm2z=$centerz[$k];
                                                                        $raygx=$pharm1x-$pharm2x;
                                                                        $raygx=$raygx*$raygx;
                                                                        $raygy=$pharm1y-$pharm2y;
                                                                        $raygy=$raygy*$raygy;
                                                                        $raygz=$pharm1z-$pharm2z;
                                                                        $raygz=$raygz*$raygz;
                                                                        $rayg=sqrt($raygx+$raygy+$raygz);
                                                                        if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
                                                                                $getvacpharms[$pharmi]=1;
                                                                                last;
                                                                        };
                                                                };
                                                        };
                                                };

					};
					if($getvacpharms[$pharmi]){
						print "pharm no $pharmi [$getvacpharm1[$pharmi] $getvacpharm2[$pharmi] $getvacmin[$pharmi] <= d() <= $getvacmax[$pharmi]] found\n" if($param{VERBOSITY} >= 1);
						last;
					};
				};
			};	
		}
		elsif($centertype[0] ne ""){
			foreach $l (0..@centertype-1){
				&pharlie($getvacpharm1[$pharmi],$l);
				if($flagpharverif){
					if($getvacpharm1[$pharmi] eq $getvacpharm2[$pharmi] && $getvacmin[$pharmi] == $getvacmax[$pharmi] && $getvacmin[$pharmi] == 0){
                                                # Atom or Center just exist
                                                $getvacpharms[$pharmi]=1;
                                        }
                                        else{
                                                $pharm1x=$centerx[$l];
                                                $pharm1y=$centery[$l];
                                                $pharm1z=$centerz[$l];
                                                if($getvacpharm2[$pharmi] eq "AH" || $getvacpharm2[$pharmi] eq "DH"){
                                                        foreach $k (1..$istratom){
                                                                &pharlie($getvacpharm2[$pharmi],$k);
                                                                if($flagpharverif){
                                                                        $pharm2x=$strx[$k];
                                                                        $pharm2y=$stry[$k];
                                                                        $pharm2z=$strz[$k];
                                                                        $raygx=$pharm1x-$pharm2x;
                                                                        $raygx=$raygx*$raygx;
                                                                        $raygy=$pharm1y-$pharm2y;
                                                                        $raygy=$raygy*$raygy;
                                                                        $raygz=$pharm1z-$pharm2z;
                                                                        $raygz=$raygz*$raygz;
                                                                        $rayg=sqrt($raygx+$raygy+$raygz);
                                                                        if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
                                                                                $getvacpharms[$pharmi]=1;
                                                                                last;
                                                                        };
                                                                };
                                                        };
                                                }
                                                elsif($atommol2[1] ne "" && $getvacpharm1[$pharmi] !~/LIP/ && $getvacpharm1[$pharmi] !~/AR/){
                                                        foreach $k (1..@atommol2-1){
                                                                &pharlie($getvacpharm2[$pharmi],$k);
                                                                if($flagpharverif){
                                                                        $pharm2x=$atommol2x[$k];
                                                                        $pharm2y=$atommol2y[$k];
                                                                        $pharm2z=$atommol2z[$k];
                                                                        $raygx=$pharm1x-$pharm2x;
                                                                        $raygx=$raygx*$raygx;
                                                                        $raygy=$pharm1y-$pharm2y;
                                                                        $raygy=$raygy*$raygy;
                                                                        $raygz=$pharm1z-$pharm2z;
                                                                        $raygz=$raygz*$raygz;
                                                                        $rayg=sqrt($raygx+$raygy+$raygz);
                                                                        if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
                                                                                $getvacpharms[$pharmi]=1;
                                                                                last;
                                                                        };
                                                                };
                                                        };
                                                }
                                                elsif($centertype[0] ne ""){
                                                        foreach $k (0..@centertype-1){
                                                                &pharlie($getvacpharm2[$pharmi],$k);
                                                                if($flagpharverif){
                                                                        $pharm2x=$centerx[$k];
                                                                        $pharm2y=$centery[$k];
                                                                        $pharm2z=$centerz[$k];
                                                                        $raygx=$pharm1x-$pharm2x;
                                                                        $raygx=$raygx*$raygx;
                                                                        $raygy=$pharm1y-$pharm2y;
                                                                        $raygy=$raygy*$raygy;
                                                                        $raygz=$pharm1z-$pharm2z;
                                                                        $raygz=$raygz*$raygz;
                                                                        $rayg=sqrt($raygx+$raygy+$raygz);
                                                                        if($rayg <= $getvacmax[$pharmi] && $rayg >= $getvacmin[$pharmi]){
                                                                                $getvacpharms[$pharmi]=1;
                                                                                last;
                                                                        };
                                                                };
                                                        };
                                                };
                                        };
                                        if($getvacpharms[$pharmi]){
                                                print "pharm no $pharmi [$getvacpharm1[$pharmi] $getvacpharm2[$pharmi] $getvacmin[$pharmi] <= d() <= $getvacmax[$pharmi]] found\n" if($param{VERBOSITY} >= 1);
                                                last;
                                        };
                                };
                        };
		};		
		};
		$pharm_match=0 if($getvacpharms[$pharmi] == 0);
	};
	$pharm_match=0 if($getvacpharms[0] eq "");
	#print "pharm_match = $pharm_match\n";
	
	$pharm_match;
		
};


########################################################
########################################################


sub pharlie{
        local($pharverif,$pharnum)=@_;

	$flagpharverif=0;


# MOL2 sybyl atom type

         %order_sybyl=(
                'C.3',1,
                'C.2',2,
                'C.1',3,
                'C.ar',4,
                'C.cat',5,
                'N.3',6,
                'N.2',7,
                'N.1',8,
                'N.ar',9,
                'N.am',10,
                'N.pl3',11,
                'N.4',12,
                'O.3',13,
                'O.2',14,
                'O.co2',15,
                'O.spc',16,
                'O.t3p',17,
                'S.3',18,
                'S.2',19,
                'S.o',20,
                'S.o2',21,
                'P.3',22,
                'H',23,
                'X',38,
                'H.spc',24,
                'H.t3p',25,
                'F',26,
                'Cl',27,
                'Br',28,
                'I',29,
                'Si',30,
                'LP',31,
                'Du',32,
                'Na',33,
                'K',34,
                'Ca',35,
                'Li',36,
                'Al',37,
        );

# SDF DH or AH
 
        $dh=" O N S ";
        $ah=" O N S F ";


# Center types
	$centerAR=" AR LIP ";


	if($pharverif=~/0\./){
		print "Warning: in pharmacophore search: O has been replaced by 0 (the number) ! that will not match !\n";
	};

        if ($pharverif eq "DH" && $dh=~/ $atom[$pharnum] /){
		$flagpharverif=1 if($fonc[$pharnum] =~/ 1-H /);
		print "DH $pharnum $atom[$pharnum]\n" if($flagpharverif && $param{VERBOSITY} >= 1);
        }
        elsif ($pharverif eq "AH" && $ah=~/ $atom[$pharnum] /){
		$flagpharverif=1 if($atom[$pharnum] eq "N" || $atom[$pharnum] eq "S" || $atom[$pharnum] eq "F");
        	# but not ether
		if($atom[$pharnum] eq "O"){
			@det=split(' ',$fonc[$pharnum]);
			$nocc=@det;
			if($nocc == 2 && $fonc[$pharnum] =~/ 1-H /){
				$flagpharverif=1;
			}
			elsif($nocc == 1){
				$flagpharverif=1;
			};
		}
		elsif($atom[$pharnum] eq "S"){
			@det=split(' ',$fonc[$pharnum]);
			$nocc=@det;
			if($nocc == 2 && $fonc[$pharnum] =~/ 1-H /){
				$flagpharverif=1;
			}
			elsif($nocc == 1){
				$flagpharverif=1;
			};
		};
		print "AH $pharnum $atom[$pharnum]\n" if($flagpharverif && $param{VERBOSITY} >= 1);
	}
	elsif($atommol2[$pharnum] ne "" && $order_sybyl{$pharverif} ne "" && $order_sybyl{$pharverif} > 0){
			if($atommol2[$pharnum] eq $pharverif){
				$flagpharverif=1;
			};
			print "$pharnum $atom[$pharnum] $atommol2[$pharnum]\n" if($flagpharverif && $param{VERBOSITY} >= 1); 
	}
	elsif($centerAR=~/ $pharverif /){
		$flagpharverif=1 if($pharverif eq $centertype[$pharnum]);
		print "$pharnum $atom[$pharnum] \n" if($flagpharverif && $param{VERBOSITY} >= 1);
	}; 	
											
	$flagpharverif;
};


#################################################################################################"
#################################################################################################"



