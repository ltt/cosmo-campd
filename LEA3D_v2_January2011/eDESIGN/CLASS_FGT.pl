#!/usr/bin/perl

$leaexe=$ENV{LEA3D};
die "\nCheck environment variable (setenv LEA3D) \n\n" if (!-e "$leaexe/MAIN");

require "$leaexe/KEY.pl";

$fileadd='';
$f='';
$filekey='';
$optionx='';
$optionframe='';

my($f,$filekey,$optionx,$fileadd,$optionframe)=@ARGV;

if($f eq ''|| $filekey eq '' || $optionx eq ''){
	print "usage: classfgt <file.sdf> <file_key> <'X' or '-'> <output file.sdf> <option: '0' (default) or '1' for using the framework mode> \n";
	exit;
};

     	&keymol2($f,$filekey,$optionx,$optionframe);

     	unlink "tmp";

     	$fileadd="diff.sdf" if($fileadd eq '');
	
     	if(-e "diff.sdf" && !-z "diff.sdf"){
     		chop($nb = `$leaexe/NBSDF.pl diff.sdf`);
     		print "different molecules : $nb in $fileadd\n";
		rename "diff.sdf", "$fileadd";
	};	

	if(-e "same.sdf" && !-z "same.sdf"){
     		chop($nb = `$leaexe/NBSDF.pl same.sdf`);
     		print "equivalentes molecules : $nb in same_$fileadd\n";
     		rename "same.sdf", "same_$fileadd";
	};	

     	if(-e "same_additional_key.sdf" && !-z "same_additional_key.sdf"){
		chop($nb = `$leaexe/NBSDF.pl same_additional_key.sdf`);
     		print "equivalentes molecules to new generated keys : $nb in same_additional_key.sdf\n";
	};	
     
    	if(-e "exclude.sdf" && !-z "exclude.sdf"){ 
     		chop($nb = `$leaexe/NBSDF.pl exclude.sdf` );
     		print "excluded molecules : $nb in exclude_$fileadd\n";
     		rename "exclude.sdf", "exclude_$fileadd";
	};	

