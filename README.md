# COSMO-CAMPD

## Description
In this directory, you find the source code for our COSMO-CAMPD method for molecular design of solvents in chemical processes.

## Installation
The COSMO-CAMPD method combines various programming languages and programs (see our publications for details).

The frame of the code is provided by LEA3D, the evolutionary algorithm for molecular design. LEA3D is written in Perl.

To score candidate molecules, LEA3D calls MATLAB code. Within MATLAB, further external programs for property prediction using quantum chemistry are called via command line.

For proper application of COSMO-CAMPD you will need the following programs:

External proprietary programs:
- Matlab (min version R2018b)
- BIOVIA COSMOsuite (former COSMOlogic), i.e. COSMOconf, Turbomole, COSMOtherm, COSMOfrag
- Gaussian09

Open source tools:
- Parallel Forkmanager (see on CPAN)
- TAMKin

Please check LEA_run_options.pl in the workspace directory and the MATLAB struct user.mat in workspace/Paths for setting the path variables to the external programs correctly.


## Usage
Once all programs are installed, navigate to the workspace folder and execute LEA3D:

```
lea job.in > logfile
```

## License
The MIT License

